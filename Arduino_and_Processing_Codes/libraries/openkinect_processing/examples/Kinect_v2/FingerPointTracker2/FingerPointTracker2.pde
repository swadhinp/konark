// Daniel Shiffman and Thomas Sanchez Lengeling
// Tracking the average location beyond a given depth threshold
// Thanks to Dan O'Sullivan

// https://github.com/shiffman/OpenKinect-for-Processing
// http://shiffman.net/p5/kinect/

import org.openkinect.processing.*;
import gab.opencv.*;


Kinect2 kinect2;
OpenCV opencv;


void setup() {
  
   size(640, 480);
   kinect2 = new Kinect2(this);
   //kinect2.initDepth();
   kinect2.initVideo();
   kinect2.initDevice();

}

void draw() {
  //background(255);

  // Run the tracking analysis
  //tracker.track();
  // Show the image
  //tracker.display();
  
  PImage img = kinect2.getVideoImage();
  
  // Being overly cautious here
    //if ( img == null) return;
    
  //opencv.loadImage(img);
 
  image(img, 0, 0);
  opencv = new OpenCV(this, img);  
  
  //image(opencv.getOutput(), 0, 0 );
  /* Brightest Point Detection */
  
    PVector loc = opencv.max();
  
    stroke(255, 0, 0);
    strokeWeight(4);
    noFill();
    ellipse(loc.x, loc.y, 10, 10);
    

}


// Adjust the threshold with key presses
void keyPressed() {
  int t = 5;
  if (key == CODED) {
    if (keyCode == UP) {
      t +=5;
      //tracker.setThreshold(t);
    } else if (keyCode == DOWN) {
      t -=5;
      //tracker.setThreshold(t);
    }
  }
}
