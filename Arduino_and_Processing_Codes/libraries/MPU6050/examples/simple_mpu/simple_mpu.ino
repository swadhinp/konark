

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#include "I2Cdev.h"
#include "MPU6050.h"


#if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
    #include "Wire.h"
#endif


MPU6050 accelgyro;


int16_t ax, ay, az;
int16_t gx, gy, gz;




#define OUTPUT_READABLE_ACCELGYRO


//#define OUTPUT_BINARY_ACCELGYRO



unsigned long time;
void setup() {
    // join I2C bus (I2Cdev library doesn't do this automatically)
     #if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
        Wire.begin();
    #elif I2CDEV_IMPLEMENTATION == I2CDEV_BUILTIN_FASTWIRE
        Fastwire::setup(400, true);
    #endif
        
   

   
    Serial.println("CLEARDATA");
    Serial.println("LABEL,Time,time,ax,ay,az,gx,gy,gz");
    
   
   
   
}

void loop() 
{
  
  time=millis();
    // read raw accel/gyro measurements from device
    accelgyro.getMotion6(&ax, &ay, &az, &gx, &gy, &gz);

   
        Serial.print("DATA,TIME,");
        Serial.print(time);
        Serial.print(",");
        Serial.print(ax); Serial.print(",");
        Serial.print(ay); Serial.print(",");
        Serial.print(az); Serial.print(",");
        Serial.print(gx); Serial.print(",");
        Serial.print(gy); Serial.print(",");
        Serial.println(gz);
    

delay(10);
}

