#include <Wire.h>
#include <I2Cdev.h>
#include <MPU6050.h>

MPU6050 accelGyro;                                        // Instansiate the MPU6050 object
int16_t ax, ay, az, gx, gy, gz;                           // Create the 3 axis gyro/accel variables

void setup() {
  Serial.begin(115200);                                   // Initialise the serial port
  Wire.begin();                                           // Initialise the wire library
  accelGyro.initialize();                                 // Initialise the gyro and accelerometer
  //accelGyro.setRate(0x00);                              // Set sampling rate, not used here  
  accelGyro.setDLPFMode(0x04);                            // Set digital low pass filter to 20Hz  
  accelGyro.setFullScaleGyroRange(0x01);                  // Set gyro full scale range to +/-500 degrees/s
  accelGyro.setFullScaleAccelRange(0x01);                 // Set accelerometer full scale range to +/-4g     
  accelGyro.getMotion6(&ax, &ay, &az, &gx, &gy, &gz);     // Discard the first set of results as they are incorrect
}

void loop() {
  accelGyro.getMotion6(&ax, &ay, &az, &gx, &gy, &gz);     // Read the gyro and acceleromter
  Serial.print(ax);                                       // Output the results to the console
  Serial.print(F("   "));
  Serial.print(ay);
  Serial.print(F("   "));
  Serial.print(az);
  Serial.print(F("   "));
  Serial.print(gx);
  Serial.print(F("   "));
  Serial.print(gy);
  Serial.print(F("   "));
  Serial.println(gz);
  delay(1000);
}
