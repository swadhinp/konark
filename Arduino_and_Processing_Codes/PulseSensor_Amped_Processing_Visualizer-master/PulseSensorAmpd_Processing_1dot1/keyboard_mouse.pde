
void mousePressed(){
  scaleBar.press(mouseX, mouseY);
}

void mouseReleased(){
  scaleBar.release();
}

void keyPressed(){

 switch(key){
   case 's':    // pressing 's' or 'S' will take a jpg of the processing window
   case 'S':
     saveFrame("heartLight-####.jpg");    // take a shot of that!
     
     //Closing Files
     rawOutputWriter.close();
     beatOutputWriter.close();
     ibiOutputWriter.close();
     
     //Opening Files again
    counter = counter + 1;
    rawOutputWriter = createWriter( "rawHRData-" + counter + ".csv" );
    beatOutputWriter = createWriter( "rawBeatData-" + counter + ".csv" );
    ibiOutputWriter = createWriter( "rawIBIData-" + counter + ".csv" );
     
     break;

  case 'x':
    //Closing Files
     rawOutputWriter.close();
     beatOutputWriter.close();
     ibiOutputWriter.close();
     exit();
     break;
     
   default:
     break;
 }
}