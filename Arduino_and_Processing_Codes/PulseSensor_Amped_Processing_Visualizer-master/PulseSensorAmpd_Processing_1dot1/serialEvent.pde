



void serialEvent(Serial port){ 
   String inData = port.readStringUntil('\n');

   if (inData == null) {                 // bail if we didn't get anything
     return;
   }   
   if (inData.isEmpty()) {                // bail if we got an empty line
     return;
   }
   inData = trim(inData);                 // cut off white space (carriage return)   
   if(inData.length() <= 0) {             // bail if there's nothing there
     return;
   }

   if (inData.charAt(0) == 'S'){          // leading 'S' for sensor data
   
    
     inData = inData.substring(1);        // cut off the leading 'S'
     int incomingValues[] = int(split(inData, ","));
     Sensor = int(incomingValues[0]);                // convert the string to usable int
     
     rawOutputWriter.println( incomingValues[1] + ":" + Sensor);
   }
   if (inData.charAt(0) == 'B'){          // leading 'B' for BPM data
     inData = inData.substring(1);        // cut off the leading 'B'
     int incomingValues[] = int(split(inData, ","));
     BPM = int(incomingValues[0]);                   // convert the string to usable int
     beat = true;                         // set beat flag to advance heart rate graph
     heart = 20;                          // begin heart image 'swell' timer
     
     beatOutputWriter.println( incomingValues[1] + ":" + BPM);
   }
 if (inData.charAt(0) == 'Q'){            // leading 'Q' means IBI data 
     inData = inData.substring(1);        // cut off the leading 'Q'
     int incomingValues[] = int(split(inData, ","));
     IBI = int(incomingValues[0]);                   // convert the string to usable int
     
     ibiOutputWriter.println( incomingValues[1] + ":" + IBI);
   }
}