close all;
clear all;

% Designs an antenna, and applies a box extension programmatically.                                           

% Operating frequency of the antenna
freq = 900e6;

idx = 1;
Zin = [];
boxPos = -1:0.2:0.4;
for n = boxPos
    disp(['BoxPos=' num2str(n)]);
    rfidDipole = createAntenna(n);
    Zin(idx) = impedance(rfidDipole, freq);
    idx = idx + 1;
end

figure;
plot(boxPos, Zin);
subplot(311);
plot(boxPos, abs(Zin), '-x', 'LineWidth', 2);
ylabel('Impedance magnitude (|Z|)');

% subplot(312);
% plot(boxPos, imag(Zin), '-x', 'LineWidth', 2);
% ylabel('Imag Zin');

subplot(312);
plot(boxPos, angle(Zin), '-x', 'LineWidth', 2);
ylabel('Z Phase (in radian)');

subplot(313);
plot(boxPos, tanh(imag(Zin)./(real(Zin))), '-x', 'LineWidth', 2);
ylabel('Backscattered Phase (in radian)');
xlabel('Box position');

