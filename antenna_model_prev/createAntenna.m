
% Parameters:
% pos = normalized offset of extension block

function [antennaMesh] = createAntenna(pos)
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Custom RFID Antenna
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % Dimensions of the dipole antenna
    L = 117e-3;         % Total half-length of blade dipole                          
    W = 140e-3;         % Width of blade dipole                                      
    Ld = 112e-3;        % Half-length excluding impedance match taper                
    fw = 3e-3;          % Feed width                                                 
    g = 3e-3;           % Feed gap                               
    fp = 2e-3;          % Feed point location
    freq = 800e3;

    % Block (extension) dimensions
    blockWidth = 10e-3; 
    blockHeight = 1000e-3; % Height of block excluding taper
    blockGap = 2e-3;
    
    % Define the shape of the two dipole arms
    ArmLeft  = [-L (-L+Ld) -g/2 -g/2 (-L+Ld) -L;-W/2 -W/2 -fw/2 fw/2 W/2 W/2];
    ArmRight = [(circshift(-1.*ArmLeft(1,:)',[2,0]))';(circshift(-1.*ArmLeft(2,:)',[2 0]))'];

    % Describe the shape of the antenna
    gdArmLeft = [2 size(ArmLeft, 2) reshape(ArmLeft', 1, [])]';
    gdArmRight = [2 size(ArmRight, 2) reshape(ArmRight', 1, [])]';
    gd = [gdArmLeft gdArmRight];

    % Describe the block position if necessary
    if nargin ~= 0  
        assert((abs(pos) >= 0) && (abs(pos) <= 1), ...
            'Box pos must be between 0 and 1');
        % Create the block that will describe the positon of the offset
        xCenter = pos * L;
        xLeftEdge = xCenter - (blockWidth/2);
        xRightEdge = xCenter + (blockWidth/2);
        yBottomEdge = W/2 + blockGap;
        yTopEdge = yBottomEdge + blockHeight;
        blockDim = [3 4 xLeftEdge xLeftEdge xRightEdge xRightEdge ...
            yBottomEdge yTopEdge yTopEdge yBottomEdge];

        gdBlock = [blockDim zeros(1, size(gd, 1) - length(blockDim))]';
        gd = [gd gdBlock];
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Construct the antenna using the PDE tool
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    antEdges = decsg(gd);
    pdeModel = createpde();
    geometryFromEdges(pdeModel, antEdges);
    generateMesh(pdeModel, 'Hmax', 0.005);

    % Get the points and triangles from the mesh data
    [p,e,t] = meshToPet(pdeModel.Mesh);
   
    % Use the points and triangle data to construct the antenna
    antennaMesh = customAntennaMesh(p, t);

    % Add the feed points to the antenna
    createFeed(antennaMesh, [-fp, 0], [fp, 0]);
end 
