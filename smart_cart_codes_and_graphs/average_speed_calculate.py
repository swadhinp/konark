import os
import sys
import numpy as np

if len(sys.argv) < 2:
    print "python <speed file>"
    sys.exit(-1)

fhandle = open( sys.argv[1], 'r' )
flines = fhandle.readlines()
fhandle.close()

speed_arr = []

for line in flines:
    l_lst = line.strip().split(':')

    speed_arr.append( float(l_lst[1]) )

print np.mean( np.array(speed_arr) )

