import os
import sys

#For Mobility Experiment

for ts in [10, 20, 30, 40]:
    for expr in ['slow', 'medium', 'fast']:
        for count in [1,2,3,4,5]:
            os.system("cat ../data_logs/EU_Reader_Experiment_Logs/12thAugust_Cart_Experiments/mobility/" + str(ts) +"s/" + expr +"/sample"+str(count)+"/all_data_group2.csv > ../data_logs/EU_Reader_Experiment_Logs/12thAugust_Cart_Experiments/mobility/" + str(ts) +"s/" + expr +"/sample"+str(count)+"/all_data_combined.csv")
            os.system("cat ../data_logs/EU_Reader_Experiment_Logs/12thAugust_Cart_Experiments/mobility/" + str(ts) +"s/" + expr +"/sample"+str(count)+"/all_data.csv >> ../data_logs/EU_Reader_Experiment_Logs/12thAugust_Cart_Experiments/mobility/" + str(ts) +"s/" + expr +"/sample"+str(count)+"/all_data_combined.csv")
            os.system("python mobility_detector.py ../data_logs/EU_Reader_Experiment_Logs/12thAugust_Cart_Experiments/mobility/" + str(ts) +"s/" + expr +"/sample"+str(count)+"/all_data_combined.csv ../outside_reference_tags6_locations.txt ../data_logs/EU_Reader_Experiment_Logs/12thAugust_Cart_Experiments/mobility/" + str(ts) + "s/" + expr + "/sample"+ str(count) + "/SpeedData.csv " + str(ts))

'''
#For Static Experiments
for ts in [10, 20, 30, 40]:
    for count in [1,2,3,4,5]:
        os.system("python mobility_detector.py ../data_logs/EU_Reader_Experiment_Logs/12thAugust_Cart_Experiments/static/" + str(ts) +"s//sample"+str(count)+"/all_data_group2.csv ../outside_reference_tags6_locations.txt ../data_logs/EU_Reader_Experiment_Logs/12thAugust_Cart_Experiments/static/" + str(ts) + "s//sample"+ str(count) + "/SpeedData.csv " + str(ts))
'''
