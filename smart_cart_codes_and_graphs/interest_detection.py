#Here we will use reference tags inside and outside cart
#This is the final version of cart clustering code

#!/usr/bin/python
import sys 
import operator
import numpy as np
import scipy.signal
import scipy.cluster.vq
import math

if len(sys.argv) < 5:
    print("python <cart_file> <outside_file> <cart_id_file> <reference_tag_cart_file> <refernce_tag_outside_file>")
    sys.exit(-1)


fhandle = open( sys.argv[1], 'r')
cartLines = fhandle.readlines()
fhandle.close()

fhandle = open( sys.argv[2], 'r')
outLines = fhandle.readlines()
fhandle.close()

fhandle = open( sys.argv[3], 'r')
cart_ids = fhandle.readlines()
fhandle.close()


fhandle = open( sys.argv[4], 'r')
ref_ids = fhandle.readlines()
fhandle.close()

fhandle = open( sys.argv[5], 'r')
out_ref_ids = fhandle.readlines()
fhandle.close()


cart_id_arr = []
ref_id_arr = []
out_ref_id_arr = []

epc_data_dict = {}
epc_first_data = {}

tstamp_arr = []
epc_arr = []

ref_id_arr = []
cart_id_arr = []
out_ref_id_arr = []

channel_arr = []

for cart in cart_ids:
    cart_id_arr.append(str(cart).strip())

for ref in ref_ids:
    ref_id_arr.append(str(ref).strip())

for out_ref in out_ref_ids:
    out_ref_id_arr.append(str(out_ref).strip().split(':')[0])

#print out_ref_id_arr
#Data from Cart Tags
for line in cartLines:
    l_str = line.strip().split(',')
            
    epc = str(l_str[0])
    antenna = int(l_str[1])
    tstamp = float(l_str[3])
    doppler = float(l_str[5])
    rssi = float(l_str[7])
    channel = float(l_str[8])
    phase = float(l_str[9])

    
    #if (epc not in ref_id_arr) and (epc not in out_ref_id_arr) and 3 == antenna:  #Ignore them
    if antenna == 3:  #Ignore them
   
        #print antenna
        tstamp_arr.append( tstamp )
   

        if epc not in epc_data_dict:
            epc_data_dict[epc] = [[tstamp,doppler,rssi,phase,channel]]
            epc_first_data[epc] = tstamp
    	    epc_arr.append( epc )
        else:
            epc_data_dict[epc].append([tstamp,doppler,rssi,phase,channel])



ref_phase_dict = {}
expected_channel_arr_dict = {}
expected_channel_arr = []
indx = 0

#Data from Outside Tags
for line in outLines:
    l_str = line.strip().split(',')
            
    epc = str(l_str[0])
    antenna = int(l_str[1])
    tstamp = float(l_str[3])
    doppler = float(l_str[5])
    rssi = float(l_str[7])
    channel = float(l_str[8])
    phase = float(l_str[9])

    

    if (antenna == 3):  #Ignore them
       
        #print antenna
        tstamp_arr.append( tstamp )

        #Select any random reference Tag in the cart
        if epc == ref_id_arr[1]:
            if channel not in channel_arr:
                channel_arr.append(channel)
                ref_phase_dict[channel] = [phase]
            else:
                ref_phase_dict[channel].append(phase)

        if channel not in expected_channel_arr_dict:
            expected_channel_arr_dict[channel] = indx
            indx = indx + 1
            expected_channel_arr.append(channel)

        if epc not in epc_data_dict:
            epc_data_dict[epc] = [[tstamp,doppler,rssi,phase,channel]]
            epc_first_data[epc] = tstamp
    	    epc_arr.append( epc )
            
        else:
            epc_data_dict[epc].append([tstamp,doppler,rssi,phase,channel])


channel_level_reference_phase_dict = {}

for channel in channel_arr:
    #print channel, ref_phase_dict[channel]
    channel_level_reference_phase_dict[channel] = np.median( np.array(ref_phase_dict[channel]) )


base_channel = 902.75

np_channel_arr = np.array(channel_arr)

'''
#Adding Missing channel Values
if len(np_channel_arr) < 50:
    for channel in expected_channel_arr:
        if channel not in channel_level_reference_phase_dict:
            
            np.insert(np_channel_arr, int(expected_channel_arr_dict[channel]), channel)
            prev_channel = np_channel_arr[expected_channel_arr_dict[channel]-1]
            correction = (((channel-prev_channel)*1.0)/0.5)*-0.11
            channel_level_reference_phase_dict[channel] = (correction + channel_level_reference_phase_dict[prev_channel])%(2*np.pi)
            if len(np_channel_arr) == 50:
                break
'''
#print "--------------------------------------------"
#for channel in expected_channel_arr:
 #   print channel, channel_level_reference_phase_dict[channel]

phase_trend_tag_dict = {}
phase_trend_var_tag_dict = {}

first_flag = True
base_channel = 915.25

for epc in epc_data_dict:

    epc_data_arr = epc_data_dict[epc]
    prev_val = 0.0
    phase_temp_arr = []
    rssi_temp_arr = []
    doppler_temp_arr = []

    per_channel_phase_data_dict = {}

    for inx in range(len(epc_data_arr)):
        val_list = epc_data_arr[inx]
        tstamp = val_list[0]
        doppler = val_list[1]
        rssi = val_list[2]
        phase = val_list[3]
        channel = val_list[4]

       
        if channel not in per_channel_phase_data_dict:
            per_channel_phase_data_dict[channel] = [phase]
        else:
            per_channel_phase_data_dict[channel].append(phase)

        #per_channel_phase_data_dict
        #Correction Calculation
#        if first_flag == True:
 #           base_channel = channel #Random Valid channel
  #          base_channel = 902.75 #Random Valid channel
   #         first_flag = False
    #        continue

        #correction = 0.0
        
        #TagGyro : Phi( Fr, d ) = mod( ( Phi ( F_i, d ) - Phi ( F_i, d_0 ) )* F_r/F_i + Phi( F_r, d_0 ), 2*pi )
        '''
        if channel in channel_level_reference_phase_dict:
            #print "Here"
            ch_index = expected_channel_arr_dict[channel]
            base_index = expected_channel_arr_dict[base_channel]

            if ch_index != base_index:

                start_i = end_i = 0

                if ch_index < base_index:
                    start_i = ch_index
                    end_i = base_index
                else:
                    start_i = base_index
                    end_i = ch_index

                for i in range(start_i, end_i ):
                    correction += channel_level_reference_phase_dict[expected_channel_arr[i+1]] - channel_level_reference_phase_dict[expected_channel_arr[i]]
        else:
        '''
        #TagGyro
        #new_val = ((float(phase) - channel_level_reference_phase_dict[channel])*( (base_channel*1.0)/(channel*1.0) ) + channel_level_reference_phase_dict[channel])%(2*np.pi)
        
        #diff = ((float(channel) - float(base_channel))*1.0)/0.5
        #correction = -0.11*diff
        #new_val = (float(phase) + correction)%(2*np.pi)
        #print epc, channel, phase, new_val

        #phase_temp_arr.append(new_val)
        #epc_data_dict[epc][inx][3] = float(new_val)
        #phase_temp_arr.append(str(phase) + ":" +  str(channel))
        #phase_temp_arr.append(float(new_val))

        rssi_temp_arr.append(rssi)
        doppler_temp_arr.append(doppler)

    phase_diff_temp_arr = []

    for channel in per_channel_phase_data_dict:
        phase_arr = per_channel_phase_data_dict[channel]
        if len(phase_arr) > 1:
            for ix in range(len(phase_arr) - 1):
                if math.fabs(phase_arr[ix+1] - phase_arr[ix]) < np.pi:
                    phase_diff_temp_arr.append( phase_arr[ix+1] - phase_arr[ix] )
                else:
                    phase_diff_temp_arr.append( math.fabs((phase_arr[ix+1] - phase_arr[ix])) - (2*np.pi) )


    #phase_trend_tag_dict[epc] = phase_temp_arr
    #phase_trend_var_tag_dict[epc] = np.std(np.array((phase_temp_arr))) + np.std(np.array((rssi_temp_arr)))
    #phase_trend_var_tag_dict[epc] = np.std(np.array((doppler_temp_arr)))
    #phase_trend_var_tag_dict[epc] = np.std(np.array((rssi_temp_arr)))
    if len(phase_diff_temp_arr) > 2:
        phase_trend_tag_dict[epc] = phase_diff_temp_arr
        phase_trend_var_tag_dict[epc] = np.std(np.array((phase_diff_temp_arr)))

sorted_epc_by_phase_trend_val = sorted(phase_trend_var_tag_dict.items(), key=operator.itemgetter(1))

#print sorted_epc_by_phase_trend_val[0][0]
for elem in sorted_epc_by_phase_trend_val:
#for elem in epc_data_dict:
    if elem[0] not in ref_id_arr:
            #if elem[0] not in cart_id_arr:
            if elem[0] not in out_ref_id_arr:
                print elem[0]
                for ex in phase_trend_tag_dict[ elem[0] ]:
                    print ex
                #print elem[0], elem[1]
#print 2*np.pi
