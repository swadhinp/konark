#!/usr/bin/python
import sys
import operator
import numpy as np
import scipy.signal
import scipy.cluster.vq

if len(sys.argv) < 2:
    print("python <interest_file>")
    sys.exit(-1)

fhandle = open( sys.argv[1], 'r')
cartLines = fhandle.readlines()
fhandle.close()

epc_data_dict = {}

phase_arr_before = []
phase_arr_after = []

#Data from Cart Tags
for line in cartLines:
    l_str = line.strip().split(',')

    epc = str(l_str[0])
    tstamp = float(l_str[3])
    doppler = float(l_str[5])
    rssi = float(l_str[7])
    channel = float(l_str[8])
    phase = float(l_str[9])

    phase_arr_before.append(phase)
    #EPC wise data population
    if epc not in epc_data_dict:
        epc_data_dict[epc] = [[tstamp,doppler,rssi,phase,channel]]
    else:
        epc_data_dict[epc].append([tstamp,doppler,rssi,phase,channel])

base_channel = 902.75
first_flag = True

#print("Channel, Base_Channel, Actual_Phase, Scaled_Phase, Diff")

for epc in epc_data_dict:

    epc_data_arr = epc_data_dict[epc]
    prev_val = 0.0
    phase_temp_arr = []

    for inx in range(len(epc_data_arr)):
        val_list = epc_data_arr[inx]
        tstamp = val_list[0]
        phase = val_list[3]
        channel = val_list[4]

        #Correction Calculation
        if first_flag == True:
            base_channel = 902.75#Random Valid channel
            first_flag = False

        diff = ((float(channel) - float(base_channel))*1.0)/0.5
        correction = 0.11*diff

        new_val = (float(phase) + correction)%(2*np.pi)
        #print epc, channel, phase, new_val

        #phase_temp_arr.append(new_val)
        epc_data_dict[epc][inx][3] = new_val
        phase_arr_after.append(new_val)

#print "Before : ", phase_arr_before
print "After : ", phase_arr_after
