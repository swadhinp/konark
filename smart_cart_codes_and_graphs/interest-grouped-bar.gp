set title ''
set terminal postscript eps color size 6.0in,4.5in font 'Helvetica,28'
set output 'interest-grouped-bar.eps'
set ylabel 'Accuracy' font 'Helvetica,28'
set xlabel 'Number of items in the vicinity of the cart' font 'Helvetica,28' offset 2

set offset 0,1.3,0,0
set pointsize 4.0
set style fill solid border -1
set style histogram errorbars gap 2 lw 4
set style data histogram
set grid ytics
set yrange [85:100]
set datafile separator ","

plot 'interest-grouped-bar.dat' using 2:3:xtic(1) ti "20s" linecolor rgb "#FF0000", \
     '' using 4:5 ti "30s" lt 1 lc rgb "#00FF00", \
     '' using 6:7 ti "40s" lt 1 lc rgb "#00FFFF"
