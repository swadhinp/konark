set terminal postscript eps color size 6.0in,4.5in font 'Helvetica,28'
set output 'compare_phase_diff.eps'
set ylabel 'Phase Variation across Channel' font 'Helvetica,28' offset 2
set xlabel 'Reading Count' font 'Helvetica,28' offset 2
set title ''

set key top left
set xrange [1:57]
#set style fill empty
set xtics auto
set ytics auto

#set style line 1 lc rgb '#0060ad' lt 1 lw 4 pt 10 ps 1.5   # --- blue

plot  "compare_phase_diff.dat" using 1:2 w linespoints lt 1 lw 6 pt 1 ps 2 title 'Item of Interest', \
 "compare_phase_diff.dat" using 1:3 w linespoints lw 6 pt 1 ps 2 title 'Random Item'

