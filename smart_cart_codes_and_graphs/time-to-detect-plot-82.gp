set terminal postscript eps color size 6.0in,4.5in font 'Helvetica,28'
set output 'time-to-detect-plot-82.eps'
set ylabel 'Value' font 'Helvetica,28' offset 2
set xlabel 'Detection Latency (in Seconds)' font 'Helvetica,28' offset 2
set title ''

#set key top left
#set yrange [0:100]
#set style fill empty
set xtics auto
set ytics auto

#set style line 1 lc rgb '#0060ad' lt 1 lw 4 pt 10 ps 1.5   # --- blue

plot "time-to-detect-plot-82.dat" using 1:2:3 w yerrorbars ls 1 lw 8 notitle, \
 "time-to-detect-plot-82.dat" using 1:2 w linespoints lt 1 lw 6 pt 2 ps 2 title 'False Positive %', \
"time-to-detect-plot-82.dat" using 1:4:5 w yerrorbars ls 3 lw 8 notitle, \
 "time-to-detect-plot-82.dat" using 1:4 w linespoints lt 3 lw 6 pt 2 ps 2 title 'False Negative %'
