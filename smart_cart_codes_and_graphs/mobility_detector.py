import sys
import time
import math
import numpy as np


ref_epc_dict = {}
record_ref_epc_dict = {}
record_ref_epc_arr = []
record_ref_epc_arr_container = []
diff = 0.0
start_time = 0.0
threshold = 3.0
count = 0

first_flag = True

def min_cord( arr ):
    min_cord = arr[0]

    for i in range(len(arr) -1):
        e_arr = arr[i]

        if e_arr[0] <= min_cord[0]:
            min_cord = e_arr

    c_arr  = str(e_arr[0]) + "--" + str(e_arr[1]) + "--" + str(e_arr[2])
    return c_arr

def max_cord( arr ):
    max_cord = arr[0]

    for i in range(len(arr) -1):
        e_arr = arr[i]

        if e_arr[0] > max_cord[0]:
            max_cord = e_arr

    c_arr  = str(e_arr[0]) + "--" + str(e_arr[1]) + "--" + str(e_arr[2])
    return c_arr

if __name__ == '__main__':

    if len(sys.argv) < 5:
        print "python <Exec File> <Data File> <Reference Tag File> <Speed File> <Time-to-Detect>"
        sys.exit(-1)

    diff = float(sys.argv[4])

    fhandle = open( sys.argv[3], 'r' )
    flines = fhandle.readlines()
    fhandle.close()

    speed_arr = []

    for line in flines:
        l_lst = line.strip().split(':')

        speed_arr.append( float(l_lst[1]) )

    speed_mean = np.mean( np.array(speed_arr) )

    speed_descriptor = ""

    if speed_mean < 3.0:
        speed_descriptor = "Slow"
    elif speed_mean < 5.0:
        speed_descriptor = "Medium"
    else:
        speed_descriptor = "Fast"

    #Updating Reference Tags
    fhandle = open(sys.argv[2], "r")
    flines = fhandle.readlines()
    fhandle.close()

    for line in flines:

        l_lst = line.strip().split(':')

        epc = str(l_lst[0])

        x = float(l_lst[1])
        y = float(l_lst[2])
        z = float(l_lst[3])

        if epc not in ref_epc_dict:
            ref_epc_dict[epc] = [x,y,z]

    #print ref_epc_dict.keys()
    #Getting info about all the tags
    fhandle = open(sys.argv[1], "r")
    loglines = fhandle.readlines()
    fhandle.close()

    for line in loglines:
        l_lst = line.strip().split(',')
        epc = str(l_lst[0])
        tstamp = str(l_lst[3])
        #print epc
        
        if first_flag == True:
            start_time = float(tstamp)

            if epc in ref_epc_dict: #Only reference tags
                record_ref_epc_dict[epc] = 1
                record_ref_epc_arr.append( ref_epc_dict[epc] )
            first_flag = False
            continue

        cur_time = float(tstamp)
        
        #print "Here", (cur_time - start_time)
        time_diff = (cur_time - start_time)*1.0/1000000.0
        #print time_diff

        if time_diff  < diff:
            #print epc
            if epc in ref_epc_dict: #Only reference tags
                #print "Match"
                #print epc
                record_ref_epc_dict[epc] = 1
                record_ref_epc_arr.append( ref_epc_dict[epc] )
        else:
            break

    #print record_ref_epc_arr

    if len(record_ref_epc_arr) <= 1:
        print "Static"
    else:
        record_ref_epc_arr_container.append(record_ref_epc_arr)

        gap_arr = []
        gap_dict = {}
        
        #Comparing all two tuples
        for i in range(len(record_ref_epc_arr)-1):
            for j in range(i+1,len(record_ref_epc_arr)):
                gap = 0.0
                for k in range(1): #x,y,z or only x,y or only x (?)
                    gap += (record_ref_epc_arr[i][k] - record_ref_epc_arr[j][k])*(record_ref_epc_arr[i][k] - record_ref_epc_arr[j][k])
                        
                    #print gap
                gap_arr.append(math.sqrt(gap))
            
                #print gap_arr
            
        if np.max(np.array(gap_arr)) > threshold:
            print speed_descriptor + ":Mobile"

            #if count > 1:
                #   print "Going from ", min_cord(record_ref_epc_arr_container[count-1]), " To ", max_cord(record_ref_epc_arr_container[count])
        else:
            print speed_descriptor + ":Static"

    #print line + "****"
    #print "End"
