import sys
import time
import math
import numpy as np

def follow(thefile):
    thefile.seek(0,2)
    while True:
        line = thefile.readline()
        if not line:
            time.sleep(0.1)
            #sys.exit(-1)
            continue
        yield line

ref_epc_dict = {}
record_ref_epc_dict = {}
record_ref_epc_arr = []
record_ref_epc_arr_container = []
diff = 5.0
start_time = time.time()
threshold = 3.0
count = 0

def min_cord( arr ):
    min_cord = arr[0]

    for i in range(len(arr) -1):
        e_arr = arr[i]

        if e_arr[0] <= min_cord[0]:
            min_cord = e_arr

    c_arr  = str(e_arr[0]) + "--" + str(e_arr[1]) + "--" + str(e_arr[2])
    return c_arr

def max_cord( arr ):
    max_cord = arr[0]

    for i in range(len(arr) -1):
        e_arr = arr[i]

        if e_arr[0] > max_cord[0]:
            max_cord = e_arr

    c_arr  = str(e_arr[0]) + "--" + str(e_arr[1]) + "--" + str(e_arr[2])
    return c_arr

if __name__ == '__main__':

    #Updating Reference Tags
    fhandle = open(sys.argv[1], "r")
    flines = fhandle.readlines()
    fhandle.close()

    for line in flines:

        l_lst = line.strip().split(':')

        epc = str(l_lst[0])

        x = float(l_lst[1])
        y = float(l_lst[2])
        z = float(l_lst[3])

        if epc not in ref_epc_dict:
            ref_epc_dict[epc] = [x,y,z]

    #print ref_epc_dict.keys()
    logfile = open(sys.argv[2], "r")
    loglines = follow(logfile)

    print "Start"
    
    for line in loglines:
        l_lst = line.strip().split(',')
        epc = str(l_lst[0])
        #print epc
         
        cur_time = time.time()
        
        #print "Here", (cur_time - start_time)

        if cur_time - start_time  < diff:

            if epc in ref_epc_dict: #Only reference tags
                #print "Match"
                record_ref_epc_dict[epc] = 1
                record_ref_epc_arr.append( ref_epc_dict[epc] )

        else:
            start_time = cur_time
            if len(record_ref_epc_arr) <= 1:
                print "Static"
            else:
                record_ref_epc_arr_container.append(record_ref_epc_arr)

                gap_arr = []
                gap_dict = {}
            
                #Comparing all two tuples
                for i in range(len(record_ref_epc_arr)-1):
                    for j in range(i+1,len(record_ref_epc_arr)):
                        gap = 0.0
                        for k in range(1): #x,y,z or only x,y or only x (?)
                            gap += (record_ref_epc_arr[i][k] - record_ref_epc_arr[j][k])*(record_ref_epc_arr[i][k] - record_ref_epc_arr[j][k])
                        
                        #print gap
                        #gap_arr.append(math.sqrt(gap))
            
                print gap_arr
            
                if np.max(np.array(gap_arr)) > threshold:
                    print "Mobile"

                    if count > 1:
                        print "Going from ", min_cord(record_ref_epc_arr_container[count-1]), " To ", max_cord(record_ref_epc_arr_container[count])
                else:
                    print "Static"

                record_ref_epc_arr = []
                count += 1

        #print line + "****"
    print "End"
