console.log('starting the firebase elasticsearch bridge for RFID sales\n');

var elasticsearch = require("elasticsearch");
var firebase = require("firebase");
var indexName = "salesindex";
var typeName = "salesrecord";

var client = new elasticsearch.Client(
	{
		host : "localhost:9200",
		log : "trace"
	}
);

client.ping(
	{
		requestTimeout : 2000,
		hello : "elasticsearch"
	}, 
	function(error) {
		if (error) {
			console.error('elasticsearch cluster is down');
			process.exit();
		} else {
			console.log('all is well\n');

			// reset the index completely
			indexExists().then(function (exists) {  
  				if (exists) { 
    				return deleteIndex(); 
  				} 
			}).then(initIndex).then(initMapping).then(initializeFirebase(syncFirebase));
		}
	}
);

// delete index
function deleteIndex() {
	return client.indices.delete(
		{
			index : indexName
		}
	);
}

// create the index
function initIndex() {  
    return client.indices.create(
	    {
	        index : indexName
	    }
    );
}

// check if the index exists
function indexExists() {  
    return client.indices.exists(
	    {
	        index : indexName
	    }
    );
}

function initMapping() {  
    return client.indices.putMapping(
	    {
	        index : indexName,
	        type : typeName,
	        body : {
	            properties: {
	                dateTime : { type : "date", format : "epoch_millis" },
	                epc : { type : "string", index : "not_analyzed"},
	                price : { type : "float" },
	                productDescription : { type : "string", index : "not_analyzed"}
	            }
	        }
	    }
    );
}

function initializeFirebase(callback) {
	firebase.initializeApp(
		{
			serviceAccount : "./RetailAnalytics-082680ae289f.json",
			databaseURL : "https://project-3045571626442137917.firebaseio.com/"
		}
	);

	var db = firebase.database();
	var ref = db.ref("/sales");	

	// after initialization is done, call syncFirebase() method
	callback(ref);
}

function syncFirebase(ref) {
	ref.on(
		"child_added", 
		function(snapshot) {
			console.log('added key ' + snapshot.key);

			var newRecord = snapshot.val();

			// create the record
			client.create(
				{
					index : indexName,
					type : typeName,
					id : snapshot.key,
					body : newRecord
				}, 
				function (error, response) {
					if (error) {
						console.error(error);
						//process.exit();
					} else {
						//console.log(response);
						console.log('starting query for sale ' + newRecord.itemID);
						// query for the item description
						queryProductInfo(newRecord.epc, snapshot.key);
					}
				}
			);
		}
	);

	ref.on(
		"child_removed",
		function(snapshot) {
			console.log('removed key ' + snapshot.key);

			client.delete(
				{
					index : indexName,
					type : typeName,
					id : snapshot.key
				},
				function (error, response) {
					if (error) {
						console.error(error);
						//process.exit();
					} else {
						//console.log(response);
					}
				}
			);
		}
	);
}

function queryProductInfo(epc, saleID) {
	var db = firebase.database();
	var ref = db.ref("/products");

	ref.orderByKey().equalTo(epc).on(
		"child_added",
		function(snapshot) {

			console.log('fetching description for product ' + epc);

			client.update(
				{
					index : indexName,
					type : typeName,
					id : saleID,
					body : { 
						doc : {
								productDescription : snapshot.val().description, 
								price : snapshot.val().price
							}
					}
				}, 
				function (error, response) {
					if (error) {
						console.error(error);
						//process.exit();
					} else {
						//console.log(response);
					}
				}
			);
		}
	);
}
