clear all;
Loss_arr = [];
Phase_arr = [];

%%Setup
lambda = (3*1e8)/(867.5*1e6);
omega = 2*pi*(867.5*1e6);

no_rays = 10000;
no_cover_rays = 1000;

tag_dim_in_m = lambda/2.0;
antenna_dist_in_m = 0.2*lambda;

%tag_dim_in_m = 0.10;
%antenna_dist_in_m = 1.7;

antenna_x = antenna_dist_in_m;
antenna_y = tag_dim_in_m/2.0;

%antenna_y = tag_dim_in_m/2.0
%antenna_y = tag_dim_in_m/4.0

tx_power = db2pow(13);
T_arr = [];
omega_arr = [];

const_t = (((0-antenna_x).^2 + (0-antenna_y).^2).^0.5)/(3*1e8);

incr = tag_dim_in_m/no_rays;

for y=0:incr:tag_dim_in_m
    %[pntrange, ~] = rangeangle( [0; y; 0], [antenna_x; antenna_y; 0] );
    X = [0,y;antenna_x,antenna_y];
    d = ((0-antenna_x).^2 + (y-antenna_y).^2).^0.5;
    Phase_arr = [Phase_arr;rem(((4*pi*d)/lambda),2*pi)];
    omega_arr = [omega_arr; omega];
    T_arr = [T_arr; (d*1.0)/(3*1e8)];
    
    %L = db2mag(tx_power-fspl(d, lambda));
    %L = db2mag(tx_power)-fspl(d, lambda);
    %L = db2mag(fspl(d, lambda));
    %L = fspl(d, lambda);
    
    L = tx_power-db2pow(20*log10(4*pi*d/lambda));
    Loss_arr = [Loss_arr; L];
end


%size(Phase_arr)
%size(Loss_arr)
%Loss_arr
%Phase_arr

%Cover_phase_arr = [sum(Loss_arr.*Phase_arr).*ones(3000,1)];
%cover_postn = (1:3000)';

%Multipath_Loss_Arr = db2mag(tx_power-fspl(d_arr, lambda));
Cover_phase_arr = [];
cover_postn = [];

for cover_indx=1:no_cover_rays:no_rays
    
    cover_postn = [cover_postn; cover_indx];
    
    A = Loss_arr(1:cover_indx,1);
    A = [ A; Loss_arr(cover_indx+no_cover_rays:no_rays,1)];
    %Anorm = ((A - min(A))./(max(A) - min(A)));
    %Anorm = A./max(A);
    %Anorm = normc(A);
    Anorm = A;
    
    B = Phase_arr(1:cover_indx,1);
    B = [ B; Phase_arr(cover_indx+no_cover_rays:no_rays,1)];
    
    C = T_arr(1:cover_indx,1);
    C = [ C; T_arr(cover_indx+no_cover_rays:no_rays,1)];
    
    D = omega_arr(1:cover_indx,1);
    D = [ D; omega_arr(cover_indx+no_cover_rays:no_rays,1)];
   

    res_vector= 0;
    
    for i=1:1:size(C,1)
        res_vector = res_vector + Anorm(i,1)*sin( C(i,1)*D(i,1) );
        
   end
    
    Cover_phase_arr = [Cover_phase_arr; res_vector];
    
    
end
%cover_postn = [cover_postn;(no_rays+3001:no_rays+6000)'];
%Cover_phase_arr = [Cover_phase_arr;sum(Loss_arr.*Phase_arr).*ones(3000,1)];

%sum(Loss_arr.*Phase_arr).*ones(3000,1);
%size(cover_postn)
%size(Cover_phase_arr)
%Cover_phase_arr = [Cover_phase_arr; sum(Loss_arr.*Phase_arr).*ones(30,1)];
%Cover_phase_arr/max(Cover_phase_arr)
figure(1)

plot(cover_postn, asin(Cover_phase_arr));
    