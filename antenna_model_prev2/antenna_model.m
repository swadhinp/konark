close all;
clear all;

% Designs an antenna, and applies a box extension programmatically.                                           

% Operating frequency of the antenna
freq = 900e6;

% Find the impedance of the default, untouched RFID antenna
origDipole = createAntenna(freq);
Zorig = impedance(origDipole, freq);

boxPos = -1:0.1:1;
Zin = zeros(1, length(boxPos));
rfidAntennas = cell(1,length(boxPos));
for n = 1:length(boxPos)
    p = boxPos(n);
    disp(['BoxPos=' num2str(p)]);
    rfidDipole = createAntenna(freq, p);
    Zin(n) = impedance(rfidDipole, freq);
    rfidAntennas{n} = rfidDipole;
end

plotImpedance(boxPos, Zin);

