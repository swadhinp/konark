
function [] = plotImpedance(xdata, Z)

    figure;
    subplot(411);
    plot(xdata, real(Z), '-x', 'LineWidth', 2);
    ylabel('Real Z');
    
    subplot(412);
    plot(xdata, imag(Z), '-x', 'LineWidth', 2);
    ylabel('Imag Z');
    
    subplot(413);
    plot(xdata, abs(Z), '-x', 'LineWidth', 2);
    ylabel('|Z|');

    subplot(414);
    plot(xdata, tanh(imag(Z)./(real(Z)+73)), '-x', 'LineWidth', 2);
    ylabel('Phase Z');

    xlabel('Box Position');
end



