
% Parameters:
% pos = normalized offset of extension block

function [antennaMesh] = createAntenna(freq, pos)
    % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % % Custom RFID Antenna
    % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % % Dimensions of the dipole antenna
    % L = 117e-3;         % Total half-length of blade dipole                          
    % W = 140e-3;         % Width of blade dipole                                      
    % Ld = 112e-3;        % Half-length excluding impedance match taper                
    % fw = 3e-3;          % Feed width                                                 
    % g = 3e-3;           % Feed gap                               
    % fp = 2e-3;          % Feed point location

    % 
    % % Define the shape of the two dipole arms
    % ArmLeft  = [-L (-L+Ld) -g/2 -g/2 (-L+Ld) -L;-W/2 -W/2 -fw/2 fw/2 W/2 W/2];
    % ArmRight = [(circshift(-1.*ArmLeft(1,:)',[2,0]))';(circshift(-1.*ArmLeft(2,:)',[2 0]))'];

    % % Describe the shape of the antenna
    % gdArmLeft = [2 size(ArmLeft, 2) reshape(ArmLeft', 1, [])]';
    % gdArmRight = [2 size(ArmRight, 2) reshape(ArmRight', 1, [])]';
    % gd = [gdArmLeft gdArmRight];

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Dipole antenna with better impedance matching
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    dipoleAntenna = design(dipole, freq);
    L = dipoleAntenna.Length/2;         % Total half-length of blade dipole                          
    W = dipoleAntenna.Width;         % Width of blade dipole                                      
    
    % Dimensions of the dipole antenna
    Ld = L-5e-3;        % Half-length excluding impedance match taper                
    fw = 2e-4;                                  % Feed width                                                 
    g = 3e-3;            % Feed gap                               
    fp = 1.6e-3;          % Feed point location
    
    % Define the shape of the two dipole arms
    ArmLeft  = [-L (-L+Ld) -g/2 -g/2 (-L+Ld) -L;-W/2 -W/2 -fw/2 fw/2 W/2 W/2];
    ArmRight = [(circshift(-1.*ArmLeft(1,:)',[2,0]))';(circshift(-1.*ArmLeft(2,:)',[2 0]))'];

    % Describe the shape of the antenna
    gdArmLeft = [2 size(ArmLeft, 2) reshape(ArmLeft', 1, [])]';
    gdArmRight = [2 size(ArmRight, 2) reshape(ArmRight', 1, [])]';
    gd = [gdArmLeft gdArmRight];

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Block extension
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Block (extension) dimensions
    blockWidth = L/2; 
    %blockHeight = 10e-3; % Height of block excluding taper
    blockHeight = L/2; % Height of block excluding taper
    blockGap = 1e-5;

    % Describe the block position if necessary
    if nargin > 1
        assert((abs(pos) >= 0) && (abs(pos) <= 1), ...
            'Box pos must be between 0 and 1');
        
        % Create the block at the offset position
        xCenter = pos * L;
        xLeftEdge = xCenter - (blockWidth/2);
        xRightEdge = xCenter + (blockWidth/2);
        yBottomEdge = W/2 + blockGap;
        yTopEdge = yBottomEdge + blockHeight;
        blockDim = [3 4 xLeftEdge xLeftEdge xRightEdge xRightEdge ...
            yBottomEdge yTopEdge yTopEdge yBottomEdge];

        gdBlock = [blockDim zeros(1, size(gd, 1) - length(blockDim))]';
        gd = [gd gdBlock];
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Construct the antenna using the PDE tool
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    antEdges = decsg(gd);
    pdeModel = createpde();
    geometryFromEdges(pdeModel, antEdges);
    generateMesh(pdeModel, 'Hmax', 0.005);

    % Get the points and triangles from the mesh data
    [p,e,t] = meshToPet(pdeModel.Mesh);
   
    % Use the points and triangle data to construct the antenna mesh
    antennaMesh = customAntennaMesh(p, t);

    % Add the feed points to the antenna
    createFeed(antennaMesh, [-fp, 0], [fp, 0]);
end 
