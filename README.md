Reads phase, RSSI and doppler information from GEN2 RFID tags using the Impinj R420.
Data is plotted on a graph.

== Impinj Documentation ==
----------------------------
1. Unzip "Impinj-SDK.zip". 
2. Unzip "Octane_SDK_Java_1_22_0.zip"
3. The documentation for the Impinj API is now located at: 

    Octane_SDK_Java_1_22_0/documentation/javadoc

== Setup ==
----------------------------
1. Ensure that the host machine has a DHCP server installed.
2. Connect the R420 to an available, DHCP-supported ethernet port.  If DHCP is setup correctly, the Impinj reader will
   be assigned an IP address.
3. Use this reader IP address in the execution command below.

== Build Instructions ==
----------------------------
Run the following command to build the interface software:

    ant all

== Execution ==
----------------------------
To run the software, use the following command:

    ant run-Reader -Dreadername=<IP ADDRESS>

All information on tags detected by the reader are written to "tag.csv" (this can be changed in the Java sources).
The dashboard can then be started by running

    cat tag.csv | python python/dashboard.py
