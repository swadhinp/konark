
function [] = plotImpedance(xdata, Z)

    setDefaultFigureProperties;

    % Plot the magnitude and phase
    figure;
    %subplot(121);
    plot(xdata, abs(Z), '-o');
    ylabel('Impedance Magnitude (|Z|)');
    xlabel('Relative box position');
    
    % Plot the graph according to the following parameters
    export_fig 'simulated_impedance.pdf' -native
    
    %
    %subplot(122);
    figure;
    plot(xdata, angle(Z), '-o');
    ylabel('Phase (in radian)');
    xlabel('Relative box position');

    % Plot the graph according to the following parameters
    export_fig 'simulated_phase.pdf' -native
end



