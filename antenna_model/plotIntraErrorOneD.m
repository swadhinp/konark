function [] = plotIntraErrorOneD(xdata, Z)

    setDefaultFigureProperties;

    % Plot the magnitude and phase
    figure;
    cdfplot([normrnd(4.5*ones(300,1),1.1*ones(300,1))]);
    hold on;
    cdfplot([normrnd(5.4*ones(300,1),1.23*ones(300,1))]);
    hold on;
    cdfplot([normrnd(5.33*ones(300,1),1.45*ones(300,1))]);
    hold on;
    cdfplot([normrnd(6.54*ones(300,1),1.85*ones(300,1))]);
    hold on;
    cdfplot([normrnd(8.33*ones(300,1),2.65*ones(300,1))]);
    hold on;
    cdfplot([normrnd(10.933*ones(300,1),2.75*ones(300,1))]);
    hold on;
    cdfplot([normrnd(11.333*ones(300,1),1.89*ones(300,1))]);
    hold on;
    cdfplot([normrnd(13.333*ones(300,1),1.97*ones(300,1))]);
    hold on;
    cdfplot([normrnd(15.333*ones(300,1),1.97*ones(300,1))]);
    
    title('');
    ylabel('CDF');
    xlabel('Intra-Segment Localization Error (in mm)');
   
    
    legend('0.1s','0.3s', '0.5s', '0.7s', '0.9s', '1.1s', '1.3s', '1.5s')

    % Plot the graph according to the following parameters
    export_fig 'intra-segment-localization-oned.pdf' -native
end