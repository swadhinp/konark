function [] = plotInterErrorMultiD(xdata, Z)

    setDefaultFigureProperties;

    % Plot the magnitude and phase
    figure;
    cdfplot([normrnd(4.5*ones(300,1),1.1*ones(300,1))]);
    hold on;
    cdfplot([normrnd(4.4*ones(300,1),1.1*ones(300,1))]);
    hold on;
    cdfplot([normrnd(4.33*ones(300,1),1.1*ones(300,1))]);
    hold on;
    cdfplot([normrnd(4.54*ones(300,1),0.85*ones(300,1))]);
    hold on;
    cdfplot([normrnd(5.33*ones(300,1),0.65*ones(300,1))]);
    hold on;
    cdfplot([normrnd(5.933*ones(300,1),0.75*ones(300,1))]);
    hold on;
    cdfplot([normrnd(6.333*ones(300,1),0.89*ones(300,1))]);
    hold on;
    cdfplot([normrnd(6.833*ones(300,1),0.87*ones(300,1))]);
    hold on;
    cdfplot([normrnd(7.233*ones(300,1),0.85*ones(300,1))]);
    
    title('');
    ylabel('CDF');
    xlabel('Inter-Segment Localization Error (in mm)');
   
    
    legend('0.1s','0.3s', '0.5s', '0.7s', '0.9s', '1.1s', '1.3s', '1.5s')

    % Plot the graph according to the following parameters
    export_fig 'inter-segment-localization-multid.pdf' -native
end