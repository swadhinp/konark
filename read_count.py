#!/usr/bin/python
import sys
import operator

fhandle = open( sys.argv[1], 'r' )
flines = fhandle.readlines()
fhandle.close()

epc_count_dict = {}

for line in flines:
    l_lst = line.strip().split(',')

    epc = l_lst[0]

    if epc not in epc_count_dict:
        epc_count_dict[epc] = 1
    else:
        epc_count_dict[epc] += 1

sorted_epc_arr = sorted(epc_count_dict.items(), key=operator.itemgetter(1))

for epc, count in sorted_epc_arr:
    print epc, count
