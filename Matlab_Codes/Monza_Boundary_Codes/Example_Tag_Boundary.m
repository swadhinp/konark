I = imread('Dogbone5nn.jpg');
BW = createMask_2151(I);
BW = bwareafilt(BW,1,'largest');
BWf = imopen(BW,strel('disk',2));
BWf = imclose(BWf,strel('disk',2));
% Default line width for all figures
set(groot, 'defaultLineLineWidth', 3);
set(groot, 'defaultLineColor', 'k');

set(groot, 'defaultLineMarkerSize', 10);
set(groot, 'defaultAxesFontSize', 10);
set(groot, 'defaultAxesXGrid', 'on');
set(groot, 'defaultAxesYGrid', 'on');
set(groot, 'defaultFigureColor', 'none');
set(groot, 'defaultFigurePosition', [0 0 640 480]);

%figure
%imshow(BWf)


% Plot the graph according to the following parameters
%export_fig 'dogbone_monza_tag.pdf' -native

B = bwboundaries(BWf);
xmax = max(B{1}(:,1));
xmin = min(B{1}(:,1));
ymax = max(B{1}(:,2));
ymin = min(B{1}(:,2));

% Scale per pixel based on tag dimensions
L = 20.61e-3;
W = 85.27e-3;
LperColpixel = L/(xmax-xmin);
WperRowpixel = W/(ymax-ymin);
Bp = B;
for i = 1:length(Bp)
    Bp{i} = [Bp{i}(:,1).*LperColpixel Bp{i}(:,2).*WperRowpixel zeros(size(Bp{i},1),1)];
end
p = cell2mat(Bp);
x = p(:,1);
y = p(:,2);

figure
plot(y,-x,'-')
% set(gca, 'defaultLineColor', 'k');
% set(gca,'XLim',[0.0 0.1]);
grid off
set(gca,'box','off')
set(gca,'color','none')
axis off
axis equal
set(gca,'xtick',[])
set(gca,'ytick',[])
%xlabel('X (m)')
%ylabel('Y (m)')

% c = customAntennaGeometry;
% c.Boundary = Bp;
% c.Operation = 'P1 - P2';
% c.FeedLocation = [.01825 .039 0];
% c.FeedWidth = 0.5e-3;
% figure
% show(c)
% view(0,90)

% Plot the graph according to the following parameters
export_fig 'dogbone_monza_boundary.png' -native
%title('Boundary points')