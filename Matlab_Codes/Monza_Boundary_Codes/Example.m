I = imread('Dogbone5nn.jpg');
BW = createMask_2151(I);
BW = bwareafilt(BW,1,'largest');
BWf = imopen(BW,strel('disk',2));
BWf = imclose(BWf,strel('disk',2));
%figure
%imshow(BWf)

% Default line width for all figures
set(groot, 'defaultLineLineWidth', 2);
%set(groot, 'defaultLineColor', 'black');
set(groot, 'defaultLineMarkerSize', 10);
set(groot, 'defaultAxesFontSize', 30);
set(groot, 'defaultAxesXGrid', 'on');
set(groot, 'defaultAxesYGrid', 'on');
set(groot, 'defaultFigureColor', 'white');
set(groot, 'defaultFigurePosition', [0 0 640 480]);

B = bwboundaries(BWf);
xmax = max(B{1}(:,1));
xmin = min(B{1}(:,1));
ymax = max(B{1}(:,2));
ymin = min(B{1}(:,2));

% Scale per pixel based on tag dimensions
L = 18.61e-3;
W = 72.27e-3;
LperColpixel = L/(xmax-xmin);
WperRowpixel = W/(ymax-ymin);
Bp = B;
for i = 1:length(Bp)
    Bp{i} = [Bp{i}(:,1).*LperColpixel Bp{i}(:,2).*WperRowpixel zeros(size(Bp{i},1),1)];
end
p = cell2mat(Bp);
x = p(:,1);
y = p(:,2);
figure
plot(y,-x,'-')
grid on
axis equal
xlabel('X (m)')
ylabel('Y (m)')

% Plot the graph according to the following parameters
export_fig 'dogbone_monza_boundary.pdf' -native
%title('Boundary points')