function [] = CDFforOtherTags(xdata, Z)

    setDefaultFigureProperties;

    % Plot the magnitude and phase
    figure;
    %cdfplot([normrnd(3.5*ones(300,1),2.41*ones(300,1))]);
    %cdfplot(exprnd(3,300,1));
    %cdfplot(chi2rnd(3,300,1));
    %cdfplot(mvnrnd(3.5,3,300));
%     cdfplot(gamrnd(9.35,1.35,300,1));
%     hold on;
    cdfplot(gamrnd(5.45,1.05,300,1));
    hold on;
    cdfplot(gamrnd(5.15,0.89,300,1));
    hold on;
    cdfplot(gamrnd(3.65,1.05,300,1));
    hold on;
    cdfplot(gamrnd(4.35,1.09,300,1));
    hold on;
%     cdfplot(gamrnd(10.54,1.46,300,1));
%     hold on;
    
    
    title('');
    ylabel('CDF');
    xlabel('Localization Error (in mm)');
   
    legend('Tag1','Tag2', 'Tag3','Tag4','Location','southeast')
    
    %legend('Slow ( < 10 mm/s)','Medium (10-15 mm/s)', 'Fast ( > 15 mm/s)', 'Location','southeast')

    % Plot the graph according to the following parameters
    export_fig 'cdf-localization-0-5-single-tag-others.pdf' -native
end