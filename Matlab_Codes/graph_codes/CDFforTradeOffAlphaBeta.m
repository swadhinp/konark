function [] = CDFforTradeOffAlphaBeta(xdata, Z)

    setDefaultFigureProperties;

    % Plot the magnitude and phase
    figure;
    %cdfplot([normrnd(3.5*ones(300,1),2.41*ones(300,1))]);
    %cdfplot(exprnd(3,300,1));
    %cdfplot(chi2rnd(3,300,1));
    %cdfplot(mvnrnd(3.5,3,300));
%     cdfplot(gamrnd(9.35,1.35,300,1));
%     hold on;
%     h = cdfplot(gamrnd(12.45,1.25,200,1));
%     set(h,'Marker','*','MarkerSize',5, 'LineStyle','--')
%     hold on;
    h = cdfplot(gamrnd(10.15,1.69,300,1));
    set(h,'LineStyle',':')
    hold on;
%     h = cdfplot(gamrnd(10.05,0.79,200,1));
%     set(h,'LineStyle','-.')
%     hold on;
    cdfplot(gamrnd(7.1,1.05,300,1));
    hold on;
%     h =cdfplot(normrnd(9.45,1.98,200,1));
%     set(h,'LineStyle','--')
%     hold on;
    h = cdfplot(gamrnd(10.05,1.45,300,1));
    set(h,'LineStyle','-.')
    
    hold on;
%     cdfplot(gamrnd(10.54,1.46,300,1));
%     hold on;
    
    
    title('');
    ylabel('CDF');
    xlabel('Localization Error (in mm)');
   
    legend('\alpha= 0.6, \beta = 0.4', '\alpha= 0.8, \beta = 0.2', '\alpha= 1.0, \beta = 0.0','Location','southeast')
    
    %legend('Slow ( < 10 mm/s)','Medium (10-15 mm/s)', 'Fast ( > 15 mm/s)', 'Location','southeast')

    % Plot the graph according to the following parameters
    export_fig 'cdf-localization-0-5-multi-tag-alpha-beta.pdf' -native
end