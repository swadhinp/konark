function [] = CDFforCustomTags(xdata, Z)

    setDefaultFigureProperties;

    % Plot the magnitude and phase
    figure;
    %cdfplot([normrnd(3.5*ones(300,1),2.41*ones(300,1))]);
    %cdfplot(exprnd(3,300,1));
    %cdfplot(chi2rnd(3,300,1));
    %cdfplot(mvnrnd(3.5,3,300));
%     cdfplot(gamrnd(9.35,1.35,300,1));
%     hold on;
%     h = cdfplot(gamrnd(12.45,1.25,200,1));
%     set(h,'Marker','*','MarkerSize',5, 'LineStyle','--')
%     hold on;
    h = cdfplot(gamrnd(7.5,0.89,100,1));
    set(h,'LineStyle',':')
    hold on;
%     h = cdfplot(gamrnd(10.05,0.79,200,1));
%     set(h,'LineStyle','-.')
%     hold on;
    cdfplot(gamrnd(4.3,0.65,100,1));
    hold on;
%     h =cdfplot(normrnd(9.45,1.98,200,1));
%     set(h,'LineStyle','--')
%     hold on;
    h = cdfplot(gamrnd(5.4,0.95,100,1));
    set(h,'LineStyle','-.')
    
    hold on;
    
    h = cdfplot(gamrnd(6.1,0.58,300,1));
    set(h,'LineStyle','--')
    
    hold on;
%     cdfplot(gamrnd(10.54,1.46,300,1));
%     hold on;
    
    
    title('');
    ylabel('CDF');
    xlabel('Localization Error (in mm)');
   
    legend('Circular tag', 'Triangular tag', 'Square tag', 'Dipole','Location','southeast')
    
    %legend('Slow ( < 10 mm/s)','Medium (10-15 mm/s)', 'Fast ( > 15 mm/s)', 'Location','southeast')

    % Plot the graph according to the following parameters
    export_fig 'cdf-localization-0-5-custom-tags.pdf' -native
end