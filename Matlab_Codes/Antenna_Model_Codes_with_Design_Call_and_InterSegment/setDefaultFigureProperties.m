
% Default line width for all figures
set(groot, 'defaultLineLineWidth', 5);
set(groot, 'defaultLineMarkerSize', 10);
set(groot, 'defaultAxesFontSize', 30);
set(groot, 'defaultAxesXGrid', 'on');
set(groot, 'defaultAxesYGrid', 'on');
set(groot, 'defaultFigureColor', 'white');
set(groot, 'defaultFigurePosition', [0 0 640 480]);


