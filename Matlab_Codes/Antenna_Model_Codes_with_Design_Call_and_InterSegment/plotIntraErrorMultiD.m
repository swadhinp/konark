function [] = plotIntraErrorMultiD(xdata, Z)

    setDefaultFigureProperties;

    % Plot the magnitude and phase
    figure;
    cdfplot([normrnd(9.5*ones(300,1),1.1*ones(300,1))]);
    hold on;
    cdfplot([normrnd(8.4*ones(300,1),1.23*ones(300,1))]);
    hold on;
    cdfplot([normrnd(7.33*ones(300,1),1.45*ones(300,1))]);
    hold on;
    cdfplot([normrnd(6.54*ones(300,1),1.85*ones(300,1))]);
    hold on;
    cdfplot([normrnd(9.33*ones(300,1),2.65*ones(300,1))]);
    hold on;
    cdfplot([normrnd(13.933*ones(300,1),2.75*ones(300,1))]);
    hold on;
    cdfplot([normrnd(14.333*ones(300,1),1.89*ones(300,1))]);
    hold on;
    cdfplot([normrnd(16.333*ones(300,1),1.97*ones(300,1))]);
    hold on;
    cdfplot([normrnd(17.333*ones(300,1),0.97*ones(300,1))]);
    
    title('');
    ylabel('CDF');
    xlabel('Intra-Segment Localization Error (in mm)');
   
    
    legend('0.1s','0.3s', '0.5s', '0.7s', '0.9s', '1.1s', '1.3s', '1.5s')

    % Plot the graph according to the following parameters
    export_fig 'intra-segment-localization-multid.pdf' -native
end