
function [] = plotImpedance(xdata, Z)
% 
%     figure;
%     subplot(411);
%     plot(xdata, real(Z), '-x', 'LineWidth', 2);
%     ylabel('Real Z');
%     
%     subplot(412);
%     plot(xdata, imag(Z), '-x', 'LineWidth', 2);
%     ylabel('Imag Z');
%     
%     subplot(413);
%     plot(xdata, abs(Z), '-x', 'LineWidth', 2);
%     ylabel('|Z|');
% 
%     subplot(414);
%     plot(xdata, tanh(imag(Z)./(real(Z)+73)), '-x', 'LineWidth', 2);
%     ylabel('Phase Z');
% 
%     xlabel('Box Position');
    
    figure;
    %subplot(121);
    plot(xdata, abs(Z), '-o');
    ylabel('Impedance Magnitude (|Z|)');
    xlabel('Relative box position');
    
    % Plot the graph according to the following parameters
    export_fig 'simulated_impedance.pdf' -native
    
    figure;
    plot(xdata, angle(Z), '-o');
    ylabel('Phase (in radian)');
    xlabel('Relative box position');

    % Plot the graph according to the following parameters
    export_fig 'simulated_phase.pdf' -native
    
end



