import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation

count = 0
class PolygonHandler:
    # Constructor
    def __init__(self):
        self.X = []
        self.Y = []
        self.numberPoints = 0
        self.fig, ax = plt.subplots()
        self.hl,= plt.plot([], [])
        #self.sc = ax.scatter(self.X,self.Y)
        ax.set_xlim(0,1000)
        ax.set_ylim(0,10)

    # Print the polygon
    def update(hl,_):
        global count
        count += 1
        #self.X.append(count*count+ np.random.normal()*0.01)
        #self.Y.append( 5 )
        #self.numberPoints += 1
        new_data = count*count+ np.random.normal()*0.01
        hl.set_xdata(numpy.append(hl.get_xdata(), new_data))
        hl.set_ydata(numpy.append(hl.get_ydata(), 5))
        plt.draw()
        #return self.sc,

    # append a point
    def add(self,x,y):
        self.numberPoints += 1
        self.X.append(x)
        self.Y.append(y)


P = PolygonHandler()
P.add(0,5)
#P.add(1,5)
#P.add(5,5)

ani = animation.FuncAnimation(P.hl, P.update, interval=100,blit=False)
plt.show()
