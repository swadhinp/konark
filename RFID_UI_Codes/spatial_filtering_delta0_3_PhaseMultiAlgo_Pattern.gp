set terminal postscript eps color size 6.4in,4.8in font 'Helvetica,45'
set output 'spatial_filtering_delta0_3_PhaseMultiAlgo_Pattern.eps'
set ylabel 'Position on Tag (in mm)' font 'Helvetica,45' offset 0
set xlabel 'Time (in s)' font 'Helvetica,45' offset 0
set title ''

set key top left
set grid
#set yrange [1:8.0]
#set xrange [0.5:4.5]
#set style fill empty
#set xtics ("0" 0, "1" 2, "2" 4, "3" 6, "4" 8, "5" 10, "6" 12, "7" 14)
set ytics auto

#set style line 1 lc rgb '#0060ad' lt 1 lw 4 pt 10 ps 1.5   # --- blue

plot "spatial_filtering_delta0_3_PhaseMultiAlgo_Pattern.dat" using 1:2 w lines lt 1 lw 4 title 'Predicted', \
    "spatial_filtering_delta0_3_PhaseMultiAlgo_Pattern.dat" using 1:3 w lines lt 3 lw 4 title 'Ground-truth'

