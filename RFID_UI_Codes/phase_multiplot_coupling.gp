set terminal postscript eps color size 6.0in,4.5in font 'Helvetica,14'
set output 'phase_multiplot_coupling.eps'

set multiplot layout 7, 1 title "Tag Swiping" font ",14"

set tmargin 4

set title "Tag 1"
unset key
set ylabel 'Phase' font 'Helvetica,14' offset 2
set xlabel 'Time (in s)' font 'Helvetica,14' offset 2
#set yrange [0:6]
set xtics auto
set ytics auto
plot "tag1.dat" using 1:2 w lines lt 1 lw 4 notitle

#
set title "Tag 2"
unset key
set ylabel 'Phase' font 'Helvetica,14' offset 2
set xlabel 'Time (in s)' font 'Helvetica,14' offset 2
#set yrange [0:6]
set xtics auto
set ytics auto
plot "tag2.dat" using 1:2 w lines lt 1 lw 4 notitle

set title "Tag 3"
unset key
set ylabel 'Phase' font 'Helvetica,14' offset 2
set xlabel 'Time (in s)' font 'Helvetica,14' offset 2
#set yrange [0:6]
set xtics auto
set ytics auto
plot "tag3.dat" using 1:2 w lines lt 1 lw 4 notitle

set title "Tag 4"
unset key
set ylabel 'Phase' font 'Helvetica,14' offset 2
set xlabel 'Time (in s)' font 'Helvetica,14' offset 2
#set yrange [0:6]
set xtics auto
set ytics auto
plot "tag4.dat" using 1:2 w lines lt 1 lw 4 notitle

set title "Tag 5"
unset key
set ylabel 'Phase' font 'Helvetica,14' offset 2
set xlabel 'Time (in s)' font 'Helvetica,14' offset 2
#set yrange [0:6]
set xtics auto
set ytics auto
plot "tag5.dat" using 1:2 w lines lt 1 lw 4 notitle

set title "Tag 6"
unset key
set ylabel 'Phase' font 'Helvetica,14' offset 2
set xlabel 'Time (in s)' font 'Helvetica,14' offset 2
#set yrange [0:6]
set xtics auto
set ytics auto
plot "tag6.dat" using 1:2 w lines lt 1 lw 4 notitle

set title "Tag 7"
unset key
set ylabel 'Phase' font 'Helvetica,14' offset 2
set xlabel 'Time (in s)' font 'Helvetica,14' offset 2
#set yrange [0:6]
set xtics auto
set ytics auto
plot "tag7.dat" using 1:2 w lines lt 1 lw 4 notitle

unset multiplot

