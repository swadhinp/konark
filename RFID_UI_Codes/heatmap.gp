set terminal postscript eps color size 6.0in,4.5in font 'Helvetica,28'
set output 'heatmap.eps'
set title 'HeatMap'

#set view map
#set dgrid3d
#set pallete grey
set cbrange [0:3]
set palette defined (0 "blue",17 "#00ffff",33 "white",50 "yellow",\
    66 "red",100 "#990000",101 "grey")
set xrange [0:1]
#set yrange [0:7]
set xlabel 'Col #'
set ylabel 'Row #'

set ytics ("1" 0, "2" 1, "3" 2, "4" 3, "5" 4, "6" 5, "7" 6, "8" 7, "9" 8, "10" 9, "11" 10, "12" 11, "13" 12, "14" 13, "15" 14, "16" 15, "17" 16, "18" 17, "19" 18 )
set xtics ("1" 0)

#splot "heatmap_3_3.dat" using 2:1:3 with pm3d notitle
plot "heatmap.dat" using 2:1:3 with image notitle

