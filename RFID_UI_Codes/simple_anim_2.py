import numpy as np
import matplotlib.pyplot as plt

plt.axis([0, 100, 0, 10])
plt.ion()

len_tag = 0

while len_tag <= 85:
    len_tag += 2+np.random.random()
    plt.scatter(len_tag, 5)
    plt.pause(0.05)

while True:
    plt.pause(0.05)
