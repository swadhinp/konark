set terminal postscript eps color size 6.0in,4.5in font 'Helvetica,14'
set output 'phase_multiplot.eps'

set multiplot layout 3, 1 title "Phase Trend" font ",14"

set tmargin 2

set title "Top Tag"
unset key
set ylabel 'Phase' font 'Helvetica,14' offset 2
set xlabel 'Samples' font 'Helvetica,14' offset 2
#set yrange [0:6]
set xtics auto
set ytics auto
plot "phase_multiplot.dat" using 1:2 w lines lt 1 lw 4 notitle

#
set title "Tag of Interest"
unset key
set ylabel 'Phase' font 'Helvetica,14' offset 2
set xlabel 'Samples' font 'Helvetica,14' offset 2
#set yrange [0:6]
set xtics auto
set ytics auto
plot "phase_multiplot.dat" using 1:3 w lines lt 1 lw 4 notitle


#
set title "Bottom Tag"
set ylabel 'Phase' font 'Helvetica,14' offset 2
set xlabel 'Samples' font 'Helvetica,14' offset 2
#set yrange [0:6]
set xtics auto
set ytics auto
plot "phase_multiplot.dat" using 1:4 w lines lt 1 lw 4 notitle


#
unset multiplot

