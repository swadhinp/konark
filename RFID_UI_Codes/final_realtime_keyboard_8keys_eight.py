#This code is for real-time touch detection for 8 configuration
import scipy.signal
from detect_cusum import detect_cusum
import sys
import time
import math
import numpy as np
from scipy import stats
import gi
import threading
import signal
import operator
import subprocess

gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk, GLib, GObject


if len(sys.argv) < 3:
    #print ("python <file.py> <data_file> <step_movement_speed_in_sec> <starting_tag_indx> <starting_tag_pos>")
    print ("python <file.py> <data_file> <step_movement_speed_in_sec>")
    sys.exit(-1)

#Basic Info and Global Variables

threshold = 1.5
window_size = 11
count = 0

current_tag ="RX"

delta_time_in_sec = float(sys.argv[2])
#starting_tag = str(sys.argv[3])
#starting_pos = str(sys.argv[4])

#Configuration Info
focus_epc_arr = [ '100000000000000000006600', '200000000000000000006600', '300000000000000000006600', '400000000000000000006600', '500000000000000000006600', '600000000000000000006600', '700000000000000000006600', '800000000000000000006600']

focus_epc_dict = {}
focus_epc_dict['100000000000000000006600'] = "R1"
focus_epc_dict['200000000000000000006600'] = "R2"
focus_epc_dict['300000000000000000006600'] = "R3"
focus_epc_dict['400000000000000000006600'] = "R4"
focus_epc_dict['500000000000000000006600'] = "R5"
focus_epc_dict['600000000000000000006600'] = "R6"
focus_epc_dict['700000000000000000006600'] = "R7"
focus_epc_dict['800000000000000000006600'] = "R8"

focus_epc_dict_rev = {}
focus_epc_dict_rev["R1"] = '100000000000000000006600'
focus_epc_dict_rev["R2"] = '200000000000000000006600'
focus_epc_dict_rev["R3"] = '300000000000000000006600'
focus_epc_dict_rev["R4"] = '400000000000000000006600'
focus_epc_dict_rev["R5"] = '500000000000000000006600'
focus_epc_dict_rev["R6"] = '600000000000000000006600'
focus_epc_dict_rev["R7"] = '700000000000000000006600'
focus_epc_dict_rev["R8"] = '800000000000000000006600'



tag_zone_dict = {}

tag_zone_dict["R1"] = ["R1", "R2", "R3"]
tag_zone_dict["R2"] = ["R2", "R1", "R4"]
tag_zone_dict["R3"] = ["R1", "R3", "R4"]
tag_zone_dict["R4"] = ["R2", "R3", "R5", "R4", "R6"]
tag_zone_dict["R5"] = ["R4", "R5", "R7"]
tag_zone_dict["R6"] = ["R4", "R7", "R6"]
tag_zone_dict["R7"] = ["R5", "R6", "R7"]


tag_zone_arr = ["R1", "R2", "R3", "R4", "R5", "R6", "R7", "R8"]

path_dict_arr = []

start_flag = 0

current_tag = "RX"
#current_pos = starting_pos
builder = 0

#path_arr = [ str(current_tag)+ "-" + str(current_pos) ]
path_arr = [ str(current_tag) ]

path_dict_arr.append({ str(current_tag) : 1})

def follow(thefile):
    thefile.seek(0,2)
    while True:
        line = thefile.readline()
        if not line:
            time.sleep(0.1)
            #sys.exit(-1)
            continue
        yield line

#GUI Class
startFlag = 0
lastid = str("RX")

def gui_render():

    builder = Gtk.Builder()
    builder.add_from_file("keyboard.glade")
    window = builder.get_object("keyboardWin")
    window.connect("delete-event", Gtk.main_quit)
    window.set_title("KeyBoard GUI")

    def update_switch(sid):
        
        global startFlag
        global lastid

        #print( sid + " : " + lastid)

        if sid == "RX":
            return False

        if startFlag == 0:

            startFlag = 1
            lastid = str(sid)
            audio_file = "/home/swadhin/RIO_RFIDTracker/konark/RFID_UI_Codes/music_files/" + str(sid) + ".wav"
            #return_code = subprocess.call(["afplay", audio_file])
            switch1 = builder.get_object(sid)
            switch1.set_active(True)

        else:

            if sid != lastid:
                switch1 = builder.get_object(lastid)
                switch1.set_active(False)
                switch2 = builder.get_object(sid)
                switch2.set_active(True)
		print(sid)
                audio_file = "/home/swadhin/RIO_RFIDTracker/konark/RFID_UI_Codes/music_files/" + str(sid) + ".wav"
                #return_code = subprocess.call(["afplay", audio_file])
                lastid = sid

        return False

    #def switch_change(threadName, run_event):
    def switch_change():
        #while run_event.is_set():
        while True:
            #sid = str(current_tag) + str(current_pos)
            sid = str(current_tag)
            GLib.idle_add(update_switch, sid)
            time.sleep(1.0)

    window.show_all()
    #run_event = threading.Event()
    #run_event.set()

    #Touch Predictor Thread
    thread1 = threading.Thread(target=touch_predict)
    thread1.daemon = True
    thread1.start()
    
    time.sleep(0.1)

    #GUI Switch Change Thread
    thread2 = threading.Thread(target=switch_change)
    thread2.daemon = True
    thread2.start()


#def touch_predict(threadName, run_event):
def touch_predict():

    #while run_event.is_set():
    try:
        epc_phase_dict = {}
        epc_phase_filt_dict = {}
        epc_time_dict = {}
 
        global path_arr
        global current_tag
        global start_flag

        logfile = open(sys.argv[1], "r")
        loglines = follow(logfile)
    
        #print ("Start \n")

        #print (str(current_tag) + "-" + str(current_pos) + "\n")
        #print (str(current_tag) + "\n")
    
        #Phase recording for the time interval
        for line in loglines:
            l_lst = line.strip().split(',')
        
            epc = str(l_lst[0])
            tstamp = float(l_lst[3])
            phase = float(l_lst[9])
            #print epc
        
            if start_flag == 0:
                start_time = tstamp
                start_flag = 1


            cur_time = tstamp

            diff = float(cur_time - start_time)/1000000.0

            if diff < delta_time_in_sec:
            
                #print epc
                if epc in focus_epc_arr: #Only focus on filtered tags
                
                    #print "mtch"
                    if epc not in epc_phase_dict: 
                        epc_phase_dict[epc] = [phase]
                        epc_time_dict[epc] = [tstamp]
                    else:
                        epc_phase_dict[epc].append(phase)
                        epc_time_dict[epc].append(tstamp)

            else:
                #print epc_phase_dict.keys()
                for epc in epc_phase_dict:
                    #epc_phase_dict[epc] = scipy.signal.savgol_filter( np.unwrap( np.array( epc_phase_dict[epc] ), discont=np.pi ), window_size, 2 )
                    epc_phase_dict[epc] = scipy.signal.medfilt( np.unwrap( np.array( epc_phase_dict[epc] ), discont=np.pi ), window_size)
                    #epc_phase_dict[epc] = np.unwrap( np.array( epc_phase_dict[epc] ) )

            
                #Phase slicing for the zone to be interrogated

                tag_zone_phase_arr_dict = {}

                for elem in tag_zone_arr:
                    try:
                        #print("Going through phases of : " + str(elem) )
                        tag_zone_phase_arr_dict[elem] = epc_phase_dict[ focus_epc_dict_rev[elem] ]

                    except KeyError:
                        epc_phase_dict = {}
                        epc_time_dict = {}
                        start_time = cur_time
                        continue
            

                num_tag_more_phase_dict = {}
                #Finding max phase change of tags within the current zone
                for elm in tag_zone_phase_arr_dict:

                    try:
                        tag_phase_arr = tag_zone_phase_arr_dict[elm]
                    
                    except KeyError:
                        current_tag = elem
                        break

                    
                    if len(tag_phase_arr) > 1:
                        ta, tai, taf, tamparr = detect_cusum(tag_phase_arr, threshold, 0.0, True, False)

                        

                        if len(tamparr) > 0:
                            print( str(elm) + " : " + str(tamparr) )
                            print(tag_phase_arr)

                            for i in range(len(tamparr)):
                                tamparr[i] = abs(tamparr[i])

                            tamp =  np.max( np.array(tamparr) )
                            
                            num_tag_more_phase_dict[elm] = tamp

                            #print( str(elm) + " : " + str(tamp) )

                max_elem = current_tag
                second_max_elem = current_tag

                if len(num_tag_more_phase_dict.keys()) >0:

                    sorted_vals_rev = sorted(num_tag_more_phase_dict.items(), key=operator.itemgetter(1))

                    if len(sorted_vals_rev) > 0:
                        max_elem = sorted_vals_rev[-1][0]

                    if len(sorted_vals_rev) > 1:
                        second_max_elem = sorted_vals_rev[-2][0]

                if max_elem == current_tag:
                    if len(num_tag_more_phase_dict.keys()) > 1:
                        current_tag = second_max_elem
                else:        
                    current_tag = max_elem


                bamp = 0.0
                camp = 0.0
                tamp = 0.0
                epc_phase_dict = {}
                epc_time_dict = {}
                start_time = cur_time

        #print line + "****"
    except KeyboardInterrupt:
        print ("Path Traced : \n")

       # for i in range( len(path_arr) - 1):
        #    print (path_arr[i] + " =>", end=" ")

        #print (path_arr[len(path_arr) - 1] + "\n")

        sys.exit(-1)


def signal_handler(*args):
    print ("Path Traced : \n")

    #for i in range( len(path_arr) - 1):
     #   print (path_arr[i] + " =>", end=" ")

    #print (path_arr[len(path_arr) - 1] + "\n")


    sys.exit(-1)

if __name__ == "__main__":

    #signal.signal(signal.SIGINT, signal.SIG_DFL)
    signal.signal(signal.SIGINT, signal_handler)
    try:
        
        GObject.threads_init()
        gui_render()
        Gtk.main()
        GObject.threads_leave()
        
        #touch_predict()

    except KeyboardInterrupt:
        # do nothing here
        print ("Path Traced : \n")

        #for i in range( len(path_arr) - 1):
         #   print (path_arr[i] + " =>", end=" ")

        #print (path_arr[len(path_arr) - 1] + "\n")

        sys.exit(-1)
