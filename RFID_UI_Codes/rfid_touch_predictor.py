
import sys
import numpy as np
if len(sys.argv) < 3:
    print "python <pyhton_file>  <data_file> <tag_array_file>"
    sys.exit(-1)


fhandle = open(sys.argv[1], 'r')
flines = fhandle.readlines()
fhandle.close()

fhandle = open(sys.argv[2], 'r')
tlines = fhandle.readlines()
fhandle.close()

tag_locn_dict = {}


for line in tlines:

    l_lst = line.strip().split(':')

    tag_locn_dict[str(l_lst[0])] = str(l_lst[1])

#print tag_locn_dict

epc_phase_dict = {}

for line in flines:

    l_lst = line.strip().split(',')

    epc = str(l_lst[0])
    tstamp = float(l_lst[3])
    phase = float(l_lst[9])

    #iprint tstamp, epc, phase

    if epc not in epc_phase_dict:
        #epc_phase_dict[epc] = [tstamp, phase]
        epc_phase_dict[epc] = [phase]
    else:
        #epc_phase_dict[epc].append([tstamp, phase])
        epc_phase_dict[epc].append(phase)

epc_phase_unwrap_dict = {}

for epc in epc_phase_dict:
    
    diff_arr = np.array(epc_phase_dict[epc])
    epc_phase_unwrap_dict[epc] = np.unwrap( np.array( diff_arr), discont=np.pi )

#print epc_phase_dict.keys()

win_size = 60
thresh = 1.12

count = 0
epc_phase_unwrap_dict_sampled = {}
val_arr = []

for epc in tag_locn_dict:

    #print epc_phase_unwrap_dict[epc]

    for val in  epc_phase_unwrap_dict[epc]:

        if count == win_size :
            count = 0
            if epc not in epc_phase_unwrap_dict_sampled:
                epc_phase_unwrap_dict_sampled[epc] = [np.median( np.array(val_arr) )]
            else:
                epc_phase_unwrap_dict_sampled[epc].append( np.median( np.array(val_arr) ) )

            val_arr = []

        count = count + 1
        val_arr.append(val)

    if len(val_arr) > win_size/2 :
        epc_phase_unwrap_dict_sampled[epc].append( np.median( np.array(val_arr) ) )

print len(epc_phase_unwrap_dict_sampled.keys())

epc_touch_detect = {}

for epc in  epc_phase_unwrap_dict_sampled:

    epc_touch_detect[epc] = []
    

    for i in range(len(epc_phase_unwrap_dict_sampled[epc])-1):
        val = abs(epc_phase_unwrap_dict_sampled[epc][i+1] - epc_phase_unwrap_dict_sampled[epc][i])

        if val >= thresh:
            epc_touch_detect[epc].append(1)
        else:
            epc_touch_detect[epc].append(0)
           


for epc in epc_touch_detect:
    print epc, tag_locn_dict[epc], epc_touch_detect[epc]
