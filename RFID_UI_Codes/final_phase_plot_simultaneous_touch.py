#This code is for detecting simultaneous touch
from __future__ import division, print_function
import scipy.signal
from detect_cusum import detect_cusum
import sys
import time
import math
import numpy as np
from scipy import stats
import threading
import signal
import matplotlib.pyplot as plt
from detect_peaks import detect_peaks
from scipy.interpolate import interp1d
from scipy import interpolate
import warnings
import operator

warnings.filterwarnings(action="ignore", module="scipy", message="^internal gelsd")

if len(sys.argv) < 4:
    print ("python <file.py> <phase_file> <sample_no> <epc1> <epc2>")
    sys.exit(-1)

#Basic Info and Global Variables

fhandle = open( sys.argv[1], 'r' )
loglines = fhandle.readlines()
fhandle.close()

sno = sys.argv[2]

epc_test1 = str(sys.argv[3])
epc_test2 = str(sys.argv[4])


window_size = 11
threshold = 1.2
#threshold = 3.0
sample_time_sec = 0.20
tag_dimn_in_mm = 85
delta_phase_change = 0.50
delta_time_window = 0.20
phase_slope_threshold = 1.3
slope_threshold = 0.10

phase_arr_dict_test = {}
camera_arr_test = []
tstamp_arr_dict_test = {}
tstamp_arr_phase_tag_dict_test = {}
tstamp_arr_camera_test = []
camera_tstamp_arr_test = []



def file_read():

    global phase_arr_dict_test
    global camera_arr_test
    global tstamp_arr_dict_test
    global tstamp_arr_phase_tag_dict_test
    global tstamp_arr_camera_test
    global camera_tstamp_arr_test


    #Test Files Read
    for line in loglines:
        l_lst = line.strip().split(',')
        
        epc = str(l_lst[0])
        tstamp = float(l_lst[3])
        phase = float(l_lst[9])

        if epc not in phase_arr_dict_test:
            phase_arr_dict_test[epc] = [phase]
            tstamp_arr_dict_test[epc] = [tstamp]
        else:
            phase_arr_dict_test[epc].append(phase)
            tstamp_arr_dict_test[epc].append(tstamp)

    for epc in tstamp_arr_dict_test:
        tstamp_arr_phase_tag_dict_test[epc] = []
        
        for i in range(len(tstamp_arr_dict_test[epc])):
            tstamp_arr_phase_tag_dict_test[epc].append( (tstamp_arr_dict_test[epc][i] - tstamp_arr_dict_test[epc][0])*1.0/1000000.0 )

        prev_phase_arr = phase_arr_dict_test[epc]
        new_phase_arr = scipy.signal.medfilt( np.unwrap( np.array( prev_phase_arr ), discont=np.pi ), window_size)
        phase_arr_dict_test[epc] = new_phase_arr

    #phase_arr = scipy.signal.medfilt( np.unwrap( np.array( phase_arr ), discont=np.pi ), window_size)

    #Interpolation and making all the Phase data in a single time window

    min_time_window = 10000.0
    max_len_reqd = 0.0
    
    for epc in tstamp_arr_phase_tag_dict_test:
        
        time_window = tstamp_arr_phase_tag_dict_test[epc][-1] - tstamp_arr_phase_tag_dict_test[epc][0]
        
        if time_window < min_time_window:
            min_time_window = time_window

        if len(tstamp_arr_phase_tag_dict_test[epc]) > max_len_reqd:
            max_len_reqd = len(tstamp_arr_phase_tag_dict_test[epc])

    
    #Getting same length interpolation array for calculation
    time_interp_for_phase_data_arr = np.linspace(0, min_time_window, num = int(max_len_reqd*2.2), endpoint=True)
    
    for epc in phase_arr_dict_test:

        tstamp_arr = tstamp_arr_phase_tag_dict_test[epc]
        phase_arr = phase_arr_dict_test[epc]
        func_linear_phase = interp1d(tstamp_arr, phase_arr, kind='linear')
        phase_arr_dict_test[epc] = func_linear_phase(time_interp_for_phase_data_arr)
        tstamp_arr_phase_tag_dict_test[epc] = time_interp_for_phase_data_arr

    for epc in tstamp_arr_phase_tag_dict_test:

        plt.ylabel("Phase (in Radian)")
        plt.xlabel("Time")
        plt.plot(tstamp_arr_phase_tag_dict_test[epc], phase_arr_dict_test[epc])
        #plt.savefig( "pos_rfid_onetag_predict" + str(sno) + ".png" )
        plt.savefig( "phase_Tag" + str(epc).split('0')[0] + "_" + str(sno)  + "_Tag" + str(epc_test1).split('0')[0]+ "_Tag" + str(epc_test2).split('0')[0] +"Touch.png" )
        plt.cla()
        plt.clf()


def signal_handler(*args):
    print ("Exiting after getting an interrupt \n")
    sys.exit(-1)


if __name__ == "__main__":

    #signal.signal(signal.SIGINT, signal_handler)

    try:
        
        file_read()

    except KeyboardInterrupt:
        # do nothing here
        print ("Exiting after getting an interrupt \n")
        sys.exit(-1)
