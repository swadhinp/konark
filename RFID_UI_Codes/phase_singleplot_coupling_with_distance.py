import sys
import os
import numpy as np

if len(sys.argv) < 4:
    print("python <py_file> <phase_file> <out_file_name> <epc_id>")
    sys.exit(-1)

fhandle = open( sys.argv[1], 'r' )
flines1 = fhandle.readlines()
fhandle.close()

out_file = str(sys.argv[2])
epc_id = str(sys.argv[3])

start_flag = 0
start_time = 0.0

file1 = open(out_file, 'w')

tstamp_arr = []
phase_arr = []
rssi_arr = []

for line in flines1:
    
    l_lst = line.strip().split(',')
    
    epc = str(l_lst[0])
    tstamp = float(l_lst[3])
    phase = float(l_lst[9])
    rssi = float(l_lst[7])

    
    if epc == epc_id:
        if start_flag == 0:
            start_time = tstamp
            start_flag = 1

        l_str = str((tstamp-start_time)/1000000.0) + " " + str(phase) + "\n"
        #file1.write(l_str)
        tstamp_arr.append(str((tstamp-start_time)/1000000.0))
        phase_arr.append(phase)
        rssi_arr.append(rssi)

phase_arr = np.unwrap( np.array( phase_arr ), discont=np.pi )

for i in range( len(phase_arr) ):
#for i in range( len(rssi_arr) ):
    l_str = str(tstamp_arr[i]) + " " + str(phase_arr[i]) + "\n"
    #l_str = str(tstamp_arr[i]) + " " + str(rssi_arr[i]) + "\n"
    file1.write(l_str)

file1.close()

