set terminal postscript eps color size 6.0in,4.5in font 'Helvetica,28'
set output 'vna_lineplot.eps'
set ylabel 'Phase (In Radian)' font 'Helvetica,28' offset 2
set xlabel 'Positions' font 'Helvetica,28' offset 2
set title ''

set key top left
#set xrange [0:23]
#set style fill empty
set xtics auto
set ytics auto

plot "vna_lineplot.dat" using 1:2 w linespoints lt 1 lw 4 pt 2 ps 2 notitle

