#It is for tracking for multi-tag using spatial filter
from __future__ import division, print_function
import scipy.signal
from detect_cusum import detect_cusum
import sys
import time
import math
import numpy as np
from scipy import stats
import threading
import signal
import matplotlib.pyplot as plt
from detect_peaks import detect_peaks
from scipy.interpolate import interp1d
from scipy import interpolate
import warnings
import operator

warnings.filterwarnings(action="ignore", module="scipy", message="^internal gelsd")

if len(sys.argv) < 7:
    print ("python <file.py> <phase_calibration_file> <calibration_loc_file> <sample_no> <test_data_file> <test_loc_file> <epc>")
    sys.exit(-1)

#Basic Info and Global Variables

fhandle = open( sys.argv[1], 'r' )
loglines = fhandle.readlines()
fhandle.close()

fhandle = open(sys.argv[2], 'r')
flines = fhandle.readlines()
fhandle.close()

sno = sys.argv[3]

#print ("Start \n")

fhandle_in = open(sys.argv[4], 'r')
data_lines = fhandle_in.readlines()
fhandle_in.close()

fhandle_in = open(sys.argv[5], 'r')
camera_lines = fhandle_in.readlines()
fhandle_in.close()

epc_test = str(sys.argv[6])


window_size = 11
threshold = 1.2
#threshold = 3.0
sample_time_sec = 0.20
tag_dimn_in_mm = 85
delta_phase_change = 0.30
delta_time_window = 0.40
phase_slope_threshold = 1.3
slope_threshold = 0.10

neighbor_window_dict = {}
neighbor_window_dict['100000000000000000001100'] = ['200000000000000000001100']
neighbor_window_dict['200000000000000000001100'] = ['100000000000000000001100', '300000000000000000001100']
neighbor_window_dict['300000000000000000001100'] = ['200000000000000000001100', '400000000000000000001100']
neighbor_window_dict['400000000000000000001100'] = ['300000000000000000001100', '500000000000000000001100']
neighbor_window_dict['500000000000000000001100'] = ['400000000000000000001100', '600000000000000000001100']
neighbor_window_dict['600000000000000000001100'] = ['500000000000000000001100', '700000000000000000001100']
neighbor_window_dict['700000000000000000001100'] = ['600000000000000000001100', '800000000000000000001100']
neighbor_window_dict['800000000000000000001100'] = ['700000000000000000001100', '900000000000000000001100']
neighbor_window_dict['900000000000000000001100'] = ['800000000000000000001100', 'A00000000000000000001100']
neighbor_window_dict['A00000000000000000001100'] = ['900000000000000000001100']

phase_arr_dict_test = {}
camera_arr_test = []
tstamp_arr_dict_test = {}
tstamp_arr_phase_tag_dict_test = {}
tstamp_arr_camera_test = []
camera_tstamp_arr_test = []

phase_arr_calib = []
camera_arr_calib = []
tstamp_arr_phase_tag_calib = []
tstamp_arr_camera_calib = []
tstamp_arr_calib = []
camera_tstamp_arr_calib = []


def file_read():

    global phase_arr_dict_test
    global camera_arr_test
    global tstamp_arr_dict_test
    global tstamp_arr_phase_tag_dict_test
    global tstamp_arr_camera_test
    global camera_tstamp_arr_test

    global phase_arr_calib
    global camera_arr_calib
    global tstamp_arr_phase_tag_calib
    global tstamp_arr_camera_calib
    global tstamp_arr_calib
    global camera_tstamp_arr_calib

    #Test Files Read
    #for line in loglines:
    for line in data_lines:
        l_lst = line.strip().split(',')
        
        epc = str(l_lst[0])
        tstamp = float(l_lst[3])
        phase = float(l_lst[9])

        if epc not in phase_arr_dict_test:
            phase_arr_dict_test[epc] = [phase]
            tstamp_arr_dict_test[epc] = [tstamp]
        else:
            phase_arr_dict_test[epc].append(phase)
            tstamp_arr_dict_test[epc].append(tstamp)

    for epc in tstamp_arr_dict_test:
        tstamp_arr_phase_tag_dict_test[epc] = []
        
        for i in range(len(tstamp_arr_dict_test[epc])):
            tstamp_arr_phase_tag_dict_test[epc].append( (tstamp_arr_dict_test[epc][i] - tstamp_arr_dict_test[epc][0])*1.0/1000000.0 )

        prev_phase_arr = phase_arr_dict_test[epc]
        new_phase_arr = scipy.signal.medfilt( np.unwrap( np.array( prev_phase_arr ), discont=np.pi ), window_size)
        phase_arr_dict_test[epc] = new_phase_arr

    #phase_arr = scipy.signal.medfilt( np.unwrap( np.array( phase_arr ), discont=np.pi ), window_size)

    count = 0
    for line in camera_lines:

        l_lst = line.strip().split(':')

        tstamp = float(l_lst[0])
        xval = float(l_lst[1])
        yval = float(l_lst[2])

        #if count < 8:
         #   count += 1
          #  continue

        camera_arr_test.append( math.fabs(xval) )
        camera_tstamp_arr_test.append( tstamp )
    
    #Camera X filter
    #camera_arr = scipy.signal.savgol_filter( np.array( camera_arr ), 9, 2 )
    camera_arr_test = scipy.signal.medfilt( np.array( camera_arr_test ), window_size )
    
    for i in range(len(camera_tstamp_arr_test)):
        tstamp_arr_camera_test.append( (camera_tstamp_arr_test[i] - camera_tstamp_arr_test[0])*1.0 )


    #Calibration file read and process

    for line in loglines:
        l_lst = line.strip().split(',')
        
        epc = str(l_lst[0])
        tstamp = float(l_lst[3])
        phase = float(l_lst[9])

        if epc  == epc_test :
            phase_arr_calib.append( phase )
            tstamp_arr_calib.append( tstamp )


    for i in range(len(tstamp_arr_calib)):
        tstamp_arr_phase_tag_calib.append( (tstamp_arr_calib[i] - tstamp_arr_calib[0])*1.0/1000000.0 )

    #phase_arr_test = scipy.signal.savgol_filter( np.unwrap( np.array( phase_arr_test ), discont=np.pi ), window_size, 2 )
    phase_arr_calib = scipy.signal.medfilt( np.unwrap( np.array( phase_arr_calib ), discont=np.pi ), window_size)

    
    count = 0

    for line in flines:

        l_lst = line.strip().split(':')

        tstamp = float(l_lst[0])
        xval = float(l_lst[1])
        yval = float(l_lst[2])

        #if count < 8:
         #   count += 1
          #  continue

        camera_arr_calib.append( math.fabs(xval) )
        camera_tstamp_arr_calib.append( tstamp )
    
    #print(camera_arr_calib)

    #Camera X filter
    camera_arr_calib = scipy.signal.medfilt( np.array( camera_arr_calib ), window_size)
    
    for i in range(len(camera_tstamp_arr_calib)):
        tstamp_arr_camera_calib.append( (camera_tstamp_arr_calib[i] - camera_tstamp_arr_calib[0])*1.0 )



def signal_handler(*args):
    print ("Exiting after getting an interrupt \n")
    sys.exit(-1)

camera_loc_arr = []
tag_phase_arr = []
tag_length_pixel_ratio = 0.0

def phase_calibrate():

    global camera_loc_arr
    global tag_phase_arr
    global tag_length_pixel_ratio
    
    global phase_arr_dict_test
    global tstamp_arr_phase_tag_dict_test
    global camera_arr_test
    global camera_tstamp_arr_test


    #Interpolation and making all the Phase data in a single time window
    
    #Selecting the time window to interpolate all the phase series data

    min_time_window = 10000.0
    max_len_reqd = 0.0
    
    for epc in tstamp_arr_phase_tag_dict_test:
        
        time_window = tstamp_arr_phase_tag_dict_test[epc][-1] - tstamp_arr_phase_tag_dict_test[epc][0]
        
        if time_window < min_time_window:
            min_time_window = time_window

        if len(tstamp_arr_phase_tag_dict_test[epc]) > max_len_reqd:
            max_len_reqd = len(tstamp_arr_phase_tag_dict_test[epc])

    
    #Getting same length interpolation array for calculation
    time_interp_for_phase_data_arr = np.linspace(0, min_time_window, num = int(max_len_reqd*2.2), endpoint=True)
    
    for epc in phase_arr_dict_test:

        tstamp_arr = tstamp_arr_phase_tag_dict_test[epc]
        phase_arr = phase_arr_dict_test[epc]
        func_linear_phase = interp1d(tstamp_arr, phase_arr, kind='linear')
        phase_arr_dict_test[epc] = func_linear_phase(time_interp_for_phase_data_arr)
        tstamp_arr_phase_tag_dict_test[epc] = time_interp_for_phase_data_arr

    '''
    for epc in tstamp_arr_phase_tag_dict_test:

        plt.ylabel("Phase (in Radian)")
        plt.xlabel("Time")
        plt.plot(tstamp_arr_phase_tag_dict_test[epc], phase_arr_dict_test[epc])
        #plt.savefig( "pos_rfid_onetag_predict" + str(sno) + ".png" )
        plt.savefig( "phase_multitag_Tag" + str(epc).split('0')[0] + "_" + str(sno)  + "_Tag" + str(epc_test).split('0')[0]+ "Touch.png" )
        plt.cla()
        plt.clf()
    '''

    #for line in flines:

    
    #Doing Phase Calibration for Delta_P Algorithm

    #Interpolate to make the phase_arr and camera_arr in same length
    #Pixel to mm conversion
    
    start_value = camera_arr_calib[0]
    tag_length_pixel_ratio = (tag_dimn_in_mm*1.0)/abs(float(camera_arr_calib[-1]-camera_arr_calib[0]))*1.0

    for i in range(len(camera_arr_calib)):
        camera_arr_calib[i] = (abs(camera_arr_calib[i] - start_value))*tag_length_pixel_ratio


    #Creating Interpolation Functions
    #print(len(tstamp_arr_phase_tag_calib), len(phase_arr_calib))
    f_phasevel_linear = interp1d(tstamp_arr_phase_tag_calib, phase_arr_calib, kind='linear')
    f_cameravel_linear = interp1d(tstamp_arr_camera_calib, camera_arr_calib, kind='linear')

    len_reqd = max( len(tstamp_arr_phase_tag_calib), len(tstamp_arr_camera_calib) )
    time_for_interp = min( tstamp_arr_phase_tag_calib[-1], tstamp_arr_camera_calib[-1])

    #Getting same length interpolation arrays for calculation
    time_interp_arr = np.linspace(0, time_for_interp, num = int(len_reqd*2.2), endpoint=True)
    phase_interp_arr = f_phasevel_linear(time_interp_arr)
    camera_interp_arr = f_cameravel_linear(time_interp_arr)

    #Plot graphs after and before interpolation

    '''
    graph1, = plt.plot(time_interp_arr, phase_interp_arr, label="Interpolated Phase Values", linestyle='--', marker='o', linewidth=3, markersize=6)
    graph2, = plt.plot(tstamp_arr_phase_tag_calib, phase_arr_calib, label="Actual Phase Values", linestyle='-', marker='x', linewidth=3, markersize=6)

    #plt.legend(handles=[graph1, graph2], loc=4)
    plt.xlabel('Time')
    plt.ylabel('Phase')
    plt.grid(True)
    #plt.savefig( "phase_change_onetag_interp" + str(sno) + ".png" )
    plt.savefig( "phase_change_multitag_interp" + str(sno) + ".png" )
    plt.cla()
    plt.clf()

    graph3, = plt.plot(time_interp_arr, camera_interp_arr, label="Interpolated Camera Values", linestyle='--', marker='o', linewidth=3, markersize=6)
    graph4, = plt.plot(tstamp_arr_camera_calib, camera_arr_calib, label="Actual Camera Values", linestyle='-', marker='x', linewidth=3, markersize=6)

    #plt.legend(handles=[graph3, graph4], loc=4)
    plt.xlabel('Time')
    plt.ylabel('Camera Y Values')
    plt.grid(True)
    #plt.savefig( "camera_xval_onetag_interp" + str(sno) + ".png" )
    plt.savefig( "phase_change_multitag" + str(sno) + ".png" )
    plt.cla()
    plt.clf()
    '''

    #Phase Calibration

    cur_indx = 0
    start_time = time_interp_arr[0]
    
    while cur_indx < len(time_interp_arr):

        count = 0
        X_arr = []
        Y_arr = []
        Z_arr = []

        interval = 0.0

        start_phase = phase_interp_arr[cur_indx]
        start_time = time_interp_arr[cur_indx]
        phase_diff = 0.0

        #print(cur_indx)

        while phase_diff < delta_phase_change :

            phase_diff = abs(phase_interp_arr[cur_indx] - start_phase)
            interval = float(time_interp_arr[cur_indx] - start_time)

            #X_arr.append(count)
            X_arr.append(interval)
            Y_arr.append(phase_interp_arr[cur_indx])
            Z_arr.append(camera_interp_arr[cur_indx])
            
            count += 1
            cur_indx += 1

            if cur_indx >= len(time_interp_arr):
                break

        if cur_indx < len(time_interp_arr):
            start_time = float(time_interp_arr[cur_indx])

        
        #print(phase_diff)
        camera_slope, intercept, r_value, p_value, std_err = stats.linregress(X_arr, Z_arr)
        camera_loc_arr.append( camera_slope*interval )
        #print( "Camera Diff : ", camera_slope*interval)
        phase_slope, intercept, r_value, p_value, std_err = stats.linregress(X_arr, Y_arr)
        #print( "Phase Diff : ", phase_slope*interval, phase_diff)
        tag_phase_arr.append( abs(phase_slope)*interval )
        #accel_slope, intercept, r_value, p_value, std_err = stats.linregress(X_arr, Y_arr)
        #print(np.mean(np.gradient(Y_arr)))
        #distance = init_speed*sample_time_sec*1.0 + 0.5*abs(accel_slope)*sample_time_sec*sample_time_sec


if __name__ == "__main__":

    #signal.signal(signal.SIGINT, signal_handler)

    try:
    
        #global camera_arr
        
        file_read()
        phase_calibrate()
        print( len(camera_loc_arr), len(tag_phase_arr) )
        print( tag_phase_arr )

        min_time_window = 10000.0
        epc_series_cur_indx_dict = {}
        slope_arr_dict = {}

        #Selecting the time window to look at all the phase series data

        for epc in tstamp_arr_phase_tag_dict_test:
            time_window = tstamp_arr_phase_tag_dict_test[epc][-1] - tstamp_arr_phase_tag_dict_test[epc][0]
            epc_series_cur_indx_dict[epc] = 0
            slope_arr_dict[epc] = []

            if time_window < min_time_window:
                min_time_window = time_window


        touch_candidates = []
        time_keeper = 0.0

        #Processing the Phase Data from different Tags

        segment_count = 1
        prev_tag = "TagX"
        prev_tag_count = 0

        time_track_start = -1.0

        while time_keeper < min_time_window:

            sample_epc_series_dict = {}
            sample_epc_time_series_dict = {}

            for epc in phase_arr_dict_test:
            
                start_index = epc_series_cur_indx_dict[epc]
                
                if start_index >= len(tstamp_arr_phase_tag_dict_test[epc]) :
                    break
                
                sample_epc_series_dict[epc] = []
                sample_epc_time_series_dict[epc] = []

                interval = 0.0

                while interval <= delta_time_window:

                    interval = tstamp_arr_phase_tag_dict_test[epc][ epc_series_cur_indx_dict[epc] ] - tstamp_arr_phase_tag_dict_test[epc][start_index]
                    sample_epc_series_dict[epc].append( phase_arr_dict_test[epc][epc_series_cur_indx_dict[epc]] )
                    sample_epc_time_series_dict[epc].append( interval )
                    epc_series_cur_indx_dict[epc] += 1

                    if epc_series_cur_indx_dict[epc] >= len(tstamp_arr_phase_tag_dict_test[epc]) :
                        break

            epc_slope_dict = {}

            #print("Segment : ", segment_count)

            for epc in sample_epc_series_dict:
                
                Y_arr = sample_epc_series_dict[epc]
                X_arr = sample_epc_time_series_dict[epc]

                if len(Y_arr) > 0:
                    accel_slope, intercept, r_value, p_value, std_err = stats.linregress(X_arr, Y_arr)
                    epc_slope_dict[epc] = accel_slope*delta_time_window

                    #print("EPC : ", epc, "Phase Change : ", epc_slope_dict[epc])
                #epc_slope_dict[epc] = np.max(Y_arr) - np.min(Y_arr)

            time_keeper = time_keeper + delta_time_window

            segment_count += 1

            #print(epc_slope_dict.keys())
            #print(time_keeper, min_time_window)
            epc_neighbor_slope_dict = {}

            for epc in epc_slope_dict:

                neighbor_epc_arr = neighbor_window_dict[epc]

                phase_diff = 0.0
                
                for i in range(len(neighbor_epc_arr)):
                    if neighbor_epc_arr[i] in epc_slope_dict:
                        phase_diff += epc_slope_dict[neighbor_epc_arr[i]]
                

                if phase_diff == 0.0:
                    phase_diff = 1.0

                sign1 = 1.0
                sign2 = -1.0

                if epc_slope_dict[epc] < 0:
                    sign1 = -1.0
                    sign2 = 1.0

                epc_neighbor_slope_dict[epc] = sign1*epc_slope_dict[epc]*0.80 + 0.20*phase_diff*sign2
                #epc_neighbor_slope_dict[epc] = abs(epc_slope_dict[epc])*1.0 #+ 0.25*abs(phase_diff)
                #epc_neighbor_slope_dict[epc] = -(epc_slope_dict[epc]*epc_slope_dict[epc])/0.0001*phase_diff/0.01  #+ 0.25*abs(phase_diff)

                #print("EPC : " + epc + "\n")
                #if epc == epc_test:
                 #   print(epc_slope_dict[epc]) 
                slope_arr_dict[epc].append(epc_slope_dict[epc])

                #if epc_slope_dict[epc] > phase_slope_threshold:
                 #   print("Track : ", epc)

            sorted_x = sorted(epc_neighbor_slope_dict.items(), key=operator.itemgetter(1))
            
            print("Predicted Locn : ")
            #print(sorted_x)
            #for elem in sorted_x:
             #   print(elem)
            if ( len (sorted_x) > 0 ):
                
                if prev_tag == "TagX":
                    if math.fabs(sorted_x[-1][1] - sorted_x[-2][1]) < 0.15 :
                        print("Not Decided ")
                    else:
                        prev_tag = sorted_x[-1][0]
                        prev_tag_count += 1
                        print(prev_tag)

                        if epc_slope_dict[prev_tag] > 0.10:
                            time_track_start = time_keeper - delta_time_window 

                else:
                    if math.fabs(sorted_x[-1][1] - sorted_x[-2][1]) >= 0.15 :
                        
                        if prev_tag == sorted_x[-1][0]:
                            prev_tag_count += 1

                        if prev_tag_count < 3:
                            prev_tag = sorted_x[-1][0]
                            prev_tag_count = 1

                        if epc_slope_dict[prev_tag] > 0.10:
                            if time_track_start < 0:
                                time_track_start = time_keeper - delta_time_window 
                        print(prev_tag)
                    else:
                        if epc_slope_dict[prev_tag] > 0.10:
                            if time_track_start < 0:
                                time_track_start = time_keeper - delta_time_window 

                        prev_tag_count += 1
                        print(prev_tag)

                #print(sorted_x[-1][0], sorted_x[-1][1])

        print(time_track_start)
        #print(phase_arr_for_track)
        '''
        for epc in slope_arr_dict:
            plt.xlabel("Steps")
            plt.ylabel("Slope")
            plt.plot(slope_arr_dict[epc])
            plt.savefig( "Slope_" + str(sno) + "_Tag" + str(epc).split('0')[0] + ".png" )
            plt.cla()
            plt.clf()
        '''

        #Preprocess and interpolate the test data series

        phase_arr_test = phase_arr_dict_test[epc_test]
        tstamp_arr_phase_tag_test = tstamp_arr_phase_tag_dict_test[epc_test]

        start_value = camera_arr_test[0]
        for i in range(len(camera_arr_test)):
            camera_arr_test[i] = (abs(camera_arr_test[i] - start_value))*tag_length_pixel_ratio

        tstamp_arr_phase_tag_test_new = []
        phase_arr_test_new = []
        tstamp_arr_camera_test_new = []
        camera_arr_test_new = []

        start_flag = 0

        for i in range( len(tstamp_arr_phase_tag_test) ):
            
            if tstamp_arr_phase_tag_test[i] > tstamp_arr_phase_tag_test[0]:
                if start_flag == 0:
                    start_flag = tstamp_arr_phase_tag_test[i]

                tstamp_arr_phase_tag_test_new.append(tstamp_arr_phase_tag_test[i]-start_flag)
                phase_arr_test_new.append(phase_arr_test[i])

        start_flag = 0
        for i in range( len(tstamp_arr_camera_test) ):

            if tstamp_arr_camera_test[i] > tstamp_arr_phase_tag_test[0]:
                
                if start_flag == 0:
                    start_flag = tstamp_arr_camera_test[i]

                tstamp_arr_camera_test_new.append(tstamp_arr_camera_test[i]-start_flag)
                camera_arr_test_new.append(camera_arr_test[i])


        #print( len(tstamp_arr_phase_tag_test_new), len(phase_arr_test_new), len(tstamp_arr_camera_test_new), len(camera_arr_test_new) )

        f_phasevel_test_linear = interp1d(tstamp_arr_phase_tag_test_new, phase_arr_test_new , kind='linear')
        f_cameravel_test_linear = interp1d(tstamp_arr_camera_test_new, camera_arr_test_new, kind='linear')

        len_reqd_test = max( len(tstamp_arr_phase_tag_test_new), len(tstamp_arr_camera_test_new) )
        time_for_interp_test = min(tstamp_arr_phase_tag_test_new[-1], tstamp_arr_camera_test_new[-1])
        time_interp_arr_test = np.linspace(0, time_for_interp_test, num = int(len_reqd_test*3.2), endpoint=True)

        rfid = f_phasevel_test_linear(time_interp_arr_test)
        camera = f_cameravel_test_linear(time_interp_arr_test)

        '''
        #print(rfid)
        plt.xlabel("Step")
        plt.ylabel("Phase")
        plt.plot(rfid)
        plt.savefig( "RFID_" + str(sno) + "_Tag" + str(epc).split('0')[0] + ".png" )
        plt.cla()
        plt.clf()
        '''



        camera_result_loc_arr = []
        rfid_result_loc_arr = []

        start_loc = camera[0]
        step_arr = []

        step_arr.append(0)
        camera_result_loc_arr.append(start_loc)
        rfid_result_loc_arr.append(start_loc)

        cur_idx = 0
        calib_count = 0

        time_keeper = 0.0
        #Comparing the test accuracy

        cur_idx = 0
        tag_phase_idx = 0
        step_arr = []
        count = 0

        step_arr.append(count)
        print(rfid)
        print(tag_phase_arr)

        while time_keeper < time_interp_arr_test[-1]:

            phase_diff = 0.0
            start_phase = rfid[cur_idx]
            interval = 0.0
            start_time = time_interp_arr_test[cur_idx]

            while interval < delta_time_window:

                if cur_idx >= len(time_interp_arr_test):
                    break
                interval = time_interp_arr_test[cur_idx] - start_time 
                cur_idx += 1

            if cur_idx >= len(time_interp_arr_test):
                break
            

            phase_diff = abs(rfid[cur_idx] - start_phase)
            print(phase_diff)    
            

            phase_change = 0.0

            while tag_phase_idx < len(tag_phase_arr):

                phase_change += tag_phase_arr[tag_phase_idx]
                start_loc += camera_loc_arr[tag_phase_idx]

                print(phase_change, tag_phase_idx, len(tag_phase_arr))

                if phase_change > phase_diff:
                    count += 1
                    step_arr.append(count)
                    camera_result_loc_arr.append(camera[cur_idx])

                    predicted_locn = start_loc - (phase_change-phase_diff)*camera_loc_arr[tag_phase_idx]/tag_phase_arr[tag_phase_idx]
                    if predicted_locn > tag_dimn_in_mm:
                        predicted_locn = tag_dimn_in_mm

                    rfid_result_loc_arr.append( predicted_locn )
                    camera_loc_arr[tag_phase_idx] = (phase_change - phase_diff)*1.0*camera_loc_arr[tag_phase_idx]/tag_phase_arr[tag_phase_idx] 
                    tag_phase_arr[tag_phase_idx] = phase_change - phase_diff
                    break

                tag_phase_idx += 1
            
            time_keeper += interval


        print( len(step_arr), len(rfid_result_loc_arr) )


        fname1 = "rfid_multitag_spatial_predict" + str(sno) + ".tag"
        fhandle = open( fname1, 'w')

        for i in range(len(step_arr)):
            fhandle.write( str(step_arr[i]) + " " + str(rfid_result_loc_arr[i]) + " " + str(camera_result_loc_arr[i]) + "\n")

        fhandle.close()

        line1, = plt.plot(step_arr, rfid_result_loc_arr, label="RFID Predicted", linestyle='--', marker='o', linewidth=3, markersize=6)
        line2, = plt.plot(step_arr, camera_result_loc_arr, label="Camera Actual", linestyle='-', marker='x', linewidth=3, markersize=6)
        plt.legend(handles=[line1, line2], loc=4)
        plt.xlabel("Steps")
        plt.ylabel("On Tag Location (in mm)")
        plt.savefig( "rfid_multitag_spatial_predict" + str(sno) + ".pdf" )
        #plt.savefig( "rfid_onetag_predict" + str(sno) + ".png" )
        plt.cla()
        plt.clf()

        r = np.corrcoef( camera_result_loc_arr, rfid_result_loc_arr )
        print(r)

        dist_arr = []

        for i in range( len(rfid_result_loc_arr) ):
            dist_arr.append( abs(camera_result_loc_arr[i] - rfid_result_loc_arr[i]) )

        sorted_data = np.sort(np.array(dist_arr))
        yvals = np.arange(len(sorted_data))/float(len(sorted_data))
        
        fname2 = "cdf_rfid_spatial_predict" + str(sno) + ".tag"
        fhandle = open( fname2, 'w')

        for i in range( len(yvals) ):
            fhandle.write( str(sorted_data[i]) + " " + str(yvals[i]) + "\n")
        
        fhandle.close()


        plt.xlabel("Localisation prediction error (in mm)")
        plt.ylabel("CDF")
        plt.plot(sorted_data, yvals)
        #plt.savefig( "cdf_rfid_onetag_predict" + str(sno) + ".png" )
        plt.savefig( "cdf_rfid_spatial_predict" + str(sno) + ".pdf" )
        plt.cla()
        plt.clf()
    
        '''
        plt.ylabel("Localisation prediction error (in mm)")
        plt.xlabel("Time")
        plt.plot(dist_arr)
        #plt.savefig( "pos_rfid_onetag_predict" + str(sno) + ".png" )
        plt.savefig( "pos_rfid_multitag_predict" + str(sno) + ".png" )
        plt.cla()
        plt.clf()
        '''
        print("Median Error: " , np.median( np.array(dist_arr) ))

    except KeyboardInterrupt:
        # do nothing here
        print ("Exiting after getting an interrupt \n")
        sys.exit(-1)
