import matplotlib.pyplot as plt
import numpy as np
from detect_cusum import detect_cusum

from scipy import stats
from scipy.spatial.distance import euclidean

from fastdtw import fastdtw
from dtw import dtw

x1 = np.array([[1,1], [2,2], [3,3], [4,4], [5,5]])
y1 = np.array([[2,2], [3,3], [4,4]])

x = np.array([0, 0, 1, 1, 2, 4, 2, 1, 2, 0]).reshape(-1, 1)
y = np.array([1, 1, 1, 2, 2, 2, 2, 3, 2, 0]).reshape(-1, 1)

distance, path = fastdtw(x, y, dist=euclidean)
print(distance)

#plt.plot(x)
#plt.plot(y)

dist, cost, acc, path = dtw(x, y, dist=lambda x, y: np.linalg.norm(x - y, ord=1))
print 'Minimum distance found:', dist

length = np.random.random(10)
length.sort()

time = np.random.random(10)
print time
time.sort()


slope, intercept = np.polyfit(np.log(length), np.log(length), 1)
print(slope)



length1 = np.random.random(10)
length1 = sorted(length1,reverse=True)

length2 = np.random.random(10)
length2 = sorted(length2,reverse=True)

#plt.plot(length, length, '--')
#plt.plot(length1)
#plt.plot(length2)
#plt.show()
#length2.sort()

X = [ 1.0, 2.0, 3.0, 4.0,  5.0,  6.0,  7.0,  8.0,  9.0,  10.0]

slope_0, intercept, r_value, p_value, std_err_0 = stats.linregress(X, length)
slope0, intercept = np.polyfit(X, length, 1)
slope_1, intercept, r_value, p_value, std_err_1 = stats.linregress(X, length1)
slope1, intercept = np.polyfit(X, length1, 1)
slope_2, intercept, r_value, p_value, std_err_2 = stats.linregress(X, length2)
slope2, intercept = np.polyfit(X, length2, 1)


window    = [1, 0, 0, -1]
slopeA     = np.convolve(length, window, mode='valid') / float(len(window) - 1)
padlength = len(window) -1
slope     = np.hstack([np.ones(padlength), slope])

window    = [1, 0, 0, -1]
slopeB     = np.convolve(length1, window, mode='valid') / float(len(window) - 1)
padlength = len(window) -1
slope     = np.hstack([np.ones(padlength), slope])

window    = [1, 0, 0, -1]
slopeC     = np.convolve(length1, window, mode='valid') / float(len(window) - 1)
padlength = len(window) -1
slope     = np.hstack([np.ones(padlength), slope])


print slope_0, std_err_0, slope0, slopeA
print slope_1, std_err_1, slope1, slopeB
print slope_2, std_err_2, slope2, slopeC

ta, tai, taf, amp = detect_cusum(length1, 0.4, 0.0, True, False)

print ta
print tai
print taf
print amp
