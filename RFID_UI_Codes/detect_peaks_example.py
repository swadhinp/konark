from __future__ import division, print_function
import numpy as np
import matplotlib.pyplot as plt
from detect_peaks import detect_peaks

x = np.sin(2*np.pi*5*np.linspace(0, 1, 200)) + np.random.randn(200)/5

detect_peaks(x, mph=0, mpd=20, threshold=0, edge='rising', valley=True, show=True)

