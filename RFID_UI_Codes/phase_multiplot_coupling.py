import sys
import os
import numpy as np
from scipy.fftpack import fft, fftfreq, fftshift, ifft, rfft, irfft
import scipy.signal
import matplotlib.pyplot as plt

Fs = 100.0;  # sampling rate
Ts = 1.0/Fs; # sampling interval

if len(sys.argv) < 4:
    print("python <py_file> <phase_file> <touch_tag> <sample_no>")
    sys.exit(-1)

fhandle = open( sys.argv[1], 'r' )
flines1 = fhandle.readlines()
fhandle.close()

tag_touch = str(sys.argv[2])
sno = str(sys.argv[3])

start_flag = 0
start_time = 0.0


file1 = open("tag1.dat", 'w')
file2 = open("tag2.dat", 'w')
file3 = open("tag3.dat", 'w')
file4 = open("tag4.dat", 'w')
file5 = open("tag5.dat", 'w')
file6 = open("tag6.dat", 'w')
file7 = open("tag7.dat", 'w')

tstamp_arr = []
phase_arr = []
rssi_arr = []

for line in flines1:
    
    l_lst = line.strip().split(',')
    
    epc = str(l_lst[0])
    tstamp = float(l_lst[3])
    phase = float(l_lst[9])
    rssi = float(l_lst[7])

    
    if epc == "10000000000000000000AA00":
        if start_flag == 0:
            start_time = tstamp
            start_flag = 1

        l_str = str((tstamp-start_time)/1000000.0) + " " + str(phase) + "\n"
        #file1.write(l_str)
        tstamp_arr.append(str((tstamp-start_time)/1000000.0))
        phase_arr.append(phase)
        rssi_arr.append(rssi)

phase_arr = np.unwrap( np.array( phase_arr ), discont=np.pi )

#One Scipy based FFT calculation

#data
y = np.array(scipy.signal.detrend(phase_arr))
#y = np.array(scipy.signal.detrend(phase_arr))- np.mean(phase_arr)
N = len(y) # length of the signal
x = np.linspace(0.0,N*Ts,N) # time vector

yf = fft(y)

xplot = np.linspace(0.0, 1.0/(2.0*Ts), N/2.0)
yplot = 2.0/N * np.abs(yf[0:N/2])

#plt.plot(xplot, yplot, '-')

#Second Numpy based FFT calculation
N = len(y) # length of the signal
k = np.arange(N)
T = N/Fs
frq = k/T # two sides frequency range
frq = frq[range(N/2)] # one side frequency range

Y = np.fft.fft(y)/N # fft computing and normalization
Y = Y[range(N/2)]

plt.plot(frq, abs(Y), '-')
plt.xlabel('Freq (Hz)')
plt.ylabel('|Y(freq)|')
plt.grid(True)
plt.savefig( "phase_fft_" + str(tag_touch) + "touch_tag1" + str(sno) + ".png" )
plt.cla()
plt.clf()

#for i in range( len(phase_arr) ):
for i in range( len(rssi_arr) ):
    #l_str = str(tstamp_arr[i]) + " " + str(phase_arr[i]) + "\n"
    l_str = str(tstamp_arr[i]) + " " + str(rssi_arr[i]) + "\n"
    file1.write(l_str)

file1.close()

start_flag1 = 0
start_time1 = 0.0

tstamp_arr1 = []
phase_arr1 = []
rssi_arr = []

for line in flines1:
    
    l_lst = line.strip().split(',')
    
    epc = str(l_lst[0])
    tstamp = float(l_lst[3])
    phase = float(l_lst[9])
    rssi = float(l_lst[7])

    if epc == "20000000000000000000AA00":
        if start_flag1 == 0:
            start_time1 = tstamp
            start_flag1 = 1

        l_str = str((tstamp-start_time1)/1000000.0) + " " + str(phase) + "\n"
        #file2.write(l_str)
        tstamp_arr1.append(str((tstamp-start_time1)/1000000.0))
        phase_arr1.append(phase)
        rssi_arr.append(rssi)

phase_arr1 = np.unwrap( np.array( phase_arr1 ), discont=np.pi )

#data
y = np.array(scipy.signal.detrend(phase_arr1))
N = len(y) # length of the signal
x = np.linspace(0.0,N*Ts,N) # time vector

yf = fft(y)

xplot = np.linspace(0.0, 1.0/(2.0*Ts), N/2.0)
yplot = 2.0/N * np.abs(yf[0:N/2])

plt.plot(xplot, yplot, '-')
plt.xlabel('Freq (Hz)')
plt.ylabel('|Y(Freq)|')
plt.grid(True)
plt.savefig( "phase_fft_" + str(tag_touch) + "touch_tag2" + str(sno) + ".png" )
plt.cla()
plt.clf()

for i in range( len(phase_arr1) ):
    #l_str = str(tstamp_arr1[i]) + " " + str(phase_arr1[i]) + "\n"
    l_str = str(tstamp_arr1[i]) + " " + str(rssi_arr[i]) + "\n"
    file2.write(l_str)

file2.close()


start_flag1 = 0
start_time1 = 0.0

tstamp_arr1 = []
phase_arr1 = []
rssi_arr = []

for line in flines1:
    
    l_lst = line.strip().split(',')
    
    epc = str(l_lst[0])
    tstamp = float(l_lst[3])
    phase = float(l_lst[9])
    rssi = float(l_lst[7])

    if epc == "30000000000000000000AA00":
        if start_flag1 == 0:
            start_time1 = tstamp
            start_flag1 = 1

        l_str = str((tstamp-start_time1)/1000000.0) + " " + str(phase) + "\n"
        #file2.write(l_str)
        tstamp_arr1.append(str((tstamp-start_time1)/1000000.0))
        phase_arr1.append(phase)
        rssi_arr.append(rssi)

phase_arr1 = np.unwrap( np.array( phase_arr1 ), discont=np.pi )

#data
y = np.array(scipy.signal.detrend(phase_arr1))
N = len(y) # length of the signal
x = np.linspace(0.0,N*Ts,N) # time vector

yf = fft(y)

xplot = np.linspace(0.0, 1.0/(2.0*Ts), N/2.0)
yplot = 2.0/N * np.abs(yf[0:N/2])

plt.plot(xplot, yplot, '-')
plt.xlabel('Freq (Hz)')
plt.ylabel('|Y(Freq)|')
plt.grid(True)
plt.savefig( "phase_fft_" + str(tag_touch) + "touch_tag3" + str(sno) + ".png" )
plt.cla()
plt.clf()

for i in range( len(phase_arr1) ):
    #l_str = str(tstamp_arr1[i]) + " " + str(phase_arr1[i]) + "\n"
    l_str = str(tstamp_arr1[i]) + " " + str(rssi_arr[i]) + "\n"
    file3.write(l_str)

file3.close()

start_flag1 = 0
start_time1 = 0.0

tstamp_arr1 = []
phase_arr1 = []
rssi_arr = []

for line in flines1:
    
    l_lst = line.strip().split(',')
    
    epc = str(l_lst[0])
    tstamp = float(l_lst[3])
    phase = float(l_lst[9])
    rssi = float(l_lst[7])

    if epc == "40000000000000000000AA00":
        if start_flag1 == 0:
            start_time1 = tstamp
            start_flag1 = 1

        l_str = str((tstamp-start_time1)/1000000.0) + " " + str(phase) + "\n"
        #file2.write(l_str)
        tstamp_arr1.append(str((tstamp-start_time1)/1000000.0))
        phase_arr1.append(phase)
        rssi_arr.append(rssi)

phase_arr1 = np.unwrap( np.array( phase_arr1 ), discont=np.pi )

#data
y = np.array(scipy.signal.detrend(phase_arr1))
N = len(y) # length of the signal
x = np.linspace(0.0,N*Ts,N) # time vector

yf = fft(y)

xplot = np.linspace(0.0, 1.0/(2.0*Ts), N/2.0)
yplot = 2.0/N * np.abs(yf[0:N/2])

plt.plot(xplot, yplot, '-')
plt.xlabel('Freq (Hz)')
plt.ylabel('|Y(Freq)|')
plt.grid(True)
plt.savefig( "phase_fft_" + str(tag_touch) + "touch_tag4" + str(sno) + ".png" )
plt.cla()
plt.clf()


for i in range( len(phase_arr1) ):
    #l_str = str(tstamp_arr1[i]) + " " + str(phase_arr1[i]) + "\n"
    l_str = str(tstamp_arr1[i]) + " " + str(rssi_arr[i]) + "\n"
    file4.write(l_str)

file4.close()

start_flag1 = 0
start_time1 = 0.0

tstamp_arr1 = []
phase_arr1 = []
rssi_arr = []

for line in flines1:
    
    l_lst = line.strip().split(',')
    
    epc = str(l_lst[0])
    tstamp = float(l_lst[3])
    phase = float(l_lst[9])
    rssi = float(l_lst[7])

    if epc == "50000000000000000000AA00":
        if start_flag1 == 0:
            start_time1 = tstamp
            start_flag1 = 1

        l_str = str((tstamp-start_time1)/1000000.0) + " " + str(phase) + "\n"
        #file2.write(l_str)
        tstamp_arr1.append(str((tstamp-start_time1)/1000000.0))
        phase_arr1.append(phase)
        rssi_arr.append(rssi)

phase_arr1 = np.unwrap( np.array( phase_arr1 ), discont=np.pi )

#data
y = np.array(scipy.signal.detrend(phase_arr1))
N = len(y) # length of the signal
x = np.linspace(0.0,N*Ts,N) # time vector

yf = fft(y)

xplot = np.linspace(0.0, 1.0/(2.0*Ts), N/2.0)
yplot = 2.0/N * np.abs(yf[0:N/2])

plt.plot(xplot, yplot, '-')
plt.xlabel('Freq (Hz)')
plt.ylabel('|Y(Freq)|')
plt.grid(True)
plt.savefig( "phase_fft_" + str(tag_touch) + "touch_tag5" + str(sno) + ".png" )
plt.cla()
plt.clf()


for i in range( len(phase_arr1) ):
    #l_str = str(tstamp_arr1[i]) + " " + str(phase_arr1[i]) + "\n"
    l_str = str(tstamp_arr1[i]) + " " + str(rssi_arr[i]) + "\n"
    file5.write(l_str)

file5.close()

start_flag1 = 0
start_time1 = 0.0

tstamp_arr1 = []
phase_arr1 = []

rssi_arr = []
for line in flines1:
    
    l_lst = line.strip().split(',')
    
    epc = str(l_lst[0])
    tstamp = float(l_lst[3])
    phase = float(l_lst[9])
    rssi = float(l_lst[7])

    if epc == "60000000000000000000AA00":
        if start_flag1 == 0:
            start_time1 = tstamp
            start_flag1 = 1

        l_str = str((tstamp-start_time1)/1000000.0) + " " + str(phase) + "\n"
        #file2.write(l_str)
        tstamp_arr1.append(str((tstamp-start_time1)/1000000.0))
        phase_arr1.append(phase)
        rssi_arr.append(rssi)

phase_arr1 = np.unwrap( np.array( phase_arr1 ), discont=np.pi )

#data
y = np.array(scipy.signal.detrend(phase_arr1))
N = len(y) # length of the signal
x = np.linspace(0.0,N*Ts,N) # time vector

yf = fft(y)

xplot = np.linspace(0.0, 1.0/(2.0*Ts), N/2.0)
yplot = 2.0/N * np.abs(yf[0:N/2])

plt.plot(xplot, yplot, '-')
plt.xlabel('Freq (Hz)')
plt.ylabel('|Y(Freq)|')
plt.grid(True)
plt.savefig( "phase_fft_" + str(tag_touch) + "touch_tag6" + str(sno) + ".png" )
plt.cla()
plt.clf()


for i in range( len(phase_arr1) ):
    #l_str = str(tstamp_arr1[i]) + " " + str(phase_arr1[i]) + "\n"
    l_str = str(tstamp_arr1[i]) + " " + str(rssi_arr[i]) + "\n"
    file6.write(l_str)

file6.close()

start_flag1 = 0
start_time1 = 0.0

tstamp_arr1 = []
phase_arr1 = []

rssi_arr = []
for line in flines1:
    
    l_lst = line.strip().split(',')
    
    epc = str(l_lst[0])
    tstamp = float(l_lst[3])
    phase = float(l_lst[9])
    rssi = float(l_lst[7])

    if epc == "70000000000000000000AA00":
        if start_flag1 == 0:
            start_time1 = tstamp
            start_flag1 = 1

        l_str = str((tstamp-start_time1)/1000000.0) + " " + str(phase) + "\n"
        #file2.write(l_str)
        tstamp_arr1.append(str((tstamp-start_time1)/1000000.0))
        phase_arr1.append(phase)
        rssi_arr.append(rssi)

phase_arr1 = np.unwrap( np.array( phase_arr1 ), discont=np.pi )
#data
y = np.array(scipy.signal.detrend(phase_arr1))
N = len(y) # length of the signal
x = np.linspace(0.0,N*Ts,N) # time vector

yf = fft(y)

xplot = np.linspace(0.0, 1.0/(2.0*Ts), N/2.0)
yplot = 2.0/N * np.abs(yf[0:N/2])

plt.plot(xplot, yplot, '-')
plt.xlabel('Freq (Hz)')
plt.ylabel('|Y(Freq)|')
plt.grid(True)
plt.savefig( "phase_fft_" + str(tag_touch) + "touch_tag7" + str(sno) + ".png" )
plt.cla()
plt.clf()


for i in range( len(phase_arr1) ):
    #l_str = str(tstamp_arr1[i]) + " " + str(phase_arr1[i]) + "\n"
    l_str = str(tstamp_arr1[i]) + " " + str(rssi_arr[i]) + "\n"
    file7.write(l_str)

file7.close()
