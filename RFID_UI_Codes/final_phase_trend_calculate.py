#This code is for single tag tracking (Offline and based on Pmax)
from __future__ import division, print_function
import scipy.signal
from detect_cusum import detect_cusum
import sys
import time
import math
import numpy as np
from scipy import stats
import threading
import signal
import matplotlib.pyplot as plt
from detect_peaks import detect_peaks
from scipy.interpolate import interp1d
from scipy import interpolate
import warnings

warnings.filterwarnings(action="ignore", module="scipy", message="^internal gelsd")

if len(sys.argv) < 5:
    print ("python <file.py> <data_file> <output_data_file> <speed_file> <sample_no>")
    sys.exit(-1)

#Basic Info and Global Variables

fhandle = open( sys.argv[1], 'r' )
loglines = fhandle.readlines()
fhandle.close()

fhandle_out = open( sys.argv[2], 'w' )

fhandle = open(sys.argv[3], 'r')
flines = fhandle.readlines()
fhandle.close()

sno = sys.argv[4]

#print ("Start \n")

phase_arr = []
tstamp_arr = []

camera_arr = []
camera_tstamp_arr = []

window_size = 151
threshold = 1.3
#threshold = 3.0
sample_time_sec = 0.5
tag_dimn_in_mm = 85

camera_tstamp_arr_new = []

def file_read():

    global phase_arr
    global tstamp_arr
    global camera_arr


    for line in loglines:
        l_lst = line.strip().split(',')
        
        epc = str(l_lst[0])
        tstamp = float(l_lst[3])
        phase = float(l_lst[9])

        phase_arr.append( phase )
        tstamp_arr.append( tstamp )


    tstamp_arr_relative = []
    for i in range(len(tstamp_arr)):
        tstamp_arr_relative.append( (tstamp_arr[i] - tstamp_arr[0])*1.0/1000000.0 )

    phase_arr = scipy.signal.savgol_filter( np.unwrap( np.array( phase_arr ), discont=np.pi ), window_size, 2 )
    #phase_arr = scipy.signal.medfilt( np.unwrap( np.array( phase_arr ), discont=np.pi ), window_size)

    '''
    #Absolute Phase Graph Code
    plt.plot(tstamp_arr, phase_arr, 'o')
    plt.xlabel('Time')
    plt.ylabel('Phase')
    plt.grid(True)
    #plt.savefig( "phase_change_onetag" + str(sno) + ".png" )
    plt.savefig( "phase_change_multitag" + str(sno) + ".png" )
    plt.cla()
    plt.clf()
    '''

    for line in flines:

        l_lst = line.strip().split(':')

        tstamp = float(l_lst[0])
        xval = float(l_lst[1])
        yval = float(l_lst[2])

        camera_arr.append( yval )
        camera_tstamp_arr.append( tstamp )
    
    camera_arr = scipy.signal.savgol_filter( np.array( camera_arr ), 15, 2 )
    
    for i in range(len(camera_tstamp_arr)):
        camera_tstamp_arr_new.append( (camera_tstamp_arr[i] - camera_tstamp_arr[0])*1.0 )

    #Camera Graph Code

    '''
    plt.plot(camera_tstamp_arr, camera_arr, 'o')
    plt.xlabel('Time')
    plt.ylabel('X Val')
    plt.grid(True)
    plt.savefig( "cameraX_change_" + str(sno) + ".png" )
    plt.cla()
    plt.clf()
    '''


def file_write():

    count = 0

    for phase in phase_arr:

        fhandle_out.write( str(count) + " " + str(phase) + "\n")
        count += 1

    fhandle_out.close()


def signal_handler(*args):
    print ("Exiting after getting an interrupt \n")
    sys.exit(-1)

camera_loc_arr = []
rfid_loc_arr = []

camera_vel_arr = []
rfid_vel_arr = []

tstamp_arr_new_for_phase = []

def phase_calculate():


    global camera_loc_arr
    global rfid_loc_arr
    global camera_vel_arr
    global rfid_vel_arr

    cur_indx = 0
    start_time = camera_tstamp_arr[0]
    init_locn = 0.0
    init_speed = 0.0

    time_keeper_camera = 0.0

    #Camera Calculate

    while cur_indx < len(camera_arr):

        X_arr = []
        Y_arr = []
        count = 0
        interval = 0.0

        while interval < sample_time_sec :

            interval = float(camera_tstamp_arr[cur_indx] - start_time)
            
            
            X_arr.append(count)
            count += 1
            Y_arr.append(camera_arr[cur_indx])

            cur_indx += 1
            if cur_indx >= len(camera_arr):
                break

        if cur_indx < len(camera_arr):
            start_time = float(camera_tstamp_arr[cur_indx])


        #camera_tstamp_arr_new.append(time_keeper_camera)
        time_keeper_camera += interval

        accel_slope, intercept, r_value, p_value, std_err = stats.linregress(X_arr, Y_arr)
        distance = (Y_arr[0] - Y_arr[-1])*(Y_arr[0] - Y_arr[-1])
        #distance = init_speed*sample_time_sec*1.0 + 0.5*accel_slope*sample_time_sec*sample_time_sec
        velocity = init_speed + accel_slope*sample_time_sec

        camera_loc_arr.append(distance)
        if interval > 0:
            camera_vel_arr.append( (distance*1.0)/(interval*1.0) )
        #camera_vel_arr.append( abs(accel_slope) )

        init_locn = init_locn + distance
        init_speed = velocity

    #Phase Calculation

    cur_indx = 0
    start_time = tstamp_arr[0]
    init_speed = 0.0
    init_locn = 0.0
    time_keeper = 0.0

    step = 0
    while cur_indx < len(phase_arr):

        count = 0
        X_arr = []
        Y_arr = []
        interval = 0.0

        #print(cur_indx)

        while interval < sample_time_sec :

            interval = float(tstamp_arr[cur_indx] - start_time)/1000000.0
            
            #X_arr.append(count)
            X_arr.append(interval)
            Y_arr.append(phase_arr[cur_indx])
            
            count += 1
            cur_indx += 1
            if cur_indx >= len(phase_arr):
                break

        if cur_indx < len(phase_arr):
            start_time = float(tstamp_arr[cur_indx])


        #print( "Len : ", len(Y_arr) )
        '''
        plt.ylim(0.0, 4.0)
        plt.plot(X_arr, Y_arr)

   
        plt.xlabel('Sample')
        plt.ylabel('Phase')
        plt.grid(True)
        plt.savefig( str(step) + "test.png" )

        plt.cla()
        plt.clf()
        '''
        
        step = step+1
        
        accel_slope, intercept, r_value, p_value, std_err = stats.linregress(X_arr, Y_arr)
        #print(np.mean(np.gradient(Y_arr)))
        distance = init_speed*sample_time_sec*1.0 + 0.5*abs(accel_slope)*sample_time_sec*sample_time_sec

        #tstamp_arr_new_for_phase.append((time_keeper*1.0)/2.0)
        tstamp_arr_new_for_phase.append((time_keeper*1.0))
        time_keeper += interval

        if interval > 0:
            #velocity = init_speed + abs(accel_slope)*interval
            #velocity = abs(accel_slope)*interval
            #print(interval)
            velocity = np.max(Y_arr) - np.min(Y_arr)

        rfid_loc_arr.append(distance)
        rfid_vel_arr.append( velocity )
        
        init_locn = init_locn + distance
        init_speed = velocity




if __name__ == "__main__":

    #signal.signal(signal.SIGINT, signal_handler)


    try:
    
        #global camera_arr
        
        file_read()
        file_write()
        phase_calculate()


        #Graph Code for slope change
        '''
        plt.plot(rfid_vel_arr)
        plt.xlabel('Number')
        plt.ylabel('Slope')
        plt.grid(True)
        plt.savefig( "rfid_slope_change_" + str(sno) + ".png" )
        plt.cla()
        plt.clf()

        plt.plot(camera_vel_arr)
        plt.xlabel('Number')
        plt.ylabel('Vel')
        plt.grid(True)
        plt.savefig( "camera_slope_change_" + str(sno) + ".png" )
        plt.cla()
        plt.clf()
        '''

        tstamp_arr_new = []
        for i in range( len(tstamp_arr) ):
            tstamp_arr_new.append( (tstamp_arr[i] - tstamp_arr[0])*1.0/1000000.0 )

        #Phase Change Rate Calculation
        #For multi-tag
        #pa, pai, paf, parr = detect_cusum( phase_arr, threshold, 0.0, True, False)
        
        #For Single Tag
        pa, pai, paf, parr = detect_cusum( phase_arr, threshold, 0.0, True, False)
       
        #print(pai)
        #print(paf)

        #if len(parr) > 2:
            #print("Fatal Calculation Error")

        delta_phase_change_first_half = phase_arr[paf[0]] - phase_arr[pai[0]]
        delta_phase_change_second_half = phase_arr[paf[0]] - np.min( np.array(phase_arr[(paf[0]+1):]))

        #print( str(delta_phase_change_first_half) + " : " + str(delta_phase_change_second_half))

        delta_phase_change = delta_phase_change_first_half

        delta_T = tstamp_arr_new[paf[0]] - tstamp_arr_new[pai[0]]
        #delta_T = paf[0] - pai[0]
       
        ###################### Camera Mid distance calibration ##############
        index = 0
        for i in range(len(camera_tstamp_arr_new) - 1):
            diff_t = camera_tstamp_arr[i+1] - camera_tstamp_arr[0]

            if diff_t > delta_T:
                index = i+1
                break
        
        #dist_to_middle = camera_arr[index] - camera_arr[0]
        
        ########## Camera Mid distance calibration ##############

        dist_to_middle = float(tag_dimn_in_mm)*1.0/2.0

        Y_arr = phase_arr[ pai[0]:paf[0] ]
        X_arr = tstamp_arr_new[ pai[0]:paf[0] ]

        accel_slope, intercept, r_value, p_value, std_err = stats.linregress(X_arr, Y_arr)
        phase_change_rate = dist_to_middle/(accel_slope*delta_T*1.0)

        #print("Phase Change 1 : " + str(delta_phase_change) + " Phase Change 2 : " + str(delta_T *accel_slope*1.0))

        rfid_new_pos_arr = []

        cur_pos = 0.0

        inx = 0

        for elem in rfid_vel_arr:
            if inx <= paf[0]:
                cur_pos = cur_pos + (float(elem)*dist_to_middle*1.0)/delta_phase_change_first_half
            else:
                cur_pos = cur_pos + (float(elem)*dist_to_middle*1.0)/delta_phase_change_second_half

            #rfid_new_pos_arr.append(cur_pos)
            
            if cur_pos < (tag_dimn_in_mm+1):
                rfid_new_pos_arr.append(cur_pos)
            else:
                rfid_new_pos_arr.append(tag_dimn_in_mm)
            
            inx += 1

        start_value = camera_arr[0]
        tag_length_pixel_ratio = (tag_dimn_in_mm*1.0)/float(camera_arr[-1]-camera_arr[0])

        for i in range(len(camera_arr)):
            camera_arr[i] = (camera_arr[i] - start_value)*tag_length_pixel_ratio


        f_phasevel = interp1d(tstamp_arr_new_for_phase, rfid_new_pos_arr, kind='cubic')
        f_phasevel_linear = interp1d(tstamp_arr_new_for_phase, rfid_new_pos_arr, kind='linear')
        #f_phasevel = interp1d(tstamp_arr_new, rfid_vel_arr, kind='cubic')
        #f_phasevel_linear = interp1d(tstamp_arr_new, rfid_vel_arr, kind='linear')
        f_cameravel = interp1d(camera_tstamp_arr_new, camera_arr, kind='cubic')
        f_cameravel_linear = interp1d(camera_tstamp_arr_new, camera_arr, kind='linear')

        len_reqd = max( len(tstamp_arr_new_for_phase), len(camera_tstamp_arr_new) )
        timr_for_interp = min(tstamp_arr_new_for_phase[-1], camera_tstamp_arr_new[-1])

        time_interp_arr = np.linspace(0, timr_for_interp, num = int(len_reqd*3.2), endpoint=True)
        #xnew_camera = np.linspace(0, tstamp_arr_new_for_phase[-1], num = int(len_reqd*3.2), endpoint=True)

        #Prev
        #rfid_predicted_loc_arr = f_phasevel_linear(xnew_phase)*phase_change_rate

        camera = f_cameravel_linear(time_interp_arr)
        rfid = f_phasevel_linear(time_interp_arr)

        fname1 = "rfid_onetag_pmax_predict" + str(sno) + ".tag"
        fhandle = open( fname1, 'w')

        for i in range(len(time_interp_arr)):
            fhandle.write( str(time_interp_arr[i]) + " " + str(rfid[i]) + " " + str(camera[i]) + "\n")

        fhandle.close()
        
        line1, = plt.plot(time_interp_arr, rfid, label="RFID Predicted", linestyle='--', marker='o', linewidth=3, markersize=6)
        line2, = plt.plot(time_interp_arr, camera, label="Camera Actual", linestyle='-', marker='x', linewidth=3, markersize=6)
        plt.legend(handles=[line1, line2], loc=4)
        plt.xlabel("Time (in s)")
        plt.ylabel("On Tag Location (in mm)")
        #plt.savefig( "rfid_multitag_predict" + str(sno) + ".png" )
        plt.savefig( "rfid_onetag_predict" + str(sno) + ".pdf" )
        plt.cla()
        plt.clf()
        


        #Graph code for comparing different interpolation
        '''

        line1, = plt.plot(xnew_phase, f_phasevel(xnew_phase), label="Cubic", linestyle='--', marker='o', linewidth=6, markersize=4)
        line2, = plt.plot(tstamp_arr_new, rfid_vel_arr, label="Actual", linestyle='-', marker='$\lambda$', linewidth=3, markersize=10)
        line3, = plt.plot(xnew_phase, f_phasevel_linear(xnew_phase), linestyle=':', label="Linear", marker='x', linewidth=2, markersize=6)

        plt.legend(handles=[line1, line2, line3])
        plt.savefig( "rfid_interpolate" + str(sno) + ".png" )
        plt.cla()
        plt.clf()

        line1, = plt.plot(xnew_camera, f_cameravel(xnew_camera), label="Cubic", linestyle='--', marker='o', linewidth=2, markersize=4)
        line2, = plt.plot(camera_tstamp_arr_new, camera_arr, label="Actual", linestyle='-', marker='$\lambda$', linewidth=2, markersize=12)
        line3, = plt.plot(xnew_camera, f_cameravel_linear(xnew_camera), linestyle=':', label="Linear", marker='x', linewidth=2, markersize=8)

        plt.legend(handles=[line1, line2, line3])
        plt.savefig( "camera_dist_interpolate" + str(sno) + ".png" )
        plt.cla()
        plt.clf()

        '''

        #r = np.corrcoef( f_phasevel(xnew_phase), f_cameravel(xnew_camera) )
        #print(r)

        #camera_filt = scipy.signal.savgol_filter( np.array( camera ), 15, 2 )
        #rfid_filt = scipy.signal.savgol_filter( np.array( rfid ), 15, 2 )

        r = np.corrcoef( rfid, camera )
        #print(r)

        dist_arr = []

        for i in range( len(camera) ):
            dist_arr.append( abs(camera[i] - rfid[i]) )

        sorted_data = np.sort(np.array(dist_arr))
        yvals = np.arange(len(sorted_data))/float(len(sorted_data))
        
        fname2 = "cdf_rfid_onetag_pmax_predict" + str(sno) + ".tag"
        fhandle = open( fname2, 'w')

        for i in range( len(yvals) ):
            fhandle.write( str(sorted_data[i]) + " " + str(yvals[i]) + "\n")
        
        fhandle.close()
        
        plt.xlabel("Localisation prediction error (in mm)")
        plt.ylabel("CDF")
        plt.plot(sorted_data, yvals)
        plt.savefig( "cdf_rfid_onetag_predict" + str(sno) + ".pdf" )
        #plt.savefig( "cdf_rfid_multitag_predict" + str(sno) + ".png" )
        plt.cla()
        plt.clf()
        '''
        plt.ylabel("Localisation prediction error (in mm)")
        plt.xlabel("Time")
        plt.plot(dist_arr)
        #plt.savefig( "pos_rfid_onetag_predict" + str(sno) + ".png" )
        plt.savefig( "pos_rfid_multitag_predict" + str(sno) + ".png" )
        plt.cla()
        plt.clf()
        '''

        #print("Median Error")
        print(np.median( np.array(dist_arr) ))

        


    except KeyboardInterrupt:
        # do nothing here
        print ("Exiting after getting an interrupt \n")
        sys.exit(-1)
