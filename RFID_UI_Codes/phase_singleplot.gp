set terminal postscript eps color size 6.0in,4.5in font 'Helvetica,14'
set output 'phase_singleplot.eps'

set tmargin 2

unset key
set ylabel 'Phase' font 'Helvetica,14' offset 2
set xlabel 'Samples' font 'Helvetica,14' offset 2
#set yrange [0:6]
set xtics auto
set ytics auto
plot "phase_plot1.dat" using 1:2 w linespoints lt 1 lw 4 notitle

