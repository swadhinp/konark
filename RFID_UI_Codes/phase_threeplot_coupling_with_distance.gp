set terminal postscript eps color size 6.0in,4.5in font 'Helvetica,14'
set output 'phase_threeplot_coupling_with_distance.eps'

set multiplot layout 3, 1 title "Top Tag Swiping, Impact on Bottom Tag" font ",14"

set tmargin 4

set title "90 mm distance"
unset key
set ylabel 'Phase (in radian)' font 'Helvetica,14' offset 2
set xlabel 'Time (in s)' font 'Helvetica,14' offset 2
set xtics auto
set ytics auto
plot "tag2_90_s2.txt" using 1:2 w lines lt 1 lw 4 notitle

#
set title "110 mm distance"
unset key
set ylabel 'Phase (in radian)' font 'Helvetica,14' offset 2
set xlabel 'Time (in s)' font 'Helvetica,14' offset 2
set xtics auto
set ytics auto
plot "tag2_110_s2.txt" using 1:2 w lines lt 1 lw 4 notitle

set title "140 mm distance"
unset key
set ylabel 'Phase (in radian)' font 'Helvetica,14' offset 2
set xlabel 'Time (in s)' font 'Helvetica,14' offset 2
set xtics auto
set ytics auto
plot "tag2_140_s2.txt" using 1:2 w lines lt 1 lw 4 notitle

unset multiplot

