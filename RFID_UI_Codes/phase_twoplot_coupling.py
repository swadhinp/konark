import sys
import os
import numpy as np

if len(sys.argv) < 3:
    print("python <py_file> <no_filter_phase_file> <phase_file>")
    sys.exit(-1)

fhandle = open( sys.argv[1], 'r' )
flines1 = fhandle.readlines()
fhandle.close()


fhandle = open( sys.argv[2], 'r' )
flines2 = fhandle.readlines()
fhandle.close()

start_flag = 0
start_time = 0.0

file1 = open("nofilter.dat", 'w')
file2 = open("filter.dat", 'w')

tstamp_arr = []
phase_arr = []

for line in flines1:
    
    l_lst = line.strip().split(',')
    
    epc = str(l_lst[0])
    tstamp = float(l_lst[3])
    phase = float(l_lst[9])

    if epc == "60000000000000000000AA00":
        if start_flag == 0:
            start_time = tstamp
            start_flag = 1

        l_str = str((tstamp-start_time)/1000000.0) + " " + str(phase) + "\n"
        #file1.write(l_str)
        tstamp_arr.append(str((tstamp-start_time)/1000000.0))
        phase_arr.append(phase)

phase_arr = np.unwrap( np.array( phase_arr ), discont=np.pi )

for i in range( len(phase_arr) ):
    l_str = str(tstamp_arr[i]) + " " + str(phase_arr[i]) + "\n"
    file1.write(l_str)

file1.close()

start_flag1 = 0
start_time1 = 0.0

tstamp_arr1 = []
phase_arr1 = []

for line in flines2:
    
    l_lst = line.strip().split(',')
    
    epc = str(l_lst[0])
    tstamp = float(l_lst[3])
    phase = float(l_lst[9])

    if epc == "70000000000000000000AA00":
        if start_flag1 == 0:
            start_time1 = tstamp
            start_flag1 = 1

        l_str = str((tstamp-start_time1)/1000000.0) + " " + str(phase) + "\n"
        #file2.write(l_str)
        tstamp_arr1.append(str((tstamp-start_time1)/1000000.0))
        phase_arr1.append(phase)

phase_arr1 = np.unwrap( np.array( phase_arr1 ), discont=np.pi )

for i in range( len(phase_arr1) ):
    l_str = str(tstamp_arr1[i]) + " " + str(phase_arr1[i]) + "\n"
    file2.write(l_str)

file2.close()


