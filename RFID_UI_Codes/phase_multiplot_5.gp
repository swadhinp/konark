set terminal postscript eps color size 6.0in,4.5in font 'Helvetica,14'
set output 'phase_multiplot_5.eps'

set multiplot layout 5, 1 title "Phase Trend" font ",14"

set tmargin 2

set title "Tag Left"
unset key
set ylabel 'Phase' font 'Helvetica,14' offset 2
set xlabel 'Samples' font 'Helvetica,14' offset 2
#set yrange [-6:6]
set xtics auto
set ytics auto
plot "phase_multiplot_5.dat" using 1:2 w lines lt 1 lw 4 notitle

#
set title "Tag Right"
unset key
set ylabel 'Phase' font 'Helvetica,14' offset 2
set xlabel 'Samples' font 'Helvetica,14' offset 2
#set yrange [-6:6]
set xtics auto
set ytics auto
plot "phase_multiplot_5.dat" using 1:3 w lines lt 1 lw 4 notitle


#
set title "Top Tag"
set ylabel 'Phase' font 'Helvetica,14' offset 2
set xlabel 'Samples' font 'Helvetica,14' offset 2
#set yrange [-6:6]
set xtics auto
set ytics auto
plot "phase_multiplot_5.dat" using 1:4 w lines lt 1 lw 4 notitle


#
set title "Medium Tag"
set ylabel 'Phase' font 'Helvetica,14' offset 2
set xlabel 'Samples' font 'Helvetica,14' offset 2
#set yrange [-6:6]
set xtics auto
set ytics auto
plot "phase_multiplot_5.dat" using 1:5 w lines lt 1 lw 4 notitle

#
set title "Bottom Tag"
set ylabel 'Phase' font 'Helvetica,14' offset 2
set xlabel 'Samples' font 'Helvetica,14' offset 2
#set yrange [-6:6]
set xtics auto
set ytics auto
plot "phase_multiplot_5.dat" using 1:6 w lines lt 1 lw 4 notitle

#
unset multiplot

