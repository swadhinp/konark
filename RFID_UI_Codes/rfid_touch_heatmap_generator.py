
import sys
import numpy as np
if len(sys.argv) < 5:
    print "python <pyhton_file> <base_data_file> <another_data_file> <tag_array_file> <output_file>"
    sys.exit(-1)


fhandle = open(sys.argv[1], 'r')
flines = fhandle.readlines()
fhandle.close()

fhandle = open(sys.argv[2], 'r')
flines1 = fhandle.readlines()
fhandle.close()

fhandle = open(sys.argv[3], 'r')
tlines = fhandle.readlines()
fhandle.close()

tag_locn_dict = {}


for line in tlines:

    l_lst = line.strip().split(':')
    tag_locn_dict[str(l_lst[0])] = [ int(l_lst[1]), int(l_lst[2]) ]

#print tag_locn_dict

epc_phase_dict = {}

for line in flines:

    l_lst = line.strip().split(',')

    epc = str(l_lst[0])
    tstamp = float(l_lst[3])
    phase = float(l_lst[9])

    #iprint tstamp, epc, phase

    if epc in tag_locn_dict:

        if epc not in epc_phase_dict:
            #epc_phase_dict[epc] = [tstamp, phase]
            epc_phase_dict[epc] = [phase]
        else:
            #epc_phase_dict[epc].append([tstamp, phase])
            epc_phase_dict[epc].append(phase)

#print epc_phase_dict.keys()

epc_phase_unwrap_dict = {}

for epc in epc_phase_dict:
    
    diff_arr = np.array(epc_phase_dict[epc])
    epc_phase_unwrap_dict[epc] = np.unwrap( np.array( diff_arr), discont=np.pi )


#print len(epc_phase_unwrap_dict.keys())

epc_phase_dict1 = {}


for line in flines1:

    l_lst = line.strip().split(',')

    epc = str(l_lst[0])
    tstamp = float(l_lst[3])
    phase = float(l_lst[9])

    #iprint tstamp, epc, phase

    if epc in tag_locn_dict:
        if epc not in epc_phase_dict1:
            #epc_phase_dict[epc] = [tstamp, phase]
            epc_phase_dict1[epc] = [phase]
        else:
            #epc_phase_dict[epc].append([tstamp, phase])
            epc_phase_dict1[epc].append(phase)

#print epc_phase_dict1.keys()

epc_phase_unwrap_dict1 = {}

for epc in epc_phase_dict1:
    
    diff_arr = np.array(epc_phase_dict1[epc])
    epc_phase_unwrap_dict1[epc] = np.unwrap( np.array(diff_arr), discont=np.pi )


for epc in epc_phase_dict:
    if epc not in epc_phase_dict1:
    
        print epc + " Not in dict1 but in dict"
        print tag_locn_dict[epc]

for epc in epc_phase_dict1:
    if epc not in epc_phase_dict:
        print epc + " Not in dict but in dict1"
        print tag_locn_dict[epc]

#print len(epc_phase_unwrap_dict1.keys())

num_rows = 16
num_cols = 1
final_heat_map = []

for i in range(num_rows):
    final_heat_map.append([0.0])

for epc in tag_locn_dict:

    row_num = int(tag_locn_dict[epc][0])
    col_num = int(tag_locn_dict[epc][1])
    

    val = 0.0

    if epc in epc_phase_unwrap_dict1 and epc in epc_phase_unwrap_dict :
        
        a = [ np.mean(epc_phase_unwrap_dict1[epc]), np.mean(epc_phase_unwrap_dict[epc]) ]
        a1 = np.unwrap( np.array(a) )
        val = abs(float(a1[0]) - float(a1[1]))
        #print row_num, col_num, a[0], a[1], a1[0], a1[1], val
        #print epc, row_num, col_num
        #print epc_phase_dict1[epc]
        #print "----------------------------\n"
        #print epc_phase_dict[epc]
        #print a, a1
        
    else:
        if epc in epc_phase_unwrap_dict1:
            val = 0.0
        if epc in epc_phase_unwrap_dict:
            val = 0.0

    if val > np.pi:
        val = 0.0

    #print epc, row_num, col_num, val

    final_heat_map[row_num-1][col_num-1] = val

fhandle = open(sys.argv[4], 'w')

for i in range(num_rows):
    for j in range(num_cols):
        print i+1, j+1, final_heat_map[i][j]
        fhandle.write( str(i) + " " + str(j) + " " + str(final_heat_map[i][j]) + "\n")

fhandle.close()
