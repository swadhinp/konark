#This code is for single tag tracking (Online and DeltaP)
from __future__ import division, print_function
import scipy.signal
from detect_cusum import detect_cusum
import sys
import time
import math
import numpy as np
from scipy import stats
import threading
import signal
import matplotlib.pyplot as plt
from detect_peaks import detect_peaks
from scipy.interpolate import interp1d
from scipy import interpolate
import warnings

warnings.filterwarnings(action="ignore", module="scipy", message="^internal gelsd")

if len(sys.argv) < 6:
    print ("python <file.py> <phase_calibration_file> <calibration_loc_file> <sample_no> <test_data_file> <test_loc_file>")
    sys.exit(-1)

#Basic Info and Global Variables

fhandle = open( sys.argv[1], 'r' )
loglines = fhandle.readlines()
fhandle.close()

fhandle = open(sys.argv[2], 'r')
flines = fhandle.readlines()
fhandle.close()

sno = sys.argv[3]

#print ("Start \n")

fhandle_in = open(sys.argv[4], 'r')
data_lines = fhandle_in.readlines()
fhandle_in.close()

fhandle_in = open(sys.argv[5], 'r')
camera_lines = fhandle_in.readlines()
fhandle_in.close()


window_size = 51
threshold = 1.2
#threshold = 3.0
sample_time_sec = 0.7
tag_dimn_in_mm = 85
delta_phase_change = 1.5


phase_arr = []
camera_arr = []
tstamp_arr_phase_tag = []
tstamp_arr_camera = []
tstamp_arr = []
camera_tstamp_arr = []

phase_arr_test = []
camera_arr_test = []
tstamp_arr_phase_tag_test = []
tstamp_arr_camera_test = []
tstamp_arr_test = []
camera_tstamp_arr_test = []

def file_read():

    global phase_arr
    global camera_arr
    global tstamp_arr_phase_tag
    global tstamp_arr_camera
    global tstamp_arr
    global camera_tstamp_arr

    global phase_arr_test
    global camera_arr_test
    global tstamp_arr_phase_tag_test
    global tstamp_arr_camera_test
    global tstamp_arr_test
    global camera_tstamp_arr_test

    #Calibration File Read
    for line in loglines:
        l_lst = line.strip().split(',')
        
        epc = str(l_lst[0])
        tstamp = float(l_lst[3])
        phase = float(l_lst[9])

        phase_arr.append( phase )
        tstamp_arr.append( tstamp )


    for i in range(len(tstamp_arr)):
        tstamp_arr_phase_tag.append( (tstamp_arr[i] - tstamp_arr[0])*1.0/1000000.0 )

    phase_arr = scipy.signal.savgol_filter( np.unwrap( np.array( phase_arr ), discont=np.pi ), window_size, 2 )
    #phase_arr = scipy.signal.medfilt( np.unwrap( np.array( phase_arr ), discont=np.pi ), window_size)


    '''
    graph2, = plt.plot(tstamp_arr_phase_tag, phase_arr, linestyle='-', marker='x', linewidth=3, markersize=6)

    font = {'weight' : 'bold',
            'size'   : 18 }

    plt.rc('font', **font)
    axes = plt.gca()
    #axes.set_xlim([0,5])
    #axes.set_ylim([1,8])


    plt.xlabel('Time')
    plt.ylabel('Phase')
    plt.grid(True)
    #plt.savefig( "phase_change_onetag_interp" + str(sno) + ".png" )
    plt.savefig( "phase_change_multitag" + str(sno) + ".pdf" )
    plt.cla()
    plt.clf()
    '''

    for line in flines:

        l_lst = line.strip().split(':')

        tstamp = float(l_lst[0])
        xval = float(l_lst[1])
        yval = float(l_lst[2])

        camera_arr.append( yval )
        camera_tstamp_arr.append( tstamp )
    
    #Camera X filter
    camera_arr = scipy.signal.savgol_filter( np.array( camera_arr ), 9, 2 )
    
    for i in range(len(camera_tstamp_arr)):
        tstamp_arr_camera.append( (camera_tstamp_arr[i] - camera_tstamp_arr[0])*1.0 )


    #Test file read

    for line in data_lines:
        l_lst = line.strip().split(',')
        
        epc = str(l_lst[0])
        tstamp = float(l_lst[3])
        phase = float(l_lst[9])

        phase_arr_test.append( phase )
        tstamp_arr_test.append( tstamp )


    for i in range(len(tstamp_arr_test)):
        tstamp_arr_phase_tag_test.append( (tstamp_arr_test[i] - tstamp_arr_test[0])*1.0/1000000.0 )

    phase_arr_test = scipy.signal.savgol_filter( np.unwrap( np.array( phase_arr_test ), discont=np.pi ), window_size, 2 )
    #phase_arr = scipy.signal.medfilt( np.unwrap( np.array( phase_arr ), discont=np.pi ), window_size)


    for line in camera_lines:

        l_lst = line.strip().split(':')

        tstamp = float(l_lst[0])
        xval = float(l_lst[1])
        yval = float(l_lst[2])

        camera_arr_test.append( yval )
        camera_tstamp_arr_test.append( tstamp )
    
    #Camera X filter
    camera_arr_test = scipy.signal.savgol_filter( np.array( camera_arr_test ), 9, 2 )
    
    for i in range(len(camera_tstamp_arr_test)):
        tstamp_arr_camera_test.append( (camera_tstamp_arr_test[i] - camera_tstamp_arr_test[0])*1.0 )



def signal_handler(*args):
    print ("Exiting after getting an interrupt \n")
    sys.exit(-1)

camera_loc_arr = []
tag_phase_arr = []
tag_length_pixel_ratio = 0.0

def phase_calibrate():

    global camera_loc_arr
    global tag_phase_arr
    global tag_length_pixel_ratio

    #Interpolate to make the phase_arr and camera_arr in same length
    #Pixel to mm conversion
    
    start_value = camera_arr[0]
    tag_length_pixel_ratio = (tag_dimn_in_mm*1.0)/abs(float(camera_arr[-1]-camera_arr[0]))*1.0

    for i in range(len(camera_arr)):
        camera_arr[i] = (camera_arr[i] - start_value)*tag_length_pixel_ratio


    #Creating Interpolation Functions
    #print(len(tstamp_arr_phase_tag), len(phase_arr))
    f_phasevel_linear = interp1d(tstamp_arr_phase_tag, phase_arr, kind='linear')
    f_cameravel_linear = interp1d(tstamp_arr_camera, camera_arr, kind='linear')

    len_reqd = max( len(tstamp_arr_phase_tag), len(tstamp_arr_camera) )
    timr_for_interp = min(tstamp_arr_phase_tag[-1], tstamp_arr_camera[-1])

    #Getting same length interpolation arrays for calculation
    time_interp_arr = np.linspace(0, timr_for_interp, num = int(len_reqd*2.2), endpoint=True)
    phase_interp_arr = f_phasevel_linear(time_interp_arr)
    camera_interp_arr = f_cameravel_linear(time_interp_arr)

    '''
    #Plot graphs after and before interpolation

    graph1, = plt.plot(time_interp_arr, phase_interp_arr, label="Interpolated Phase Values", linestyle='--', marker='o', linewidth=3, markersize=6)
    graph2, = plt.plot(tstamp_arr_phase_tag, phase_arr, label="Actual Phase Values", linestyle='-', marker='x', linewidth=3, markersize=6)

    plt.legend(handles=[graph1, graph2], loc=4)
    plt.xlabel('Time')
    plt.ylabel('Phase')
    plt.grid(True)
    #plt.savefig( "phase_change_onetag_interp" + str(sno) + ".png" )
    plt.savefig( "phase_change_multitag" + str(sno) + ".png" )
    plt.cla()
    plt.clf()

    graph3, = plt.plot(time_interp_arr, camera_interp_arr, label="Interpolated Camera Values", linestyle='--', marker='o', linewidth=3, markersize=6)
    graph4, = plt.plot(tstamp_arr_camera, camera_arr, label="Actual Camera Values", linestyle='-', marker='x', linewidth=3, markersize=6)

    plt.legend(handles=[graph3, graph4], loc=4)
    plt.xlabel('Time')
    plt.ylabel('Camera X Values')
    plt.grid(True)
    #plt.savefig( "camera_xval_onetag_interp" + str(sno) + ".png" )
    plt.savefig( "phase_change_multitag" + str(sno) + ".png" )
    plt.cla()
    plt.clf()
    '''

    #Phase Calibration

    cur_indx = 0
    start_time = time_interp_arr[0]
    
    while cur_indx < len(time_interp_arr):

        count = 0
        X_arr = []
        Y_arr = []
        Z_arr = []

        interval = 0.0

        start_phase = phase_interp_arr[cur_indx]
        start_time = time_interp_arr[cur_indx]
        phase_diff = 0.0

        #print(cur_indx)

        while phase_diff < delta_phase_change :

            phase_diff = abs(phase_interp_arr[cur_indx] - start_phase)
            interval = float(time_interp_arr[cur_indx] - start_time)

            #X_arr.append(count)
            X_arr.append(interval)
            Y_arr.append(phase_interp_arr[cur_indx])
            Z_arr.append(camera_interp_arr[cur_indx])
            
            count += 1
            cur_indx += 1

            if cur_indx >= len(time_interp_arr):
                break

        if cur_indx < len(time_interp_arr):
            start_time = float(time_interp_arr[cur_indx])

        
        #print(phase_diff)
        camera_loc_arr.append( Z_arr[-1] - Z_arr[0] )
        tag_phase_arr.append( phase_diff )
        #accel_slope, intercept, r_value, p_value, std_err = stats.linregress(X_arr, Y_arr)
        #print(np.mean(np.gradient(Y_arr)))
        #distance = init_speed*sample_time_sec*1.0 + 0.5*abs(accel_slope)*sample_time_sec*sample_time_sec


if __name__ == "__main__":

    #signal.signal(signal.SIGINT, signal_handler)

    try:
    
        #global camera_arr
        
        file_read()
        phase_calibrate()
        #print( len(camera_loc_arr), len(tag_phase_arr) )

        #Preprocess and interpolate the test data series

        start_value = camera_arr_test[0]
        for i in range(len(camera_arr_test)):
            camera_arr_test[i] = (camera_arr_test[i] - start_value)*tag_length_pixel_ratio
        
        f_phasevel_test_linear = interp1d(tstamp_arr_phase_tag_test, phase_arr_test , kind='linear')
        f_cameravel_test_linear = interp1d(tstamp_arr_camera_test, camera_arr_test, kind='linear')

        len_reqd_test = max( len(tstamp_arr_phase_tag_test), len(tstamp_arr_camera_test) )
        time_for_interp_test = min(tstamp_arr_phase_tag_test[-1], tstamp_arr_camera_test[-1])
        time_interp_arr_test = np.linspace(0, time_for_interp_test, num = int(len_reqd_test*3.2), endpoint=True)

        rfid = f_phasevel_test_linear(time_interp_arr_test)
        camera = f_cameravel_test_linear(time_interp_arr_test)

        
        camera_result_loc_arr = []
        rfid_result_loc_arr = []

        start_loc = camera[0]
        step_arr = []

        step_arr.append(0)
        camera_result_loc_arr.append(start_loc)
        rfid_result_loc_arr.append(start_loc)


        cur_idx = 0
        calib_count = 0

        #Comparing the test accuracy

        while cur_idx < len(time_interp_arr_test):

            phase_diff = 0.0
            start_phase = rfid[cur_idx]

            while phase_diff < delta_phase_change :

                phase_diff = abs(rfid[cur_idx] - start_phase)

                if cur_idx == len(time_interp_arr_test)-1:
                    break

                cur_idx += 1


            if calib_count == len(camera_loc_arr):
                break

            step_arr.append(calib_count+1)

            #print("Calib Count : ", calib_count)
            #print( "Calib : ", phase_diff)
            
            predicted_locn = camera_loc_arr[calib_count] + rfid_result_loc_arr[calib_count]
            camera_locn = camera[cur_idx] - start_loc

            camera_result_loc_arr.append(abs(camera_locn))
            rfid_result_loc_arr.append(predicted_locn)

            if cur_idx == len(time_interp_arr_test)-1:
                break

            calib_count += 1

        for i in range(len(rfid_result_loc_arr)):
            rfid_result_loc_arr[i] = abs(rfid_result_loc_arr[i])

        #print( calib_count, len(camera_loc_arr), len(tag_phase_arr) )

        fname1 = "rfid_onetag_predict" + str(sno) + ".tag"
        fhandle = open( fname1, 'w')

        for i in range(len(step_arr)):
            fhandle.write( str(step_arr[i]) + " " + str(rfid_result_loc_arr[i]) + " " + str(camera_result_loc_arr[i]) + "\n")

        fhandle.close()

        
        line1, = plt.plot(step_arr, rfid_result_loc_arr, label="RFID Predicted", linestyle='--', marker='o', linewidth=3, markersize=6)
        line2, = plt.plot(step_arr, camera_result_loc_arr, label="Camera Actual", linestyle='-', marker='x', linewidth=3, markersize=6)
        plt.legend(handles=[line1, line2], loc=4)
        plt.xlabel("Steps")
        plt.ylabel("On Tag Location (in mm)")
        #plt.savefig( "rfid_multitag_predict" + str(sno) + ".png" )
        plt.savefig( "rfid_onetag_predict" + str(sno) + ".pdf" )
        plt.cla()
        plt.clf()

        r = np.corrcoef( camera_result_loc_arr, rfid_result_loc_arr )
        #print(r)

        dist_arr = []

        for i in range( len(rfid_result_loc_arr) ):
            dist_arr.append( abs(camera_result_loc_arr[i] - rfid_result_loc_arr[i]) )


        sorted_data = np.sort(np.array(dist_arr))
        yvals = np.arange(len(sorted_data))/float(len(sorted_data))
        
        fname2 = "cdf_rfid_onetag_predict" + str(sno) + ".tag"
        fhandle = open( fname2, 'w')

        for i in range( len(yvals) ):
            fhandle.write( str(sorted_data[i]) + " " + str(yvals[i]) + "\n")
        
        fhandle.close()


        plt.xlabel("Localisation prediction error (in mm)")
        plt.ylabel("CDF")
        plt.plot(sorted_data, yvals)
        plt.savefig( "cdf_rfid_onetag_predict" + str(sno) + ".pdf" )
        #plt.savefig( "cdf_rfid_multitag_predict" + str(sno) + ".png" )
        plt.cla()
        plt.clf()
        
        '''
        plt.ylabel("Localisation prediction error (in mm)")
        plt.xlabel("Time")
        plt.plot(dist_arr)
        #plt.savefig( "pos_rfid_onetag_predict" + str(sno) + ".png" )
        plt.savefig( "pos_rfid_multitag_predict" + str(sno) + ".png" )
        plt.cla()
        plt.clf()
        '''

        #print("Median Error: " , np.median( np.array(dist_arr) ))
        print(np.median( np.array(dist_arr) ))


    except KeyboardInterrupt:
        # do nothing here
        print ("Exiting after getting an interrupt \n")
        sys.exit(-1)
