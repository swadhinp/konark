import scipy.signal
from detect_cusum import detect_cusum
import sys
import time
import math
import numpy as np
from scipy import stats
import gi
import threading
import signal

gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk, GLib, GObject


if len(sys.argv) < 5:
    print ("python <file.py> <data_file> <step_movement_speed_in_sec> <starting_tag_indx> <starting_tag_pos>")
    sys.exit(-1)

#Basic Info and Global Variables

threshold = 1.8
side_threshold = 0.6
window_size = 15
count = 0

delta_time_in_sec = float(sys.argv[2])
starting_tag = str(sys.argv[3])
starting_pos = str(sys.argv[4])

#Configuration Info
focus_epc_arr = [ '10000000000000000000AA00', '20000000000000000000AA00', '30000000000000000000AA00', '40000000000000000000AA00', '50000000000000000000AA00', '60000000000000000000AA00', '70000000000000000000AA00']

focus_epc_dict = {}
focus_epc_dict['10000000000000000000AA00'] = "R1"
focus_epc_dict['20000000000000000000AA00'] = "R2"
focus_epc_dict['30000000000000000000AA00'] = "R3"
focus_epc_dict['40000000000000000000AA00'] = "R4"
focus_epc_dict['50000000000000000000AA00'] = "R5"
focus_epc_dict['60000000000000000000AA00'] = "R6"
focus_epc_dict['70000000000000000000AA00'] = "R7"

focus_epc_dict_rev = {}
focus_epc_dict_rev["R1"] = '10000000000000000000AA00'
focus_epc_dict_rev["R2"] = '20000000000000000000AA00'
focus_epc_dict_rev["R3"] = '30000000000000000000AA00'
focus_epc_dict_rev["R4"] = '40000000000000000000AA00'
focus_epc_dict_rev["R5"] = '50000000000000000000AA00'
focus_epc_dict_rev["R6"] = '60000000000000000000AA00'
focus_epc_dict_rev["R7"] = '70000000000000000000AA00'

'''
#Callibrated Phase
ref_tag_dict = {}
ref_tag_dict["R1"] = { 'L' : 3.96, 'R' : 1.9  }
ref_tag_dict["R2"] = { 'L' : 4.59, 'R' : 1.50  }
ref_tag_dict["R3"] = { 'L' : 4.45, 'R' : 2.59 }
ref_tag_dict["R4"] = { 'L' : 4.81, 'R' : 5.11  }
ref_tag_dict["R5"] = { 'L' : 5.38, 'R' : 5.10  }
ref_tag_dict["R6"] = { 'L' : 1.56, 'R' : 5.45 }
ref_tag_dict["R7"] = { 'L' : 1.99, 'R' : 0.89 }
'''

ref_tag_dict = {}
#With Finger
ref_tag_dict["R1"] = { 'L' : 2.71, 'R' : 4.10  }
ref_tag_dict["R2"] = { 'L' : 3.55, 'R' : 4.17  }
ref_tag_dict["R3"] = { 'L' : 3.17, 'R' : 4.35 }
ref_tag_dict["R4"] = { 'L' : 2.17, 'R' : 1.95  }
ref_tag_dict["R5"] = { 'L' : 1.79, 'R' : 2.65  }
ref_tag_dict["R6"] = { 'L' : 2.65, 'R' : 4.55 }
ref_tag_dict["R7"] = { 'L' : 0.90, 'R' : 5.85 }

start_flag = 0

current_tag = starting_tag
current_pos = starting_pos
builder = 0

path_arr = [ str(current_tag)+ "-" + str(current_pos) ]

def follow(thefile):
    thefile.seek(0,2)
    while True:
        line = thefile.readline()
        if not line:
            time.sleep(0.1)
            #sys.exit(-1)
            continue
        yield line

#GUI Class
startFlag = 0
lastid = str(starting_tag)+str(starting_pos)

def gui_render():

    builder = Gtk.Builder()
    builder.add_from_file("RFTrackR_final.glade")
    window = builder.get_object("RFTrackrWin")
    window.connect("delete-event", Gtk.main_quit)
    window.set_title("RFTrackR GUI")

    def update_switch(sid):
        
        global startFlag
        global lastid

        #print( sid + " : " + lastid)

        if startFlag == 0:

            startFlag = 1
            lastid = str(sid)
            switch1 = builder.get_object(sid)
            switch1.set_active(True)

        else:

            if sid != lastid:
                switch1 = builder.get_object(lastid)
                switch1.set_active(False)
                switch2 = builder.get_object(sid)
                switch2.set_active(True)
                lastid = sid

        return False

    #def switch_change(threadName, run_event):
    def switch_change():
        #while run_event.is_set():
        while True:
            sid = str(current_tag) + str(current_pos)
            GLib.idle_add(update_switch, sid)
            time.sleep(1.0)

    window.show_all()
    #run_event = threading.Event()
    #run_event.set()

    #Touch Predictor Thread
    thread1 = threading.Thread(target=touch_predict)
    thread1.daemon = True
    thread1.start()
    
    time.sleep(0.1)

    #GUI Switch Change Thread
    thread2 = threading.Thread(target=switch_change)
    thread2.daemon = True
    thread2.start()


#def touch_predict(threadName, run_event):
def touch_predict():

    #while run_event.is_set():
    try:
        epc_phase_dict = {}
        epc_time_dict = {}
 
        global path_arr
        global current_pos 
        global current_tag
        global start_flag

        logfile = open(sys.argv[1], "r")
        loglines = follow(logfile)
    
        print ("Start \n")

        print (str(current_tag) + "-" + str(current_pos) + "\n")
    
        for line in loglines:
            l_lst = line.strip().split(',')
        
            epc = str(l_lst[0])
            tstamp = float(l_lst[3])
            phase = float(l_lst[9])
            #print epc
        
            if start_flag == 0:
                start_time = tstamp
                start_flag = 1


            cur_time = tstamp

            diff = float(cur_time - start_time)/1000000.0

            if diff < delta_time_in_sec:
            
                #print epc
                if epc in focus_epc_arr: #Only focus on filtered tags
                
                    #print "mtch"
                    if epc not in epc_phase_dict: 
                        epc_phase_dict[epc] = [phase]
                        epc_time_dict[epc] = [tstamp]
                    else:
                        epc_phase_dict[epc].append(phase)
                        epc_time_dict[epc].append(tstamp)

            else:
                #print epc_phase_dict.keys()
                for epc in epc_phase_dict:
                    #epc_phase_dict[epc] = scipy.signal.savgol_filter( np.unwrap( np.array( epc_phase_dict[epc] ), discont=np.pi ), window_size, 2 )
                    epc_phase_dict[epc] = scipy.signal.medfilt( np.unwrap( np.array( epc_phase_dict[epc] ), discont=np.pi ), window_size)

            
                #Decide if it is middle tag or boundary tag or window selection

                top_tag_phase_arr = []
                bottom_tag_phase_arr = []

                if  focus_epc_dict_rev[current_tag] not in epc_phase_dict:
                    print (str(current_tag) + "-" + str(current_pos) + "\n")
                    path_arr.append( str(current_tag) + "-" + str(current_pos) )

                    epc_phase_dict = {}
                    epc_time_dict = {}
                    start_time = cur_time
                    continue
                else:
                    current_tag_phase_arr = epc_phase_dict[ focus_epc_dict_rev[current_tag] ]


                if current_tag == "R1":
                    try:
                        bottom_tag_phase_arr = epc_phase_dict[ focus_epc_dict_rev["R2"] ]
                    except KeyError:
                        current_tag = "R2"
                        print (str(current_tag) + "-" + str(current_pos) + "\n")
                        path_arr.append( str(current_tag) + "-" + str(current_pos) )
                        epc_phase_dict = {}
                        epc_time_dict = {}
                        start_time = cur_time
                        continue
                    try:
                        top_tag_phase_arr = epc_phase_dict[ focus_epc_dict_rev["R3"] ] #Just for another extra boundary check
                    except KeyError:
                        current_tag = "R3"
                        print (str(current_tag) + "-" + str(current_pos) + "\n")
                        path_arr.append( str(current_tag) + "-" + str(current_pos) )
                        epc_phase_dict = {}
                        epc_time_dict = {}
                        start_time = cur_time
                        continue

                elif current_tag == "R7":
                    try:
                        bottom_tag_phase_arr = epc_phase_dict[ focus_epc_dict_rev["R5"] ]
                    except KeyError:
                        current_tag = "R5"
                        print (str(current_tag) + "-" + str(current_pos) + "\n")
                        path_arr.append( str(current_tag) + "-" + str(current_pos) )
                        epc_phase_dict = {}
                        epc_time_dict = {}
                        start_time = cur_time
                        continue
                    try:
                        top_tag_phase_arr = epc_phase_dict[focus_epc_dict_rev["R6"]] #Just for another extra boundary check
                    except KeyError:
                        current_tag = "R6"
                        print (str(current_tag) + "-" + str(current_pos) + "\n")
                        path_arr.append( str(current_tag) + "-" + str(current_pos) )
                        epc_phase_dict = {}
                        epc_time_dict = {}
                        start_time = cur_time
                        continue

                elif current_tag == "R2":
                    try:
                        top_tag_phase_arr = epc_phase_dict[ focus_epc_dict_rev["R1"] ]
                    except KeyError:
                        current_tag = "R1"
                        print (str(current_tag) + "-" + str(current_pos) + "\n")
                        path_arr.append( str(current_tag) + "-" + str(current_pos) )
                        epc_phase_dict = {}
                        epc_time_dict = {}
                        start_time = cur_time
                        continue
                    try:
                        bottom_tag_phase_arr = epc_phase_dict[ focus_epc_dict_rev["R3"] ]
                    except KeyError:
                        current_tag = "R3"
                        print (str(current_tag) + "-" + str(current_pos) + "\n")
                        path_arr.append( str(current_tag) + "-" + str(current_pos) )
                        epc_phase_dict = {}
                        epc_time_dict = {}
                        start_time = cur_time
                        continue
                elif current_tag == "R3":
                    try:
                        top_tag_phase_arr = epc_phase_dict[ focus_epc_dict_rev["R2"] ]
                    except KeyError:
                        current_tag = "R2"
                        print (str(current_tag) + "-" + str(current_pos) + "\n")
                        path_arr.append( str(current_tag) + "-" + str(current_pos) )
                        epc_phase_dict = {}
                        epc_time_dict = {}
                        start_time = cur_time
                        continue

                    try:
                        bottom_tag_phase_arr = epc_phase_dict[ focus_epc_dict_rev["R4"] ]
                    except KeyError:
                        current_tag = "R4"
                        print (str(current_tag) + "-" + str(current_pos) + "\n")
                        path_arr.append( str(current_tag) + "-" + str(current_pos) )
                        epc_phase_dict = {}
                        epc_time_dict = {}
                        start_time = cur_time
                        continue
                elif current_tag == "R4":
                    try:
                        top_tag_phase_arr = epc_phase_dict[ focus_epc_dict_rev["R3"] ]
                        
                    except KeyError:
                        current_tag = "R3"
                        print (str(current_tag) + "-" + str(current_pos) + "\n")
                        path_arr.append( str(current_tag) + "-" + str(current_pos) )
                        epc_phase_dict = {}
                        epc_time_dict = {}
                        start_time = cur_time
                        continue
                    try:
                        bottom_tag_phase_arr = epc_phase_dict[ focus_epc_dict_rev["R5"] ]
                    except KeyError:
                        current_tag = "R5"
                        print (str(current_tag) + "-" + str(current_pos) + "\n")
                        path_arr.append( str(current_tag) + "-" + str(current_pos) )
                        epc_phase_dict = {}
                        epc_time_dict = {}
                        start_time = cur_time
                        continue
                elif current_tag == "R5":
                    try:
                        top_tag_phase_arr = epc_phase_dict[ focus_epc_dict_rev["R4"] ]
                    except KeyError:
                        current_tag = "R4"
                        print (str(current_tag) + "-" + str(current_pos) + "\n")
                        path_arr.append( str(current_tag) + "-" + str(current_pos) )
                        epc_phase_dict = {}
                        epc_time_dict = {}
                        start_time = cur_time
                        continue
                    try:
                        bottom_tag_phase_arr = epc_phase_dict[ focus_epc_dict_rev["R6"] ]
                    except KeyError:
                        current_tag = "R6"
                        print (str(current_tag) + "-" + str(current_pos) + "\n")
                        path_arr.append( str(current_tag) + "-" + str(current_pos) )
                        epc_phase_dict = {}
                        epc_time_dict = {}
                        start_time = cur_time
                        continue

                elif current_tag == "R6":
                    try:
                        top_tag_phase_arr = epc_phase_dict[ focus_epc_dict_rev["R5"] ]
                    except KeyError:
                        current_tag = "R5"
                        print (str(current_tag) + "-" + str(current_pos) + "\n")
                        path_arr.append( str(current_tag) + "-" + str(current_pos) )
                        epc_phase_dict = {}
                        epc_time_dict = {}
                        start_time = cur_time
                        continue

                    try:
                        bottom_tag_phase_arr = epc_phase_dict[ focus_epc_dict_rev["R7"] ]
                    except KeyError:
                        current_tag = "R7"
                        print (str(current_tag) + "-" + str(current_pos) + "\n")
                        path_arr.append( str(current_tag) + "-" + str(current_pos) )
                        epc_phase_dict = {}
                        epc_time_dict = {}
                        start_time = cur_time
                        continue
            
                bamp = 0.0
                camp = 0.0
                tamp = 0.0

                bslope = 0.0
                cslope = 0.0
                tslope = 0.0

                bamparr = []
                camparr = []
                tamparr = []
            
                if len(bottom_tag_phase_arr) > 1:
                    ba, bai, baf, bamparr = detect_cusum(bottom_tag_phase_arr, threshold, 0.0, True, False)
                    if len(bamparr) > 0:
                        for i in range(len(bamparr)):
                            bamparr[i] = bamparr[i]%(2*np.pi)

                        bamp = np.max( np.absolute( bamparr ) )
                
                    #X = []
                    #for i in range(len(bottom_tag_phase_arr)):
                     #  X.append(i+1)

                    #bslope, intercept, r_value, p_value, std_err = stats.linregress(X, bottom_tag_phase_arr)


                if len(current_tag_phase_arr) > 1:
                    ca, cai, caf, camparr = detect_cusum(current_tag_phase_arr, threshold, 0.0, True, False)
                    if len(camparr) > 0:
                        
                        for i in range(len(camparr)):
                            camparr[i] = camparr[i]%(2*np.pi)

                        camp = np.max( np.absolute( camparr ) )
                
                    X = []
                    for i in range(len(current_tag_phase_arr)):
                       X.append(i+1)

                    cslope, intercept, r_value, p_value, std_err = stats.linregress(X, current_tag_phase_arr)

                if len(top_tag_phase_arr) > 1:
                    ta, tai, taf, tamparr = detect_cusum(top_tag_phase_arr, threshold, 0.0, True, False)
                    if len(tamparr) > 0:
                        for i in range(len(tamparr)):
                            tamparr[i] = tamparr[i]%(2*np.pi)
                        
                        tamp = np.max( np.absolute( tamparr ) )
                
                    #X = []
                    #for i in range(len(top_tag_phase_arr)):
                     #  X.append(i+1)

                    #tslope, intercept, r_value, p_value, std_err = stats.linregress(X, top_tag_phase_arr)
            
                print (tamp)
                print (camp)
                print (bamp)

                if current_pos == "R" or current_pos == "L":
               
                    if bamp > threshold and camp >= threshold:
                        
                        if current_tag == "R1":
                            current_tag = "R2"
                        elif current_tag == "R2":
                            current_tag = "R3"
                        elif current_tag == "R3":
                            current_tag = "R4"
                        elif current_tag == "R4":
                            current_tag = "R5"
                        elif current_tag == "R5":
                            current_tag = "R6"
                        elif current_tag == "R6":
                            current_tag = "R7"

                    elif tamp > threshold and camp >= threshold:
                        
                        if current_tag == "R2":
                            current_tag = "R1"
                        elif current_tag == "R3":
                            current_tag = "R2"
                        elif current_tag == "R4":
                            current_tag = "R3"
                        elif current_tag == "R5":
                            current_tag = "R4"
                        elif current_tag == "R6":
                            current_tag = "R5"
                        elif current_tag == "R7":
                            current_tag = "R6"

                    elif bamp > threshold:
                    
                        if current_tag == "R1":
                            current_tag = "R2"
                        elif current_tag == "R2":
                            current_tag = "R3"
                        elif current_tag == "R3":
                            current_tag = "R4"
                        elif current_tag == "R4":
                            current_tag = "R5"
                        elif current_tag == "R5":
                            current_tag = "R6"
                        elif current_tag == "R6":
                            current_tag = "R7"
                
                    elif tamp > threshold:
                    
                        if current_tag == "R2":
                            current_tag = "R1"
                        elif current_tag == "R3":
                            current_tag = "R2"
                        elif current_tag == "R4":
                            current_tag = "R3"
                        elif current_tag == "R5":
                            current_tag = "R4"
                        elif current_tag == "R6":
                            current_tag = "R5"
                        elif current_tag == "R7":
                            current_tag = "R6"

                    elif camp >= threshold :
                    
                        #Slope detection to be added
                        if cslope > 0.0:
                            current_pos = "M"

                    else:
                        print("No Change " + str(cslope))
            
                elif current_pos == "M":
                
                    if bamp > threshold and camp >= threshold:
                        
                        if current_tag == "R1":
                            current_tag = "R2"
                        elif current_tag == "R2":
                            current_tag = "R3"
                        elif current_tag == "R3":
                            current_tag = "R4"
                        elif current_tag == "R4":
                            current_tag = "R5"
                        elif current_tag == "R5":
                            current_tag = "R6"
                        elif current_tag == "R6":
                            current_tag = "R7"
                    
                    elif bamp > threshold:
                        
                        if current_tag == "R1":
                            current_tag = "R2"
                        elif current_tag == "R2":
                            current_tag = "R3"
                        elif current_tag == "R3":
                            current_tag = "R4"
                        elif current_tag == "R4":
                            current_tag = "R5"
                        elif current_tag == "R5":
                            current_tag = "R6"
                        elif current_tag == "R6":
                            current_tag = "R7"
                
                    elif tamp > threshold and camp >= threshold:
                    
                        if current_tag == "R2":
                            current_tag = "R1"
                        elif current_tag == "R3":
                            current_tag = "R2"
                        elif current_tag == "R4":
                            current_tag = "R3"
                        elif current_tag == "R5":
                            current_tag = "R4"
                        elif current_tag == "R6":
                            current_tag = "R5"
                        elif current_tag == "R7":
                            current_tag = "R6"
                    
                    elif tamp > threshold:
                    
                        if current_tag == "R2":
                            current_tag = "R1"
                        elif current_tag == "R3":
                            current_tag = "R2"
                        elif current_tag == "R4":
                            current_tag = "R3"
                        elif current_tag == "R5":
                            current_tag = "R4"
                        elif current_tag == "R6":
                            current_tag = "R5"
                        elif current_tag == "R7":
                            current_tag = "R6"


                    elif camp >= threshold:
                    
                        cur_phase_arr = epc_phase_dict[ focus_epc_dict_rev[current_tag] ]
                        cur_phase_arr_interested = cur_phase_arr[len(cur_phase_arr)-10:len(cur_phase_arr)]
                        phase_val = np.mean(np.array(cur_phase_arr_interested))
                        
                        lval = ref_tag_dict[current_tag]['L']
                        rval = ref_tag_dict[current_tag]['R']

                        ldiff = abs(lval - phase_val)
                        rdiff = abs(rval - phase_val)

                        if ldiff < rdiff:
                            current_pos = 'R'
                        else:
                            current_pos = 'L'


                    else:
                        print("No Change " + str(cslope))

                        #Slope detection to be added
                        #other_tag_phase_arr = epc_phase_dict[ ref_tag_dict[current_tag] ]
                        #oa, oai, oaf, oamparr = detect_cusum(other_tag_phase_arr, side_threshold, 0.0, True, False)
            
                        #oamp = 0.0
                        #if len(oamparr) >1:
                         #   oamp = np.max( np.absolute( oamparr ) )

                        #X = []
                        #for i in range(len(other_tag_phase_arr)):
                            #X.append(i+1)

                        #oslope, intercept, r_value, p_value, std_err = stats.linregress(X, other_tag_phase_arr)

                        #if oamp > side_threshold :
                         #   current_pos = "L"
                        #else:
                            #if oslope < 0.0:
                         #   current_pos = "R"
                            #else:
                            #   current_pos = "L"

                
                else:
                    print ("Fatal Error")


                print (str(current_tag) + "-" + str(current_pos) + "\n")
                path_arr.append( str(current_tag) + "-" + str(current_pos) )

                bamp = 0.0
                camp = 0.0
                tamp = 0.0
                epc_phase_dict = {}
                epc_time_dict = {}
                start_time = cur_time
        #print line + "****"
    except KeyboardInterrupt:
        print ("Path Traced : \n")

        for i in range( len(path_arr) - 1):
            print (path_arr[i] + " =>", end=" ")

        print (path_arr[len(path_arr) - 1] + "\n")

        sys.exit(-1)

def check_change_for_correction():

    global epc_phase_dict


def signal_handler(*args):
    print ("Path Traced : \n")

    for i in range( len(path_arr) - 1):
        print (path_arr[i] + " =>", end=" ")

    print (path_arr[len(path_arr) - 1] + "\n")

    sys.exit(-1)

if __name__ == "__main__":

    #signal.signal(signal.SIGINT, signal.SIG_DFL)
    signal.signal(signal.SIGINT, signal_handler)
    try:
        
        GObject.threads_init()
        gui_render()
        Gtk.main()
        GObject.threads_leave()
        
        #touch_predict()

    except KeyboardInterrupt:
        # do nothing here
        print ("Path Traced : \n")

        for i in range( len(path_arr) - 1):
            print (path_arr[i] + " =>", end=" ")

        print (path_arr[len(path_arr) - 1] + "\n")

        sys.exit(-1)
