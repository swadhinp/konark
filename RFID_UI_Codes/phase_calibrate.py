import sys
import numpy as np


fhandle = open ( sys.argv[1], 'r')
flines = fhandle.readlines()
fhandle.close()

phase_arr = []

for lin in flines:

    l_lst = lin.strip().split(',')

    phase = float(l_lst[1])

    phase_arr.append(phase)

print( str(np.mean(np.array(phase_arr))) )
