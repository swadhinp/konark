set terminal postscript eps color size 6.0in,4.5in font 'Helvetica,14'
set output 'camera_xy.eps'

set tmargin 2

unset key
set ylabel 'Y' font 'Helvetica,14' offset 2
set xlabel 'Time' font 'Helvetica,14' offset 2
set xtics auto
set ytics auto
plot "camera_xy.dat" using 1:2 w lines lt 1 lw 4 notitle

