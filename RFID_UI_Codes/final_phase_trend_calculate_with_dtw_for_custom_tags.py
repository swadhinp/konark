#This code is for single tag tracking using DTW technique
from __future__ import division, print_function
import scipy.signal
from detect_cusum import detect_cusum
import sys
import time
import math
import numpy as np
from scipy import stats
import threading
import signal
import matplotlib.pyplot as plt
from detect_peaks import detect_peaks
from scipy.interpolate import interp1d
from scipy import interpolate
import warnings
from scipy.interpolate import InterpolatedUnivariateSpline
from numpy.polynomial import Polynomial
import itertools
from scipy.spatial.distance import euclidean
from fastdtw import fastdtw
from dtw import dtw
from itertools import islice
import datetime

def downsample_to_proportion(rows, proportion=1):
    return list(islice(rows, 0, len(rows), int(1/proportion)))

warnings.filterwarnings(action="ignore", module="scipy", message="^internal gelsd")

if len(sys.argv) < 5:
    print ("python <file.py> <phase_calibration_file> <sample_no> <test_data_file> <test_loc_file>")
    sys.exit(-1)

#Basic Info and Global Variables

fhandle = open( sys.argv[1], 'r' )
loglines = fhandle.readlines()
fhandle.close()

sno = sys.argv[2]

#print ("Start \n")

fhandle_in = open(sys.argv[3], 'r')
data_lines = fhandle_in.readlines()
fhandle_in.close()

fhandle_in = open(sys.argv[4], 'r')
camera_lines = fhandle_in.readlines()
fhandle_in.close()


window_size = 51
threshold = 1.2
#threshold = 3.0
sample_time_sec = 0.5
tag_dimn_in_mm = 120
delta_phase_change = 1.5


phase_arr = []
camera_arr_x = []
camera_arr_y = []
tstamp_arr = []

phase_arr_test = []
camera_arr_test_x = []
camera_arr_test_y = []
tstamp_arr_phase_tag_test = []
tstamp_arr_camera_test = []
tstamp_arr_test = []
camera_tstamp_arr_test = []

def file_read():

    global phase_arr
    global camera_arr_x
    global camera_arr_y

    global phase_arr_test
    global camera_arr_test_x
    global camera_arr_test_y
    global tstamp_arr_phase_tag_test
    global tstamp_arr_camera_test
    global tstamp_arr_test
    global camera_tstamp_arr_test

    #Calibration File Read
    for line in loglines:
        l_lst = line.strip().split(',')
        
        phase = float(l_lst[0])
        locx = float(l_lst[1])
        locy = float(l_lst[2])
        tstamp = float(l_lst[3])

        phase_arr.append( phase )
        camera_arr_x.append( locx )
        camera_arr_y.append( locy )
        tstamp_arr.append( tstamp )


    #Test file read

    for line in data_lines:
        l_lst = line.strip().split(',')
        
        epc = str(l_lst[0])
        tstamp = float(l_lst[3])
        phase = float(l_lst[9])

        phase_arr_test.append( phase )
        tstamp_arr_test.append( tstamp )


    for i in range(len(tstamp_arr_test)):
        tstamp_arr_phase_tag_test.append( (tstamp_arr_test[i] - tstamp_arr_test[0])*1.0/1000000.0 )

    phase_arr_test = scipy.signal.savgol_filter( np.unwrap( np.array( phase_arr_test ), discont=np.pi ), window_size, 2 )
    #phase_arr = scipy.signal.medfilt( np.unwrap( np.array( phase_arr ), discont=np.pi ), window_size)


    for line in camera_lines:

        l_lst = line.strip().split(':')

        tstamp = float(l_lst[0])
        xval = float(l_lst[1])
        yval = float(l_lst[2])

        camera_arr_test_x.append( xval )
        camera_arr_test_y.append( yval )
        camera_tstamp_arr_test.append( tstamp )
    
    #Camera X filter
    camera_arr_test_x = scipy.signal.savgol_filter( np.array( camera_arr_test_x ), 9, 2 )
    camera_arr_test_y = scipy.signal.savgol_filter( np.array( camera_arr_test_y ), 9, 2 )
    
    for i in range(len(camera_tstamp_arr_test)):
        tstamp_arr_camera_test.append( (camera_tstamp_arr_test[i] - camera_tstamp_arr_test[0])*1.0 )

    #p = Polynomial.fit( tstamp_arr_camera_test, camera_arr_test, 1 )
    #camera_arr_test = list(itertools.chain.from_iterable(p.linspace()))
    func_linear_camera_x = interp1d(tstamp_arr_camera_test, camera_arr_test_x, kind='linear', fill_value='extrapolate')
    camera_arr_test_x = func_linear_camera_x(tstamp_arr_phase_tag_test)
    func_linear_camera_y = interp1d(tstamp_arr_camera_test, camera_arr_test_y, kind='linear', fill_value='extrapolate')
    camera_arr_test_y = func_linear_camera_y(tstamp_arr_phase_tag_test)

def signal_handler(*args):
    print ("Exiting after getting an interrupt \n")
    sys.exit(-1)


if __name__ == "__main__":

    #signal.signal(signal.SIGINT, signal_handler)

    try:
    
        #global camera_arr
        
        file_read()

        #print( len(camera_loc_arr), len(tag_phase_arr) )

        #Preprocess and interpolate the test data series

        start_value = camera_arr_test_x[0]
        for i in range(len(camera_arr_test_x)):
            #camera_arr_test[i] = (abs(camera_arr_test[i] - start_value)*1.0*tag_dimn_in_mm)/(abs(camera_arr_test[-1] - start_value)*1.0)
            camera_arr_test_x[i] = camera_arr_test_x[i] - start_value
        
        start_value = camera_arr_test_y[0]
        for i in range(len(camera_arr_test_y)):
            #camera_arr_test[i] = (abs(camera_arr_test[i] - start_value)*1.0*tag_dimn_in_mm)/(abs(camera_arr_test[-1] - start_value)*1.0)
            camera_arr_test_y[i] = camera_arr_test_y[i] - start_value
        
        
        camera_result_loc_arr_x = []
        camera_result_loc_arr_y = []
        rfid_result_loc_arr_x = []
        rfid_result_loc_arr_y = []

        start_loc_x = camera_arr_test_x[0]
        start_loc_y = camera_arr_test_y[0]
        step_arr = []

        step_arr.append(0)
        camera_result_loc_arr_x.append(start_loc_x)
        camera_result_loc_arr_y.append(start_loc_y)
        rfid_result_loc_arr_x.append(start_loc_x)
        rfid_result_loc_arr_y.append(start_loc_y)


        cur_idx = 0
        calib_cur_idx = 0
        start_calib_count_x = 0
        start_calib_count_y = 0

        step_count = 0

        #Comparing the test accuracy

        while cur_idx < len(tstamp_arr_phase_tag_test) and calib_cur_idx < len(tstamp_arr):

            time_diff = 0.0
            start_time = tstamp_arr_phase_tag_test[cur_idx]

            test_phase_slice = []
            test_tstamp_arr = []
            
            #Cutting the phase slice from the test series
            while time_diff < sample_time_sec :

                time_diff = abs(tstamp_arr_phase_tag_test[cur_idx] - start_time)
                test_phase_slice.append(phase_arr_test[cur_idx])
                test_tstamp_arr.append(time_diff)

                if cur_idx == len(tstamp_arr_test)-1:
                    break

                cur_idx += 1

            calib_time_diff = 0.0
            start_time = tstamp_arr[calib_cur_idx]
            calib_slice_count = 0
            calib_tstamp_arr = []

            while calib_time_diff < sample_time_sec: #and calib_cur_idx < len(tstamp_arr):
               
                if calib_cur_idx >= len(tstamp_arr):
                    break
                
                calib_time_diff = abs(tstamp_arr[calib_cur_idx] - start_time)
                calib_tstamp_arr.append(calib_time_diff)

                calib_cur_idx+=1
                calib_slice_count += 1


            
            dtw_arr = []
            compare_phase_idx_arr = []

            #Comparing with the phase slice of calibrated series
            i = int(calib_slice_count/4)
            frame_count = 0
            
            ts = datetime.datetime.now()
            while i < int(2*calib_slice_count) :

                calib_phase_slice = phase_arr[start_calib_count_x:start_calib_count_x+i]
                
                min_len = min( len(calib_phase_slice), len(test_phase_slice) )
                
                new_calib_phase_slice = downsample_to_proportion(calib_phase_slice, min_len*1.0/len(calib_phase_slice))
                new_test_phase_slice = downsample_to_proportion(test_phase_slice, min_len*1.0/len(test_phase_slice))

                x = np.array(new_calib_phase_slice).reshape(-1,1)
                y = np.array(new_test_phase_slice).reshape(-1,1)

                dtw_arr.append( fastdtw(x, y, dist=euclidean) )
                compare_phase_idx_arr.append(i)

                i = i + 1
                frame_count += 1

            tf = datetime.datetime.now()
            te = tf - ts
            #print("Number of Frames : ", str(frame_count) )
            #print("Duration : ", str(te) )

            start_calib_count_x =  start_calib_count_x + compare_phase_idx_arr[ dtw_arr.index(min(dtw_arr)) ]
            start_calib_count_y =  start_calib_count_y + compare_phase_idx_arr[ dtw_arr.index(min(dtw_arr)) ]
            calib_cur_idx = start_calib_count_x
            
            #print( "Test Slice Length : " + str( len(test_phase_slice) ) + " Calib Slice Length : " + str( calib_slice_count ) + " Max Length : " + str(compare_phase_idx_arr[ dtw_arr.index(min(dtw_arr)) ]) + "\n")
            #print( cur_idx, calib_cur_idx, int(calib_slice_count), int(2*calib_slice_count), start_calib_count, len(camera_arr), len(phase_arr) )

            if start_calib_count_x >= len(camera_arr_x):
                break
            if start_calib_count_y >= len(camera_arr_y):
                break
            predicted_locn_x = camera_arr_x[start_calib_count_x-1]
            predicted_locn_y = camera_arr_y[start_calib_count_y-1]

            rfid_result_loc_arr_x.append(predicted_locn_x)
            rfid_result_loc_arr_y.append(predicted_locn_y)

            camera_result_loc_arr_x.append(camera_arr_test_x[cur_idx])
            camera_result_loc_arr_y.append(camera_arr_test_y[cur_idx])

            step_arr.append(step_count+1)

            #print("Calib Count : ", calib_count)
            #print( "Calib : ", phase_diff)
            
            if cur_idx == len(tstamp_arr_test)-1:
                break

            step_count += 1

        #for i in range(len(rfid_result_loc_arr)):
         #   rfid_result_loc_arr[i] = abs(rfid_result_loc_arr[i])

        #print( calib_count, len(camera_loc_arr), len(tag_phase_arr) )

        fname1 = "rfid_onetag_predict_camera_" + str(sno) + ".tag"
        fhandle = open( fname1, 'w')

        for i in range(len(rfid_result_loc_arr_x)):
            fhandle.write( str(rfid_result_loc_arr_x[i]) + " " + str(rfid_result_loc_arr_y[i]) + "\n")

        fhandle.close()
        
        fname1 = "rfid_onetag_predict_ground_truth_" + str(sno) + ".tag"
        fhandle = open( fname1, 'w')

        for i in range(len(camera_result_loc_arr_x)):
            fhandle.write( str(camera_result_loc_arr_x[i]) + " " + str(camera_result_loc_arr_y[i]) + "\n")

        fhandle.close()
        
        line1, = plt.plot(rfid_result_loc_arr_x, rfid_result_loc_arr_y, label="RFID Predicted", linestyle='--', marker='o', linewidth=3, markersize=6)
        line2, = plt.plot(camera_result_loc_arr_x, camera_result_loc_arr_y, label="Camera Actual", linestyle='-', marker='x', linewidth=3, markersize=6)
        plt.legend(handles=[line1, line2], loc=4)
        plt.xlabel("Steps")
        plt.ylabel("On Tag Location (in mm)")
        #plt.savefig( "rfid_multitag_predict" + str(sno) + ".png" )
        plt.savefig( "rfid_onetag_predict" + str(sno) + ".pdf" )
        plt.cla()
        plt.clf()

        #r = np.corrcoef( camera_result_loc_arr, rfid_result_loc_arr )
        #print(r)

        dist_arr = []

        for i in range( len(rfid_result_loc_arr_x) ):
            dist_arr.append( abs(camera_result_loc_arr_x[i] - rfid_result_loc_arr_x[i]) )


        sorted_data = np.sort(np.array(dist_arr))
        yvals = np.arange(len(sorted_data))/float(len(sorted_data))
        
        fname2 = "cdf_rfid_onetag_predict" + str(sno) + ".tag"
        fhandle = open( fname2, 'w')

        for i in range( len(yvals) ):
            fhandle.write( str(sorted_data[i]) + " " + str(yvals[i]) + "\n")
        
        fhandle.close()


        plt.xlabel("Localisation prediction error (in mm)")
        plt.ylabel("CDF")
        plt.plot(sorted_data, yvals)
        plt.savefig( "cdf_rfid_onetag_predict" + str(sno) + ".pdf" )
        #plt.savefig( "cdf_rfid_multitag_predict" + str(sno) + ".png" )
        plt.cla()
        plt.clf()
        
        #print("Median Error: " , np.median( np.array(dist_arr) ))
        print(np.median( np.array(dist_arr) ))


    except KeyboardInterrupt:
        # do nothing here
        print ("Exiting after getting an interrupt \n")
        sys.exit(-1)
