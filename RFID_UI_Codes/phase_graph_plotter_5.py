import scipy.signal
import sys
import numpy as np
from changepoint.mean_shift_model import MeanShiftModel
from detect_cusum import detect_cusum

if len(sys.argv) < 2:
    print "python <pyhton_file> <data_file> <phase_dat_out_file>"
    sys.exit(-1)


#focus_epc_arr1 = [  '000000000000AAAAAAAA0000', '0000000000AAAAAAAA000000', '00000000AAAAAAAA00000000' ]
focus_epc_arr1 = [  'E28011002000769D57E208E5', 'E28011002000775D57E208E5', 'E28011002000755D57E208E5', 'E28011002000759D57E208E5', 'E280110020007ADD57E608E5' ]

#focus_epc_arr1 = focus_epc_arr2

fhandle = open( sys.argv[1], 'r' )
flines =  fhandle.readlines()
fhandle.close()

#rr_handle = open( sys.argv[2], 'w' )
phase_handle = open( sys.argv[2], 'w' )

phase_epc_dict = {}

for line in flines:

    l_lst = line.strip().split(',')

    epc = str(l_lst[0])
    tstamp = float(l_lst[3])
    phase = float(l_lst[9])

    if epc in focus_epc_arr1:

        if epc not in phase_epc_dict:
            phase_epc_dict[epc] = [phase]
        else:
            phase_epc_dict[epc].append(phase)

for i in range(5):
    phase_epc_dict[focus_epc_arr1[i]] = scipy.signal.savgol_filter( np.unwrap( np.array( phase_epc_dict[focus_epc_arr1[i]] ), discont=np.pi ), 25, 2 )
    #phase_epc_dict[focus_epc_arr1[i]] = scipy.signal.medfilt( np.unwrap( np.array( phase_epc_dict[focus_epc_arr1[i]] ), discont=np.pi ), 15)

#for i in range(3):
 #   data_arr = phase_epc_dict[focus_epc_arr1[i]]
    
  #  print i
#    detect_cusum(data_arr, 1.7)
    #model = MeanShiftModel()
    #stats_ts, pvals, nums = model.detect_mean_shift(data_arr, B=20)
    
series_length = min( len(phase_epc_dict[focus_epc_arr1[0]]), len(phase_epc_dict[focus_epc_arr1[1]]), len(phase_epc_dict[focus_epc_arr1[2]]), len(phase_epc_dict[focus_epc_arr1[3]]), len(phase_epc_dict[focus_epc_arr1[4]]) )


for i in range(series_length):
    phase_handle.write( str(i) + " " + str(phase_epc_dict[focus_epc_arr1[0]][i]) + " " + str(phase_epc_dict[focus_epc_arr1[1]][i]) + " " + str(phase_epc_dict[focus_epc_arr1[2]][i]) + " " + str(phase_epc_dict[focus_epc_arr1[3]][i]) + " " + str(phase_epc_dict[focus_epc_arr1[4]][i]) + "\n")

#rr_handle.close()
phase_handle.close()
