import scipy.signal
import sys
import numpy as np

if len(sys.argv) < 5:
    print "python <pyhton_file> <notouch_data_file> <touch_data_file> <tag_array_file> <Lside_ref_tags>"
    sys.exit(-1)


fhandle = open(sys.argv[1], 'r')
flines_prev = fhandle.readlines()
fhandle.close()

fhandle = open(sys.argv[2], 'r')
flines = fhandle.readlines()
fhandle.close()

fhandle = open(sys.argv[3], 'r')
tlines = fhandle.readlines()
fhandle.close()

fhandle = open(sys.argv[4], 'r')
ref_lines = fhandle.readlines()
fhandle.close()

tag_locn_dict = {}

#Tag Location File Reading
for line in tlines:

    l_lst = line.strip().split(':')

    tag_locn_dict[str(l_lst[0])] = str(l_lst[1])

#print tag_locn_dict

lside_ref_tags_dict = {}

for line in ref_lines:
    l_lst = line.strip().split(':')

    lside_ref_tags_dict[str(l_lst[0])] = str(l_lst[1]).strip().split(',')

#print lside_ref_tags_dict

def strictly_increasing(L):
    return all(x<y for x, y in zip(L, L[1:]))

def strictly_decreasing(L):
    return all(x>y for x, y in zip(L, L[1:]))

def non_increasing(L):
    return all(x>=y for x, y in zip(L, L[1:]))

def non_decreasing(L):
    return all(x<=y for x, y in zip(L, L[1:]))


epc_phase_dict = {}
epc_phase_unwrap_dict = {}
epc_phase_notouch_dict = {}
epc_phase_notouch_unwrap_dict = {}

focus_epc_arr = [ '000000000000AAAAAAAA0000', '0000000000AAAAAAAA000000', '00000000AAAAAAAA00000000' ]

#Touch data file loading
for line in flines:

    l_lst = line.strip().split(',')

    epc = str(l_lst[0])
    tstamp = float(l_lst[3])
    phase = float(l_lst[9])

    #iprint tstamp, epc, phase


    if epc in focus_epc_arr:
        #print epc
        if epc not in epc_phase_dict:
            #epc_phase_dict[epc] = [tstamp, phase]
            epc_phase_dict[epc] = [phase]
        else:
            #epc_phase_dict[epc].append([tstamp, phase])
            epc_phase_dict[epc].append(phase)


for epc in epc_phase_dict:
    
    diff_arr = np.array(epc_phase_dict[epc])
    epc_phase_unwrap_dict[epc] = np.unwrap( np.array( diff_arr), discont=np.pi )

#No touch data file loading
for line in flines_prev:

    l_lst = line.strip().split(',')

    epc = str(l_lst[0])
    tstamp = float(l_lst[3])
    phase = float(l_lst[9])

    #iprint tstamp, epc, phase

    if epc in focus_epc_arr:

        if epc not in epc_phase_notouch_dict:
            #epc_phase_dict[epc] = [tstamp, phase]
            epc_phase_notouch_dict[epc] = [phase]
        else:
            #epc_phase_dict[epc].append([tstamp, phase])
            epc_phase_notouch_dict[epc].append(phase)


for epc in epc_phase_notouch_dict:
    
    diff_arr = np.array(epc_phase_notouch_dict[epc])
    epc_phase_notouch_unwrap_dict[epc] = np.mean(np.unwrap( np.array( diff_arr), discont=np.pi ))
    #epc_phase_notouch_unwrap_dict[epc] = [np.mean(np.unwrap( np.array( diff_arr), discont=np.pi )), len(diff_arr)/30.0 ]

#print epc_phase_dict.keys()

#Sampling the touch phase file
win_size = 5
thresh = 1.12

count = 0
epc_phase_unwrap_dict_sampled = {}
val_arr = []


for epc in focus_epc_arr:

    #print epc
    diff_arr_smoothed = scipy.signal.medfilt(epc_phase_unwrap_dict[epc], win_size)
    
    #print np.mean(diff_arr_smoothed)
    #print len(epc_phase_unwrap_dict[epc])
    #print strictly_decreasing(diff_arr_smoothed)
    #epc_phase_unwrap_dict_sampled[epc] = np.mean(diff_arr_smoothed)
    #print diff_arr_smoothed
    #epc_phase_unwrap_dict_sampled[epc] = [abs(np.max(diff_arr_smoothed) - np.min(diff_arr_smoothed)), len(diff_arr_smoothed)/5.0]
    epc_phase_unwrap_dict_sampled[epc] = np.mean(diff_arr_smoothed)

print "After Touch"


print "Top", abs(epc_phase_unwrap_dict_sampled[focus_epc_arr[0]] - epc_phase_notouch_unwrap_dict[focus_epc_arr[0]])
print "Middle", abs(epc_phase_unwrap_dict_sampled[focus_epc_arr[1]] - epc_phase_notouch_unwrap_dict[focus_epc_arr[1]])
print "Bottom", abs(epc_phase_unwrap_dict_sampled[focus_epc_arr[2]] - epc_phase_notouch_unwrap_dict[focus_epc_arr[2]])

#print "Top", abs(epc_phase_unwrap_dict_sampled['E28011002000795E57E608E5'][0])
#print "Middle", abs(epc_phase_unwrap_dict_sampled['E28011002000725E57EA08E5'][0])
#print "Bottom", abs(epc_phase_unwrap_dict_sampled['E2801100200078DE57E608E5'][0])
#print "Side", abs(epc_phase_unwrap_dict_sampled['E28011002000765E57EB08E5'][0])

#print len(epc_phase_unwrap_dict_sampled.keys())

'''
num_rows = 16
num_cols = 1
final_heat_map = []

for i in range(num_rows):
    final_heat_map.append([0.0])

for epc in tag_locn_dict:

    row_num = int(tag_locn_dict[epc][0])
    col_num = int(tag_locn_dict[epc][1])
    

    val = 0.0

'''
