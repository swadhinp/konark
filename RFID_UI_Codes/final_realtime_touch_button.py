#This code is for real-time touch detection for 8 configuration
import scipy.signal
from detect_cusum import detect_cusum
import sys
import time
import math
import numpy as np
from scipy import stats
import gi
import threading
import signal
import operator

#gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk, GLib, GObject


if len(sys.argv) < 3:
    #print ("python <file.py> <data_file> <step_movement_speed_in_sec> <starting_tag_indx> <starting_tag_pos>")
    print ("python <file.py> <data_file> <step_movement_speed_in_sec>")
    sys.exit(-1)

#Basic Info and Global Variables

threshold = 0.9
window_size = 11
count = 0

touch_flag =0
start_flag =0
start_time =0

delta_time_in_sec = float(sys.argv[2])
#starting_pos = str(sys.argv[4])

#Configuration Info


def follow(thefile):
	thefile.seek(0,2)
	while True:
        	line = thefile.readline()
        	if not line:
            		time.sleep(0.1)
            		#sys.exit(-1)
            		continue
        	yield line

def touch_predict():

	global start_flag
	global touch_flag
	
 	try:
		epc_phase_arr = []
		epc_phase_filt_arr = []
		epc_time_arr = []
 

        	logfile = open(sys.argv[1], "r")
        	loglines = follow(logfile)
    

    
        	#Phase recording for the time interval
        	for line in loglines:
            		l_lst = line.strip().split(',')
        
            		epc = str(l_lst[0])
            		tstamp = float(l_lst[3])
            		phase = float(l_lst[9])
            		#print epc
        
            		if start_flag == 0:
                		start_time = tstamp
                		start_flag = 1


            		cur_time = tstamp
			diff = float(cur_time - start_time)/1000000.0

            		if diff < delta_time_in_sec:
            
                		#print epc
                		#print "mtch"
                		epc_phase_arr.append(phase)
                		epc_time_arr.append(tstamp)

            		else:
				break
                
		epc_phase_arr = np.unwrap( np.array( epc_phase_arr ) )

                #Finding max phase change of tags within the current zone


		start_time = 0
		start_flag = 0

		if len(epc_phase_arr) > 1:
                	ta, tai, taf, tamparr = detect_cusum(epc_phase_arr, threshold, 0.0, True, False)

                if len(tamparr) > 0:
			if touch_flag == 1:
				print("Finger removed.\n")
				touch_flag = 0
			else:
				print("Finger placed.\n")
				touch_flag = 1
		else:
			if touch_flag == 1:
				print("Finger stays.\n")
			else:
				print("Tag untouched\n")

	except KeyboardInterrupt:
        	print ("Path Traced : \n")
        	sys.exit(-1)


def signal_handler(*args):

    sys.exit(-1)

if __name__ == "__main__":
    #signal.signal(signal.SIGINT, signal.SIG_DFL)
	signal.signal(signal.SIGINT, signal_handler)
	
	try:
		while True:
			touch_predict()
		#Touch Predictor Thread
		#while True:
		#	thread1 = threading.Thread(target=touch_predict)
		#	thread1.daemon = True
		#	thread1.start()

	except KeyboardInterrupt:

		sys.exit(-1)
