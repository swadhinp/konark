set terminal postscript eps color size 6.0in,4.5in font 'Helvetica,14'
set output 'phase_twoplot_coupling.eps'

set multiplot layout 2, 1 title "Tag 7 Swiping" font ",14"

set tmargin 4

set title "Tag 6"
unset key
set ylabel 'RSSI (in dBm)' font 'Helvetica,14' offset 2
set xlabel 'Time (in s)' font 'Helvetica,14' offset 2
set xtics auto
set ytics auto
plot "tag6.dat" using 1:2 w lines lt 1 lw 4 notitle

#
set title "Tag 7"
unset key
set ylabel 'RSSI (in dBm)' font 'Helvetica,14' offset 2
set xlabel 'Time (in s)' font 'Helvetica,14' offset 2
set xtics auto
set ytics auto
plot "tag7.dat" using 1:2 w lines lt 1 lw 4 notitle

unset multiplot

