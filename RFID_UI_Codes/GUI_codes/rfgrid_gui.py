import gi

gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk

class Handler:
    def onDeleteWindow(self, *args):
        Gtk.main_quit(*args)


builder = Gtk.Builder()
builder.add_from_file("RFTrackR_final.glade")
builder.connect_signals(Handler())
window = builder.get_object("RFTrackrWin")

#color = Gdk.color_parse('#ff0000')
#btn.modify_bg(Gtk.StateType.PRELIGHT, color)

window.show_all()

Gtk.main()
