import scipy.signal
from detect_cusum import detect_cusum
import sys
import time
import math
import numpy as np
from scipy import stats
import gi
import threading
import signal

gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk, GLib, GObject


if len(sys.argv) < 5:
    print ("python <file.py> <data_file> <step_movement_speed_in_sec> <starting_tag_indx> <starting_tag_pos>")
    sys.exit(-1)

#Basic Info and Global Variables

threshold = 1.9
side_threshold = 0.6
window_size = 9
count = 0

delta_time_in_sec = float(sys.argv[2])
starting_tag = str(sys.argv[3])
starting_pos = str(sys.argv[4])

#Configuration Info
focus_epc_arr = [ '10000000000000000000AA00', '20000000000000000000AA00', '30000000000000000000AA00', '40000000000000000000AA00', '50000000000000000000AA00', '60000000000000000000AA00', '70000000000000000000AA00']

focus_epc_dict = {}
focus_epc_dict['10000000000000000000AA00'] = "R1"
focus_epc_dict['20000000000000000000AA00'] = "R2"
focus_epc_dict['30000000000000000000AA00'] = "R3"
focus_epc_dict['40000000000000000000AA00'] = "R4"
focus_epc_dict['50000000000000000000AA00'] = "R5"
focus_epc_dict['60000000000000000000AA00'] = "R6"
focus_epc_dict['70000000000000000000AA00'] = "R7"

focus_epc_dict_rev = {}
focus_epc_dict_rev["R1"] = '10000000000000000000AA00'
focus_epc_dict_rev["R2"] = '20000000000000000000AA00'
focus_epc_dict_rev["R3"] = '30000000000000000000AA00'
focus_epc_dict_rev["R4"] = '40000000000000000000AA00'
focus_epc_dict_rev["R5"] = '50000000000000000000AA00'
focus_epc_dict_rev["R6"] = '60000000000000000000AA00'
focus_epc_dict_rev["R7"] = '70000000000000000000AA00'

#Callibrated Phase
ref_tag_phase_dict = {}

'''
#With Stylus
ref_tag_dict["R1"] = { 'L' : 3.96, 'R' : 1.9  }
ref_tag_dict["R2"] = { 'L' : 4.59, 'R' : 1.50  }
ref_tag_dict["R3"] = { 'L' : 4.45, 'R' : 2.59 }
ref_tag_dict["R4"] = { 'L' : 4.81, 'R' : 5.11  }
ref_tag_dict["R5"] = { 'L' : 5.38, 'R' : 5.10  }
ref_tag_dict["R6"] = { 'L' : 1.56, 'R' : 5.45 }
ref_tag_dict["R7"] = { 'L' : 1.99, 'R' : 0.89 }
'''

#With Finger
ref_tag_phase_dict["R1"] = { 'L' : 2.71, 'R' : 4.10  }
ref_tag_phase_dict["R2"] = { 'L' : 3.55, 'R' : 4.17  }
ref_tag_phase_dict["R3"] = { 'L' : 3.17, 'R' : 4.35 }
ref_tag_phase_dict["R4"] = { 'L' : 2.17, 'R' : 1.95  }
ref_tag_phase_dict["R5"] = { 'L' : 1.79, 'R' : 2.65  }
ref_tag_phase_dict["R6"] = { 'L' : 2.65, 'R' : 4.55 }
ref_tag_phase_dict["R7"] = { 'L' : 0.90, 'R' : 5.85 }

tag_zone_dict = {}

tag_zone_dict["R1"] = ["R1", "R2", "R3", "R4"]
tag_zone_dict["R2"] = ["R2", "R1", "R3", "R4"]
tag_zone_dict["R3"] = ["R1", "R2", "R3", "R4", "R5"]
tag_zone_dict["R4"] = ["R2", "R3", "R5", "R4", "R6"]
tag_zone_dict["R5"] = ["R3", "R4", "R5", "R6", "R7"]
tag_zone_dict["R6"] = ["R4", "R5", "R7", "R6"]
tag_zone_dict["R7"] = ["R4", "R5", "R6", "R7"]

neighbor_zone_dict = {}

neighbor_zone_dict["R1"] = ["R2"]
neighbor_zone_dict["R2"] = ["R1", "R3"]
neighbor_zone_dict["R3"] = ["R2", "R4"]
neighbor_zone_dict["R4"] = ["R3", "R5"]
neighbor_zone_dict["R5"] = ["R4", "R6"]
neighbor_zone_dict["R6"] = ["R5", "R7"]
neighbor_zone_dict["R7"] = ["R6"]

path_dict_arr = []

start_flag = 0

current_tag = starting_tag
current_pos = starting_pos
builder = 0

path_arr = [ str(current_tag)+ "-" + str(current_pos) ]

path_dict_arr.append({ str(current_tag) + str(current_pos) : 1})

def follow(thefile):
    thefile.seek(0,2)
    while True:
        line = thefile.readline()
        if not line:
            time.sleep(0.1)
            #sys.exit(-1)
            continue
        yield line

#GUI Class
startFlag = 0
lastid = str(starting_tag)+str(starting_pos)

def gui_render():

    builder = Gtk.Builder()
    builder.add_from_file("RFTrackR_final.glade")
    window = builder.get_object("RFTrackrWin")
    window.connect("delete-event", Gtk.main_quit)
    window.set_title("RFTrackR GUI")

    def update_switch(sid):
        
        global startFlag
        global lastid

        #print( sid + " : " + lastid)

        if startFlag == 0:

            startFlag = 1
            lastid = str(sid)
            switch1 = builder.get_object(sid)
            switch1.set_active(True)

        else:

            if sid != lastid:
                switch1 = builder.get_object(lastid)
                switch1.set_active(False)
                switch2 = builder.get_object(sid)
                switch2.set_active(True)
                lastid = sid

        return False

    #def switch_change(threadName, run_event):
    def switch_change():
        #while run_event.is_set():
        while True:
            sid = str(current_tag) + str(current_pos)
            GLib.idle_add(update_switch, sid)
            time.sleep(1.0)

    window.show_all()
    #run_event = threading.Event()
    #run_event.set()

    #Touch Predictor Thread
    thread1 = threading.Thread(target=touch_predict)
    thread1.daemon = True
    thread1.start()
    
    time.sleep(0.1)

    #GUI Switch Change Thread
    thread2 = threading.Thread(target=switch_change)
    thread2.daemon = True
    thread2.start()


#def touch_predict(threadName, run_event):
def touch_predict():

    #while run_event.is_set():
    try:
        epc_phase_dict = {}
        epc_phase_filt_dict = {}
        epc_time_dict = {}
 
        global path_arr
        global current_pos 
        global current_tag
        global start_flag

        logfile = open(sys.argv[1], "r")
        loglines = follow(logfile)
    
        print ("Start \n")

        print (str(current_tag) + "-" + str(current_pos) + "\n")
    
        #Phase recording for the time interval
        for line in loglines:
            l_lst = line.strip().split(',')
        
            epc = str(l_lst[0])
            tstamp = float(l_lst[3])
            phase = float(l_lst[9])
            #print epc
        
            if start_flag == 0:
                start_time = tstamp
                start_flag = 1


            cur_time = tstamp

            diff = float(cur_time - start_time)/1000000.0

            if diff < delta_time_in_sec:
            
                #print epc
                if epc in focus_epc_arr: #Only focus on filtered tags
                
                    #print "mtch"
                    if epc not in epc_phase_dict: 
                        epc_phase_dict[epc] = [phase]
                        epc_time_dict[epc] = [tstamp]
                    else:
                        epc_phase_dict[epc].append(phase)
                        epc_time_dict[epc].append(tstamp)

            else:
                #print epc_phase_dict.keys()
                for epc in epc_phase_dict:
                    #epc_phase_dict[epc] = scipy.signal.savgol_filter( np.unwrap( np.array( epc_phase_dict[epc] ), discont=np.pi ), window_size, 2 )
                    epc_phase_dict[epc] = scipy.signal.medfilt( np.unwrap( np.array( epc_phase_dict[epc] ), discont=np.pi ), window_size)
                    epc_phase_filt_dict[epc] = scipy.signal.medfilt( np.array( epc_phase_dict[epc] ), window_size)

            
                #Phase slicing for the zone to be interrogated

                tag_zone_phase_arr_dict = {}
                tag_zone_phase_filt_arr_dict = {}

                for elem in tag_zone_dict[current_tag]:
                    try:
                        tag_zone_phase_arr_dict[elem] = epc_phase_dict[ focus_epc_dict_rev[elem] ]
                        tag_zone_phase_filt_arr_dict[elem] = epc_phase_filt_dict[ focus_epc_dict_rev[elem] ]

                    except KeyError:

                        current_tag = elem
                        print (str(current_tag) + "-" + str(current_pos) + "\n")
                        path_arr.append( str(current_tag) + "-" + str(current_pos) )
                        path_dict_arr.append({ str(current_tag) + str(current_pos) : 1})
                        epc_phase_dict = {}
                        epc_time_dict = {}
                        epc_phase_filt_dict = {}
                        start_time = cur_time
                        continue
            
                max_amp = 0.0
                second_max_amp = 0.0
                cur_slope = 0.0
                max_amparr = []
                second_max_amparr = []
                max_elem = current_tag
                second_max_elem = ""
                
                #Slope of Current Tag
                current_tag_phase_arr = epc_phase_dict[ focus_epc_dict_rev[current_tag] ]

                X = []
                for i in range(len(current_tag_phase_arr)):
                    X.append(i+1)

                cur_slope, intercept, r_value, p_value, std_err = stats.linregress(X, current_tag_phase_arr)

                #Finding max phase change of tags within the current zone
                for elm in tag_zone_phase_arr_dict:

                    tag_phase_arr = tag_zone_phase_arr_dict[elem]

                    if len(tag_phase_arr) > 1:
                        ta, tai, taf, tamparr = detect_cusum(tag_phase_arr, threshold, 0.0, True, False)

                        if len(tamparr) > 0:
                            for i in range(len(tamparr)):
                                tamparr[i] = abs(tamparr[i])%(2*np.pi)

                            tamp =  np.max( np.array(tamparr) )

                            if tamp >= max_amp:
                                max_amp = tamp
                                max_elem = elm
                                max_amparr = tamparr
                            else:
                                if tamp >= second_max_amp:
                                    second_max_amp = tamp
                                    second_max_elem = elm
                                    second_max_amparr = tamparr

                if max_elem == current_tag:
                
                    if current_pos == "R" or current_pos == "L":
                        current_pos = "M"

                    else:
                        cur_phase_arr = epc_phase_filt_dict[focus_epc_dict_rev[current_tag]]
                        cur_phase_arr_interested = cur_phase_arr[len(cur_phase_arr)-10:len(cur_phase_arr)]
                        phase_val = np.mean(np.array(cur_phase_arr_interested))
                        
                        lval = ref_tag_phase_dict[current_tag]['L']
                        rval = ref_tag_phase_dict[current_tag]['R']

                        ldiff = abs(lval - phase_val)
                        rdiff = abs(rval - phase_val)

                        if ldiff < rdiff:
                            current_pos = 'L'
                        else:
                            current_pos = 'R'
                    
                    path_dict_arr.append({ str(current_tag) + str(current_pos) : 1})

                else:

                    if max_elem in neighbor_zone_dict[current_tag]:
                        current_tag = max_elem
                    else:
                        if second_max_elem != "":
                            if second_max_elem in neighbor_zone_dict[current_tag]:
                                current_tag = max_elem
                            else:
                                current_tag = max_elem
                            
                        path_dict_arr.append({ str(max_elem) + str(current_pos) : 1, str(current_tag) + str(current_pos) : 1})
                        
                        #Slope detection to be added
                        #other_tag_phase_arr = epc_phase_dict[ ref_tag_dict[current_tag] ]
                        #oa, oai, oaf, oamparr = detect_cusum(other_tag_phase_arr, side_threshold, 0.0, True, False)
            
                        #oamp = 0.0
                        #if len(oamparr) >1:
                         #   oamp = np.max( np.absolute( oamparr ) )

                        #X = []
                        #for i in range(len(other_tag_phase_arr)):
                            #X.append(i+1)

                        #oslope, intercept, r_value, p_value, std_err = stats.linregress(X, other_tag_phase_arr)

                        #if oamp > side_threshold :
                         #   current_pos = "L"
                        #else:
                            #if oslope < 0.0:
                         #   current_pos = "R"
                            #else:
                            #   current_pos = "L"


                print (str(current_tag) + "-" + str(current_pos) + "\n")
                path_arr.append( str(current_tag) + "-" + str(current_pos) )

                bamp = 0.0
                camp = 0.0
                tamp = 0.0
                epc_phase_dict = {}
                epc_phase_filt_dict = {}
                epc_time_dict = {}
                start_time = cur_time
        #print line + "****"
    except KeyboardInterrupt:
        print ("Path Traced : \n")

        for i in range( len(path_arr) - 1):
            print (path_arr[i] + " =>", end=" ")

        print (path_arr[len(path_arr) - 1] + "\n")

        sys.exit(-1)

def check_change_for_correction():

    global epc_phase_dict


def signal_handler(*args):
    print ("Path Traced : \n")

    for i in range( len(path_arr) - 1):
        print (path_arr[i] + " =>", end=" ")

    print (path_arr[len(path_arr) - 1] + "\n")
    
    print("Path Dict Arr")
    print(path_dict_arr)


    sys.exit(-1)

if __name__ == "__main__":

    #signal.signal(signal.SIGINT, signal.SIG_DFL)
    signal.signal(signal.SIGINT, signal_handler)
    try:
        
        GObject.threads_init()
        gui_render()
        Gtk.main()
        GObject.threads_leave()
        
        #touch_predict()

    except KeyboardInterrupt:
        # do nothing here
        print ("Path Traced : \n")

        for i in range( len(path_arr) - 1):
            print (path_arr[i] + " =>", end=" ")

        print (path_arr[len(path_arr) - 1] + "\n")

        sys.exit(-1)
