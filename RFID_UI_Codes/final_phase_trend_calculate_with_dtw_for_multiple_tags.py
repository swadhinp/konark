#This code is for single tag tracking using DTW technique
from __future__ import division, print_function
import scipy.signal
from detect_cusum import detect_cusum
import sys
import time
import math
import numpy as np
from scipy import stats
import threading
import signal
import matplotlib.pyplot as plt
from detect_peaks import detect_peaks
from scipy.interpolate import interp1d
from scipy import interpolate
import warnings
from scipy.interpolate import InterpolatedUnivariateSpline
from numpy.polynomial import Polynomial
import itertools
from scipy.spatial.distance import euclidean
from fastdtw import fastdtw
from dtw import dtw
from itertools import islice

def downsample_to_proportion(rows, proportion=1):
    return list(islice(rows, 0, len(rows), int(1/proportion)))

warnings.filterwarnings(action="ignore", module="scipy", message="^internal gelsd")

if len(sys.argv) < 5:
    print ("python <file.py> <phase_calibration_file> <tag_name> <test_data_file> <test_loc_file>")
    sys.exit(-1)

tag_ref_dict = {}
tag_ref_dict["Tag1"] = '100000000000000000006600'
tag_ref_dict["Tag2"] = '200000000000000000006600'
tag_ref_dict["Tag3"] = '300000000000000000006600'
tag_ref_dict["Tag4"] = '400000000000000000006600'
tag_ref_dict["Tag5"] = '500000000000000000006600'
tag_ref_dict["Tag6"] = '600000000000000000006600'
tag_ref_dict["Tag7"] = '700000000000000000006600'
tag_ref_dict["Tag8"] = '800000000000000000006600'

neighbor_window_dict = {}
neighbor_window_dict['100000000000000000006600'] = ['200000000000000000006600', '200000000000000000006600']
neighbor_window_dict['200000000000000000006600'] = ['100000000000000000006600', '300000000000000000006600']
neighbor_window_dict['300000000000000000006600'] = ['200000000000000000006600', '400000000000000000006600']
neighbor_window_dict['400000000000000000006600'] = ['300000000000000000006600', '500000000000000000006600']
neighbor_window_dict['500000000000000000006600'] = ['400000000000000000006600', '600000000000000000006600']
neighbor_window_dict['600000000000000000006600'] = ['500000000000000000006600', '700000000000000000006600']
neighbor_window_dict['700000000000000000006600'] = ['600000000000000000006600', '800000000000000000006600']
neighbor_window_dict['800000000000000000006600'] = ['700000000000000000006600', '700000000000000000006600']

#Basic Info and Global Variables

fhandle = open( sys.argv[1], 'r' )
loglines = fhandle.readlines()
fhandle.close()

tag_no = sys.argv[2]

#print ("Start \n")

fhandle_in = open(sys.argv[3], 'r')
data_lines = fhandle_in.readlines()
fhandle_in.close()

fhandle_in = open(sys.argv[4], 'r')
camera_lines = fhandle_in.readlines()
fhandle_in.close()


window_size = 51
threshold = 1.2
#threshold = 3.0
sample_time_sec = 0.5
tag_dimn_in_mm = 85
delta_phase_change = 1.5


phase_arr = []
phase_arr_n1 = []
phase_arr_n2 = []
camera_arr = []
tstamp_arr = []

phase_arr_test = []
phase_arr_test_n1 = []
phase_arr_test_n2 = []
camera_arr_test = []
tstamp_arr_phase_tag_test = []
tstamp_arr_phase_tag_test_n1 = []
tstamp_arr_phase_tag_test_n2 = []

tstamp_arr_camera_test = []
tstamp_arr_test = []
tstamp_arr_test_n1 = []
tstamp_arr_test_n2 = []
camera_tstamp_arr_test = []

epc_code = tag_ref_dict[tag_no]
epc_code_n1 = neighbor_window_dict[epc_code][0]
epc_code_n2 = neighbor_window_dict[epc_code][1]

def file_read():

    global phase_arr
    global phase_arr_n1
    global phase_arr_n2
    global camera_arr

    global phase_arr_test
    global phase_arr_test_n1
    global phase_arr_test_n2
    global camera_arr_test
    global tstamp_arr_phase_tag_test
    global tstamp_arr_phase_tag_test_n1
    global tstamp_arr_phase_tag_test_n2

    global tstamp_arr
    global tstamp_arr_camera_test
    global tstamp_arr_test
    global tstamp_arr_test_n1
    global tstamp_arr_test_n2
    global camera_tstamp_arr_test

    #Calibration File Read
    for line in loglines:
        l_lst = line.strip().split(',')
        
        phase = float(l_lst[0])
        phase_n1 = float(l_lst[1])
        phase_n2 = float(l_lst[2])
        loc = float(l_lst[3])
        tstamp = float(l_lst[4])

        phase_arr.append( phase )
        phase_arr_n1.append( phase_n1 )
        phase_arr_n2.append( phase_n2 )
        camera_arr.append( loc )
        tstamp_arr.append( tstamp )


    #Test file read

    for line in data_lines:
        l_lst = line.strip().split(',')
        
        epc = str(l_lst[0])
        tstamp = float(l_lst[3])
        phase = float(l_lst[9])

        if epc == epc_code:
            phase_arr_test.append( phase )
            tstamp_arr_test.append( tstamp )

        if epc == epc_code_n1: 
            phase_arr_test_n1.append( phase )
            tstamp_arr_test_n1.append( tstamp )

        if epc == epc_code_n2: 
            phase_arr_test_n2.append( phase )
            tstamp_arr_test_n2.append( tstamp )


    for i in range(len(tstamp_arr_test)):
        tstamp_arr_phase_tag_test.append( (tstamp_arr_test[i] - tstamp_arr_test[0])*1.0/1000000.0 )
    
    for i in range(len(tstamp_arr_test_n1)):
        tstamp_arr_phase_tag_test_n1.append( (tstamp_arr_test_n1[i] - tstamp_arr_test_n1[0])*1.0/1000000.0 )
    
    for i in range(len(tstamp_arr_test_n2)):
        tstamp_arr_phase_tag_test_n2.append( (tstamp_arr_test_n2[i] - tstamp_arr_test_n2[0])*1.0/1000000.0 )

    phase_arr_test = scipy.signal.savgol_filter( np.unwrap( np.array( phase_arr_test ), discont=np.pi ), window_size, 2 )
    phase_arr_test_n1 = scipy.signal.savgol_filter( np.unwrap( np.array( phase_arr_test_n1 ), discont=np.pi ), window_size, 2 )
    phase_arr_test_n2 = scipy.signal.savgol_filter( np.unwrap( np.array( phase_arr_test_n2 ), discont=np.pi ), window_size, 2 )
    #phase_arr = scipy.signal.medfilt( np.unwrap( np.array( phase_arr ), discont=np.pi ), window_size)

    print("Time : ", str(tag_dimn_in_mm*1.0/tstamp_arr_phase_tag_test[-1]))

    for line in camera_lines:

        l_lst = line.strip().split(':')

        tstamp = float(l_lst[0])
        xval = float(l_lst[1])
        yval = float(l_lst[2])

        camera_arr_test.append( yval )
        camera_tstamp_arr_test.append( tstamp )
    
    #Camera X filter
    camera_arr_test = scipy.signal.savgol_filter( np.array( camera_arr_test ), 9, 2 )
    
    for i in range(len(camera_tstamp_arr_test)):
        tstamp_arr_camera_test.append( (camera_tstamp_arr_test[i] - camera_tstamp_arr_test[0])*1.0 )

    #p = Polynomial.fit( tstamp_arr_camera_test, camera_arr_test, 1 )
    #camera_arr_test = list(itertools.chain.from_iterable(p.linspace()))
    func_linear_camera = interp1d(tstamp_arr_camera_test, camera_arr_test, kind='linear', fill_value='extrapolate')
    camera_arr_test = func_linear_camera(tstamp_arr_phase_tag_test)


def signal_handler(*args):
    print ("Exiting after getting an interrupt \n")
    sys.exit(-1)


if __name__ == "__main__":

    #signal.signal(signal.SIGINT, signal_handler)

    try:
    
        #global camera_arr
        
        file_read()

        #print( len(camera_loc_arr), len(tag_phase_arr) )

        #Preprocess and interpolate the test data series

        start_value = camera_arr_test[0]
        for i in range(len(camera_arr_test)):
            camera_arr_test[i] = (abs(camera_arr_test[i] - start_value)*1.0*tag_dimn_in_mm)/(abs(camera_arr_test[-1] - start_value)*1.0)
        
        
        camera_result_loc_arr = []
        rfid_result_loc_arr = []

        start_loc = camera_arr_test[0]
        step_arr = []

        step_arr.append(0)
        camera_result_loc_arr.append(start_loc)
        rfid_result_loc_arr.append(start_loc)


        cur_idx = 0
        calib_cur_idx = 0
        start_calib_count = 0

        step_count = 0

        #Comparing the test accuracy

        while cur_idx < len(tstamp_arr_phase_tag_test) and calib_cur_idx < len(tstamp_arr):

            time_diff = 0.0
            start_time = tstamp_arr_phase_tag_test[cur_idx]

            test_phase_slice = []
            test_phase_slice_n1 = []
            test_phase_slice_n2 = []
            test_tstamp_arr = []
            
            #Cutting the phase slice from the test series
            while time_diff < sample_time_sec :

                time_diff = abs(tstamp_arr_phase_tag_test[cur_idx] - start_time)
                
                test_phase_slice.append(phase_arr_test[cur_idx])
                
                #if cur_idx < len(test_phase_slice_n1):
                test_phase_slice_n1.append(phase_arr_test_n1[cur_idx])
                
                #if cur_idx < len(test_phase_slice_n2):
                test_phase_slice_n2.append(phase_arr_test_n2[cur_idx])

                test_tstamp_arr.append(time_diff)

                if cur_idx == len(tstamp_arr_test)-1:
                    break


                cur_idx += 1

            calib_time_diff = 0.0
            start_time = tstamp_arr[calib_cur_idx]
            calib_slice_count = 0
            calib_tstamp_arr = []

            while calib_time_diff < sample_time_sec: #and calib_cur_idx < len(tstamp_arr):
               
                if calib_cur_idx >= len(tstamp_arr):
                    break
                
                calib_time_diff = abs(tstamp_arr[calib_cur_idx] - start_time)
                calib_tstamp_arr.append(calib_time_diff)

                calib_cur_idx+=1
                calib_slice_count += 1


            
            dtw_arr = []
            compare_phase_idx_arr = []

            #Comparing with the phase slice of calibrated series
            i = int(calib_slice_count/4)
            #for i in range( int(calib_slice_count/2), int(2*calib_slice_count) ):
            while i < int(2*calib_slice_count):

                calib_phase_slice = phase_arr[start_calib_count:start_calib_count+i]
                calib_phase_slice_n1 = phase_arr_n1[start_calib_count:start_calib_count+i]
                calib_phase_slice_n2 = phase_arr_n2[start_calib_count:start_calib_count+i]
                
                if len(calib_phase_slice) == 0 or len(calib_phase_slice_n1) == 0 or len(calib_phase_slice_n2) == 0:
                    continue
                
                if len(test_phase_slice) == 0 or len(test_phase_slice_n1) == 0 or len(test_phase_slice_n2) == 0:
                    continue

                min_len = min( len(calib_phase_slice), len(test_phase_slice) )
                min_len1 = min( len(calib_phase_slice_n1), len(test_phase_slice_n1))
                min_len2 = min( len(calib_phase_slice_n2), len(test_phase_slice_n2))
                
                new_calib_phase_slice = downsample_to_proportion(calib_phase_slice, min_len*1.0/len(calib_phase_slice))
                new_calib_phase_slice_n1 = downsample_to_proportion(calib_phase_slice_n1, min_len1*1.0/len(calib_phase_slice_n1))
                new_calib_phase_slice_n2 = downsample_to_proportion(calib_phase_slice_n2, min_len2*1.0/len(calib_phase_slice_n2))

                new_test_phase_slice = downsample_to_proportion(test_phase_slice, min_len*1.0/len(test_phase_slice))
                new_test_phase_slice1 = downsample_to_proportion(test_phase_slice_n1, min_len1*1.0/len(test_phase_slice_n1))
                new_test_phase_slice2 = downsample_to_proportion(test_phase_slice_n2, min_len2*1.0/len(test_phase_slice_n2))

                x = np.array(new_calib_phase_slice).reshape(-1,1)
                x1 = np.array(new_calib_phase_slice_n1).reshape(-1,1)
                x2 = np.array(new_calib_phase_slice_n2).reshape(-1,1)
                y = np.array(new_test_phase_slice).reshape(-1,1)
                y1 = np.array(new_test_phase_slice1).reshape(-1,1)
                y2 = np.array(new_test_phase_slice2).reshape(-1,1)

                val1, path1 = fastdtw(x, y, dist=euclidean)
                val2, path2 = fastdtw(x1, y1, dist=euclidean)
                val3, path3 = fastdtw(x2, y2, dist=euclidean)

                #print(val1, val2, val3)

                dtw_arr.append( 0.8*val1 + 0.2*( val2 + val3) )

                compare_phase_idx_arr.append(i)
                i += 10
            
            start_calib_count =  start_calib_count + compare_phase_idx_arr[ dtw_arr.index(min(dtw_arr)) ]
            calib_cur_idx = start_calib_count
            
            #print( "Test Slice Length : " + str( len(test_phase_slice) ) + " Calib Slice Length : " + str( calib_slice_count ) + " Max Length : " + str(compare_phase_idx_arr[ dtw_arr.index(min(dtw_arr)) ]) + "\n")
            #print( cur_idx, calib_cur_idx, int(calib_slice_count), int(2*calib_slice_count), start_calib_count, len(camera_arr), len(phase_arr) )

            if start_calib_count >= len(camera_arr):
                break

            predicted_locn = camera_arr[start_calib_count-1]
            rfid_result_loc_arr.append(predicted_locn)
            camera_result_loc_arr.append(camera_arr_test[cur_idx])

            step_arr.append(step_count+1)

            #print("Calib Count : ", calib_count)
            #print( "Calib : ", phase_diff)
            
            if cur_idx == len(tstamp_arr_test)-1:
                break

            step_count += 1

        for i in range(len(rfid_result_loc_arr)):
            rfid_result_loc_arr[i] = abs(rfid_result_loc_arr[i])

        #print( calib_count, len(camera_loc_arr), len(tag_phase_arr) )

        fname1 = "rfid_multitag_predict_" + str(tag_no) + ".tag"
        fhandle = open( fname1, 'w')

        for i in range(len(step_arr)):
            fhandle.write( str(step_arr[i]) + " " + str(rfid_result_loc_arr[i]) + " " + str(camera_result_loc_arr[i]) + "\n")

        fhandle.close()

        
        line1, = plt.plot(step_arr, rfid_result_loc_arr, label="RFID Predicted", linestyle='--', marker='o', linewidth=3, markersize=6)
        line2, = plt.plot(step_arr, camera_result_loc_arr, label="Camera Actual", linestyle='-', marker='x', linewidth=3, markersize=6)
        plt.legend(handles=[line1, line2], loc=4)
        plt.xlabel("Steps")
        plt.ylabel("On Tag Location (in mm)")
        #plt.savefig( "rfid_multitag_predict" + str(sno) + ".png" )
        plt.savefig( "rfid_multitag_predict" + str(tag_no) + ".pdf" )
        plt.cla()
        plt.clf()

        r = np.corrcoef( camera_result_loc_arr, rfid_result_loc_arr )
        print(r)

        dist_arr = []

        for i in range( len(rfid_result_loc_arr) ):
            dist_arr.append( abs(camera_result_loc_arr[i] - rfid_result_loc_arr[i]) )


        sorted_data = np.sort(np.array(dist_arr))
        yvals = np.arange(len(sorted_data))/float(len(sorted_data))
        
        fname2 = "cdf_rfid_onetag_predict" + str(tag_no) + ".tag"
        fhandle = open( fname2, 'w')

        for i in range( len(yvals) ):
            fhandle.write( str(sorted_data[i]) + " " + str(yvals[i]) + "\n")
        
        fhandle.close()


        plt.xlabel("Localisation prediction error (in mm)")
        plt.ylabel("CDF")
        plt.plot(sorted_data, yvals)
        plt.savefig( "cdf_rfid_onetag_predict" + str(tag_no) + ".pdf" )
        #plt.savefig( "cdf_rfid_multitag_predict" + str(sno) + ".png" )
        plt.cla()
        plt.clf()
        
        #print("Median Error: " , np.median( np.array(dist_arr) ))
        print(np.median( np.array(dist_arr) ))


    except KeyboardInterrupt:
        # do nothing here
        print ("Exiting after getting an interrupt \n")
        sys.exit(-1)
