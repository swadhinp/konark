import sys

fhandle = open( sys.argv[1], 'r' )
flines = fhandle.readlines()
fhandle.close()

fout = open( "camera_xy.dat", 'w' )

for line in flines:

    l_lst = line.strip().split(':')

    fout.write( str(l_lst[0]) + " " + str(l_lst[2]) + "\n" )

fout.close()
