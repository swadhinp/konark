set terminal postscript eps color size 6.4in,4.8in font 'Helvetica,45'
set output 'Pattern_for_0_7time_DeltaP.eps'
set ylabel 'Position on Tag (in mm) on X' font 'Helvetica,45' offset 0
set xlabel 'Position on Tag (in mm) on Y' font 'Helvetica,45' offset 0
set title ''

set key bottom left
set grid
set yrange [-2:2]
#set xrange [0.5:4.5]
#set style fill empty
#set xtics ("0" 0, "1" 2, "2" 4, "3" 6, "4" 8, "5" 10, "6" 12, "7" 14)
set xtics auto
set ytics auto

#set style line 1 lc rgb '#0060ad' lt 1 lw 4 pt 10 ps 1.5   # --- blue

plot "Pattern_for_0_7time_DeltaP.dat" using 2:1 w lines lt 1 lw 4 title 'Predicted', \
    "Pattern_for_0_7time_DeltaP.dat" using 3:1 w lines lt 3 lw 4 title 'Ground-truth'

