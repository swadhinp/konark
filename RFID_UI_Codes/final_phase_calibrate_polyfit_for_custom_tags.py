#This code is for creating calibration phase for DTW based tracking
from __future__ import division, print_function
import scipy.signal
from detect_cusum import detect_cusum
import sys
import time
import math
import numpy as np
from scipy import stats
import threading
import signal
import matplotlib.pyplot as plt
from detect_peaks import detect_peaks
from scipy.interpolate import interp1d
from scipy import interpolate
import warnings
from fastdtw import fastdtw
from dtw import dtw
from scipy.interpolate import InterpolatedUnivariateSpline

from numpy.polynomial import Polynomial


warnings.filterwarnings(action="ignore", module="scipy", message="^internal gelsd")

if len(sys.argv) < 3:
    print ("python <file.py> <phase_calibration_folder_path> <uniq_str>")
    sys.exit(-1)    

folder_path = str(sys.argv[1])
ustr = str(sys.argv[2])

tag_dimension_in_mm = 90

file_count = 5
window_size = 91

phase_arr_dict = {}
tstamp_phase_arr_dict = {}
camera_arr_dict_x = {}
camera_arr_dict_y = {}
tstamp_camera_arr_dict = {}

max_time_window = 0
max_len_reqd = 0

for fcount in range(file_count):

    file_path = folder_path + "/S" + str(fcount+1) + "/all_data_group2.csv"

    fhandle = open( file_path , 'r' )
    loglines = fhandle.readlines()
    fhandle.close()

    file_camera_path = folder_path + "/S" + str(fcount+1) + "/posn_time.csv"

    fhandle = open( file_camera_path , 'r' )
    flines = fhandle.readlines()
    fhandle.close()

    phase_arr = []
    tstamp_arr = []
    tstamp_arr_phase_tag = []
    camera_arr_x = []
    camera_arr_y = []
    camera_tstamp_arr = []
    tstamp_arr_camera = []

    #Calibration File Read
    for line in loglines:
        l_lst = line.strip().split(',')
        
        epc = str(l_lst[0])
        tstamp = float(l_lst[3])
        phase = float(l_lst[9])

        phase_arr.append( phase )
        tstamp_arr.append( tstamp )


    for i in range(len(tstamp_arr)):
        tstamp_arr_phase_tag.append( (tstamp_arr[i] - tstamp_arr[0])*1.0/1000000.0 )

    if ( tstamp_arr_phase_tag[-1] > max_time_window ):
        max_time_window = tstamp_arr_phase_tag[-1]

    phase_arr = scipy.signal.savgol_filter( np.unwrap( np.array( phase_arr ), discont=np.pi ), window_size, 2 )

    if len(phase_arr) > max_len_reqd :
        max_len_reqd = len(phase_arr)


    phase_arr_dict[fcount] = phase_arr
    tstamp_phase_arr_dict[fcount] = tstamp_arr_phase_tag

    for line in flines:

        l_lst = line.strip().split(':')

        tstamp = float(l_lst[0])
        xval = float(l_lst[1])
        yval = float(l_lst[2])

        camera_arr_x.append( xval )
        camera_arr_y.append( yval )
        camera_tstamp_arr.append( tstamp )
    
    #Camera X filter
    camera_arr_x = scipy.signal.savgol_filter( np.array( camera_arr_x ), 9, 2 )
    camera_arr_y = scipy.signal.savgol_filter( np.array( camera_arr_y ), 9, 2 )
 

    for i in range(len(camera_tstamp_arr)):
        tstamp_arr_camera.append( (camera_tstamp_arr[i] - camera_tstamp_arr[0])*1.0 )


    camera_arr_dict_x[fcount] = camera_arr_x
    camera_arr_dict_y[fcount] = camera_arr_y

    tstamp_camera_arr_dict[fcount] = tstamp_arr_camera

time_interp_for_phase_data_arr = np.linspace(0, max_time_window - 0.5, num = int(max_len_reqd*2.2), endpoint=True)

for elem in phase_arr_dict :
    func_linear_phase = interp1d(tstamp_phase_arr_dict[elem], phase_arr_dict[elem], kind='linear', fill_value='extrapolate') #Using Interpolation
    #func_linear_phase = InterpolatedUnivariateSpline(tstamp_phase_arr_dict[elem], phase_arr_dict[elem], k=5) #Using Spline based Method
    phase_arr_interp = func_linear_phase(time_interp_for_phase_data_arr)
    phase_arr_dict[elem] = phase_arr_interp

sliding_window_size = 10
final_phase_arr = []
final_tstamp_arr = []

temp_phase_arr = []

start = 0
end = start+sliding_window_size

while end < len(time_interp_for_phase_data_arr):

    for elem in phase_arr_dict :
        temp_phase_arr.append(np.mean(phase_arr_dict[elem][start:end]))
    
    final_phase_arr.append( np.mean(temp_phase_arr) )
    final_tstamp_arr.append( (time_interp_for_phase_data_arr[start] + time_interp_for_phase_data_arr[end])/2.0 )
    temp_phase_arr = []

    start += 1
    end = start+sliding_window_size


func_linear_phase = interp1d(final_tstamp_arr, final_phase_arr, kind='linear', fill_value='extrapolate')
phase_arr_interp = func_linear_phase(time_interp_for_phase_data_arr)

graph1, = plt.plot( time_interp_for_phase_data_arr, phase_arr_interp, linestyle='-', marker='x', linewidth=3, markersize=3, label="Average")

axes = plt.gca()

#axes.set_ylim([3,7])
#axes.set_xlim([0,3600])

#graph1, = plt.plot( final_tstamp_arr, final_phase_arr, linestyle='-', marker='x', linewidth=3, markersize=3, label="Average")
#labels = ['1', '2', '3', '4', '5', '6', '7', '8', '9']
#plt.xticks([0, 450, 900, 1350, 1800, 2250, 2700, 3150, 3600], labels)

font = {'weight' : 'bold', 'size'   : 18 }
plt.rc('font', **font)

p = Polynomial.fit(time_interp_for_phase_data_arr, phase_arr_interp, 1)
graph2, = plt.plot(*p.linspace(), label="Linear")

p = Polynomial.fit(time_interp_for_phase_data_arr, phase_arr_interp, 2)
graph3, = plt.plot(*p.linspace(), label="Quadratic")

p = Polynomial.fit(time_interp_for_phase_data_arr, phase_arr_interp, 3)
graph4, = plt.plot(*p.linspace(), label="Cubic")

p = Polynomial.fit(time_interp_for_phase_data_arr, phase_arr_interp, 7)
fit_phase_arr = p.linspace()
#print(p)
graph5, = plt.plot(*p.linspace(), label="7th")


plt.legend(handles=[graph1, graph2, graph3, graph4, graph5], loc=4)

plt.xlabel('Location')
plt.ylabel('Phase')
plt.grid(True)
plt.savefig( "phase_change_average_" + ustr + ".pdf" )
plt.cla()
plt.clf()

for elem in phase_arr_dict:

    graph2, = plt.plot( time_interp_for_phase_data_arr, phase_arr_dict[elem], linestyle='-', marker='x', linewidth=3, markersize=3)

    font = {'weight' : 'bold',
            'size'   : 18 }

    plt.rc('font', **font)
    axes = plt.gca()
    #axes.set_xlim([0,5])
    #axes.set_ylim([1,8])


    plt.xlabel('Time')
    plt.ylabel('Phase')
    plt.grid(True)
    #plt.savefig( "phase_change_onetag_interp" + str(sno) + ".png" )

plt.savefig( "phase_change_multitag_interp_" + ustr + ".pdf" )
plt.cla()
plt.clf()

for elem in camera_arr_dict_y:

    graph2, = plt.plot( tstamp_camera_arr_dict[elem], camera_arr_dict_y[elem], linestyle='-', marker='x', linewidth=3, markersize=3)

    font = {'weight' : 'bold',
            'size'   : 18 }

    plt.rc('font', **font)
    axes = plt.gca()
    #axes.set_xlim([0,5])
    #axes.set_ylim([1,8])


    plt.xlabel('Time')
    plt.ylabel('Yval')
    plt.grid(True)
    #plt.savefig( "phase_change_onetag_interp" + str(sno) + ".png" )

plt.savefig( "camera_change_multitag_yval_" + ustr + ".pdf" )
plt.cla()
plt.clf()


time_interp_for_camera_data_arr = np.linspace(0, max_time_window - 0.5, num = int(max_len_reqd*2.2), endpoint=True)

for elem in camera_arr_dict_x :
    func_linear_phase = interp1d(tstamp_camera_arr_dict[elem], camera_arr_dict_x[elem], kind='linear', fill_value='extrapolate')
    #func_linear_phase = InterpolatedUnivariateSpline(tstamp_camera_arr_dict[elem], camera_arr_dict[elem], k=2) #Using Spline based Method
    camera_arr_interp = func_linear_phase(time_interp_for_camera_data_arr)
    camera_arr_dict_x[elem] = camera_arr_interp

for elem in camera_arr_dict_y :
    func_linear_phase = interp1d(tstamp_camera_arr_dict[elem], camera_arr_dict_y[elem], kind='linear', fill_value='extrapolate')
    #func_linear_phase = InterpolatedUnivariateSpline(tstamp_camera_arr_dict[elem], camera_arr_dict[elem], k=2) #Using Spline based Method
    camera_arr_interp = func_linear_phase(time_interp_for_camera_data_arr)
    camera_arr_dict_y[elem] = camera_arr_interp

sliding_window_size = 50
final_camera_arr_x = []
final_camera_arr_y = []
final_tstamp_arr = []

temp_camera_arr_x = []
temp_camera_arr_y = []

start = 0
end = start+sliding_window_size

while end < len(time_interp_for_camera_data_arr):

    for elem in camera_arr_dict_x :
        temp_camera_arr_x.append(np.mean(camera_arr_dict_x[elem][start:end]))
    
    for elem in camera_arr_dict_y :
        temp_camera_arr_y.append(np.mean(camera_arr_dict_y[elem][start:end]))
    
    final_camera_arr_x.append( np.mean(temp_camera_arr_x) )
    final_camera_arr_y.append( np.mean(temp_camera_arr_y) )

    final_tstamp_arr.append( (time_interp_for_camera_data_arr[start] + time_interp_for_camera_data_arr[end])/2.0 )
    temp_camera_arr_x = []
    temp_camera_arr_y = []

    start += 1
    end = start+sliding_window_size


func_linear_camera = interp1d(final_tstamp_arr, final_camera_arr_x, kind='linear', fill_value='extrapolate')
camera_arr_interp_x = func_linear_camera(time_interp_for_phase_data_arr)

func_linear_camera = interp1d(final_tstamp_arr, final_camera_arr_y, kind='linear', fill_value='extrapolate')
camera_arr_interp_y = func_linear_camera(time_interp_for_phase_data_arr)

graph1, = plt.plot( camera_arr_interp_x, camera_arr_interp_y, linestyle='-', marker='x', linewidth=3, markersize=3, label="Average")

p1 = Polynomial.fit(time_interp_for_phase_data_arr, camera_arr_interp_y, 4)
#graph2, = plt.plot(*p.linspace(), label="Linear")

fit_camera_arr_y = p1.linspace()
fit_camera_arr_y_arr = []

for elem in fit_camera_arr_y:
    fit_camera_arr_y_arr.extend( elem )

p2 = Polynomial.fit(time_interp_for_phase_data_arr, camera_arr_interp_x, 4)
#graph2, = plt.plot(*p.linspace(), label="Linear")

fit_camera_arr_x = p2.linspace()
fit_camera_arr_x_arr = []

for elem in fit_camera_arr_x:
    fit_camera_arr_x_arr.extend( elem )

graph2, = plt.plot( fit_camera_arr_x_arr, fit_camera_arr_y_arr, linestyle='-', marker='x', linewidth=3, markersize=3, label="Average")

#plt.legend(handles=[graph1, graph2], loc=1)

plt.xlabel('Xval')
plt.ylabel('Yval')
plt.grid(True)
plt.savefig( "camera_change_" + ustr + ".pdf" )
plt.cla()
plt.clf()


#Writing to calibrated files

fhandle = open("phase_calib_fit_custom1.dat", 'w')

tag_loc = 0
#pixel_ratio = (tag_dimension_in_mm*1.0) / (final_camera_arr[-1] - final_camera_arr[0])*1.0
#tag_increase = (tag_dimension_in_mm*1.0)/(len(final_phase_arr)*1.0)

for i in range( len(time_interp_for_phase_data_arr) ):

    #tag_loc = (fit_camera_arr[i] - fit_camera_arr[0])*1.0*tag_dimension_in_mm/(fit_camera_arr[-1] - final_camera_arr[0])*1.0
    
    tag_loc_1 = camera_arr_interp_x[i] - camera_arr_interp_x[0]
    tag_loc_2 = camera_arr_interp_y[i] - camera_arr_interp_y[0]
    fhandle.write( str(phase_arr_interp[i]) + "," + str(tag_loc_1) + "," + str(tag_loc_2) + "," + str(time_interp_for_phase_data_arr[i]) + "\n" )

fhandle.close()

fhandle = open("phase_calib_average_custom1.dat", 'w')

for i in range( len(time_interp_for_phase_data_arr) ):

    #tag_loc_1 = (abs(camera_arr_interp_y[i] - camera_arr_interp_y[0]))/(abs(camera_arr_interp_y[-1] - camera_arr_interp_y[0]))*1.0
    tag_loc_1 = camera_arr_interp_x[i] - camera_arr_interp_x[0]
    tag_loc_2 = camera_arr_interp_y[i] - camera_arr_interp_y[0]
    
    fhandle.write( str(phase_arr_interp[i]) + "," + str(tag_loc_1) + "," + str(tag_loc_2) + "," + str(time_interp_for_phase_data_arr[i]) +"\n" )

fhandle.close()
