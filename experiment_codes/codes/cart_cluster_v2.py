#This program does kmeans clustering based on temporal features and it differes from v1 with different features and printing routine and also reference tag file

#!/usr/bin/python
import sys 
import operator
import numpy as np
import scipy.signal
import scipy.cluster.vq

if len(sys.argv) < 4:
    print "python <cart_file> <outside_file> <cart_id_file> <reference_tag_file>"
    sys.exit(-1)


fhandle = open( sys.argv[1], 'r')
cartLines = fhandle.readlines()
fhandle.close()

fhandle = open( sys.argv[2], 'r')
outLines = fhandle.readlines()
fhandle.close()

fhandle = open( sys.argv[3], 'r')
cart_ids = fhandle.readlines()
fhandle.close()


fhandle = open( sys.argv[4], 'r')
ref_ids = fhandle.readlines()
fhandle.close()

cart_id_arr = []
ref_id_arr = []
epc_data_dict = {}
epc_first_data = {}
epc_channel_first_freq_dict = {}

for line in cartLines:
    l_str = line.strip().split(',')
            
    epc = str(l_str[0])
    tstamp = float(l_str[3])
    doppler = float(l_str[5])
    rssi = float(l_str[7])
    channel = float(l_str[8])
    phase = float(l_str[9])

    if epc not in epc_data_dict:
        epc_data_dict[epc] = [[tstamp,doppler,rssi,phase,channel]]
        epc_first_data[epc] = tstamp
        
        epc_channel_first_freq_dict[epc] ={}
        epc_channel_first_freq_dict[epc][channel] = {}
        epc_channel_first_freq_dict[epc][channel][tstamp] = phase
    
    else:
        epc_data_dict[epc].append([tstamp,doppler,rssi,phase,channel])

        if channel not in epc_channel_first_freq_dict[epc]:
            epc_channel_first_freq_dict[epc][channel] = {}
            epc_channel_first_freq_dict[epc][channel][tstamp] = phase
        else:
            epc_channel_first_freq_dict[epc][channel][tstamp] = phase


for line in outLines:
    l_str = line.strip().split(',')
            
    epc = str(l_str[0])
    tstamp = float(l_str[3])
    doppler = float(l_str[5])
    rssi = float(l_str[7])
    channel = float(l_str[8])
    phase = float(l_str[9])

    if epc not in epc_data_dict:
        epc_data_dict[epc] = [[tstamp,doppler,rssi,phase,channel]]
        epc_first_data[epc] = tstamp

        epc_channel_first_freq_dict[epc] ={}
        epc_channel_first_freq_dict[epc][channel] = {}
        epc_channel_first_freq_dict[epc][channel][tstamp] = phase

    else:
        epc_data_dict[epc].append([tstamp,doppler,rssi,phase,channel])

        if channel not in epc_channel_first_freq_dict[epc]:
            epc_channel_first_freq_dict[epc][channel] = {}
            epc_channel_first_freq_dict[epc][channel][tstamp] = phase
        else:
            epc_channel_first_freq_dict[epc][channel][tstamp] = phase


for cart in cart_ids:
    cart_id_arr.append(str(cart).strip())
    #iprint cart

for ref in ref_ids:
    ref_id_arr.append(str(ref).strip())
    #iprint cart

def modify_phase( val ):

    if val == np.pi or val == -np.pi or val == 2*np.pi or val == -2*np.pi:
        return 0.0
    else:
        return val

data_epc = []

feature_matrix = []
sample_window = 1.0

for elem in epc_data_dict:
    
    index = 0
    data_arr = []
    last_time = float(epc_first_data[elem])
    epc_data_arr = epc_data_dict[elem]

    #print len(epc_data_arr)

    while index < (len(epc_data_arr)):
        
        doppler_arr = []
        rssi_arr = []
        phase_arr = []
        diff = 0.0
        cur_time = 0.0

        #Adding 5 Second Sample
        while diff < sample_window:
    
            if index >= (len(epc_data_arr)):
                break

            cur_time = float(epc_data_arr[index][0])
            channel = epc_data_arr[index][4]

            diff = (cur_time - last_time)*1.0 /1000000.0
            #print diff

            doppler_arr.append(epc_data_dict[elem][index][1])
            rssi_arr.append(epc_data_dict[elem][index][2])
            phase_arr.append(modify_phase(epc_data_dict[elem][index][3] - epc_channel_first_freq_dict[elem][channel][cur_time]))

            index += 1

        last_time = cur_time

        dop_res_mean = 0.0
        rssi_res_mean = 0.0
        phase_res_mean = 0.0
        dop_res_median = 0.0
        rssi_res_median = 0.0
        phase_res_median = 0.0
        dop_res_std = 0.0
        rssi_res_std = 0.0
        phase_res_std = 0.0

        read_res = 0.0

        if len(doppler_arr) > 0:
            dop_res_mean = np.mean(np.array(doppler_arr))
            dop_res_median = np.median(np.array(doppler_arr))
            dop_res_std = np.std(np.array(doppler_arr))
            read_res = (len(doppler_arr)*1.0)/sample_window

        if len(rssi_arr) > 0:
            rssi_res_mean = np.mean(np.array(rssi_arr))
            rssi_res_median = np.median(np.array(rssi_arr))
            rssi_res_std = np.std(np.array(rssi_arr))


        if len(phase_arr) > 0:
            phase_res_mean = np.mean(np.array(phase_arr))
            phase_res_median = np.median(np.array(phase_arr))
            phase_res_std = np.std(np.array(phase_arr))

        #print len(doppler_arr)
        '''
        data_arr.append(dop_res_mean)
        data_arr.append(dop_res_median)
        data_arr.append(dop_res_std)
        data_arr.append(rssi_res_mean)
        data_arr.append(rssi_res_median)
        data_arr.append(rssi_res_std)
        data_arr.append(phase_res_mean)
        data_arr.append(phase_res_median)
        data_arr.append(phase_res_std)
        '''
        data_arr.append(dop_res_median)
        #data_arr.append(phase_res_median)
        #data_arr.append(rssi_res_median)
        #data_arr.append(dop_res_std)
        data_arr.append(read_res)
        #print index, len(epc_data_arr)

    feature_matrix.append(data_arr)
    data_epc.append( elem )

#print doppler_epc
#print doppler_arr
#print cart_id_arr


#print "Before"
#for indx in range(len(feature_matrix)):
 #   print len(feature_matrix[indx])

max_row_length = 0

for lst in feature_matrix:

    if max_row_length < len(lst):
        max_row_length = len(lst)

for indx in range(len(feature_matrix)):
    
    #print "Len :", len(feature_matrix[indx]) 
    #print data_epc[indx], feature_matrix[indx]

    if len(feature_matrix[indx]) < max_row_length:

        diff = max_row_length-len(feature_matrix[indx]) 
        #print "Diff :", diff

        while diff > 0:
            feature_matrix[indx].append(-1.0)
            diff -= 1

#print "After"
#for indx in range(len(feature_matrix)):
 #   print len(feature_matrix[indx])

#print feature_matrix

[X, label_arr] = scipy.cluster.vq.kmeans2( np.array(feature_matrix), 2, minit='points')

#print label_arr
#print len(label_arr)

count = 0

cluster1_epc = []
cluster2_epc = []
cluster3_epc = []
cluster4_epc = []
cluster5_epc = []
cluster6_epc = []
cluster7_epc = []

for elem in label_arr:

    #print elem
    if int(elem) == 1:
        #print doppler_epc[count]
        #if str(data_epc[count]) in cart_id_arr:
            #print "Match"
            #print data_epc[count]
        cluster1_epc.append(data_epc[count])
        #print "1 : ", data_epc[count]
    elif int(elem) == 0:
        cluster2_epc.append(data_epc[count])
        #print "2 : ", data_epc[count]
   
    '''
    elif int(elem) == 2:
        cluster3_epc.append(data_epc[count])
    elif int(elem) == 3:
        cluster4_epc.append(data_epc[count])
    elif int(elem) == 4:
        cluster5_epc.append(data_epc[count])
    elif int(elem) == 5:
        cluster6_epc.append(data_epc[count])
    elif int(elem) == 6:
        cluster7_epc.append(data_epc[count])
    '''

    count += 1

print "Match in Cluster 1"
print "Size : " + str(len(cluster1_epc))
match1 = 0

for elem in cart_id_arr:
    if elem in cluster1_epc:
        match1 += 1

for elem in ref_id_arr:
    if elem in cluster1_epc:
        match1 += 1

print match1

print "Match in Cluster 2"
print "Size : " + str(len(cluster2_epc))
match2 = 0
    
for elem in cart_id_arr:
    if elem in cluster2_epc:
        match2 += 1

for elem in ref_id_arr:
    if elem in cluster2_epc:
        match2 += 1

print match2

'''
print "Match in Cluster 3"
print "Size : " + str(len(cluster3_epc))
match3 = 0
    
for elem in cart_id_arr:
    if elem in cluster3_epc:
        match3 += 1

print match3

print "Match in Cluster 4"
print "Size : " + str(len(cluster4_epc))
match4 = 0
    
for elem in cart_id_arr:
    if elem in cluster4_epc:
        match4 += 1

print match4

print "Match in Cluster 5"
print "Size : " + str(len(cluster5_epc))
match5 = 0
    
for elem in cart_id_arr:
    if elem in cluster5_epc:
        match5 += 1

print match5

print "Match in Cluster 6"
print "Size : " + str(len(cluster3_epc))
match6 = 0
    
for elem in cart_id_arr:
    if elem in cluster6_epc:
        match6 += 1

print match6

print "Match in Cluster 7"
print "Size : " + str(len(cluster7_epc))
match7 = 0
    
for elem in cart_id_arr:
    if elem in cluster7_epc:
        match7 += 1

print match7
'''
