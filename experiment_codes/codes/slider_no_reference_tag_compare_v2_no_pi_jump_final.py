#File which calibrates the frequency offset based on the order of frequency jumped and clubs all the phases to a single frequency for US reader
#!/usr/bin/python
import sys
import operator
import numpy as np
import scipy.signal
from sklearn.cluster import MeanShift, estimate_bandwidth
from sklearn import preprocessing
from sklearn.cluster import AgglomerativeClustering
import scipy.cluster.vq
import collections

if len(sys.argv) < 3:
    print("python <data_file> <time_file>")
    sys.exit(-1)

def isOutlier( val ):

    if (val >= np.pi and val <= (np.pi + 0.1)) or ( val <= -np.pi and val >= (-np.pi - 0.1)):
        return True

    if (val <= np.pi and val >= (np.pi - 0.1)) or ( val >= -np.pi and val <= (-np.pi + 0.1)):
        return True

    if (val >= 2*np.pi and val <= (2*np.pi + 0.1)) or ( val <= -2*np.pi and val >= (-2*np.pi - 0.1)):
        return True

    if (val <= 2*np.pi and val >= (2*np.pi - 0.1)) or ( val >= -2*np.pi and val <= (-2*np.pi + 0.1)):
        return True

    if val >= 2*np.pi:
        return True

    if val <= -2*np.pi:
        return True

    return False


fhandle = open( sys.argv[1], 'r')
fLines1 = fhandle.readlines()
fhandle.close()

fhandle = open( sys.argv[2], 'r')
fLines2 = fhandle.readlines()
fhandle.close()


freq_offset_arr = []

channel_id_dict = {}

start_channel = 902.75
channel_id_dict[start_channel] = 0

for i in list(range(1,50)):
    channel_id_dict[start_channel+0.5*i] = i

#print(channel_id_dict)
#print(freq_offset_arr)

data_dict = {}
data_dict_ref = {}
time_list = []

for line in fLines2:
    l_str = line.strip().split(':')
    time_list.append( float(l_str[0]) )

time_diff_arr = []
last_time = time_list[0]

for i in range(1,len(time_list)):
    time_diff_arr.append(time_list[i] - last_time)
    last_time = time_list[i]


#Reading of files and separating the data into two dicts : one for calibration and one for testing

print(time_diff_arr)

first_flag = True
last_time = 0.0
posn_count = 0
count = 1
channel_order_arr = []
channel_order_dict = {}
index = 0
last_channel = 0.0
channel_seq_arr = []

for line in fLines1:
    l_str = line.strip().split(',')
    epc = str(l_str[0])
    tstamp = float(l_str[3])
    channel = float(l_str[8])
    phase = float(l_str[9])
    #print(channel)
    
    #if epc == "E28011606000020612B034AC" :
    #if epc == "E28011606000020612B000ED" :

    if epc == "E28011606000020612AEE43D": #Right Tag
    #if epc == "E28011606000020612AF52AD": #Middle Tag
    #if epc == "E28011606000020612AEE42D": #Left Tag
        count += 1
        if first_flag == True:

            last_time = tstamp
            data_dict_ref[channel] = [phase]
            channel_order_arr.append(channel)
            channel_order_dict[channel] = index
            #print(channel,index)
            index = index + 1
            first_flag = False
            channel_seq_arr.append(channel)
            last_channel = channel

        else:
            if last_channel != channel:
                channel_seq_arr.append(channel)
                last_channel = channel

            time_diff = ((tstamp-last_time)*1.0)/1000000.0

            if posn_count > 0:
                if time_diff > time_diff_arr[1]:
                    break
            
            if posn_count == 0:

                if time_diff > time_diff_arr[0]:
                    posn_count = 1
                    last_time = tstamp

            if posn_count == 0:
                if channel not in data_dict_ref:
                    data_dict_ref[channel] = [phase]
                    channel_order_arr.append(channel)
                    channel_order_dict[channel] = index
                    #print(channel,index)
                    index = index + 1
                else:
                    data_dict_ref[channel].append(phase)
            else:
                data_dict[tstamp] = [channel, phase]

#for ch in channel_seq_arr:
 #   print(ch)
#print(channel_order_dict.keys())

#Findind out calibrated phase differences among channel
phase_peak_arr = []
keylist = sorted(list(data_dict_ref.keys()))

#Going by the order of channel not by the sorted list
for channel in channel_order_arr:
    phase_peak_arr.append( np.median(np.array(data_dict_ref[channel])) )

#for ch in keylist:
 #   print(ch, np.median(np.array(data_dict_ref[ch])))

#Calculating the frequency offsets per channel
count = 1
prev_diff = 0.0

freq_offset_arr = []

for i in range( len(phase_peak_arr) - 1):
    diff = phase_peak_arr[i+1] - phase_peak_arr[i]

    if diff == 0.0:
        diff = prev_diff
    
    #if diff > np.pi:
     #   diff = phase_peak_arr[i+1] - 2*np.pi - 1.35*prev_diff

    freq_offset_arr.append(diff*-1.0)

    count += 1
    prev_diff = diff

#print(freq_offset_arr)

channel_phase_dict = {}

for channel in data_dict_ref:
    channel_phase_dict[channel] = np.median(np.array(data_dict_ref[channel]))
    #print(str(channel), ":", str(np.mean(np.array(data_dict_ref[channel]))), "\n")

#Finding the phase difference by going time increasing wise
ordered_data_dict = collections.OrderedDict(sorted(data_dict.items()))

prev_val = 0.0 
phase_dict_arr = []
base_channel = 0.0
first_flag = True

#print("Channel, Base_Channel, Actual_Phase, Scaled_Phase, Diff")

for key, val in ordered_data_dict.items():

    channel = val[0]
    elem = val[1]

    #Correction Calculation
    if first_flag == True:
        base_channel = 921.25
        first_flag = False

    diff = 0.0
    
    #print("Here")
    #print(channel, base_channel, channel_order_dict[base_channel], channel_order_dict[channel])
    
    if channel_order_dict[channel] > channel_order_dict[base_channel]:

        mov_list = list(range(channel_order_dict[base_channel], channel_order_dict[channel] ))
        indx = len(mov_list)-1

        while indx >= 0:
            i = mov_list[indx]
            indx = indx - 1
            #print("IF:", i)
            diff = diff + freq_offset_arr[i]
    else:
        mov_list = list(range(channel_order_dict[channel], channel_order_dict[base_channel] ))
        
        for i in mov_list:
            #print("ELSE:", i)
            diff = diff + freq_offset_arr[i]*(-1.0)

    correction = diff


    new_val = (float(elem) + correction)%(2*np.pi)
    #print(str(channel), str(elem), str(new_val))
    #print(elem, ":", new_val)

    if first_flag == False:
        if ( isOutlier(abs(new_val - prev_val))  == True ) :
            new_val = prev_val

    prev_val = new_val
    #if channel == 908.75:
    phase_dict_arr.append(new_val)


diff_arr = phase_dict_arr
win_size = 301
diff_arr_smoothed = scipy.signal.medfilt(diff_arr, win_size)
#diff_arr_smoothed = scipy.signal.wiener(diff_arr, win_size)

diff_arr_smoothed_mean = []

for i in range(len(diff_arr)-win_size):
    diff_arr_smoothed_mean.append(np.mean(np.array(diff_arr[i:i+win_size])))

fhandle = open('line-plot-phase-vs-coverage.dat', 'w')


#Kalman Filtering
n_iter = len(diff_arr)
sz = (n_iter,) # size of array
z = diff_arr

Q = 0.0032 # process variance

# allocate space for arrays
xhat=np.zeros(sz)      # a posteri estimate of x
P=np.zeros(sz)         # a posteri error estimate
xhatminus=np.zeros(sz) # a priori estimate of x
Pminus=np.zeros(sz)    # a priori error estimate
K=np.zeros(sz)         # gain or blending factor

R = 0.0032 # estimate of measurement variance, change to see effect

# intial guesses
xhat[0] = 0.0
P[0] = 1.0

for k in range(1,n_iter):
    # time update
    xhatminus[k] = xhat[k-1]
    Pminus[k] = P[k-1]+Q

    # measurement update
    K[k] = Pminus[k]/( Pminus[k]+R )
    xhat[k] = xhatminus[k]+K[k]*(z[k]-xhatminus[k])
    P[k] = (1-K[k])*Pminus[k]

diff_arr_smoothed_kalman = scipy.signal.medfilt(z, win_size)
count = 1

for elem in diff_arr_smoothed:
    #print(elem)
    fhandle.write( str(count) + " " + str(elem) + "\n")
    count += 1

fhandle.close()
