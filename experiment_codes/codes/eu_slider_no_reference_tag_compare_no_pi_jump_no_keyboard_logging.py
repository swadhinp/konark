#phase_dist_channelwise.py will create per channel phase file
#

#!/usr/bin/python
import sys
import operator
import numpy as np
import scipy.signal
from sklearn.cluster import MeanShift, estimate_bandwidth
from sklearn import preprocessing
from sklearn.cluster import AgglomerativeClustering
import scipy.cluster.vq
import collections


if len(sys.argv) < 2:
    print("python <data_file> <per_channel_phase_file>")
    sys.exit(-1)

def isOutlier( val ):
    
    #if (val >= np.pi and val <= (np.pi + 0.1)) or ( val <= -np.pi and val >= (-np.pi - 0.1)):
        #print "Pi", val
     #   return True

    #if (val <= np.pi and val >= (np.pi - 0.1)) or ( val >= -np.pi and val <= (-np.pi + 0.1)):
        #print "Pi", val
    #   return True

    if (val >= 2*np.pi and val <= (2*np.pi + 0.1)) or ( val <= -2*np.pi and val >= (-2*np.pi - 0.1)):
        #print "2Pi", val
        return True

    if val >= 2*np.pi:
        return True
    if val <= -2*np.pi:
        return True

    if (val <= 2*np.pi and val >= (2*np.pi - 0.1)) or ( val >= -2*np.pi and val <= (-2*np.pi + 0.1)):
        #print "2Pi", val
        return True

    return False


fhandle = open( sys.argv[1], 'r')
fLines1 = fhandle.readlines()
fhandle.close()

fhandle = open( sys.argv[2], 'r')
fLines3 = fhandle.readlines()
fhandle.close()

channel_phase_dict = {}

for line in fLines3:
    
    l_lst = line.strip().split(':')
    channel_phase_dict[float(l_lst[0])] = float(l_lst[1])

channel_id_dict = {}

start_channel = 902.75
channel_id_dict[start_channel] = 0

for i in list(range(1,50)):
    channel_id_dict[start_channel+0.5*i] = i

#print(channel_id_dict)
#print(freq_offset_arr)

data_dict = {}
data_dict_with_tstamp = {}

count = 1

for line in fLines1:
    l_str = line.strip().split(',')
    epc = str(l_str[0])
    tstamp = float(l_str[3])
    channel = float(l_str[8])
    phase = float(l_str[9])

    
    #if epc == "E28011606000020612B034AC" :
    #if epc == "E28011606000020612B000ED" :
    #if epc == "E28011606000020612AF52AD" :
    if epc == "E28011606000020612AEE43D" :
        count += 1

        if channel not in data_dict:
            data_dict[channel] = [ float(phase)  ]
        else:
            data_dict[channel].append( float(phase)  )

        data_dict_with_tstamp[tstamp] = [channel, phase]

#print(data_dict.keys())

#Modifying all phases of different positions to a specific channel

ordered_data_dict = collections.OrderedDict(sorted(data_dict_with_tstamp.items()))

keylist = sorted(list(data_dict.keys()))
#Special handling of the first one
base_channel = keylist[0]

phase_dict_arr = []

window_length = 150
indx = 0
last_indx = indx + window_length

prev_val = 0.0
first_flag = True
channel_arr = []

correction_factor =  0.01256637061 #Assuming 30cm distance and 2*pi*f*2d/c formula (in radian)

for key, val in ordered_data_dict.items():

    #print(key)
    channel = val[0]
    elem = val[1]

    if first_flag == True:
        first_flag = False
        channel_arr.append(elem)
        prev_channel = channel
        continue
    else:
        if channel == prev_channel:
            channel_arr.append(elem)
            continue


    '''

    indx = 0
    last_indx = indx + window_length

    #print(channel)

    while last_indx < len(data_dict[channel]):
        phase_arr = data_dict[channel][indx:last_indx]
     
        #print(indx, last_indx)

        indx = last_indx + 1
        last_indx = indx + window_length
        elem = np.mean(np.array(phase_arr))

        new_val = (float(elem) - channel_phase_dict[channel])%(2*np.pi)

        if ( isOutlier(new_val) == True ):
            new_val = prev_val

        phase_dict_arr.append(new_val)
        prev_val = new_val

    #For the remaining part
    phase_arr = data_dict[channel][len(data_dict[channel])-1-window_length:len(data_dict[channel])-1]
    elem = np.mean(np.array(phase_arr))
    new_val = (float(elem) - channel_phase_dict[channel])%(2*np.pi)
    '''

    #print("Time")
    #print(len(channel_arr))

    elem = np.mean(np.array(channel_arr))

    print("Channel : ", str(channel), "Current Phase : ", str(elem), "Callibrated Phase", str(channel_phase_dict[channel]) )

    new_val = (float(elem) - channel_phase_dict[channel])%(2*np.pi)
    offset = (channel - prev_channel)*correction_factor

    new_val = (float(new_val) - offset)%2*np.pi

    if ( isOutlier(new_val) == True ):
        print("Outlier")
        new_val = prev_val

    prev_val = new_val

    
    phase_dict_arr.append(new_val)
    channel_arr = []
    prev_channel = channel
#for elem in data_dict[base_channel]:
 #   phase_dict_arr.append(elem)


#Phase Distribution Debugging
'''
fhandle = open('phase-dist-2.dat', 'w')
for elem in phase_dict_arr[0]:
    fhandle.write( str(elem) + "\n")
fhandle.close()
'''


fhandle = open('line-plot-phase-vs-coverage.dat', 'w')

#print (len(phase_dict_arr))

#print(cluster1_arr)


diff_arr = phase_dict_arr
win_size = 21
diff_arr_smoothed = scipy.signal.medfilt(diff_arr, win_size)
#diff_arr_smoothed = scipy.signal.wiener(diff_arr, win_size)

count = 1
for elem in diff_arr:
    fhandle.write( str(count) + " " + str(elem) + "\n")
    count += 1
fhandle.close()
