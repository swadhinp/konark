#!/usr/bin/python
import sys
import operator
import numpy as np
import scipy.signal

if len(sys.argv) < 5:
    print("python <data_file_1> <data_file_2> <data_file_3> <data_file_4> <out_file>")
    sys.exit(-1)

data_dict = {}

for i in range(4):
    fhandle = open( sys.argv[i+1], 'r')
    flines = fhandle.readlines()
    fhandle.close()
    
    data_dict[i] = []
    for elem in flines:
        data_dict[i].append( float(elem.strip().split(' ')[1]) )

fhandle = open( sys.argv[5], 'w' )
print(data_dict[0])
print(data_dict[1])
print(data_dict[2])
print(data_dict[3])

for i in range(len(data_dict[0])):
    #print(data_dict[0])
    val = np.mean( np.array ( [ data_dict[0][i], data_dict[1][i], data_dict[2][i], data_dict[3][i] ] ) )
    fhandle.write( str(val) + "\n")

fhandle.close()
