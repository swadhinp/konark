#Here we will use reference tags inside and outside cart
#This is the final version of cart clustering code

#!/usr/bin/python
import sys 
import operator
import numpy as np
import scipy.signal
import scipy.cluster.vq

if len(sys.argv) < 5:
    print("python <cart_file> <outside_file> <cart_id_file> <reference_tag_cart_file> <refernce_tag_outside_file>")
    sys.exit(-1)


fhandle = open( sys.argv[1], 'r')
cartLines = fhandle.readlines()
fhandle.close()

fhandle = open( sys.argv[2], 'r')
outLines = fhandle.readlines()
fhandle.close()

fhandle = open( sys.argv[3], 'r')
cart_ids = fhandle.readlines()
fhandle.close()


fhandle = open( sys.argv[4], 'r')
ref_ids = fhandle.readlines()
fhandle.close()

fhandle = open( sys.argv[5], 'r')
out_ref_ids = fhandle.readlines()
fhandle.close()


cart_id_arr = []
ref_id_arr = []
out_ref_id_arr = []

epc_data_dict = {}
epc_first_data = {}

tstamp_arr = []
epc_arr = []

ref_id_arr = []
cart_id_arr = []
out_ref_id_arr = []

for cart in cart_ids:
    cart_id_arr.append(str(cart).strip())

for ref in ref_ids:
    ref_id_arr.append(str(ref).strip())

for out_ref in out_ref_ids:
    out_ref_id_arr.append(str(out_ref).strip())

#Data from Cart Tags
for line in cartLines:
    l_str = line.strip().split(',')
            
    epc = str(l_str[0])
    tstamp = float(l_str[3])
    doppler = float(l_str[5])
    rssi = float(l_str[7])
    channel = float(l_str[8])
    phase = float(l_str[9])

    tstamp_arr.append( tstamp )

    if epc not in epc_data_dict:
        epc_data_dict[epc] = [[tstamp,doppler,rssi,phase,channel]]
        epc_first_data[epc] = tstamp
    	epc_arr.append( epc )
        
    else:
        epc_data_dict[epc].append([tstamp,doppler,rssi,phase,channel])


#Data from Outside Tags
for line in outLines:
    l_str = line.strip().split(',')
            
    epc = str(l_str[0])
    tstamp = float(l_str[3])
    doppler = float(l_str[5])
    rssi = float(l_str[7])
    channel = float(l_str[8])
    phase = float(l_str[9])

    tstamp_arr.append( tstamp )
    
    if epc not in epc_data_dict:
        epc_data_dict[epc] = [[tstamp,doppler,rssi,phase,channel]]
        epc_first_data[epc] = tstamp
    	epc_arr.append( epc )

    else:
        epc_data_dict[epc].append([tstamp,doppler,rssi,phase,channel])


doppler_tag_dict = {}

for epc in epc_data_dict:

    data_arr = epc_data_dict[epc]
    
    temp_arr = []
    for elem in data_arr:
        temp_arr.append(elem[3])

    doppler_tag_dict[epc] = np.std(np.array((temp_arr)))

sorted_epc_by_val = sorted(doppler_tag_dict.items(), key=operator.itemgetter(1))


for elem in sorted_epc_by_val:
    if elem[0] not in ref_id_arr:
        if elem[0] not in out_ref_id_arr:
            if elem[0] not in cart_id_arr:
                print elem
#print sorted_epc_by_val
#print data_epc_arr[doppler_tag_std_arr.index(max(doppler_tag_std_arr))]
