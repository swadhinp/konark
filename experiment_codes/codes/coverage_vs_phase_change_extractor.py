#!/usr/bin/python
import sys
import operator
import numpy as np
import scipy.signal
from scipy import stats
from collections import Counter

if len(sys.argv) < 3:
    print "python <ref_data_file_1> <data_file_1> <position>"
    sys.exit(-1)

fhandle = open( sys.argv[1], 'r')
fLines1 = fhandle.readlines()
fhandle.close()

fhandle = open( sys.argv[2], 'r')
fLines2 = fhandle.readlines()
fhandle.close()

position = int(sys.argv[3])

data_dict1 = {}

ref_data_dict1 = {}

def findMode( nArr ):
    bins = np.arange( np.min(nArr)-1, np.max(nArr)+1, 0.1)
    hist = np.histogram( nArr, bins=bins )
    mode = hist[ 1 ][ np.argmax(hist[0]) - 1 ]
    return mode

def isOutlier( val ):
    
    if (val >= np.pi and val <= (np.pi + 0.01)) or ( val <= -np.pi and val >= (-np.pi - 0.01)):
        #print "Pi", val
        return True

    if (val <= np.pi and val >= (np.pi - 0.01)) or ( val >= -np.pi and val <= (-np.pi + 0.01)):
        #print "Pi", val
        return True

    if (val >= 2*np.pi and val <= (2*np.pi + 0.01)) or ( val <= -2*np.pi and val >= (-2*np.pi - 0.01)):
        #print "2Pi", val
        return True

    if val >= 2*np.pi:
        return True
    if val <= -2*np.pi:
        return True

    if (val <= 2*np.pi and val >= (2*np.pi - 0.01)) or ( val >= -2*np.pi and val <= (-2*np.pi + 0.01)):
        #print "2Pi", val
        return True

    return False


first_flag = True
hop_count = 0
last_channel = 0.0

for line in fLines1:
    l_str = line.strip().split(',')
    epc = str(l_str[0])
    tstamp = l_str[3]
    channel = l_str[8]
    phase = l_str[9]

    if epc == "E28011606000020612B0327D" :

        '''
        if first_flag == True:
            last_channel = channel
            first_flag = False
            key = channel + "_" + str(hop_count)
            ref_data_dict1[key] = [ float(phase) ]
            continue
        else:
            if last_channel != channel:
                #print "Ch " , last_channel, channel
                hop_count += 1

            last_channel = channel
            key = channel + "_" + str(hop_count)
        '''
    #key channel switch
        if channel not in ref_data_dict1:
            ref_data_dict1[channel] = [ float(phase) ]
        else:
            ref_data_dict1[channel].append( float(phase) )
    else:
        print "Terrible Error"

#print "Here1"
first_flag = True
hop_count = 0
last_channel = 0.0

for line in fLines2:
    l_str = line.strip().split(',')
    epc = str(l_str[0])
    tstamp = l_str[3]
    channel = l_str[8]
    phase = l_str[9]

    if epc == "E28011606000020612B034AC" :
    
        '''
        if first_flag == True:
            last_channel = channel
            first_flag = False
            key = channel + "_" + str(hop_count)
            data_dict1[key] = [ float(phase) ]
            continue
        else:
            if last_channel != channel:
                #print "Ch " , last_channel, channel
                hop_count += 1

            last_channel = channel
            key = channel + "_" + str(hop_count)
        '''

        #key channel switch
        if channel not in data_dict1:
            data_dict1[channel] = [ float(phase) ]
        else:
            data_dict1[channel].append( float(phase) )
    else:
        print "Terrible Error"


#print "Data Dict1"
#print len(data_dict1.keys())
#print "Data Dict2"
#print len(data_dict2.keys())
#print "Ref Data Dict1"
#print len(ref_data_dict1.keys())
#print "Ref Data Dict2"
#print len(ref_data_dict2.keys())

#print "Data Dict1"
#for ch in data_dict1:
 #   print len(data_dict1[ch])

#print "Ref Data Dict1"
#for ch in ref_data_dict1:
 #   print len(ref_data_dict1[ch])

#print "Data Dict2"
#for ch in data_dict2:
    #print len(data_dict2[ch])


diff_phase_arr1 = []
diff_phase_arr2 = []

window_size = 5

median_arr1 = []
#median_arr2 = []

fhandle = open( "diff-phase.dat", 'w' )

for channel in ref_data_dict1:

    fhandle1 = open( str(channel) + "-ref.dat", 'w' )
    fhandle2 = open( str(channel) + "-tag.dat", 'w' )
    

    phase_arr1 = ref_data_dict1[channel]
    phase_arr2 = data_dict1[channel]

    q75_1, q25_1 = np.percentile(phase_arr1, [75 ,25])
    q75_2, q25_2 = np.percentile(phase_arr2, [75 ,25])
    #else:
     #   phase_arr2 = [0.0]

    #print channel
    #print phase_arr1
    #print phase_arr2
    #print channel, len(phase_arr1), len(phase_arr2)

    phase_arr1_remove_outlier = []
    phase_arr2_remove_outlier = []

    #Quartile based outlier removal
    for elem in phase_arr1:

        fhandle1.write( str(elem) + "\n")

        if (float(elem) >= q25_1) and (float(elem) <= q75_1):
            phase_arr1_remove_outlier.append(float(elem))
        #else:
         #   print "Ref : " + str(channel) + " " + str(elem) + "q1 : " + str(q25_1) + "q3: " + str(q75_1)

    fhandle1.close()

    for elem in phase_arr2:
        
        fhandle2.write( str(elem) + "\n")
        
        if (float(elem) >= q25_2) and (float(elem) <= q75_2):
            phase_arr2_remove_outlier.append(float(elem))
        #else:
         #   print "Tag : " + str(channel) + " " + str(elem) + "q1 : " + str(q25_2) + "q3: " + str(q75_2)

    fhandle2.close()

    phase_arr1_smoothed = []
    phase_arr2_smoothed = []

    if (isOutlier(phase_arr1[0]) == True):
        phase_arr1_remove_outlier.append(0.0)
        phase_arr1[0] = 0.0 #Update
    else:
        phase_arr1_remove_outlier.append(phase_arr1[0])

    for i in range(1,len(phase_arr1)):

        cur_phase = phase_arr1[i]
        an_phase = phase_arr1[i-1]
        
        diff_phase = float(cur_phase) - float(phase_arr1[i-1])
        #print "Diff", i, i-1, cur_phase, phase_arr1[i-1], isOutlier(diff_phase)

        if isOutlier(diff_phase) == True:
            phase_arr1_remove_outlier.append(float(phase_arr1[i-1])) #Copy the previous phase
            phase_arr1[i] = float(phase_arr1[i-1]) #Update

        else:
            phase_arr1_remove_outlier.append(cur_phase)

    
    if ( isOutlier(phase_arr2[0]) == True):
        phase_arr2_remove_outlier.append(0.0)
        phase_arr2[0] = 0.0 #Update
    else:
        phase_arr2_remove_outlier.append(phase_arr2[0])

    for i in range(1,len(phase_arr2)):

        cur_phase = phase_arr2[i]
        
        diff_phase = float(cur_phase) - float(phase_arr2[i-1])
        #print diff_phase
        
        if isOutlier(diff_phase) == True:
            phase_arr2_remove_outlier.append(float(phase_arr2[i-1])) #Copy the previous phase
            phase_arr2[i] = float(phase_arr2[i-1]) #Update
        else:
            phase_arr2_remove_outlier.append(cur_phase)
    
    #Directly applying Median Filter
    phase_arr1_smoothed = scipy.signal.medfilt(phase_arr1, window_size)
    phase_arr2_smoothed = scipy.signal.medfilt(phase_arr2, window_size)

    #median_arr1.append(np.median(np.array(phase_arr1)) - np.median(np.array(phase_arr2)))
    median_arr1.append(findMode(np.array(phase_arr1)) - findMode(np.array(phase_arr2)))
    #median_arr2.append(np.median(np.array(phase_arr2)))


    #phase_arr1_smoothed = scipy.signal.wiener(phase_arr1_remove_outlier, window_size)
    #phase_arr2_smoothed = scipy.signal.wiener(phase_arr2_remove_outlier, window_size)
    
    #phase_arr1_smoothed = scipy.signal.hilbert(phase_arr1)
    #phase_arr2_smoothed = scipy.signal.hilbert(phase_arr2)

    for i in range( min(len(phase_arr2_smoothed), len(phase_arr1_smoothed))):
        diff_phase_arr1.append( float(phase_arr2_smoothed[i]) - float(phase_arr1_smoothed[i]))

for elem in median_arr1:
    fhandle.write( str(elem) + "\n" )

fhandle.close()

print str(position) + " " + str(findMode( np.array(median_arr1) ))
