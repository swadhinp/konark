import os
import sys

if len(sys.argv) < 1:
    print "python *.py <channel_file>"
    sys.exit(-1)


fname = sys.argv[1]

fhandle = open( fname , 'r' )
fLines = fhandle.readlines()
fhandle.close()

#Getting All channels
channel_list = []

for lin in fLines:
    l_lst = lin.strip().split(':')
    channel_list.append( float(l_lst[1]) )

count = 1
for channel in channel_list:

    channel_data_fname = str(channel) + "_dist.dat"
    os.environ["PLOT_FILE"] = str(channel_data_fname)
    os.system("rm -rf phase_dist.eps")
    os.system("gnuplot per_channel_phase_dist.gp")
    os.system("epstopdf phase_dist.eps")
    os.system("mv phase_dist.pdf " + str(count) + "_" + str(channel) + "_phase_dist.pdf")
    count += 1

