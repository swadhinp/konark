#!/usr/bin/python
import sys
import operator
import numpy as np
import scipy.signal
from sklearn.cluster import MeanShift, estimate_bandwidth
from sklearn import preprocessing
from sklearn.cluster import AgglomerativeClustering
import scipy.cluster.vq

if len(sys.argv) < 3:
    print("python <ref_data_file_1> <data_file_1> <count_no>")
    sys.exit(-1)

fhandle = open( sys.argv[1], 'r')
fLines1 = fhandle.readlines()
fhandle.close()

fhandle = open( sys.argv[2], 'r')
fLines2 = fhandle.readlines()
fhandle.close()

count_cover = sys.argv[3]

data_dict1 = {}
ref_data_dict1 = {}


for line in fLines1:
    l_str = line.strip().split(',')
    epc = str(l_str[0])
    tstamp = l_str[3]
    channel = l_str[8]
    phase = l_str[9]

    if epc == "E28011606000020612B0327D" :
    #if epc == "E28011606000020612B0349C" :

        if channel not in ref_data_dict1:
            ref_data_dict1[channel] = [ float(phase)  ]
        else:
            ref_data_dict1[channel].append(float(phase)  )
        

for line in fLines2:
    l_str = line.strip().split(',')
    epc = str(l_str[0])
    tstamp = l_str[3]
    channel = l_str[8]
    phase = l_str[9]

    if epc == "E28011606000020612B034AC" :
    #if epc == "E28011606000020612B0349C" :
        
        if channel not in data_dict1:
            data_dict1[channel] = [ float(phase) ]
        else:
            data_dict1[channel].append( float(phase) )


tag_phase_arr1 = []
keylist = sorted(list(data_dict1.keys()))
#keylist.sort()
ref_phase_arr1 = []

diff_phase_arr1 = []

offset = 0.11

def find_cluster_centers( phase_arr ):

    arr = []
    for elem in phase_arr:
        arr.append( [float(elem)] )

    #MeanShift Clustering
    ms = MeanShift(bandwidth= 0.5)
    ms.fit(np.array(arr))

    cluster_centers = []

    '''
    if len(ms.cluster_centers_) > 2:

        arr1 = []    
        for elem in phase_arr:
            if ( float(elem) <= 2*np.pi and float(elem) >= 2*np.pi-0.1 ):
                continue
            else:
                arr1.append([float(elem)])

        ms1 = MeanShift(bandwidth= 0.8)
        ms1.fit(np.array(arr1))
        cluster_centers = ms1.cluster_centers_
    else:
        '''
    cluster_centers = ms.cluster_centers_

    return cluster_centers

def find_difference( arr ):
    v1 = arr[0]
    v2 = arr[1]

    max1 = v1 if v1 > v2 else v2
    min1 = v1 if v1 < v2 else v2

    v3 = arr[2]
    v4 = arr[3]

    max2 = v3 if v3 > v4 else v4
    min2 = v3 if v3 < v4 else v4

    x =((max1-max2) + (min1-min2))/2.0

    y = ((v1 - v3) + (v1 - v4) + (v2 - v3) + (v2 - v4))*1.0/4.0

    y = np.median(np.array([(v1 - v3) , (v1 - v4) , (v2 - v3) , (v2 - v4)]))

    return x


count = 0
diff_arr = []

#Special handling of the first one
base_channel = keylist[0]

phase_list = data_dict1[base_channel]

for elem in phase_list:
    tag_phase_arr1.append(float(elem))


phase_list = ref_data_dict1[base_channel]

for elem in phase_list:
    ref_phase_arr1.append(float(elem))


for index in range(1, len(keylist)):

    #print("Channel")
    #print(key)
    key = keylist[index]

    phase_arr1 = data_dict1[key]
    phase_arr2 = ref_data_dict1[key]

    '''
    #Start : Channel wise Cluster Difference
    cluster_centers1 = find_cluster_centers( phase_arr1 )
    cluster_centers2 = find_cluster_centers( phase_arr2 )

    c1 = 0.0
    c2 = 0.0

    if( len(cluster_centers1) < 2 ):
        c1 = c2 = cluster_centers1[0][0]
    else:
        c1 = cluster_centers1[0][0]
        c2 = cluster_centers1[1][0]

    c3 = 0.0
    c4 = 0.0
    
    if( len(cluster_centers2) < 2 ):
        c3 = c4 = cluster_centers2[0][0]
    else:
        c3 = cluster_centers2[0][0]
        c4 = cluster_centers2[1][0]

    diff1 = find_difference( [c1,c2,c3,c4] )
    diff_arr.append(diff1)

    #End : Channel wise Cluster Difference
    '''
    #print("Clusters")
    #print (cluster_centers1)
    #print (cluster_centers2)

    diff = int(((float(key) - float(base_channel))*1.0)/0.5)

    #print("Tag", offset*count)
    for elem in phase_arr1:

        correction = offset*diff
        new_val = (float(elem) + correction)%(2*np.pi)
        #new_val = (float(elem) + correction)
        #print( "Old : " + str(float(elem)) )
        #print( "New : " + str(new_val) )
        tag_phase_arr1.append(new_val)

    #print("Ref")
    for elem in phase_arr2:

        correction = offset*diff
        new_val = (float(elem) + correction)%(2*np.pi)
        #new_val = (float(elem) + correction)
        #print( "Old : " + str(float(elem)) )
        #print( "New : " + str(new_val) )
        ref_phase_arr1.append(new_val)

    count += 1

#KMeans Clustering

#print("KMeans")
#[ cluster_centers1, X ] = scipy.cluster.vq.kmeans2( np.array(tag_phase_arr1), 2)
#[ cluster_centers2, Y ] = scipy.cluster.vq.kmeans2( np.array(ref_phase_arr1), 2)
#print (cluster_centers2)
#print (cluster_centers1)
#diff1 = find_difference( [cluster_centers1[0], cluster_centers1[1], cluster_centers2[0], cluster_centers2[1] ])
#print (count_cover + " " + str(diff1))

#Phase Distribution Debugging
fhandle = open('phase-dist-1.dat', 'w')
for elem in tag_phase_arr1:
    fhandle.write( str(elem) + "\n")
fhandle.close()


fhandle = open('phase-dist-2.dat', 'w')
for elem in ref_phase_arr1:
    fhandle.write( str(elem) + "\n")
fhandle.close()

cluster_centers1 = find_cluster_centers( tag_phase_arr1 )
cluster_centers2 = find_cluster_centers( ref_phase_arr1 )

c1 = 0.0
c2 = 0.0

if ( len(cluster_centers1) > 2 ):
    a = sorted( [ cluster_centers1[0][0], cluster_centers1[1][0], cluster_centers1[2][0] ] )
    c1 = ((float(a[0])+(np.pi*2.0) + float(a[2]))/2.0)%(np.pi*2.0)
    c2 = a[1]
else:
    c1 = cluster_centers1[0][0]
    c2 = cluster_centers1[1][0]

c3 = 0.0
c4 = 0.0

if ( len(cluster_centers2) > 2 ):
    a = sorted( [ cluster_centers2[0][0], cluster_centers2[1][0], cluster_centers2[2][0] ] )
    c3 = ((float(a[0])+ (np.pi*2.0) + float(a[2]))/2.0)%(np.pi*2.0)
    c4 = a[1]
else:
    c3 = cluster_centers2[0][0]
    c4 = cluster_centers2[1][0]

#print("MeanShift")
#print (cluster_centers2)
#print (cluster_centers1)

#print (c3,c4)
#print (c1,c2)

diff1 = find_difference( [c1, c2, c3, c4])

print (count_cover + " " + str(diff1))
#print (count_cover + " " + str(np.median(np.array(diff_arr))))

