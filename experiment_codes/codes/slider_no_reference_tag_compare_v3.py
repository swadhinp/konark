#!/usr/bin/python
import sys
import operator
import numpy as np
import scipy.signal
from sklearn.cluster import MeanShift, estimate_bandwidth
from sklearn import preprocessing
from sklearn.cluster import AgglomerativeClustering
import scipy.cluster.vq

if len(sys.argv) < 2:
    print("python <data_file> <time_file>")
    sys.exit(-1)

fhandle = open( sys.argv[1], 'r')
fLines1 = fhandle.readlines()
fhandle.close()

fhandle = open( sys.argv[2], 'r')
fLines2 = fhandle.readlines()
fhandle.close()


def isOutlier( val ):
    
    if (val >= np.pi and val <= (np.pi + 0.01)) or ( val <= -np.pi and val >= (-np.pi - 0.01)):
        #print "Pi", val
        return True

    if (val <= np.pi and val >= (np.pi - 0.01)) or ( val >= -np.pi and val <= (-np.pi + 0.01)):
        #print "Pi", val
        return True

    if (val >= 2*np.pi and val <= (2*np.pi + 0.01)) or ( val <= -2*np.pi and val >= (-2*np.pi - 0.01)):
        #print "2Pi", val
        return True

    if val >= 2*np.pi:
        return True
    if val <= -2*np.pi:
        return True

    if (val <= 2*np.pi and val >= (2*np.pi - 0.01)) or ( val >= -2*np.pi and val <= (-2*np.pi + 0.01)):
        #print "2Pi", val
        return True

    return False


first_flag = True
hop_count = 0
last_channel = 0.0
data_dict = {}
ref_data_dict = {}
time_list = []

for line in fLines2:
    l_str = line.strip().split(':')
    time_list.append( float(l_str[0]) )

time_diff_arr = []
last_time = time_list[0]

for i in range(1,len(time_list)):
    time_diff_arr.append(time_list[i] - last_time)
    last_time = time_list[i]

#print(time_diff_arr)

thres = time_diff_arr[0]
first_flag = True
last_time = 0.0
posn_count = 0
count = 1

for line in fLines1:
    l_str = line.strip().split(',')
    epc = str(l_str[0])
    tstamp = float(l_str[3])
    channel = float(l_str[8])
    phase = float(l_str[9])

    
    #if epc == "E28011606000020612B034AC" :
    if epc == "E28011606000020612B000ED" :
        count += 1
        if first_flag == True:

            last_time = tstamp
            data_dict[posn_count] = {}
            data_dict[posn_count][channel] = [ float(phase)  ]
            first_flag = False

        else:
            time_diff = ((tstamp-last_time)*1.0)/1000000.0


            if posn_count > 0:
                if time_diff > time_diff_arr[1]:
                    break
                
            if posn_count == 0:

                if time_diff > thres:
                    posn_count = 1
                    data_dict[posn_count] = {}
                    last_time = tstamp

            if channel not in data_dict[posn_count]:
                data_dict[posn_count][channel] = [ float(phase)  ]
            else:
                data_dict[posn_count][channel].append( float(phase)  )

print(data_dict.keys())

def find_cluster_centers( phase_arr ):

    arr = []
    for elem in phase_arr:
        arr.append( [float(elem)] )

    #MeanShift Clustering
    ms = MeanShift(bandwidth= 1.5, bin_seeding=True, n_jobs=-1)
    ms.fit(np.array(arr))

    cluster_centers = []

    cluster_centers = ms.cluster_centers_

    return cluster_centers

def find_difference( arr ):
    v1 = arr[0]
    v2 = arr[1]

    max1 = v1 if v1 > v2 else v2
    min1 = v1 if v1 < v2 else v2

    v3 = arr[2]
    v4 = arr[3]

    max2 = v3 if v3 > v4 else v4
    min2 = v3 if v3 < v4 else v4

    x =((max1-max2) + (min1-min2))/2.0

    y = ((v1 - v3) + (v1 - v4) + (v2 - v3) + (v2 - v4))*1.0/4.0

    #y = np.median(np.array([(v1 - v3) , (v1 - v4) , (v2 - v3) , (v2 - v4)]))

    return x


#Modifying all phases of different positions to a specific channel

offset = 0.11
keylist = sorted(list(data_dict[0].keys()))
#Special handling of the first one
base_channel = keylist[0]

phase_dict_arr = {}

for pos_indx in data_dict:

    phase_dict_arr[pos_indx] = []

    for channel in data_dict[pos_indx]:
    
        diff = ((float(channel) - float(base_channel))*1.0)/0.5
        correction = offset*diff

        for elem in data_dict[pos_indx][channel]:
            new_val = (float(elem) + correction)%(2*np.pi)
            phase_dict_arr[pos_indx].append(new_val)



fhandle = open('phase-dist-2.dat', 'w')
for elem in phase_dict_arr[0]:
    fhandle.write( str(elem) + "\n")
fhandle.close()

ref_phase_arr = phase_dict_arr[0]

fhandle = open('line-plot-phase-vs-coverage.dat', 'w')

print (len(phase_dict_arr[1]))

window_size = 150
indx= 0
last_indx = indx + window_size

diff_phase_arr = []

while last_indx < len(phase_dict_arr[1]):

    print (last_indx)
    tag_phase_arr = phase_dict_arr[1][indx:last_indx]
    indx += 1
    last_indx = indx + window_size

    cluster_centers2 = find_cluster_centers( ref_phase_arr )

    phase_diff_clustr_arr = {}
    phase_diff_actual_arr = {}
    
    for elem in tag_phase_arr:

        for clustr in cluster_centers2:
            #print(clustr)
            phase_diff_clustr_arr[elem] = abs(elem - clustr[0])
            phase_diff_actual_arr[elem] = elem - clustr[0]

    sorted_phase_diff = sorted(phase_diff_clustr_arr.items(), key=operator.itemgetter(1))

    #print (sorted_phase_diff)
    diff_phase_arr.append(phase_diff_actual_arr[sorted_phase_diff[0][0]])

last_phase = diff_phase_arr[0]
modified_diff_phase_arr = []

for i in list(range(1, len(diff_phase_arr))):
    phase_diff = diff_phase_arr[i] - last_phase

    if isOutlier(phase_diff) == False:
        modified_diff_phase_arr.append(diff_phase_arr[i])
    else:
        modified_diff_phase_arr.append(last_phase)

    last_phase = diff_phase_arr[i]


win_size = 11
diff_arr_smoothed = scipy.signal.medfilt(modified_diff_phase_arr, win_size)
diff_arr_smoothed = diff_phase_arr

count = 1
for elem in diff_arr_smoothed:
    fhandle.write( str(count) + " " + str(elem) + "\n")
    count += 1
fhandle.close()
