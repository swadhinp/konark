#!/usr/bin/python
import sys
import operator
import numpy as np
import scipy.signal
from sklearn.cluster import MeanShift, estimate_bandwidth
from sklearn import preprocessing
from sklearn.cluster import AgglomerativeClustering
import scipy.cluster.vq

if len(sys.argv) < 3:
    print("python <data_file> <frequency_offset_file> <No_Blockage_Tag_Phase_value>")
    sys.exit(-1)

def isOutlier( val ):
    
    if (val >= np.pi and val <= (np.pi + 0.1)) or ( val <= -np.pi and val >= (-np.pi - 0.1)):
        #print "Pi", val
        return True

    if (val <= np.pi and val >= (np.pi - 0.1)) or ( val >= -np.pi and val <= (-np.pi + 0.1)):
        #print "Pi", val
        return True

    if (val >= 2*np.pi and val <= (2*np.pi + 0.1)) or ( val <= -2*np.pi and val >= (-2*np.pi - 0.1)):
        #print "2Pi", val
        return True

    if val >= 2*np.pi:
        return True
    if val <= -2*np.pi:
        return True

    if (val <= 2*np.pi and val >= (2*np.pi - 0.1)) or ( val >= -2*np.pi and val <= (-2*np.pi + 0.1)):
        #print "2Pi", val
        return True

    return False

fhandle = open( sys.argv[1], 'r')
fLines1 = fhandle.readlines()
fhandle.close()

fhandle = open( sys.argv[2], 'r')
fLines3 = fhandle.readlines()
fhandle.close()

phase_value = float(sys.argv[3])

freq_offset_arr = []

for elem in fLines3:
    freq_offset_arr.append( float(elem.strip()) )

channel_id_dict = {}

start_channel = 902.75
channel_id_dict[start_channel] = 0

for i in list(range(1,50)):
    channel_id_dict[start_channel+0.5*i] = i

#print(channel_id_dict)
#print(freq_offset_arr)

data_dict = {}

count = 1

for line in fLines1:
    l_str = line.strip().split(',')
    epc = str(l_str[0])
    tstamp = float(l_str[3])
    channel = float(l_str[8])
    phase = float(l_str[9])

    
    #if epc == "E28011606000020612B034AC" :
    #if epc == "E28011606000020612B000ED" :
    if epc == "E28011606000020612AF52AD" :
        count += 1

        if channel not in data_dict:
            data_dict[channel] = [ float(phase)  ]
        else:
            data_dict[channel].append( float(phase)  )

print(data_dict.keys())

#Modifying all phases of different positions to a specific channel

keylist = sorted(list(data_dict.keys()))
#Special handling of the first one
base_channel = keylist[0]

phase_dict_arr = []


for channel in data_dict:
    
    diff = 0.0
    sign = 1.0

    if float(channel) > float(base_channel):
        for i in list(range(channel_id_dict[base_channel], channel_id_dict[channel] )):
            diff += freq_offset_arr[i]
            sign = -1.0
    else:
        for i in list(range(channel_id_dict[channel], channel_id_dict[base_channel])):
            diff += freq_offset_arr[i]

    correction = sign*diff

    for elem in data_dict[channel]:
        new_val = (float(elem) + correction)%(2*np.pi)
        phase_dict_arr.append(new_val)

#for elem in data_dict[base_channel]:
 #   phase_dict_arr.append(elem)


#Phase Distribution Debugging
'''
fhandle = open('phase-dist-2.dat', 'w')
for elem in phase_dict_arr[0]:
    fhandle.write( str(elem) + "\n")
fhandle.close()
'''


fhandle = open('line-plot-phase-vs-coverage.dat', 'w')

print (len(phase_dict_arr))

window_size = 150
indx= 0
last_indx = indx + window_size

diff_arr = []

prev_value = np.median(np.array(phase_dict_arr[indx:last_indx])) - phase_value

while last_indx < len(phase_dict_arr):

    #print (last_indx)
    tag_phase_arr = phase_dict_arr[indx:last_indx]
    indx += 1
    last_indx = indx + window_size

    bins = np.arange(0,6.01,0.01)
    [ data_arr, dataix_arr ] = np.histogram( np.array(tag_phase_arr), bins )

    data_list = data_arr.tolist()
    dataix_list = dataix_arr.tolist()
    
    #mode
    val1 = dataix_list[data_list.index(max(data_list))]

    #mean
    val1 = np.mean(np.array(tag_phase_arr))
    
    #median
    val1 = np.median(np.array(tag_phase_arr))
    val2 = phase_value

    diff1 = val1 - val2

    if isOutlier(diff1) == True:
        diff1 = prev_value

    prev_value = diff1

    diff_arr.append(diff1)
    #print(str(diff1))

#print(cluster1_arr)


win_size = 55
diff_arr_smoothed = scipy.signal.medfilt(diff_arr, win_size)
#diff_arr_smoothed = scipy.signal.wiener(diff_arr, win_size)

count = 1
for elem in diff_arr_smoothed:
    fhandle.write( str(count) + " " + str(elem) + "\n")
    count += 1
fhandle.close()
