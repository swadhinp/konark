#!/usr/bin/python
import sys
import operator
import numpy as np
import scipy.signal
import matplotlib.pyplot as plt
import scipy.fftpack
import scipy.cluster.vq
from sklearn.cluster import MeanShift, estimate_bandwidth
from sklearn import preprocessing
from sklearn.cluster import AgglomerativeClustering
import peakutils

if len(sys.argv) < 2:
    print ("python <data_file_1>")
    sys.exit(-1)

fhandle = open( sys.argv[1], 'r')
fLines1 = fhandle.readlines()
fhandle.close()

data_dict1 = {}
ref_data_dict1 = {}
another_data_dict1 = {}
data_phase_list = []
channel_order_list = []
uniq_channel_list = []

first_flag = True
hop_count = 1
last_channel = 0.0

cluster1_arr = []
def findMode( nArr ):
    bins = np.arange( np.min(nArr)-1, np.max(nArr)+1, 0.01)
    hist = np.histogram( nArr, bins=bins )
    mode = hist[ 1 ][ np.argmax(hist[0]) - 1 ] 
    return mode

for line in fLines1:
    l_str = line.strip().split(',')
    epc = str(l_str[0])
    tstamp = l_str[3]
    channel = l_str[8]
    phase = l_str[9]

    #if epc == "E28011606000020612B0349C" :
    if epc == "E28011606000020612AF52AD" :

        if channel not in uniq_channel_list:
            uniq_channel_list.append(channel)

        if first_flag == True:
            last_channel = channel
            first_flag = False
            key = channel + "_" + str(hop_count)
            data_dict1[key] = [ float(phase) ]
            ref_data_dict1[channel] = [ float(phase) ]
            channel_order_list.append( channel )
            continue
        else:
            if last_channel != channel:
                #print "Ch " , last_channel, channel
                another_data_dict1[last_channel].append([])
                channel_order_list.append( channel )
                hop_count += 1

            last_channel = channel
            key = channel + "_" + str(hop_count)
    

        if hop_count > len(data_phase_list):
            data_phase_list.append([float(phase)])
        else:
            data_phase_list[hop_count-1].append(float(phase))


        if key not in data_dict1:
            data_dict1[key] = [ float(phase) ]
        else:
            data_dict1[key].append( float(phase) )
    
    #key channel switch
        if channel not in ref_data_dict1:
            ref_data_dict1[channel] = [ float(phase) ]
        else:
            ref_data_dict1[channel].append( float(phase) )
        
        if channel not in another_data_dict1:
            another_data_dict1[channel] = [ [float(phase)] ]
        else:
            another_data_dict1[channel][len(another_data_dict1[channel])-1].append( float(phase) )
    else:
        print ("Terrible Error")


channel_list_sorted = sorted(uniq_channel_list)

phase_peak_arr = []
channel_peaks_list = []

for i in range(50):

    #print channel
    channel = channel_list_sorted[i]
    phase_list = ref_data_dict1[channel]

    phase_list_for_clustering = []

    for elem in phase_list:
        phase_list_for_clustering.append([elem])

    #Agglomerative Clustering

    #bandwidth = estimate_bandwidth(phase_list, quantile=0.2, n_samples=len(phase_list))
    #ward = AgglomerativeClustering(n_clusters=2, linkage='ward')
    #X_scaled = preprocessing.scale(np.array(phase_list))
    #ward.fit(np.array(phase_list_for_clustering))
    
    #MeanShift Clustering
    ms = MeanShift(bandwidth=0.2, bin_seeding=True)
    ms.fit(np.array(phase_list_for_clustering))

    cluster_centers = []
    if len(ms.cluster_centers_) > 2:
        phase_list_for_clustering1 = []
        
        for elem in phase_list:
            if ( float(elem) <= 2*np.pi and float(elem) >= 2*np.pi-0.2 ):
                #phase_list_for_clustering1.append([float(elem) + 2*np.pi])
                continue
            else:
                phase_list_for_clustering1.append([float(elem)])


        ms1 = MeanShift(bandwidth=0.2, bin_seeding=True)
        ms1.fit(np.array(phase_list_for_clustering1))
        cluster_centers = ms1.cluster_centers_
        print(channel)
        #print (channel, ms1.cluster_centers_)
    
    else:
        #print (channel, ms.cluster_centers_)
        print(channel)
        cluster_centers = ms.cluster_centers_
        #print (channel, len(ms.cluster_centers_), max(ms.cluster_centers_), min(ms.cluster_centers_))

    #print (cluster_centers[0][0], cluster_centers[1][0])
    channel_peaks_list.append([cluster_centers[0][0], cluster_centers[1][0]])

    #[X, label_arr] = scipy.cluster.vq.kmeans2( np.array(phase_list), 2)

    #cb = np.array(phase_list)
    #indexes = peakutils.indexes(cb, thres=0.02/max(cb), min_dist=1000)

    #peak_indices = scipy.signal.find_peaks_cwt( np.array(phase_list), np.arange(1,3) )

    phase_peak_arr.append( findMode(np.array(phase_list)) )


cluster1_arr = []
cluster2_arr = []

first_flag = True
last_1 = 0.0
last_2 = 0.0
switch_count = 0

for i in range(len(channel_peaks_list)):

    if first_flag == True:

        last_1 = channel_peaks_list[i][0]
        last_2 = channel_peaks_list[i][1]
        cluster1_arr.append(last_1)
        cluster2_arr.append(last_2)
        first_flag = False

    else:
        elem_1 = channel_peaks_list[i][0] 
        elem_2 = channel_peaks_list[i][1]

        elem1_diff1 = abs(last_1 - elem_1)
        elem1_diff2 = abs(last_2 - elem_1)


        last_diff = abs(last_1 - last_2)
        cur_diff = abs(elem_1 - elem_2)

        if abs(last_diff - cur_diff) > np.pi :
            switch_count += 1

        if (switch_count % 2) != 0:
            if elem_1 < elem_2:
                cluster1_arr.append(elem_2)
                cluster2_arr.append(elem_1)
                last_1 = elem_2
                last_2 = elem_1
            else:
                cluster1_arr.append(elem_1)
                cluster2_arr.append(elem_2)
                last_1 = elem_1
                last_2 = elem_2

        else:
            if elem_1 < elem_2:
                cluster1_arr.append(elem_1)
                cluster2_arr.append(elem_2)
                last_1 = elem_1
                last_2 = elem_2
            else:
                cluster1_arr.append(elem_2)
                cluster2_arr.append(elem_1)
                last_1 = elem_2
                last_2 = elem_1
            
        '''
        if elem1_diff1 < elem1_diff2:
            cluster1_arr.append(elem_1)
            cluster2_arr.append(elem_2)
            last_1 = elem_1
            last_2 = elem_2
        else:
            cluster1_arr.append(elem_2)
            cluster2_arr.append(elem_1)
            last_1 = elem_2
            last_2 = elem_1
        '''


phase_peak_arr = cluster1_arr

fhandle = open( "phase-peak-diff-1.dat", 'w' )

count = 1

for i in range( len(phase_peak_arr) - 1):
    diff = phase_peak_arr[i+1] - phase_peak_arr[i]
    fhandle.write( str(count) + " " + str( diff ) + "\n")
    count += 1

fhandle.close()

phase_peak_arr = cluster2_arr

fhandle = open( "phase-peak-diff-2.dat", 'w' )

count = 1

for i in range( len(phase_peak_arr) - 1):
    diff = phase_peak_arr[i+1] - phase_peak_arr[i]

    fhandle.write( str(count) + " " + str( diff ) + "\n")
    count += 1

'''
window_size = 5
median_arr1 = []
median_arr2 = []

fhandle = open( "pll-phase-diff.dat", 'w' )
N = 0
y = []
T = 1.0/10000.0

#Code Snippet for each channel Difference
for channel in ref_data_dict1:
    median_arr1.append( np.median(np.array(ref_data_dict1[channel])) )

for i in range(len(median_arr1) - 1):
    fhandle.write( str(median_arr1[i+1]-median_arr1[i]) + "\n" )
    ## FFT glue
    y.append( median_arr1[i+1]-median_arr1[i] )
    N += 1

'''

#Code Snippet for each hop Difference
'''
for channel in data_dict1:
    median_arr1.append( np.median(np.array(data_dict1[channel])) )

for i in range(len(median_arr1) - 1):
    fhandle.write( str(median_arr1[i+1]-median_arr1[i]) + "\n" )
    ## FFT glue
    y.append( median_arr1[i+1]-median_arr1[i] )
    N += 1
'''

#Code Snippet for same channel repeat difference
'''
for channel in another_data_dict1:

    for lst in another_data_dict1[channel]:
        #print len(lst)
        for i in range( len(lst) - 1):

            fhandle.write( str( np.median(np.array(lst[i+1])) - np.median(np.array(lst[i])) ) + "\n" )
            #median_arr1.append( np.median(np.array(l1)) )
'''

#Code Snippet for consecutive difference
'''
for i in range( len(data_phase_list) - 1 ):
    fhandle.write( str( np.median(np.array(data_phase_list[i+1])) - np.median(np.array(data_phase_list[i])) ) + "\n" )
    N += 1
    y.append(np.median(np.array(data_phase_list[i+1])) - np.median(np.array(data_phase_list[i])))
'''

'''
#FFT
yf = scipy.fftpack.fft(y)
#print N, T
xf = np.linspace(0.0, 1.0/(2.0*T), N/2)

fig, ax = plt.subplots()
ax.plot(xf, 2.0/N * np.abs(yf[:N/2]))
plt.show()
#for i in range(len(median_arr1)-1):
 #   fhandle.write( str(median_arr1[i+1]-median_arr1[i]) + "\n" )
'''

fhandle.close()
