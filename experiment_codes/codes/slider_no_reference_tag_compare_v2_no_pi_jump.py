#!/usr/bin/python
import sys
import operator
import numpy as np
import scipy.signal
from sklearn.cluster import MeanShift, estimate_bandwidth
from sklearn import preprocessing
from sklearn.cluster import AgglomerativeClustering
import scipy.cluster.vq

if len(sys.argv) < 3:
    print("python <data_file> <time_file> <frequency_offset_file>")
    sys.exit(-1)

fhandle = open( sys.argv[1], 'r')
fLines1 = fhandle.readlines()
fhandle.close()

fhandle = open( sys.argv[2], 'r')
fLines2 = fhandle.readlines()
fhandle.close()

fhandle = open( sys.argv[3], 'r')
fLines3 = fhandle.readlines()
fhandle.close()

freq_offset_arr = []

for elem in fLines3:
    freq_offset_arr.append( float(elem.strip()) )

channel_id_dict = {}

start_channel = 902.75
channel_id_dict[start_channel] = 0

for i in list(range(1,50)):
    channel_id_dict[start_channel+0.5*i] = i

#print(channel_id_dict)
#print(freq_offset_arr)

data_dict = {}
ref_data_dict = {}
time_list = []

for line in fLines2:
    l_str = line.strip().split(':')
    time_list.append( float(l_str[0]) )

time_diff_arr = []
last_time = time_list[0]

for i in range(1,len(time_list)):
    time_diff_arr.append(time_list[i] - last_time)
    last_time = time_list[i]

#print(time_diff_arr)

thres = time_diff_arr[0]
first_flag = True
last_time = 0.0
posn_count = 0
count = 1

for line in fLines1:
    l_str = line.strip().split(',')
    epc = str(l_str[0])
    tstamp = float(l_str[3])
    channel = float(l_str[8])
    phase = float(l_str[9])

    
    #if epc == "E28011606000020612B034AC" :
    #if epc == "E28011606000020612B000ED" :
    if epc == "E28011606000020612AF52AD" :
        count += 1
        if first_flag == True:

            last_time = tstamp
            data_dict[posn_count] = {}
            data_dict[posn_count][channel] = [ float(phase)  ]
            first_flag = False

        else:
            time_diff = ((tstamp-last_time)*1.0)/1000000.0

            if posn_count > 0:
                if time_diff > time_diff_arr[1]:
                    break
                
            if posn_count == 0:

                if time_diff > thres:
                    posn_count = 1
                    data_dict[posn_count] = {}
                    last_time = tstamp

            if channel not in data_dict[posn_count]:
                data_dict[posn_count][channel] = [ float(phase)  ]
            else:
                data_dict[posn_count][channel].append( float(phase)  )

print(data_dict.keys())

#Modifying all phases of different positions to a specific channel

offset = 0.11
keylist = sorted(list(data_dict[0].keys()))
#Special handling of the first one
base_channel = keylist[0]

phase_dict_arr = {}

for pos_indx in data_dict:

    phase_dict_arr[pos_indx] = []

    for channel in data_dict[pos_indx]:
    
        diff = 0.0
        sign = 1.0

        if float(channel) > float(base_channel):
            for i in list(range(channel_id_dict[base_channel], channel_id_dict[channel] )):
                diff += freq_offset_arr[i]
                sign = -1.0
        else:
            for i in list(range(channel_id_dict[channel], channel_id_dict[base_channel])):
                diff += freq_offset_arr[i]

        correction = sign*diff

        for elem in data_dict[pos_indx][channel]:
            new_val = (float(elem) + correction)%(2*np.pi)
            phase_dict_arr[pos_indx].append(new_val)



#Phase Distribution Debugging
'''
fhandle = open('phase-dist-2.dat', 'w')
for elem in phase_dict_arr[0]:
    fhandle.write( str(elem) + "\n")
fhandle.close()
'''

ref_phase_arr = phase_dict_arr[0]

fhandle = open('line-plot-phase-vs-coverage.dat', 'w')

print (len(phase_dict_arr[1]))

window_size = 300
indx= 0
last_indx = indx + window_size

diff_arr = []
while last_indx < len(phase_dict_arr[1]):

    #print (last_indx)
    tag_phase_arr = phase_dict_arr[1][indx:last_indx]
    indx += 1
    last_indx = indx + window_size

    bins = np.arange(0,6.01,0.01)
    [ data_arr, dataix_arr ] = np.histogram( np.array(tag_phase_arr), bins )

    data_list = data_arr.tolist()
    dataix_list = dataix_arr.tolist()
    
    #mode
    val1 = dataix_list[data_list.index(max(data_list))]

    #mean
    val1 = np.mean(np.array(tag_phase_arr))
    
    #median
    val1 = np.median(np.array(tag_phase_arr))

    [ data_arr1, dataix_arr1 ] = np.histogram( np.array(ref_phase_arr), bins )

    data_list1 = data_arr1.tolist()
    dataix_list1 = dataix_arr1.tolist()

    #mode
    val2 = dataix_list1[data_list1.index(max(data_list1))]


    diff1 = val1 - val2
    diff_arr.append(diff1)
    #print(str(diff1))

#print(cluster1_arr)


win_size = 15
diff_arr_smoothed = scipy.signal.medfilt(diff_arr, win_size)
#diff_arr_smoothed = scipy.signal.wiener(diff_arr, win_size)

count = 1
for elem in diff_arr_smoothed:
    fhandle.write( str(count) + " " + str(elem) + "\n")
    count += 1
fhandle.close()
