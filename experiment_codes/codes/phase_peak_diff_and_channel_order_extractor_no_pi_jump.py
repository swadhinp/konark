#File for calculating Frequency Offset
#!/usr/bin/python
import sys
import operator
import numpy as np
import scipy.signal
import matplotlib.pyplot as plt
import scipy.fftpack
import scipy.cluster.vq
from sklearn.cluster import MeanShift, estimate_bandwidth
from sklearn import preprocessing
from sklearn.cluster import AgglomerativeClustering
import peakutils

if len(sys.argv) < 2:
    print ("python <data_file_1>")
    sys.exit(-1)

fhandle = open( sys.argv[1], 'r')
fLines1 = fhandle.readlines()
fhandle.close()

data_dict1 = {}
ref_data_dict1 = {}
another_data_dict1 = {}
data_phase_list = []
channel_order_list = []
uniq_channel_list = []

first_flag = True
hop_count = 1
last_channel = 0.0

cluster1_arr = []
def findMode( nArr ):
    bins = np.arange( np.min(nArr)-1, np.max(nArr)+1, 0.01)
    hist = np.histogram( nArr, bins=bins )
    mode = hist[ 1 ][ np.argmax(hist[0]) - 1 ] 
    return mode

for line in fLines1:
    l_str = line.strip().split(',')
    epc = str(l_str[0])
    tstamp = l_str[3]
    channel = l_str[8]
    phase = l_str[9]

    #if epc == "E28011606000020612B0349C" :
    if epc == "E28011606000020612AF52AD" :

        if channel not in uniq_channel_list:
            uniq_channel_list.append(channel)

        if first_flag == True:
            last_channel = channel
            first_flag = False
            key = channel + "_" + str(hop_count)
            data_dict1[key] = [ float(phase) ]
            ref_data_dict1[channel] = [ float(phase) ]
            channel_order_list.append( channel )
            continue
        else:
            if last_channel != channel:
                #print "Ch " , last_channel, channel
                another_data_dict1[last_channel].append([])
                channel_order_list.append( channel )
                hop_count += 1

            last_channel = channel
            key = channel + "_" + str(hop_count)
    

        if hop_count > len(data_phase_list):
            data_phase_list.append([float(phase)])
        else:
            data_phase_list[hop_count-1].append(float(phase))


        if key not in data_dict1:
            data_dict1[key] = [ float(phase) ]
        else:
            data_dict1[key].append( float(phase) )
    
    #key channel switch
        if channel not in ref_data_dict1:
            ref_data_dict1[channel] = [ float(phase) ]
        else:
            ref_data_dict1[channel].append( float(phase) )
        
        if channel not in another_data_dict1:
            another_data_dict1[channel] = [ [float(phase)] ]
        else:
            another_data_dict1[channel][len(another_data_dict1[channel])-1].append( float(phase) )
    else:
        print ("Terrible Error")


channel_list_sorted = sorted(uniq_channel_list)

phase_peak_arr = []
channel_peaks_list = []

for i in range(50):

    #print channel
    channel = channel_list_sorted[i]
    phase_list = ref_data_dict1[channel]

    bins = np.arange(0,6.01,0.01)
    [ data_arr, dataix_arr ] = np.histogram( np.array(phase_list), bins )

    data_list = data_arr.tolist()
    dataix_list = dataix_arr.tolist()

    cluster1_arr.append(dataix_list[data_list.index(max(data_list))])

print(cluster1_arr)

'''
cluster1_arr = []
cluster2_arr = []

first_flag = True
last_1 = 0.0
last_2 = 0.0
switch_count = 0

for i in range(len(channel_peaks_list)):

    if first_flag == True:

        last_1 = channel_peaks_list[i][0]
        last_2 = channel_peaks_list[i][1]
        cluster1_arr.append(last_1)
        cluster2_arr.append(last_2)
        first_flag = False

    else:
        elem_1 = channel_peaks_list[i][0] 
        elem_2 = channel_peaks_list[i][1]

        elem1_diff1 = abs(last_1 - elem_1)
        elem1_diff2 = abs(last_2 - elem_1)


        last_diff = abs(last_1 - last_2)
        cur_diff = abs(elem_1 - elem_2)

        if abs(last_diff - cur_diff) > np.pi :
            switch_count += 1

        if (switch_count % 2) != 0:
            if elem_1 < elem_2:
                cluster1_arr.append(elem_2)
                cluster2_arr.append(elem_1)
                last_1 = elem_2
                last_2 = elem_1
            else:
                cluster1_arr.append(elem_1)
                cluster2_arr.append(elem_2)
                last_1 = elem_1
                last_2 = elem_2

        else:
            if elem_1 < elem_2:
                cluster1_arr.append(elem_1)
                cluster2_arr.append(elem_2)
                last_1 = elem_1
                last_2 = elem_2
            else:
                cluster1_arr.append(elem_2)
                cluster2_arr.append(elem_1)
                last_1 = elem_2
                last_2 = elem_1
            
'''

phase_peak_arr = cluster1_arr

fhandle = open( "phase-peak-diff-1.dat", 'w' )

count = 1
prev_diff = 0.0

for i in range( len(phase_peak_arr) - 1):
    diff = phase_peak_arr[i+1] - phase_peak_arr[i]

    if diff == 0.0:
        diff = prev_diff
    
    if diff > np.pi:
        print("Y")
        diff = phase_peak_arr[i+1] - 2*np.pi - 1.35*prev_diff

    print(diff)

    fhandle.write( str(count) + " " + str( diff ) + "\n")
    count += 1
    prev_diff = diff

fhandle.close()

