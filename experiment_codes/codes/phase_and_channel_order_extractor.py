#!/usr/bin/python
import sys
import operator
import numpy as np
import scipy.signal
import matplotlib.pyplot as plt
import scipy.fftpack

if len(sys.argv) < 2:
    print "python <data_file_1>"
    sys.exit(-1)

fhandle = open( sys.argv[1], 'r')
fLines1 = fhandle.readlines()
fhandle.close()

data_dict1 = {}
ref_data_dict1 = {}
another_data_dict1 = {}
data_phase_list = []
channel_order_list = []

first_flag = True
hop_count = 1
last_channel = 0.0

for line in fLines1:
    l_str = line.strip().split(',')
    epc = str(l_str[0])
    tstamp = l_str[3]
    channel = l_str[8]
    phase = l_str[9]
    #print channel


    #if epc == "E28011606000020612B0349C" :
    if epc == "E28011606000020612B0327D" :
    
        if channel not in channel_order_list:
            channel_order_list.append(channel)

        #key channel switch
        if channel not in ref_data_dict1:
            #print channel
            ref_data_dict1[channel] = [ float(phase) ]
        else:
            ref_data_dict1[channel].append( float(phase) )
    else:
        print "Terrible Error"


sorted_list = sorted(ref_data_dict1.keys())

count = 0
offset = 0.12


#Special handling of the first one
last_channel = sorted_list[0]
#print ref_data_dict1.keys()
#print last_channel

fname = str(last_channel) + "_dist.dat"
fhandle = open( fname , 'w' )
phase_list = ref_data_dict1[last_channel]

for elem in phase_list:
    fhandle.write( str( float(elem) ) + "\n" )

fhandle.close()

for indx in range(1, len(sorted_list)):

    channel = sorted_list[indx]
    #print channel
    fname = str(channel) + "_dist.dat"
    fhandle = open( fname , 'w' )

    diff = int((float(channel) - float(last_channel))/0.5)
    #print diff

    phase_list = ref_data_dict1[channel]

    for elem in phase_list:
        fhandle.write( str( (float(elem) + (diff)*offset)%(2*np.pi) ) + "\n")

    count += 1
    fhandle.close()
    #last_channel = channel

#Channel Order Extraction

fhandle = open( "channel-order.dat", 'w' )

count = 1

for elem in sorted(channel_order_list):
    fhandle.write( str(count) + ":" + str(elem) + "\n")
    count += 1


fhandle.close()
