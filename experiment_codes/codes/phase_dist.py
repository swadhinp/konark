#!/usr/bin/python
import sys
import operator
import numpy as np
import scipy.signal

if len(sys.argv) < 2:
    print("python <data_file> <freq_offset_file>")
    sys.exit(-1)

fhandle = open( sys.argv[1], 'r')
fLines1 = fhandle.readlines()
fhandle.close()


fhandle = open( sys.argv[2], 'r')
fLines3 = fhandle.readlines()
fhandle.close()

freq_offset_arr = []

for elem in fLines3:
    freq_offset_arr.append( float(elem.strip()) )

channel_id_dict = {}

start_channel = 902.75
channel_id_dict[start_channel] = 0 

for i in list(range(1,50)):
    channel_id_dict[float(start_channel+0.5*i)] = i 

#print(channel_id_dict)
#print(freq_offset_arr)

count = 0
data_dict = {}

for line in fLines1:
    l_str = line.strip().split(',')
    epc = str(l_str[0])
    tstamp = float(l_str[3])
    channel = float(l_str[8])
    phase = float(l_str[9])

    
    #if epc == "E28011606000020612B000ED" :
    if epc == "E28011606000020612AF52AD" :
        count += 1
        if channel not in data_dict:
            data_dict[channel] = [ float(phase)  ]
        else:
            data_dict[channel].append( float(phase)  )

#Modifying all phases of different positions to a specific channel

#print(count)

new_count = 0
offset = 0.13
keylist = sorted(list(data_dict.keys()))
#Special handling of the first one
base_channel = keylist[0]

phase_dict_arr = []

for channel in data_dict:

    diff = ((float(channel) - float(base_channel))*1.0)/0.5
    correction1 = offset*diff
    #print("Prev : ")
    #print(correction1)

    diff = 0.0
    sign = 1.0

    if float(channel) > float(base_channel):
        for i in list(range(channel_id_dict[base_channel], channel_id_dict[channel] )):
            diff += freq_offset_arr[i]
            sign = -1.0
    else:
        for i in list(range(channel_id_dict[channel], channel_id_dict[base_channel])):
            diff += freq_offset_arr[i]

    correction = sign*diff

    #print("After : ")
    #print(correction)

    #correction = correction1

    for elem in data_dict[channel]:
        new_count += 1
        new_val = (float(elem) + correction)%(2*np.pi)
        phase_dict_arr.append(new_val)

#print(new_count)
#print(len(phase_dict_arr))

bins = np.arange(0,6.01,0.01)
[ data_arr, dataix_arr ] = np.histogram( np.array(phase_dict_arr), bins )

data_list = data_arr.tolist()
dataix_list = dataix_arr.tolist()
    
#mode
val1 = dataix_list[data_list.index(max(data_list))]

print("Mode : ")
print(val1)
print("Mean : ")
print(np.mean(np.array(phase_dict_arr)))
print("Median : ")
print(np.median(np.array(phase_dict_arr)))

fhandle = open('phase-dist.dat', 'w')
for elem in phase_dict_arr:
    fhandle.write( str(elem) + "\n")
fhandle.close()
