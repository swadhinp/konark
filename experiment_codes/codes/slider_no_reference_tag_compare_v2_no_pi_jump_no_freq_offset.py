#!/usr/bin/python
import sys
import operator
import numpy as np
import scipy.signal
from sklearn.cluster import MeanShift, estimate_bandwidth
from sklearn import preprocessing
from sklearn.cluster import AgglomerativeClustering
import scipy.cluster.vq
import collections

if len(sys.argv) < 2:
    print("python <data_file> <time_file>")
    sys.exit(-1)

def isOutlier( val ):

    if (val >= 2*np.pi and val <= (2*np.pi + 0.1)) or ( val <= -2*np.pi and val >= (-2*np.pi - 0.1)):
        return True

    if (val <= 2*np.pi and val >= (2*np.pi - 0.1)) or ( val >= -2*np.pi and val <= (-2*np.pi + 0.1)):
        return True

    if val >= 2*np.pi:
        return True

    if val <= -2*np.pi:
        return True

    return False


fhandle = open( sys.argv[1], 'r')
fLines1 = fhandle.readlines()
fhandle.close()

fhandle = open( sys.argv[2], 'r')
fLines2 = fhandle.readlines()
fhandle.close()


channel_id_dict = {}

start_channel = 902.75
channel_id_dict[start_channel] = 0

for i in list(range(1,50)):
    channel_id_dict[start_channel+0.5*i] = i

#print(channel_id_dict)
#print(freq_offset_arr)

data_dict = {}
data_dict_ref = {}
time_list = []

for line in fLines2:
    l_str = line.strip().split(':')
    time_list.append( float(l_str[0]) )

time_diff_arr = []
last_time = time_list[0]

for i in range(1,len(time_list)):
    time_diff_arr.append(time_list[i] - last_time)
    last_time = time_list[i]

#print(time_diff_arr)

first_flag = True
last_time = 0.0
posn_count = 0
count = 1

for line in fLines1:
    l_str = line.strip().split(',')
    epc = str(l_str[0])
    tstamp = float(l_str[3])
    channel = float(l_str[8])
    phase = float(l_str[9])

    
    #if epc == "E28011606000020612B034AC" :
    #if epc == "E28011606000020612B000ED" :
    if epc == "E28011606000020612AF52AD" :
        count += 1
        if first_flag == True:

            last_time = tstamp
            data_dict_ref[channel] = [phase]
            first_flag = False

        else:
            time_diff = ((tstamp-last_time)*1.0)/1000000.0

            if posn_count > 0:
                if time_diff > time_diff_arr[1]:
                    break
                
            if posn_count == 0:

                if time_diff > time_diff_arr[0]:
                    posn_count = 1
                    last_time = tstamp

            if posn_count == 0:
                if channel not in data_dict_ref:
                    data_dict_ref[channel] = [phase]
                else:
                    data_dict_ref[channel].append(phase)
            else:
                data_dict[tstamp] = [channel, phase]

#print(data_dict.keys())

channel_phase_dict = {}

for channel in data_dict_ref:
    channel_phase_dict[channel] = np.mean(np.array(data_dict_ref[channel]))
    #print(str(channel), ":", str(np.mean(np.array(data_dict_ref[channel]))), "\n")

ordered_data_dict = collections.OrderedDict(sorted(data_dict.items()))

prev_val = 0.0 
first_flag = True
channel_arr = []
correction_factor =  (4*np.pi*0.5*0.3)/300.0 #Assuming 30cm distance and 2*pi*f*2d/c formula (in radian)

phase_dict_arr = []

print("Channel,Current_Phase, Calibrated_Phase, Phase_Diff, Corrected_Phase, Phase_Diff_after_Correction")
for key, val in ordered_data_dict.items():

    channel = val[0]
    elem = val[1]

    if first_flag == True:
        first_flag = False
        channel_arr.append(elem)
        prev_channel = channel
        continue
    else:
        if channel == prev_channel:
            channel_arr.append(elem)
            continue

    elem = np.mean(np.array(channel_arr))

    new_val1 = (float(elem) - channel_phase_dict[channel])

    #Offset Correction
    offset = (channel - 902.75)*correction_factor
    new_val = (float(elem) + offset)%2*np.pi


    new_val2 = (float(new_val) - channel_phase_dict[channel])

    print(str(channel), " , ", str(elem), " , ", str(channel_phase_dict[channel]), " , ", str(new_val1), " , ", str(new_val) , " , ", str(new_val2))

    #No correction
    #new_val = new_val1
    
    if ( isOutlier(new_val2) == True ):
        new_val2 = prev_val

    prev_val = new_val2

    #if channel == 914.25:
    phase_dict_arr.append(new_val2)
    channel_arr = []
    prev_channel = channel



diff_arr = phase_dict_arr
win_size = 1
diff_arr_smoothed = scipy.signal.medfilt(diff_arr, win_size)
#diff_arr_smoothed = scipy.signal.wiener(diff_arr, win_size)

fhandle = open('line-plot-phase-vs-coverage.dat', 'w')

count = 1
for elem in diff_arr_smoothed:
    fhandle.write( str(count) + " " + str(elem) + "\n")
    count += 1

fhandle.close()
