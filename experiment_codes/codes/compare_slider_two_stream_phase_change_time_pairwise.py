#!/usr/bin/python
import sys
import operator
import numpy as np
import scipy.signal

if len(sys.argv) < 2:
    print "python <ref_data_file_1> <data_file_1> <ref_data_file_2> <data_file_2>"
    sys.exit(-1)

fhandle = open( sys.argv[1], 'r')
fLines1 = fhandle.readlines()
fhandle.close()

fhandle = open( sys.argv[2], 'r')
fLines2 = fhandle.readlines()
fhandle.close()

fhandle = open( sys.argv[3], 'r')
fLines3 = fhandle.readlines()
fhandle.close()

fhandle = open( sys.argv[4], 'r')
fLines4 = fhandle.readlines()
fhandle.close()

data_dict1 = {}

ref_data_dict1 = {}

time_data_dict1 = {}


def isOutlier( val ):
    
    if (val >= np.pi and val <= (np.pi + 0.1)) or ( val <= -np.pi and val >= (-np.pi - 0.1)):
        #print "Pi", val
        return True

    if (val <= np.pi and val >= (np.pi - 0.1)) or ( val >= -np.pi and val <= (-np.pi + 0.1)):
        #print "Pi", val
        return True

    if (val >= 2*np.pi and val <= (2*np.pi + 0.1)) or ( val <= -2*np.pi and val >= (-2*np.pi - 0.1)):
        #print "2Pi", val
        return True

    if val >= 2*np.pi:
        return True
    if val <= -2*np.pi:
        return True

    if (val <= 2*np.pi and val >= (2*np.pi - 0.1)) or ( val >= -2*np.pi and val <= (-2*np.pi + 0.1)):
        #print "2Pi", val
        return True

    return False


first_flag = True
hop_count = 0
last_channel = 0.0

time_ref_data_dict1 = {}
for line in fLines1:
    l_str = line.strip().split(',')
    epc = str(l_str[0])
    tstamp = l_str[3]
    channel = l_str[8]
    phase = l_str[9]

    if epc == "E28011606000020612B0327D" :

        if first_flag == True:
            last_channel = channel
            first_flag = False
            key = channel + "_" + str(hop_count)
            ref_data_dict1[key] = [ [float(tstamp), float(phase) ] ]
            continue
        else:
            if last_channel != channel:
                #print "Ch " , last_channel, channel
                hop_count += 1

            last_channel = channel
            key = channel + "_" + str(hop_count)
    
            if key not in ref_data_dict1:
                ref_data_dict1[key] = [ [ float(tstamp), float(phase) ] ]
            else:
                ref_data_dict1[key].append( [ float(tstamp), float(phase) ] )
        
        time_ref_data_dict1[float(tstamp)] = [ float(phase), key]

    else:
        print "Terrible Error"

lst = [ value for (key, value) in sorted(time_ref_data_dict1.items())]

#print lst
#for v in lst:
 #   print v

lst_remove_outlier = []

for i in range(1,len(lst)):

    cur_phase = lst[i][0]
    #print lst[i], lst[i-1]
    an_phase = lst[i-1][0]
        
    diff_phase = float(cur_phase) - float(lst[i-1][0])

    if isOutlier(diff_phase) == True:
        #print diff_phase, cur_phase, an_phase
        lst_remove_outlier.append(lst[i-1]) #Copy the previous phase
        lst[i] = lst[i-1] #Update

    else:
        lst_remove_outlier.append(lst[i])

    
fhandle = open( "line-ex.dat", "w")
count = 1

for v in lst_remove_outlier:
    fhandle.write( str(count) + " " + str(v[0])  )
    fhandle.write("\n")
    count = count + 1

fhandle.close()
for v in lst_remove_outlier:
    print v

'''
#print sorted(time_ref_data_dict1.values())
#print "Here1"
first_flag = True
hop_count = 0
last_channel = 0.0

for line in fLines2:
    l_str = line.strip().split(',')
    epc = str(l_str[0])
    tstamp = l_str[3]
    channel = l_str[8]
    phase = l_str[9]

    if epc == "E28011606000020612B034AC" :
        
        time_data_dict1[float(tstamp)] = float(phase)
    
        if first_flag == True:
            last_channel = channel
            first_flag = False
            key = channel + "_" + str(hop_count)
            data_dict1[key] = [ [ float(tstamp), float(phase) ] ]
            continue
        else:
            if last_channel != channel:
                #print "Ch " , last_channel, channel
                hop_count += 1

            last_channel = channel
            key = channel + "_" + str(hop_count)
    
            if key not in data_dict1:
                data_dict1[key] = [ [float(tstamp), float(phase)] ]
            else:
                data_dict1[key].append( [float(tstamp), float(phase)] )
    else:
        print "Terrible Error"

#print "Here2"
first_flag = True
hop_count = 0
last_channel = 0.0

data_dict2 = {}
ref_data_dict2 = {}

time_data_dict2 = {}
time_ref_data_dict2 = {}

for line in fLines3:
    l_str = line.strip().split(',')
    epc = str(l_str[0])
    tstamp = l_str[3]
    channel = l_str[8]
    phase = l_str[9]

    if epc == "E28011606000020612B0327D" :

        time_ref_data_dict2[float(tstamp)] = float(phase)
    
        if first_flag == True:
            last_channel = channel
            first_flag = False
            key = channel + "_" + str(hop_count)
            ref_data_dict2[key] = [ [ float(tstamp), float(phase) ] ]
            continue
        else:
            if last_channel != channel:
                #print "Ch " , last_channel, channel
                hop_count += 1
            
            #print "Here"
            last_channel = channel
            key = channel + "_" + str(hop_count)
                
            if key not in ref_data_dict2:
                ref_data_dict2[key] = [ [ float(tstamp), float(phase) ] ]
            else:
                ref_data_dict2[key].append( [ float(tstamp), float(phase) ] )

    else:
        print "Terrible Error"

#print "Here3"
first_flag = True
hop_count = 0
last_channel = 0.0

for line in fLines4:
    l_str = line.strip().split(',')
    epc = str(l_str[0])
    tstatmp = l_str[3]
    channel = l_str[8]
    phase = l_str[9]

    if epc == "E28011606000020612B034AC" :
        
        time_data_dict1[float(tstamp)] = float(phase)

        if first_flag == True:
            last_channel = channel
            first_flag = False
            key = channel + "_" + str(hop_count)
            data_dict2[key] = [ [ float(tstamp), float(phase) ] ]
            continue
        else:
            if last_channel != channel:
                #print "Ch " , last_channel, channel
                hop_count += 1

            #print "There"
            last_channel = channel
            key = channel + "_" + str(hop_count)
    
            if key not in data_dict2:
                data_dict2[key] = [ [ float(tstamp), float(phase) ] ]
            else:
                data_dict2[key].append( [ float(tstamp), float(phase) ] )

    else:
        print "Terrible Error"


#print "Data Dict1"
#print len(data_dict1.keys())
#print "Data Dict2"
#print len(data_dict2.keys())
#print "Ref Data Dict1"
#print len(ref_data_dict1.keys())
#print "Ref Data Dict2"
#print len(ref_data_dict2.keys())

#print "Data Dict1"
#for ch in data_dict1:
 #   print len(data_dict1[ch])

#print "Ref Data Dict1"
#for ch in ref_data_dict1:
 #   print len(ref_data_dict1[ch])

#print "Data Dict2"
#for ch in data_dict2:
    #print len(data_dict2[ch])


diff_phase_arr1 = []
diff_phase_arr2 = []

window_size = 5

print "First Data : "
for channel in ref_data_dict1:

    phase_arr1 = ref_data_dict1[channel]
    phase_arr2 = data_dict1[channel]

    #print channel
    #print phase_arr1
    #print phase_arr2
    #print channel, len(phase_arr1), len(phase_arr2)

    phase_arr1_remove_outlier = []
    phase_arr2_remove_outlier = []

    phase_arr1_smoothed = []
    phase_arr2_smoothed = []

    if (isOutlier(phase_arr1[0]) == True):
        phase_arr1_remove_outlier.append(0.0)
        phase_arr1[0] = 0.0 #Update
    else:
        phase_arr1_remove_outlier.append(phase_arr1[0])

    for i in range(1,len(phase_arr1)):

        cur_phase = phase_arr1[i]
        an_phase = phase_arr1[i-1]
        
        diff_phase = float(cur_phase) - float(phase_arr1[i-1])
        #print "Diff", i, i-1, cur_phase, phase_arr1[i-1], isOutlier(diff_phase)

        if isOutlier(diff_phase) == True:
            phase_arr1_remove_outlier.append(float(phase_arr1[i-1])) #Copy the previous phase
            phase_arr1[i] = float(phase_arr1[i-1]) #Update

        else:
            phase_arr1_remove_outlier.append(cur_phase)

    
    if ( isOutlier(phase_arr2[0]) == True):
        phase_arr2_remove_outlier.append(0.0)
        phase_arr2[0] = 0.0 #Update
    else:
        phase_arr2_remove_outlier.append(phase_arr2[0])

    for i in range(1,len(phase_arr2)):

        cur_phase = phase_arr2[i]
        
        diff_phase = float(cur_phase) - float(phase_arr2[i-1])
        #print diff_phase
        
        if isOutlier(diff_phase) == True:
            phase_arr2_remove_outlier.append(float(phase_arr2[i-1])) #Copy the previous phase
            phase_arr2[i] = float(phase_arr2[i-1]) #Update
        else:
            phase_arr2_remove_outlier.append(cur_phase)
    
    #Directly applying Median Filter
    phase_arr1_smoothed = scipy.signal.medfilt(phase_arr1, window_size)
    phase_arr2_smoothed = scipy.signal.medfilt(phase_arr2, window_size)
    #phase_arr1_smoothed = scipy.signal.wiener(phase_arr1_remove_outlier, window_size)
    #phase_arr2_smoothed = scipy.signal.wiener(phase_arr2_remove_outlier, window_size)
    
    #phase_arr1_smoothed = scipy.signal.hilbert(phase_arr1)
    #phase_arr2_smoothed = scipy.signal.hilbert(phase_arr2)

    for i in range( min(len(phase_arr2_smoothed), len(phase_arr1_smoothed))):
        diff_phase_arr1.append( float(phase_arr2_smoothed[i]) - float(phase_arr1_smoothed[i]))


#print "Here5"

print "Second  Data : "
for channel in ref_data_dict2:

    phase_arr1 = ref_data_dict2[channel]
    phase_arr2 = data_dict2[channel]

    
    print channel
    print phase_arr1
    print phase_arr2
    #print channel, len(phase_arr1), len(phase_arr2)
    
    phase_arr1_remove_outlier = []
    phase_arr2_remove_outlier = []
    phase_arr1_smoothed = []
    phase_arr2_smoothed = []

    if (isOutlier(phase_arr1[0]) == True):
        phase_arr1_remove_outlier.append(0.0)
        phase_arr1[0] = 0.0 #Update
    else:
        phase_arr1_remove_outlier.append(phase_arr1[0])

    for i in range(1,len(phase_arr1)):

        cur_phase = phase_arr1[i]
        
        diff_phase = float(cur_phase) - float(phase_arr1[i-1])
        #print diff_phase
        
        if isOutlier(diff_phase) == True:
            phase_arr1_remove_outlier.append(float(phase_arr1[i-1])) #Copy the previous phase
            phase_arr1[i] = float(phase_arr1[i-1]) #Update

        else:
            phase_arr1_remove_outlier.append(cur_phase)

    
    if ( isOutlier(phase_arr2[0]) == True):
        phase_arr2_remove_outlier.append(0.0)
        phase_arr2[0] = 0.0 #Update
    else:
        phase_arr2_remove_outlier.append(phase_arr2[0])

    for i in range(1,len(phase_arr2)):

        cur_phase = phase_arr2[i]
        
        diff_phase = float(cur_phase) - float(phase_arr2[i-1])
        #print diff_phase
        
        if isOutlier(diff_phase) == True:
            phase_arr2_remove_outlier.append(float(phase_arr2[i-1])) #Copy the previous phase
            phase_arr2[i] = float(phase_arr2[i-1]) #Update
        else:
            phase_arr2_remove_outlier.append(cur_phase)
    
    #Directly applying Median Filter
    phase_arr1_smoothed = scipy.signal.medfilt(phase_arr1, window_size)
    phase_arr2_smoothed = scipy.signal.medfilt(phase_arr2, window_size)
    #phase_arr1_smoothed = scipy.signal.wiener(phase_arr1_remove_outlier, window_size)
    #phase_arr2_smoothed = scipy.signal.wiener(phase_arr2_remove_outlier, window_size)
    #phase_arr1_smoothed = scipy.signal.hilbert(phase_arr1)
    #phase_arr2_smoothed = scipy.signal.hilbert(phase_arr2)

    for i in range( min(len(phase_arr2_smoothed), len(phase_arr1_smoothed))):
        diff_phase_arr2.append( float(phase_arr2_smoothed[i]) - float(phase_arr1_smoothed[i]))

    

#For No smoothing
#diff_phase_arr1_smoothed = diff_phase_arr1 
#diff_phase_arr2_smoothed = diff_phase_arr2

fhandle = open( "line-plot-phase-diff-smoothed.dat", "w")
count = 1

for i in range(min(len(diff_phase_arr1), len(diff_phase_arr2))):
    fhandle.write( str(count) + " " + str(diff_phase_arr1[count-1]) + " " + str(diff_phase_arr2[count-1]) )
    fhandle.write("\n")
    count = count + 1

fhandle.close()
'''
#print sorted_rfid_epc_dict
