#!/usr/bin/python
import sys 
import operator
import numpy as np
import scipy.signal
import scipy.cluster.vq

if len(sys.argv) < 3:
    print "python <cart_file> <outside_file> <cart_id_file>"
    sys.exit(-1)


fhandle = open( sys.argv[1], 'r')
cartLines = fhandle.readlines()
fhandle.close()

fhandle = open( sys.argv[2], 'r')
outLines = fhandle.readlines()
fhandle.close()

fhandle = open( sys.argv[3], 'r')
cart_ids = fhandle.readlines()
fhandle.close()


cart_id_arr = []
epc_data_dict = {}
epc_first_data = {}
epc_channel_first_freq_dict = {}

for line in cartLines:
    l_str = line.strip().split(',')
            
    epc = str(l_str[0])
    tstamp = float(l_str[3])
    doppler = float(l_str[5])
    rssi = float(l_str[7])
    channel = float(l_str[8])
    phase = float(l_str[9])

    if epc not in epc_data_dict:
        epc_data_dict[epc] = [[tstamp,doppler,rssi,phase,channel]]
        epc_first_data[epc] = tstamp
        
        epc_channel_first_freq_dict[epc] ={}
        epc_channel_first_freq_dict[epc][channel] = {}
        epc_channel_first_freq_dict[epc][channel][tstamp] = phase
    
    else:
        epc_data_dict[epc].append([tstamp,doppler,rssi,phase,channel])

        if channel not in epc_channel_first_freq_dict[epc]:
            epc_channel_first_freq_dict[epc][channel] = {}
            epc_channel_first_freq_dict[epc][channel][tstamp] = phase
        else:
            epc_channel_first_freq_dict[epc][channel][tstamp] = phase


for line in outLines:
    l_str = line.strip().split(',')
            
    epc = str(l_str[0])
    tstamp = float(l_str[3])
    doppler = float(l_str[5])
    rssi = float(l_str[7])
    channel = float(l_str[8])
    phase = float(l_str[9])

    if epc not in epc_data_dict:
        epc_data_dict[epc] = [[tstamp,doppler,rssi,phase,channel]]
        epc_first_data[epc] = tstamp

        epc_channel_first_freq_dict[epc] ={}
        epc_channel_first_freq_dict[epc][channel] = {}
        epc_channel_first_freq_dict[epc][channel][tstamp] = phase

    else:
        epc_data_dict[epc].append([tstamp,doppler,rssi,phase,channel])

        if channel not in epc_channel_first_freq_dict[epc]:
            epc_channel_first_freq_dict[epc][channel] = {}
            epc_channel_first_freq_dict[epc][channel][tstamp] = phase
        else:
            epc_channel_first_freq_dict[epc][channel][tstamp] = phase


for cart in cart_ids:
    cart_id_arr.append(str(cart).strip())
    #iprint cart

def modify_phase( val ):

    if val == np.pi or val == -np.pi or val == 2*np.pi or val == -2*np.pi:
        return 0.0
    else:
        return val

data_epc = []

feature_matrix = []

for elem in epc_data_dict:
    
    index = 0
    data_arr = []
    last_time = float(epc_first_data[elem])
    epc_data_arr = epc_data_dict[elem]

    #print len(epc_data_arr)

    while index < (len(epc_data_arr)):
        
        doppler_arr = []
        rssi_arr = []
        phase_arr = []
        diff = 0.0
        cur_time = 0.0

        #Adding 5 Second Sample
        while diff < 5.0:
    
            if index >= (len(epc_data_arr)):
                break

            cur_time = float(epc_data_arr[index][0])
            channel = epc_data_arr[index][4]

            diff = (cur_time - last_time)*1.0 /1000000.0
            #print diff

            doppler_arr.append(epc_data_dict[elem][index][1])
            rssi_arr.append(epc_data_dict[elem][index][2])
            phase_arr.append(modify_phase(epc_data_dict[elem][index][3] - epc_channel_first_freq_dict[elem][channel][cur_time]))

            index += 1

        last_time = cur_time

        dop_res = 0.0
        rssi_res = 0.0
        phase_res = 0.0
        read_res = 0.0

        if len(doppler_arr) > 0:
            dop_res = np.median(np.array(doppler_arr))
            read_res = (len(doppler_arr)*1.0)/10.0

        if len(rssi_arr) > 0:
            rssi_res = np.median(np.array(rssi_arr))

        if len(phase_arr) > 0:
            phase_res = np.median(np.array(phase_arr))

        data_arr.append(dop_res)
        data_arr.append(rssi_res)
        data_arr.append(phase_res)
        data_arr.append(read_res)

        #print index, len(epc_data_arr)

    feature_matrix.append(data_arr)
    data_epc.append( elem )

#print doppler_epc
#print doppler_arr
#print cart_id_arr


#print "Before"
#for indx in range(len(feature_matrix)):
 #   print len(feature_matrix[indx])

max_row_length = 0

for lst in feature_matrix:

    if max_row_length < len(lst):
        max_row_length = len(lst)

for indx in range(len(feature_matrix)):

    if len(feature_matrix[indx]) < max_row_length:

        diff = max_row_length-len(feature_matrix[indx]) 

        while diff > 0:
            feature_matrix[indx].append(0.0)
            diff -= 1

#print "After"
#for indx in range(len(feature_matrix)):
 #   print len(feature_matrix[indx])

#print feature_matrix

[X, label_arr] = scipy.cluster.vq.kmeans2( np.array(feature_matrix), 2, minit='points')

print label_arr

count = 0

for elem in label_arr:

    #print elem
    if int(elem) == 1:
        #print doppler_epc[count]
        if str(data_epc[count]) in cart_id_arr:
            #print "Match"
            print data_epc[count]
    
    count += 1
