#!/usr/bin/python
import sys
import operator
import numpy as np
import collections

if len(sys.argv) < 2:
    print("python <data_file>")
    sys.exit(-1)

def isOutlier( val ):

    if (val >= np.pi and val <= (np.pi + 0.1)) or ( val <= -np.pi and val >= (-np.pi - 0.1)):
        return True

    if (val <= np.pi and val >= (np.pi - 0.1)) or ( val >= -np.pi and val <= (-np.pi + 0.1)):
        return True

    if (val >= 2*np.pi and val <= (2*np.pi + 0.1)) or ( val <= -2*np.pi and val >= (-2*np.pi - 0.1)):
        return True

    if (val <= 2*np.pi and val >= (2*np.pi - 0.1)) or ( val >= -2*np.pi and val <= (-2*np.pi + 0.1)):
        return True

    if val >= 2*np.pi:
        return True

    if val <= -2*np.pi:
        return True

    return False


fhandle = open( sys.argv[1], 'r')
fLines1 = fhandle.readlines()
fhandle.close()


#print(channel_id_dict)
#print(freq_offset_arr)

data_dict = {}
data_arr_ref = []

tag_arr = []
ref_arr = []


#Reading of files and separating the data into two dicts : one for calibration and one for testing

for line in fLines1:
    l_str = line.strip().split(',')
    epc = str(l_str[0])
    tstamp = float(l_str[3])
    rssi = float(l_str[7])
    channel = float(l_str[8])
    phase = float(l_str[9])

    if epc == "E28011606000020612AF206B" or epc == "E28011606000020612AF208B": #actual_epc == "E28011606000020612AF206B": #or ref_epc == "E28011606000020612AF208B" or epc == "E28011606000020612AF521E": #Right Tag
        data_dict[tstamp] = [ epc, phase ]
        #data_dict[tstamp] = rssi

    if epc == "E28011606000020612AF206B":
        tag_arr.append(phase)
    elif epc == "E28011606000020612AF208B":
        ref_arr.append(phase)

#Finding the phase difference by going time increasing wise
ordered_data_dict = collections.OrderedDict(sorted(data_dict.items()))

prev_val = 0.0 
phase_dict_arr = []
time_stamp_arr = []
epc_arr = []

first_flag = True
first_time = 0.0

#print("Channel, Base_Channel, Actual_Phase, Scaled_Phase, Diff")
first_phase_dict = {}

for key, val in ordered_data_dict.items():

    epc = val[0]
    elem = val[1]

    epc_arr.append(epc)
 
    if epc not in first_phase_dict:
        first_phase_dict[epc] = phase

    #Correction Calculation
    if first_flag == True:
        first_flag = False
        first_time = key
        time_stamp_arr.append( 0.0 )
    else:
        time_stamp_arr.append( (key - first_time)/1000.0 )

    new_val = (float(elem))%(2*np.pi)
    new_val1 = (float(elem))#For RSSI

    if first_flag == False:
        if ( isOutlier(abs(new_val - prev_val))  == True ) :
            new_val = prev_val

    prev_val = new_val
    #if channel == 908.75:
    phase_dict_arr.append(new_val)
    #phase_dict_arr.append(new_val1) #For RSSI


diff_arr = phase_dict_arr

fhandle = open('line-plot-phase-vs-coverage.dat', 'w')


diff_arr_unwrap = np.unwrap( np.array( diff_arr) )
tag_arr_unwrap = np.unwrap( np.array( tag_arr) )
ref_arr_unwrap = np.unwrap( np.array( ref_arr) )

count = 0
count1 = 0
count2 = 0

prev_tag_val = first_phase_dict["E28011606000020612AF208B"]
#prev_ref_tag_val = first_phase_dict["E28011606000020612AF208B"]
prev_ref_tag_val = 0.0

for elem in diff_arr_unwrap:
#for elem in diff_arr:
    #print(elem)
    #Second column is tag_of_interest and Third column is reference_tag
    if epc_arr[count] == "E28011606000020612AF208B":
        fhandle.write( str(time_stamp_arr[count]) + " " + str(ref_arr_unwrap[count1]) + " " + str(prev_ref_tag_val) + "\n")
        prev_tag_val = ref_arr[count1]
        count1 += 1
    elif epc_arr[count] == "E28011606000020612AF206B":
        fhandle.write( str(time_stamp_arr[count]) + " " + str(prev_tag_val) + " " + str(ref_arr_unwrap[count2]) + "\n")
        prev_ref_tag_val = ref_arr[count2]
        count2 += 1

    count += 1

fhandle.close()
