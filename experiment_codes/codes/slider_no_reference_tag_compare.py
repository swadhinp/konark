#!/usr/bin/python
import sys
import operator
import numpy as np
import scipy.signal
from sklearn.cluster import MeanShift, estimate_bandwidth
from sklearn import preprocessing
from sklearn.cluster import AgglomerativeClustering
import scipy.cluster.vq

if len(sys.argv) < 2:
    print("python <data_file> <time_file>")
    sys.exit(-1)

fhandle = open( sys.argv[1], 'r')
fLines1 = fhandle.readlines()
fhandle.close()

fhandle = open( sys.argv[2], 'r')
fLines2 = fhandle.readlines()
fhandle.close()


data_dict = {}
ref_data_dict = {}
time_list = []

for line in fLines2:
    l_str = line.strip().split(':')
    time_list.append( float(l_str[0]) )

time_diff_arr = []
last_time = time_list[0]

for i in range(1,len(time_list)):
    time_diff_arr.append(time_list[i] - last_time)
    last_time = time_list[i]

#print(time_diff_arr)

first_flag = True
last_time = 0.0
posn_count = 0
count = 1

for line in fLines1:
    l_str = line.strip().split(',')
    epc = str(l_str[0])
    tstamp = float(l_str[3])
    channel = float(l_str[8])
    phase = float(l_str[9])

    
    if epc == "E28011606000020612B034AC" :
        count += 1
        if first_flag == True:

            last_time = tstamp
            data_dict[posn_count] = {}
            data_dict[posn_count][channel] = [ float(phase)  ]
            first_flag = False

        else:
            time_diff = ((tstamp-last_time)*1.0)/1000000.0
            
            if posn_count > 9:
                break

            if time_diff > time_diff_arr[posn_count]:
                posn_count = posn_count + 1
                data_dict[posn_count] = {}
                last_time = tstamp

            if channel not in data_dict[posn_count]:
                data_dict[posn_count][channel] = [ float(phase)  ]
            else:
                data_dict[posn_count][channel].append( float(phase)  )

#print(data_dict.keys())

def find_cluster_centers( phase_arr ):

    arr = []
    for elem in phase_arr:
        arr.append( [float(elem)] )

    #MeanShift Clustering
    ms = MeanShift(bandwidth= 0.5)
    ms.fit(np.array(arr))

    cluster_centers = []

    cluster_centers = ms.cluster_centers_

    return cluster_centers

def find_difference( arr ):
    v1 = arr[0]
    v2 = arr[1]

    max1 = v1 if v1 > v2 else v2
    min1 = v1 if v1 < v2 else v2

    v3 = arr[2]
    v4 = arr[3]

    max2 = v3 if v3 > v4 else v4
    min2 = v3 if v3 < v4 else v4

    x =((max1-max2) + (min1-min2))/2.0

    y = ((v1 - v3) + (v1 - v4) + (v2 - v3) + (v2 - v4))*1.0/4.0

    #y = np.median(np.array([(v1 - v3) , (v1 - v4) , (v2 - v3) , (v2 - v4)]))

    return x


#Modifying all phases of different positions to a specific channel

offset = 0.11
keylist = sorted(list(data_dict[0].keys()))
#Special handling of the first one
base_channel = keylist[0]

phase_dict_arr = {}

for pos_indx in data_dict:

    phase_dict_arr[pos_indx] = []

    for channel in data_dict[pos_indx]:
    
        diff = int(((float(channel) - float(base_channel))*1.0)/0.5)
        correction = offset*diff

        for elem in data_dict[pos_indx][channel]:
            new_val = (float(elem) + correction)%(2*np.pi)
            phase_dict_arr[pos_indx].append(new_val)


#print phase_dict_arr.keys()
#KMeans Clustering
#print("KMeans")
#[ cluster_centers1, X ] = scipy.cluster.vq.kmeans2( np.array(tag_phase_arr1), 2)
#[ cluster_centers2, Y ] = scipy.cluster.vq.kmeans2( np.array(ref_phase_arr1), 2)
#print (cluster_centers2)
#print (cluster_centers1)
#diff1 = find_difference( [cluster_centers1[0], cluster_centers1[1], cluster_centers2[0], cluster_centers2[1] ])
#print (count_cover + " " + str(diff1))

#Phase Distribution Debugging

fhandle = open('phase-dist-2.dat', 'w')
for elem in phase_dict_arr[0]:
    fhandle.write( str(elem) + "\n")
fhandle.close()

ref_phase_arr = phase_dict_arr[0]

fhandle = open('line-plot-phase-vs-coverage.dat', 'w')

for i in list(range(1,10)):

    tag_phase_arr = phase_dict_arr[i]

    cluster_centers1 = find_cluster_centers( tag_phase_arr )
    cluster_centers2 = find_cluster_centers( ref_phase_arr )

    c1 = 0.0
    c2 = 0.0

    if ( len(cluster_centers1) > 2 ):
        a = sorted( [ cluster_centers1[0][0], cluster_centers1[1][0], cluster_centers1[2][0] ] )
        c1 = ((float(a[0])+(np.pi*2.0) + float(a[2]))/2.0)%(np.pi*2.0)
        c2 = a[1]
    else:
        c1 = cluster_centers1[0][0]
        c2 = cluster_centers1[1][0]

    c3 = 0.0
    c4 = 0.0

    if ( len(cluster_centers2) > 2 ):
        a = sorted( [ cluster_centers2[0][0], cluster_centers2[1][0], cluster_centers2[2][0] ] )
        c3 = ((float(a[0])+ (np.pi*2.0) + float(a[2]))/2.0)%(np.pi*2.0)
        c4 = a[1]
    else:
        c3 = cluster_centers2[0][0]
        c4 = cluster_centers2[1][0]

    #print("MeanShift")
    #print (cluster_centers2)
    #print (cluster_centers1)

    #print (c3,c4)
    #print (c1,c2)

    diff1 = find_difference( [c1, c2, c3, c4])

    fhandle.write( str(i) + " " + str(diff1) + "\n")

fhandle.close()
