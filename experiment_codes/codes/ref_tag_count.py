#!/usr/bin/python
import sys
import operator

fhandle = open( sys.argv[1], 'r' )
flines = fhandle.readlines()
fhandle.close()

fhandle1 = open( sys.argv[2], 'r' )
flines1 = fhandle1.readlines()
fhandle1.close()

epc_count_arr = []
ref_epc_arr = []

for line in flines:
    l_lst = line.strip().split(',')

    epc = l_lst[0]

    if epc not in epc_count_arr:
        epc_count_arr.append(epc)

for line in flines1:
    epc = line.strip()
    ref_epc_arr.append(epc)

match_count = 0

for elem in ref_epc_arr:

    if elem in epc_count_arr:
        match_count += 1

print match_count

