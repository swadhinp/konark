import os
import sys

if len(sys.argv) <5:
    print("python <file1> <file2> <file3> <file4> <file5>")
    sys.exit(-1)

flines = {}

for i in range(1,6):
    fhandle = open(sys.argv[i] , 'r')
    flines[i] = fhandle.readlines()
    fhandle.close()

fhandle = open( "phase_channel_diff_duration.txt", 'w')

for i in range(50):
    fhandle.write( flines[1][i].strip().split(':')[0] + ";" + flines[1][i].strip().split(':')[1] + ";" +
            flines[2][i].strip().split(':')[1] + ";" + flines[3][i].strip().split(':')[1] + ";" +
            flines[4][i].strip().split(':')[1] + ";" + flines[5][i].strip().split(':')[1] + "\n")
fhandle.close()

