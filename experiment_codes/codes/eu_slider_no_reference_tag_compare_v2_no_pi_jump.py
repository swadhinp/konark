#!/usr/bin/python
import sys
import operator
import numpy as np
import scipy.signal
from sklearn.cluster import MeanShift, estimate_bandwidth
from sklearn import preprocessing
from sklearn.cluster import AgglomerativeClustering
import scipy.cluster.vq
import collections

if len(sys.argv) < 3:
    print("python <data_file> <time_file>")
    sys.exit(-1)

def isOutlier( val ):

    if (val >= np.pi and val <= (np.pi + 0.1)) or ( val <= -np.pi and val >= (-np.pi - 0.1)):
        return True

    if (val <= np.pi and val >= (np.pi - 0.1)) or ( val >= -np.pi and val <= (-np.pi + 0.1)):
        return True

    if (val >= 2*np.pi and val <= (2*np.pi + 0.1)) or ( val <= -2*np.pi and val >= (-2*np.pi - 0.1)):
        return True

    if (val <= 2*np.pi and val >= (2*np.pi - 0.1)) or ( val >= -2*np.pi and val <= (-2*np.pi + 0.1)):
        return True

    if val >= 2*np.pi:
        return True

    if val <= -2*np.pi:
        return True

    return False


fhandle = open( sys.argv[1], 'r')
fLines1 = fhandle.readlines()
fhandle.close()

fhandle = open( sys.argv[2], 'r')
fLines2 = fhandle.readlines()
fhandle.close()


freq_offset_arr = []

#print(channel_id_dict)
#print(freq_offset_arr)

data_dict = {}
data_arr_ref = []
time_list = []

for line in fLines2:
    l_str = line.strip().split(':')
    time_list.append( float(l_str[0]) )

time_diff_arr = []
last_time = time_list[0]

for i in range(1,len(time_list)):
    time_diff_arr.append(time_list[i] - last_time)
    last_time = time_list[i]


#Reading of files and separating the data into two dicts : one for calibration and one for testing

print(time_diff_arr)

first_flag = True
last_time = 0.0
posn_count = 0
count = 1
index = 0

for line in fLines1:
    l_str = line.strip().split(',')
    epc = str(l_str[0])
    tstamp = float(l_str[3])
    channel = float(l_str[8])
    phase = float(l_str[9])
    #print(channel)
    
    #if epc == "E28011606000020612B034AC" :
    #if epc == "E28011606000020612B000ED" :

    if epc == "E28011606000020612AEE43D": #Right Tag
    #if epc == "E28011606000020612AF52AD": #Middle Tag
    #if epc == "E28011606000020612AEE42D": #Left Tag
        count += 1
        if first_flag == True:

            last_time = tstamp
            data_arr_ref = [phase]
            #print(channel,index)
            index = index + 1
            first_flag = False

        else:

            time_diff = ((tstamp-last_time)*1.0)/1000000.0

            if posn_count > 0:
                if time_diff > time_diff_arr[1]:
                    break
            
            if posn_count == 0:

                if time_diff > time_diff_arr[0]:
                    posn_count = 1
                    last_time = tstamp

            if posn_count == 0:
                data_arr_ref.append(phase)
            else:
                data_dict[tstamp] = phase

#for ch in channel_seq_arr:
 #   print(ch)
#print(channel_order_dict.keys())

#Findind out calibrated phase differences among channel

phase_median = np.median(np.array(data_arr_ref)) 
#for ch in keylist:
 #   print(ch, np.median(np.array(data_dict_ref[ch])))


#Finding the phase difference by going time increasing wise
ordered_data_dict = collections.OrderedDict(sorted(data_dict.items()))

prev_val = 0.0 
phase_dict_arr = []
first_flag = True

#print("Channel, Base_Channel, Actual_Phase, Scaled_Phase, Diff")

for key, val in ordered_data_dict.items():
    elem = val

    #Correction Calculation
    if first_flag == True:
        first_flag = False

    new_val = (float(elem))%(2*np.pi)

    if first_flag == False:
        if ( isOutlier(abs(new_val - prev_val))  == True ) :
            new_val = prev_val

    prev_val = new_val
    #if channel == 908.75:
    phase_dict_arr.append(new_val)


diff_arr = phase_dict_arr
win_size = 151
diff_arr_smoothed = scipy.signal.medfilt(diff_arr, win_size)
#diff_arr_smoothed = scipy.signal.wiener(diff_arr, win_size)

diff_arr_smoothed_mean = []

for i in range(len(diff_arr)-win_size):
    diff_arr_smoothed_mean.append(np.mean(np.array(diff_arr[i:i+win_size])))

fhandle = open('line-plot-phase-vs-coverage.dat', 'w')


#Kalman Filtering
n_iter = len(diff_arr)
sz = (n_iter,) # size of array
z = diff_arr

Q = 0.0032 # process variance

# allocate space for arrays
xhat=np.zeros(sz)      # a posteri estimate of x
P=np.zeros(sz)         # a posteri error estimate
xhatminus=np.zeros(sz) # a priori estimate of x
Pminus=np.zeros(sz)    # a priori error estimate
K=np.zeros(sz)         # gain or blending factor

R = 0.0032 # estimate of measurement variance, change to see effect

# intial guesses
xhat[0] = 0.0
P[0] = 1.0

for k in range(1,n_iter):
    # time update
    xhatminus[k] = xhat[k-1]
    Pminus[k] = P[k-1]+Q

    # measurement update
    K[k] = Pminus[k]/( Pminus[k]+R )
    xhat[k] = xhatminus[k]+K[k]*(z[k]-xhatminus[k])
    P[k] = (1-K[k])*Pminus[k]

diff_arr_smoothed_kalman = scipy.signal.medfilt(z, win_size)
count = 1

for elem in diff_arr:
    #print(elem)
    fhandle.write( str(count) + " " + str(elem) + "\n")
    count += 1

fhandle.close()
