#Here we will use reference tags inside and outside cart
#This is the final version of cart clustering code

#!/usr/bin/python
import sys 
import operator
import numpy as np
import scipy.signal
import scipy.cluster.vq

if len(sys.argv) < 5:
    print("python <cart_file> <outside_file> <cart_id_file> <reference_tag_cart_file> <refernce_tag_outside_file>")
    sys.exit(-1)


fhandle = open( sys.argv[1], 'r')
cartLines = fhandle.readlines()
fhandle.close()

fhandle = open( sys.argv[2], 'r')
outLines = fhandle.readlines()
fhandle.close()

fhandle = open( sys.argv[3], 'r')
cart_ids = fhandle.readlines()
fhandle.close()


fhandle = open( sys.argv[4], 'r')
ref_ids = fhandle.readlines()
fhandle.close()

fhandle = open( sys.argv[5], 'r')
out_ref_ids = fhandle.readlines()
fhandle.close()

def isOutlier( val ):
    
    if (val >= np.pi and val <= (np.pi + 0.01)) or ( val <= -np.pi and val >= (-np.pi - 0.01)):
        #print "Pi", val
        return True

    if (val <= np.pi and val >= (np.pi - 0.01)) or ( val >= -np.pi and val <= (-np.pi + 0.01)):
        #print "Pi", val
        return True

    if (val >= 2*np.pi and val <= (2*np.pi + 0.01)) or ( val <= -2*np.pi and val >= (-2*np.pi - 0.01)):
        #print "2Pi", val
        return True

    if val >= 2*np.pi:
        return True
    if val <= -2*np.pi:
        return True

    if (val <= 2*np.pi and val >= (2*np.pi - 0.01)) or ( val >= -2*np.pi and val <= (-2*np.pi + 0.01)):
        #print "2Pi", val
        return True

    return False

def modify_phase( val ):

    if val == np.pi or val == -np.pi or val == 2*np.pi or val == -2*np.pi:
        return 0.0
    else:
        return val


#Temporal Matrix Creation
cart_id_arr = []
ref_id_arr = []
out_ref_id_arr = []

global epc_data_dict
epc_data_dict = {}
epc_first_data = {}

tstamp_arr = []
epc_arr = []
epc_channel_first_freq_dict = {}

first_flag = True
last_channel = 0.0
channel_seq_arr = []
per_channel_phase_dict = {}
index = 0
channel_order_dict = {}

cart_count = 0
for cart in cart_ids:
    cart_id_arr.append(str(cart).strip())
    cart_count += 1

ref_count = 0
for ref in ref_ids:
    ref_id_arr.append(str(ref).strip())
    ref_count += 1

#Data from Cart Tags
for line in cartLines:
    l_str = line.strip().split(',')
            
    epc = str(l_str[0])
    tstamp = float(l_str[3])
    doppler = float(l_str[5])
    rssi = float(l_str[7])
    channel = float(l_str[8])
    phase = float(l_str[9])

    tstamp_arr.append( tstamp )

    #EPC wise data population
    if epc not in epc_data_dict:
        
        epc_data_dict[epc] = [[tstamp,doppler,rssi,phase,channel]]
        epc_first_data[epc] = tstamp
        epc_arr.append( epc )
        
        epc_channel_first_freq_dict[epc] ={}
        epc_channel_first_freq_dict[epc][channel] = {}
        epc_channel_first_freq_dict[epc][channel][tstamp] = phase
        
    else:
        epc_data_dict[epc].append([tstamp,doppler,rssi,phase,channel])

        if channel not in epc_channel_first_freq_dict[epc]:
            epc_channel_first_freq_dict[epc][channel] = {}
            epc_channel_first_freq_dict[epc][channel][tstamp] = phase
        else:
            epc_channel_first_freq_dict[epc][channel][tstamp] = phase

#Data from Outside Tags
for line in outLines:
    l_str = line.strip().split(',')
            
    epc = str(l_str[0])
    tstamp = float(l_str[3])
    doppler = float(l_str[5])
    rssi = float(l_str[7])
    channel = float(l_str[8])
    phase = float(l_str[9])

    tstamp_arr.append( tstamp )
    
    if epc not in epc_data_dict:
        #One reference tag based Calibration
        #print epc, ref_id_arr[0]
        epc_data_dict[epc] = [[tstamp,doppler,rssi,phase,channel]]
        epc_first_data[epc] = tstamp
        epc_arr.append( epc )
        
        epc_channel_first_freq_dict[epc] ={}
        epc_channel_first_freq_dict[epc][channel] = {}
        epc_channel_first_freq_dict[epc][channel][tstamp] = phase

    else:
        epc_data_dict[epc].append([tstamp,doppler,rssi,phase,channel])

        if channel not in epc_channel_first_freq_dict[epc]:
            epc_channel_first_freq_dict[epc][channel] = {}
            epc_channel_first_freq_dict[epc][channel][tstamp] = phase
        else:
            epc_channel_first_freq_dict[epc][channel][tstamp] = phase

    #Channel wise Phase List Population for only Reference Tags inside the Cart

    if epc in ref_id_arr:
        if first_flag == True:
            last_channel = channel
            channel_seq_arr.append( channel )
            first_flag = False
        else:
            if last_channel != channel:
                channel_seq_arr.append(channel)
                last_channel = channel

    if channel not in channel_order_dict:
        channel_order_dict[channel] = index
        index = index + 1

    if epc in ref_id_arr:

        if channel not in per_channel_phase_dict:
            per_channel_phase_dict[channel] = {}
            per_channel_phase_dict[channel][epc] = [phase]
        else:
            if epc not in per_channel_phase_dict[channel]:
                per_channel_phase_dict[channel] = {}
                per_channel_phase_dict[channel][epc] = [phase]
            else:
                per_channel_phase_dict[channel][epc].append(phase)


###################################################################################
####################### Phase Calibration Start ###################################
###################################################################################

#Findind out calibrated phase differences among channel
phase_peak_arr = []
keylist = sorted(list(per_channel_phase_dict.keys()))

#print per_channel_phase_dict.keys()

#Going by the order of channel not by the sorted list
prev_channel = channel_seq_arr[0]
prev_epc = per_channel_phase_dict[prev_channel].keys()[0]

#Calculating the frequency offsets per channel
count = 1
prev_diff = 0.0
offset = -0.11

freq_offset_arr = []

for ix in range( len(channel_seq_arr) - 1):

    cur_channel = channel_seq_arr[ix+1]
    diff = 0.0

    #print cur_channel, per_channel_phase_dict[cur_channel].keys()
    for epc in per_channel_phase_dict[cur_channel]:
        if epc in per_channel_phase_dict[prev_channel]:
            diff = np.median(np.array(per_channel_phase_dict[cur_channel][epc])) - np.median(np.array(per_channel_phase_dict[prev_channel][epc]))
            break

    if diff == 0.0:
        diff = (((float(cur_channel) - float(prev_channel))*1.0)/0.5)*offset

    #print cur_channel, prev_channel, diff
    freq_offset_arr.append(diff*-1.0)
    
    prev_channel = cur_channel
    count += 1
    prev_diff = diff

#print(freq_offset_arr)

#channel_phase_dict = {}

#for channel in per_channel_phase_dict:
 #   channel_phase_dict[channel] = np.median(np.array(per_channel_phase_dict[channel]))

base_channel = 902.75
first_flag = True

#print("Channel, Base_Channel, Actual_Phase, Scaled_Phase, Diff")

for epc in epc_data_dict:

    epc_data_arr = epc_data_dict[epc]
    prev_val = 0.0 
    phase_temp_arr = []

    for inx in range(len(epc_data_arr)):
        val_list = epc_data_arr[inx]
        tstamp = val_list[0]
        phase = val_list[3]
        channel = val_list[4]

        #Correction Calculation
        if first_flag == True:
            base_channel = 902.75#Random Valid channel
            first_flag = False

        diff = 0.0

        #if channel not in keylist:
         #   print channel
    
        if channel_order_dict[channel] > channel_order_dict[base_channel]:

            mov_list = list(range( channel_order_dict[base_channel], channel_order_dict[channel] ))
            indx = len(mov_list)-1

            while indx >= 0:
                i = mov_list[indx]
                indx = indx - 1
                #print("IF:", i)
                diff = diff + freq_offset_arr[i]
                #diff = diff + 0.11
        else:
            mov_list = list(range(channel_order_dict[channel], channel_order_dict[base_channel] ))
        
            for i in mov_list:
                #print("ELSE:", i)
                diff = diff + freq_offset_arr[i]*(-1.0)
                #diff = diff + 0.11*(-1.0)


        correction = diff
        #diff = ((float(channel) - float(base_channel))*1.0)/0.5
        #correction = 0.11*diff
        
        new_val = (float(phase) + correction)%(2*np.pi)
        #print epc, channel, phase, new_val

        #phase_temp_arr.append(new_val)
        epc_data_dict[epc][inx][3] = new_val

#for epc in epc_data_dict:
 #   for ix in range(len(epc_data_dict[epc])):
  #      print epc_data_dict[epc][ix][3]

    #count = 0
    #for elem_list in epc_data_dict[epc]:
        #epc_data_dict[epc][count][3] = phase_temp_arr[count]
        #count += 1

###################################################################################
####################### Phase Calibration End #####################################
###################################################################################

tmin = np.min(np.array(tstamp_arr))
tmax = np.max(np.array(tstamp_arr))

size = 1 #second
column_size = int((tmax - tmin)*1.0/(size*1000000.0)) + 1
row_size = len(epc_arr)

#print row_size

tag_data_matrix = []
ref_data_matrix = []
out_ref_data_matrix = []

#0 -> Doppler, 2 -> RSSI, 3 -> Phase, 4-> Read Rate

for i in range(row_size):
    temp_row = []

    for j in range(column_size):
        temp_row.append([[], [], [], [], [0.0]])
     
    tag_data_matrix.append(temp_row)    

for i in range(ref_count):
    temp_row = []

    for j in range(column_size):
        temp_row.append([[], [], [], [], [0.0]])
     
    ref_data_matrix.append(temp_row)    

out_ref_count = 0
for out_ref in out_ref_ids:
    out_ref_id_arr.append(str(out_ref).strip())
    out_ref_count += 1

for i in range(out_ref_count):
    temp_row = []

    for j in range(column_size):
        temp_row.append([[], [], [], [], [0.0]])
     
    out_ref_data_matrix.append(temp_row)    

data_epc = []

def timeToIndex( tstamp ):
    indx = int((tstamp - tmin)*1.0/(size*1000000.0)) - 1
    return int(indx)

count_index = 0
ref_count_index = 0
out_ref_count_index = 0

count = 0

for elem in epc_data_dict:
    
    last_time = float(epc_first_data[elem])
    epc_data_arr = epc_data_dict[elem]

    data_index = 0
    #prev_phase = epc_data_dict[elem][0][3]
    prev_phase = 0.0

    while data_index < len(epc_data_arr):
    
        tstamp = epc_data_dict[elem][data_index][0] 
        doppler = epc_data_dict[elem][data_index][1]
        rssi = epc_data_dict[elem][data_index][2]
        phase = epc_data_dict[elem][data_index][3]
        channel = epc_data_dict[elem][data_index][4]

        #Storing data to the data arr
        ix = timeToIndex(tstamp)
        tag_data_matrix[count_index][ix][0].append(doppler)
        tag_data_matrix[count_index][ix][1].append(rssi)
        tag_data_matrix[count_index][ix][2].append(phase) #Tracking phase diff for US reader and absolute phase for EU reader
        #if isOutlier(phase) == False:
        #tag_data_matrix[count_index][ix][2].append(modify_phase(phase - epc_channel_first_freq_dict[elem][channel][tstamp])) #Tracking phase diff for US reader and absolute phase for EU reader
        #else:
        #   tag_data_matrix[count_index][ix][2].append(prev_phase) #Tracking phase diff for US reader and absolute phase for EU reader
        
        prev_phase = modify_phase(phase - epc_channel_first_freq_dict[elem][channel][tstamp])
        tag_data_matrix[count_index][ix][3].append(channel)
        tag_data_matrix[count_index][ix][4][0] += 1
        
        if elem in ref_id_arr:
            
            ref_data_matrix[ref_count_index][ix][0].append(doppler)
            ref_data_matrix[ref_count_index][ix][1].append(rssi)
            ref_data_matrix[ref_count_index][ix][2].append(phase) #Tracking phase diff for US reader and absolute phase for EU reader
            #if isOutlier(phase) == False:
            #ref_data_matrix[ref_count_index][ix][2].append(modify_phase(phase - epc_channel_first_freq_dict[elem][channel][tstamp])) #Tracking phase diff for US reader and absolute phase for EU reader
            #else:
             #   ref_data_matrix[ref_count_index][ix][2].append(prev_phase) #Tracking phase diff for US reader and absolute phase for EU reader
            prev_phase = modify_phase(phase - epc_channel_first_freq_dict[elem][channel][tstamp])
            #ref_data_matrix[ref_count_index][ix][2].append(modify_phase(phase - epc_channel_first_freq_dict[elem][channel][tstamp]))
            ref_data_matrix[ref_count_index][ix][3].append(channel)
            ref_data_matrix[ref_count_index][ix][4][0] += 1

        elif elem in out_ref_id_arr:

            out_ref_data_matrix[out_ref_count_index][ix][0].append(doppler)
            out_ref_data_matrix[out_ref_count_index][ix][1].append(rssi)
            out_ref_data_matrix[out_ref_count_index][ix][2].append(phase) #Tracking phase diff for US reader and absolute phase for EU reader
            #if isOutlier(phase) == False:
            #out_ref_data_matrix[out_ref_count_index][ix][2].append(modify_phase(phase - epc_channel_first_freq_dict[elem][channel][tstamp])) #Tracking phase diff for US reader and absolute phase for EU reader
            #else:
             #   out_ref_data_matrix[out_ref_count_index][ix][2].append(prev_phase) #Tracking phase diff for US reader and absolute phase for EU reader
            prev_phase = modify_phase(phase - epc_channel_first_freq_dict[elem][channel][tstamp])
            #out_ref_data_matrix[out_ref_count_index][ix][2].append(modify_phase(phase - epc_channel_first_freq_dict[elem][channel][tstamp]))
            out_ref_data_matrix[out_ref_count_index][ix][3].append(channel)
            out_ref_data_matrix[out_ref_count_index][ix][4][0] += 1

        data_index += 1

    #Storing cooresponding epc for epc to index mapping
    data_epc.append( elem )
    count_index += 1

    if elem in ref_id_arr:
        ref_count_index += 1

    if elem in out_ref_id_arr:
        out_ref_count_index += 1

feature_matrix = []
ref_matrix = []
out_ref_matrix = []

#print ref_data_matrix

for i in range(row_size):
    feature_matrix.append([])

for i in range(row_size):

    prev_rssi_median = 0.0
    prev_phase_median = 0.0

    for j in range(column_size):

        doppler_median = 0.0
        doppler_std = 0.0
        #rssi_median = prev_rssi_median
        rssi_median = 0.0
        rssi_std = 0.0
        #phase_median = prev_phase_median
        phase_median = 0.0
        phase_std = 0.0
        channel_median = 0.0

        if len(tag_data_matrix[i][j][0]) > 0:
            doppler_median = np.median( np.array(tag_data_matrix[i][j][0] ) )
            doppler_std = np.std( np.array(tag_data_matrix[i][j][0] ) )
        if len(tag_data_matrix[i][j][1]) > 0:
            rssi_median = np.median( np.array(tag_data_matrix[i][j][1] ) )
            rssi_std = np.std( np.array(tag_data_matrix[i][j][1] ) )
            prev_rssi_median = rssi_median
        if len(tag_data_matrix[i][j][2]) > 0:
            phase_median = np.median( np.array(tag_data_matrix[i][j][2] ) )
            phase_std = np.std( np.array(tag_data_matrix[i][j][2] ) )
            prev_phase_median = phase_median
        if len(tag_data_matrix[i][j][3]) > 0:
            channel_median = np.median( np.array(tag_data_matrix[i][j][3] ) )

        read_rate = tag_data_matrix[i][j][4][0]/(size*1.0)

        #feature_matrix[i].append(doppler_median)
        #feature_matrix[i].append(doppler_std)
        #feature_matrix[i].append(rssi_median)
        #feature_matrix[i].append(rssi_std)
        #feature_matrix[i].append(phase_median)
        #feature_matrix[i].append(phase_std)
        #feature_matrix[i].append(channel_median)
        feature_matrix[i].append(read_rate)
        

for i in range(ref_count):
    ref_matrix.append([])

for i in range(ref_count):

    prev_rssi_median = 0.0
    prev_phase_median = 0.0

    for j in range(column_size):
        
        doppler_median = 0.0
        doppler_std = 0.0
        #rssi_median = prev_rssi_median
        rssi_median = 0.0
        rssi_std = 0.0
        #phase_median = prev_phase_median
        phase_median = 0.0
        phase_std = 0.0
        channel_median = 0.0

        if len(ref_data_matrix[i][j][0]) > 0:
            doppler_median = np.median( np.array(ref_data_matrix[i][j][0] ) )
            doppler_std = np.std( np.array(ref_data_matrix[i][j][0] ) )
        if len(ref_data_matrix[i][j][1]) > 0:
            rssi_median = np.median( np.array(ref_data_matrix[i][j][1] ) )
            prev_rssi_median = rssi_median
            rssi_std = np.std( np.array(ref_data_matrix[i][j][1] ) )
        if len(ref_data_matrix[i][j][2]) > 0:
            phase_median = np.median( np.array(ref_data_matrix[i][j][2] ) )
            phase_std = np.std( np.array(ref_data_matrix[i][j][2] ) )
            prev_phase_median = phase_median
        if len(ref_data_matrix[i][j][3]) > 0:
            channel_median = np.median( np.array(ref_data_matrix[i][j][3] ) )

        read_rate = (ref_data_matrix[i][j][4][0]*1.0)/(size*1.0)

        #ref_matrix[i].append(doppler_median)
        #ref_matrix[i].append(doppler_std)
        #ref_matrix[i].append(rssi_median)
        #ref_matrix[i].append(rssi_std)
        #ref_matrix[i].append(phase_median)
        #ref_matrix[i].append(phase_std)
        #ref_matrix[i].append(channel_median)
        ref_matrix[i].append(read_rate)

for i in range(out_ref_count):
    out_ref_matrix.append([])


for i in range(out_ref_count):

    prev_rssi_median = 0.0
    prev_phase_median = 0.0

    for j in range(column_size):

        doppler_median = 0.0
        doppler_std = 0.0
        #rssi_median = prev_rssi_median
        rssi_median = 0.0
        rssi_std = 0.0
        phase_median = 0.0
        phase_std = 0.0
        channel_median = 0.0

        if len(out_ref_data_matrix[i][j][0]) > 0:
            doppler_median = np.median( np.array(out_ref_data_matrix[i][j][0] ) )
            doppler_std = np.std( np.array(out_ref_data_matrix[i][j][0] ) )
        if len(out_ref_data_matrix[i][j][1]) > 0:
            rssi_median = np.median( np.array(out_ref_data_matrix[i][j][1] ) )
            rssi_std = np.std( np.array(out_ref_data_matrix[i][j][1] ) )
            prev_rssi_median = rssi_median
        if len(out_ref_data_matrix[i][j][2]) > 0:
            phase_median = np.median( np.array(out_ref_data_matrix[i][j][2] ) )
            phase_std = np.std( np.array(out_ref_data_matrix[i][j][2] ) )
            prev_phase_median = phase_median
        if len(out_ref_data_matrix[i][j][3]) > 0:
            channel_median = np.median( np.array(out_ref_data_matrix[i][j][3] ) )
        
        read_rate = (out_ref_data_matrix[i][j][4][0]*1.0)/(size*1.0)

        #out_ref_matrix[i].append(doppler_median)
        #out_ref_matrix[i].append(doppler_std)
        #out_ref_matrix[i].append(rssi_median)
        #out_ref_matrix[i].append(rssi_std)
        #out_ref_matrix[i].append(phase_median)
        #out_ref_matrix[i].append(phase_std)
        #out_ref_matrix[i].append(channel_median)
        out_ref_matrix[i].append(read_rate)


#print "Number of Total Tags Read : ", len(feature_matrix)
#print "Number of Reference Tags : ", len(ref_matrix)
#print "Number of Outside Refernce Tags : ", len(out_ref_matrix)
#print "Feature Matrix of Reference Tags :"
#print ref_matrix
#print "Feature Matrix of Outside Reference Tags :"
#print out_ref_matrix
#print "Feature Matrix of All Tags :"
#print feature_matrix

#[centroid_ref, distort] = scipy.cluster.vq.kmeans( scipy.cluster.vq.whiten(np.array(mean_ref_matrix)), 1 )
#[centroid_out_ref, out_distort] = scipy.cluster.vq.kmeans( scipy.cluster.vq.whiten(np.array(mean_out_ref_matrix)), 1 )

[centroid_ref, distort] = scipy.cluster.vq.kmeans( np.array(ref_matrix), 1 )
[centroid_out_ref, out_distort] = scipy.cluster.vq.kmeans( np.array(out_ref_matrix), 1 )

#print distort, out_distort
#print(centroid_ref, centroid_out_ref)

#print(ref_matrix)
#print(out_ref_matrix)

seed_arr = [centroid_ref[0], centroid_out_ref[0]]
#seed_arr = [ref_matrix[0], out_ref_matrix[0]]

#[X, label_arr] = scipy.cluster.vq.kmeans2( scipy.cluster.vq.whiten(np.array(mean_feature_matrix)), np.array(seed_arr), minit='matrix')
#[X, label_arr] = scipy.cluster.vq.kmeans2( scipy.cluster.vq.whiten(np.array(feature_matrix)), 2, minit='points')
#[X, label_arr] = scipy.cluster.vq.kmeans2( np.array(feature_matrix), 2, minit='points')

#[X, label_arr] = scipy.cluster.vq.kmeans2( np.array(feature_matrix), 2, minit='points')
[X, label_arr] = scipy.cluster.vq.kmeans2( np.array(feature_matrix), np.array(seed_arr), minit='matrix')

#print label_arr
#print len(label_arr)

count = 0

cluster1_epc = []
cluster2_epc = []

for i in range(len(label_arr)):

    elem = label_arr[i]

    if int(elem) == 1:
        cluster1_epc.append(data_epc[count])
    elif int(elem) == 0:
        cluster2_epc.append(data_epc[count])

    count += 1

#print "Match in Cluster 1"
#print "Size : " + str(len(cluster1_epc))
match1 = 0

for elem in cart_id_arr:
    if elem in cluster1_epc:
        #print elem
        match1 += 1

#print cluster2_epc
#print match1

#for elem in ref_id_arr:
 #   if elem in cluster1_epc:
  #      match1 += 1

#for elem in out_ref_id_arr:
#    if elem in cluster1_epc:
 #       match1 += 1
#print match1
#print cluster2_epc

#print "Match in Cluster 2"
#print "Size : " + str(len(cluster2_epc))
match2 = 0
    
for elem in cart_id_arr:
    if elem in cluster2_epc:
        match2 += 1

#print match2
for elem in ref_id_arr:
    if elem in cluster2_epc:
        match2 += 1

#for elem in out_ref_id_arr:
 #   if elem in cluster2_epc:
  #      print elem
  #      match2 += 1
#print match2

fpr =  0.0
fnr =  0.0
fp = 0.0
accuracy = 0.0

inside_predicted_items = 0.0
outside_predicted_items = 0.0

if len(cluster2_epc) < len(cluster1_epc):
    inside_predicted_items = len(cluster2_epc)
    outside_predicted_items = match1
else:
    inside_predicted_items = len(cluster1_epc)
    outside_predicted_items = match2

#print "cart"
#print cart_count

cart_count = cart_count + 6

if inside_predicted_items >= cart_count :
    fpr = (inside_predicted_items - cart_count)*100.0/(cart_count*1.0)
    fp = (inside_predicted_items - cart_count)

if outside_predicted_items > 0:
    fnr = (outside_predicted_items)*100.0/(cart_count*1.0)

accuracy = 100.0 - (fp*1.0 + outside_predicted_items*1.0)*100/(cart_count*1.0)

print str(fpr) + ":" + str(fnr) + ":" + str(accuracy)

