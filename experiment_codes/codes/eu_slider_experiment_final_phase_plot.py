#!/usr/bin/python
import sys
import operator
import numpy as np
import scipy.signal
import scipy.cluster.vq
import collections

if len(sys.argv) < 2:
    print("python <data_file>")
    sys.exit(-1)

def isOutlier( val ):

    if (val >= np.pi and val <= (np.pi + 0.1)) or ( val <= -np.pi and val >= (-np.pi - 0.1)):
        return True

    if (val <= np.pi and val >= (np.pi - 0.1)) or ( val >= -np.pi and val <= (-np.pi + 0.1)):
        return True

    if (val >= 2*np.pi and val <= (2*np.pi + 0.1)) or ( val <= -2*np.pi and val >= (-2*np.pi - 0.1)):
        return True

    if (val <= 2*np.pi and val >= (2*np.pi - 0.1)) or ( val >= -2*np.pi and val <= (-2*np.pi + 0.1)):
        return True

    if val >= 2*np.pi:
        return True

    if val <= -2*np.pi:
        return True

    return False


fhandle = open( sys.argv[1], 'r')
fLines1 = fhandle.readlines()
fhandle.close()


#print(channel_id_dict)
#print(freq_offset_arr)

data_dict = {}
data_arr_ref = []

#Reading of files and separating the data into two dicts : one for calibration and one for testing

for line in fLines1:
    l_str = line.strip().split(',')
    epc = str(l_str[0])
    tstamp = float(l_str[3])
    rssi = float(l_str[7])
    channel = float(l_str[8])
    phase = float(l_str[9])

    if epc == "300833B2DDD9014000000000": #epc == "E28011606000020612AF208B": #actual_epc == "E28011606000020612AF206B": #or ref_epc == "E28011606000020612AF208B" or epc == "E28011606000020612AF521E": #Right Tag
        data_dict[tstamp] = phase
        #data_dict[tstamp] = rssi

#Finding the phase difference by going time increasing wise
ordered_data_dict = collections.OrderedDict(sorted(data_dict.items()))

prev_val = 0.0 
phase_dict_arr = []
time_stamp_arr = []

first_flag = True
first_time = 0.0

#print("Channel, Base_Channel, Actual_Phase, Scaled_Phase, Diff")

for key, val in ordered_data_dict.items():
    elem = val
    #Correction Calculation
    if first_flag == True:
        first_flag = False
        first_time = key
        time_stamp_arr.append( 0.0 )
    else:
        time_stamp_arr.append( (key - first_time)/1000.0 )

    new_val = (float(elem))%(2*np.pi)
    new_val1 = (float(elem))#For RSSI

    if first_flag == False:
        if ( isOutlier(abs(new_val - prev_val))  == True ) :
            new_val = prev_val

    prev_val = new_val
    #if channel == 908.75:
    phase_dict_arr.append(new_val)
    #phase_dict_arr.append(new_val1) #For RSSI


diff_arr = phase_dict_arr


win_size = 151
diff_arr_smoothed = scipy.signal.medfilt(diff_arr, win_size)
#diff_arr_smoothed = scipy.signal.wiener(diff_arr, win_size)

diff_arr_smoothed_mean = []

for i in range(len(diff_arr)-win_size):
    diff_arr_smoothed_mean.append(np.mean(np.array(diff_arr[i:i+win_size])))

fhandle = open('line-plot-phase-vs-coverage.dat', 'w')

'''
#Kalman Filtering
n_iter = len(diff_arr)
sz = (n_iter,) # size of array
z = diff_arr

Q = 0.0032 # process variance

# allocate space for arrays
xhat=np.zeros(sz)      # a posteri estimate of x
P=np.zeros(sz)         # a posteri error estimate
xhatminus=np.zeros(sz) # a priori estimate of x
Pminus=np.zeros(sz)    # a priori error estimate
K=np.zeros(sz)         # gain or blending factor

R = 0.0032 # estimate of measurement variance, change to see effect

# intial guesses
xhat[0] = 0.0
P[0] = 1.0

for k in range(1,n_iter):
    # time update
    xhatminus[k] = xhat[k-1]
    Pminus[k] = P[k-1]+Q

    # measurement update
    K[k] = Pminus[k]/( Pminus[k]+R )
    xhat[k] = xhatminus[k]+K[k]*(z[k]-xhatminus[k])
    P[k] = (1-K[k])*Pminus[k]

diff_arr_smoothed_kalman = scipy.signal.medfilt(z, win_size)
'''
count = 1

diff_arr_unwrap = np.unwrap( np.array( diff_arr), discont=np.pi )

count = 0
for elem in diff_arr_unwrap:
#for elem in diff_arr:
    #print(elem)
    fhandle.write( str(time_stamp_arr[count]) + " " + str(elem) + "\n")
    count += 1

fhandle.close()
