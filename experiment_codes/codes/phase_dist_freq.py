#!/usr/bin/python
import sys
import operator
import numpy as np
import scipy.signal

if len(sys.argv) < 2:
    print("python <data_file> <freq_offset_file>")
    sys.exit(-1)

fhandle = open( sys.argv[1], 'r')
fLines1 = fhandle.readlines()
fhandle.close()


fhandle = open( sys.argv[2], 'r')
fLines3 = fhandle.readlines()
fhandle.close()

freq_offset_arr = []

for elem in fLines3:
    freq_offset_arr.append( float(elem.strip()) )

channel_id_dict = {}

start_channel = 902.75
channel_id_dict[start_channel] = 0 

for i in list(range(1,50)):
    channel_id_dict[float(start_channel+0.5*i)] = i 

#print(channel_id_dict)
#print(freq_offset_arr)

count = 0
data_dict = {}

for line in fLines1:
    l_str = line.strip().split(',')
    epc = str(l_str[0])
    tstamp = float(l_str[3])
    channel = float(l_str[8])
    phase = float(l_str[9])

    
    #if epc == "E28011606000020612B000ED" :
    if epc == "E28011606000020612AF52AD" :
        count += 1
        key = channel_id_dict[channel]
        if key not in data_dict:
            data_dict[key] = [ float(phase)  ]
        else:
            data_dict[key].append( float(phase)  )

#Modifying all phases of different positions to a specific channel

print(count)
max_col_size = 0
for elem in data_dict:
    if len(data_dict[elem]) > max_col_size:
        max_col_size = len(data_dict[elem])

print(max_col_size)

fhandle = open('phase-dist-freq.dat', 'w')
for elem in data_dict:

    val = np.mean( np.array(data_dict[elem]) )
    #for val in data_dict[elem]:
    fhandle.write( str(elem) )
    count = 0

    for el in data_dict[elem]:
        fhandle.write(" " + str(el))
        count += 1

    while count < max_col_size:
        fhandle.write( " " + str(-1))
        count += 1

    fhandle.write("\n")


fhandle.close()
