#!/usr/bin/python
import sys
import operator
import numpy as np
import scipy.signal
import collections

if len(sys.argv) < 3:
    print("python <data_file> <freq_offset_file> <duration_in_sec>")
    sys.exit(-1)

fhandle = open( sys.argv[1], 'r')
fLines1 = fhandle.readlines()
fhandle.close()


fhandle = open( sys.argv[2], 'r')
fLines3 = fhandle.readlines()
fhandle.close()

durn = float(sys.argv[3])

freq_offset_arr = []

for elem in fLines3:
    freq_offset_arr.append( float(elem.strip()) )

channel_id_dict = {}

start_channel = 902.75
channel_id_dict[start_channel] = 0 

for i in list(range(1,50)):
    channel_id_dict[float(start_channel+0.5*i)] = i 

#print(channel_id_dict)
#print(freq_offset_arr)

count = 0
data_dict = {}

first_flag = True
start_time = 0.0

for line in fLines1:
    l_str = line.strip().split(',')
    epc = str(l_str[0])
    tstamp = float(l_str[3])
    channel = float(l_str[8])
    phase = float(l_str[9])

    if first_flag == True:
        start_time = tstamp
        first_flag = False

    diff = (tstamp - start_time)*1.0/1000000.0

    if diff > durn:
        break
    
    #if epc == "E28011606000020612B000ED" :
    if epc == "E28011606000020612AF52AD" :
        count += 1
        if channel not in data_dict:
            data_dict[channel] = [ float(phase)  ]
        else:
            data_dict[channel].append( float(phase)  )

#Modifying all phases of different positions to a specific channel

#print(count)

#print(new_count)
#print(len(phase_dict_arr))

fhandle = open('per_channel_phase.txt', 'w')

channel_phase_dict = {}

for channel in data_dict:
    bins = np.arange(0,6.01,0.01)
    [ data_arr, dataix_arr ] = np.histogram( np.array(data_dict[channel]), bins )

    data_list = data_arr.tolist()
    dataix_list = dataix_arr.tolist()
    
    #print(channel)
    #mode
    val1_mode = dataix_list[data_list.index(max(data_list))]

    #print("Mode")
    #print(val1_mode)
    #print("Mean")
    val1_mean = np.mean(np.array(data_dict[channel]))
    
    channel_phase_dict[channel] = val1_mean
    #print("Median")
    #print(np.median(np.array(data_dict[channel])))



ordered_data_dict = collections.OrderedDict(sorted(channel_phase_dict.items()))

for channel, phase in ordered_data_dict.items():

    fhandle.write( str(channel) + ":" + str(phase) + "\n")

fhandle.close()
