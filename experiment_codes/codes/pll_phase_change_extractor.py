#!/usr/bin/python
import sys
import operator
import numpy as np
import scipy.signal
import matplotlib.pyplot as plt
import scipy.fftpack

if len(sys.argv) < 1:
    print "python <data_file_1>"
    sys.exit(-1)

fhandle = open( sys.argv[1], 'r')
fLines1 = fhandle.readlines()
fhandle.close()

data_dict1 = {}
ref_data_dict1 = {}
another_data_dict1 = {}
data_phase_list = []

first_flag = True
hop_count = 1
last_channel = 0.0

for line in fLines1:
    l_str = line.strip().split(',')
    epc = str(l_str[0])
    tstamp = l_str[3]
    channel = l_str[8]
    phase = l_str[9]

    #if epc == "E28011606000020612B0349C" :
    if epc == "E28011606000020612AF52AD" :

        if first_flag == True:
            last_channel = channel
            first_flag = False
            key = channel + "_" + str(hop_count)
            ref_data_dict1[key] = [ float(phase) ]
            continue
        else:
            if last_channel != channel:
                #print "Ch " , last_channel, channel
                another_data_dict1[last_channel].append([])
                hop_count += 1

            last_channel = channel
            key = channel + "_" + str(hop_count)
    

        if hop_count > len(data_phase_list):
            data_phase_list.append([float(phase)])
        else:
            data_phase_list[hop_count-1].append(float(phase))


        if key not in data_dict1:
            data_dict1[key] = [ float(phase) ]
        else:
            data_dict1[key].append( float(phase) )
    
    #key channel switch
        if channel not in ref_data_dict1:
            ref_data_dict1[channel] = [ float(phase) ]
        else:
            ref_data_dict1[channel].append( float(phase) )
        
        if channel not in another_data_dict1:
            another_data_dict1[channel] = [ [float(phase)] ]
        else:
            another_data_dict1[channel][len(another_data_dict1[channel])-1].append( float(phase) )
    else:
        print "Terrible Error"


window_size = 5

median_arr1 = []
median_arr2 = []

fhandle = open( "pll-phase-diff.dat", 'w' )
N = 0
y = []
T = 1.0/10000.0

#Code Snippet for each channel Difference
for channel in ref_data_dict1:
    median_arr1.append( np.median(np.array(ref_data_dict1[channel])) )

for i in range(len(median_arr1) - 1):
    fhandle.write( str(median_arr1[i+1]-median_arr1[i]) + "\n" )
    ## FFT glue
    y.append( median_arr1[i+1]-median_arr1[i] )
    N += 1
#Code Snippet for each hop Difference
'''
for channel in data_dict1:
    median_arr1.append( np.median(np.array(data_dict1[channel])) )

for i in range(len(median_arr1) - 1):
    fhandle.write( str(median_arr1[i+1]-median_arr1[i]) + "\n" )
    ## FFT glue
    y.append( median_arr1[i+1]-median_arr1[i] )
    N += 1
'''

#Code Snippet for same channel repeat difference
'''
for channel in another_data_dict1:

    for lst in another_data_dict1[channel]:
        #print len(lst)
        for i in range( len(lst) - 1):

            fhandle.write( str( np.median(np.array(lst[i+1])) - np.median(np.array(lst[i])) ) + "\n" )
            #median_arr1.append( np.median(np.array(l1)) )
'''

#Code Snippet for consecutive difference
'''
for i in range( len(data_phase_list) - 1 ):
    fhandle.write( str( np.median(np.array(data_phase_list[i+1])) - np.median(np.array(data_phase_list[i])) ) + "\n" )
    N += 1
    y.append(np.median(np.array(data_phase_list[i+1])) - np.median(np.array(data_phase_list[i])))
'''

'''
#FFT
yf = scipy.fftpack.fft(y)
#print N, T
xf = np.linspace(0.0, 1.0/(2.0*T), N/2)

fig, ax = plt.subplots()
ax.plot(xf, 2.0/N * np.abs(yf[:N/2]))
plt.show()
#for i in range(len(median_arr1)-1):
 #   fhandle.write( str(median_arr1[i+1]-median_arr1[i]) + "\n" )
'''

fhandle.close()
