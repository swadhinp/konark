set terminal postscript eps color size 6.0in,4.5in font 'Helvetica,28'
set output 'time-2engage-plot.eps'
set ylabel 'Daily Average Number of Engagement Events' font 'Helvetica,28' offset 2
set xlabel 'Hours' font 'Helvetica,28' offset 2
set title ''

set key top left
set xrange [0:23]
#set style fill empty
set xtics auto
set ytics auto

#set style line 1 lc rgb '#0060ad' lt 1 lw 4 pt 10 ps 1.5   # --- blue

plot "time-2engage-plot.dat" using 1:2:4 w yerrorbars ls 1 lw 2 notitle, \
 "time-2engage-plot.dat" using 1:2 w linespoints lt 1 lw 4 pt 2 ps 2 title 'Ignored', \
"time-2engage-plot.dat" using 1:3:5 w yerrorbars ls 3 lw 2 notitle, \
 "time-2engage-plot.dat" using 1:3 w linespoints lt 3 lw 4 pt 2 ps 2 title 'Interacted'

