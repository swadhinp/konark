#set terminal postscript eps color size 6.0in,4.5in font 'Helvetica,28'
set terminal png
set output 'line-plot-phase-vs-coverage.png'
set ylabel 'Phase (in Radian)' font 'Helvetica,28' offset 2
#set ylabel 'RSSI (in dB)' font 'Helvetica,28' offset 2
set xlabel 'Time (in ms)' font 'Helvetica,28' offset 2
set title ''

set key bottom left
#set yrange [-10:16.0]
#set xrange [0:6000]
set xtics auto
set ytics auto
#set ytics (-10,-9,-8,-7,-6,-5,-4,-3,-2,-1,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16)
set grid ytics lt 0 lw 2 lc rgb "#bbbbbb"
set grid xtics lt 0 lw 2 lc rgb "#bbbbbb"

#set style line 1 lc rgb '#0060ad' lt 1 lw 4 pt 10 ps 1.5   # --- blue

plot "line-plot-phase-vs-coverage.dat" using 1:2 w linespoints lt 1 pt 1 ps 1 title 'Tag of Interest', \
    "line-plot-phase-vs-coverage.dat" using 1:3 w linespoints lt 3 pt 1 ps 1 title 'Reference Tag'
