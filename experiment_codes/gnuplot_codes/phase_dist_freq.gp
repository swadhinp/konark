set terminal postscript eps color size 6.0in,4.5in font 'Helvetica,28'
set output 'phase-dist-freq.eps'
set ylabel 'Phase' font 'Helvetica,28' offset 2
set xlabel 'Channel IDs' font 'Helvetica,28' offset 2

set key off
set border 3

# Add a vertical dotted line at x=0 to show centre (mean) of distribution.
#set yzeroaxis
set xrange [0:50]
set yrange [0:6.0]

plot for [col=2:115] 'phase-dist-freq.dat' using 1:col lt 1 lw 4 notitle
