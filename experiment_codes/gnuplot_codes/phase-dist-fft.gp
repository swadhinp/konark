set terminal postscript eps color size 6.0in,4.5in font 'Helvetica,28'
set output 'phase-dist-fft.eps'
set ylabel 'Value' font 'Helvetica,28' offset 2
set xlabel 'Frequency' font 'Helvetica,28' offset 2

set key off
set border 3

# Add a vertical dotted line at x=0 to show centre (mean) of distribution.
#set yzeroaxis
#set xrange [-2.5:7.5]
#set yrange [0:300]

set xtics auto
set ytics auto

set logscale y 2

#set style line 1 lc rgb '#0060ad' lt 1 lw 4 pt 10 ps 1.5   # --- blue
plot 'phase-dist-fft.dat' using 2:xtic(1) lt 1 lw 4 notitle

