set terminal postscript eps color size 6.0in,4.5in font 'Helvetica,28'
set output 'phase_dist.eps'
set ylabel 'Frequency' font 'Helvetica,28' offset 2
set xlabel 'Phase (in Radian)' font 'Helvetica,28' offset 2

fname=system("echo $PLOT_FILE")
set title fname
set key off
set border 3

# Add a vertical dotted line at x=0 to show centre (mean) of distribution.
#set yzeroaxis
set xrange [-2.5:7.5]
set yrange [0:300]

# Each bar is half the (visual) width of its x-range.
set boxwidth 0.05 absolute
set style fill solid 1.0 noborder

bin_width = 0.001;

bin_number(x) = floor(x/bin_width)

rounded(x) = bin_width * ( bin_number(x) + 0.5 )

plot fname using (rounded($1)):(1) smooth frequency with boxes
