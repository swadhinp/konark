import sys, os, serial, threading

def monitor():

    ser = serial.Serial(COMPORT, BAUDRATE, timeout=1000)

    while (1):
        line = ser.readline()
        if (line != ""):
            fields = line[:-1].split(',');

            print line
            # write to file
            text_file = open("SpeedData.csv", "a+")
            text_file.write(line)
            text_file.close()

        # do some other things here

    print "Stop Monitoring"

""" -------------------------------------------
MAIN APPLICATION
"""  

os.system("rm -rf SpeedData.csv") 
print "Start Serial Monitor"
print "--------------------"
print 
print "Time:Speed(km/hr)"
print

COMPORT = "/dev/tty.usbmodem1412"
BAUDRATE = 115200

monitor()
