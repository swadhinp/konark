set terminal postscript eps color size 6.0in,4.5in font 'Helvetica,28'
set output 'line-plot-channel-comeback-duration-cdf.eps'
set ylabel 'CDF' font 'Helvetica,28' offset 2
set xlabel 'Comeback Duration per Channel (in S)' font 'Helvetica,28' offset 2
set title ''

set key top right
#set style fill empty
set xtics auto
set ytics auto
#set ytics 0,0.01,1

#set logscale

#set style line 1 lc rgb '#0060ad' lt 1 lw 4 pt 10 ps 1.5   # --- blue
plot 'line-plot-channel-comeback-duration-cdf.dat' with linespoints lt 1 lw 8 notitle



