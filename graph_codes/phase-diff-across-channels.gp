set terminal postscript eps color size 6.0in,4.5in font 'Helvetica,28'
set output 'phase-diff-across-channels.eps'
set ylabel 'Frequency' font 'Helvetica,28' offset 2
set xlabel 'Phase Difference (in Radian)' font 'Helvetica,28' offset 2
set title ''

#set style fill empty
set xtics auto
set ytics auto
#set ytics 0,0.01,1

set key off
set border 3

# Add a vertical dotted line at x=0 to show centre (mean) of distribution.
set yzeroaxis

# Each bar is half the (visual) width of its x-range.
set boxwidth 0.15 absolute
set style fill solid 1.0 noborder

bin_width = 0.1;
bin_number(x) = floor(x/bin_width)
rounded(x) = bin_width * ( bin_number(x) + 0.5 )


#set logscale

#set style line 1 lc rgb '#0060ad' lt 1 lw 4 pt 10 ps 1.5   # --- blue

plot 'phase-diff-across-channels.dat' using (rounded($1)):(1) smooth frequency with boxes

