#!/usr/bin/python
import sys
import operator
import numpy as np
import scipy.signal

if len(sys.argv) < 2:
    print "python <ref_data_file_1> <data_file_1> <ref_data_file_2> <data_file_2>"
    sys.exit(-1)

fhandle = open( sys.argv[1], 'r')
fLines1 = fhandle.readlines()
fhandle.close()

fhandle = open( sys.argv[2], 'r')
fLines2 = fhandle.readlines()
fhandle.close()

fhandle = open( sys.argv[3], 'r')
fLines3 = fhandle.readlines()
fhandle.close()

fhandle = open( sys.argv[4], 'r')
fLines4 = fhandle.readlines()
fhandle.close()

data_dict1 = {}
data_dict2 = {}

ref_data_dict1 = {}
ref_data_dict2 = {}

def remove_outlier( val) :

    ret_val = 0.0

    if val >= np.pi and val < 2*np.pi:
        ret_val = val - np.pi
        return ret_val

    #if val < np.pi and val >= np.pi - 0.9:
     #   ret_val = val - np.pi
      #  return ret_val

    if val >= 2*np.pi :
        ret_val = val - 2*np.pi
        return ret_val

    #if val > -np.pi and val <= -np.pi + 0.9:
     #   ret_val = val + np.pi
      #  return ret_val

    if val <= -np.pi and val > -2*np.pi:
        ret_val = val + np.pi
        return ret_val

    if val <= -2*np.pi:
        ret_val = val + 2*np.pi
        return ret_val

    return val

def isOutlier( val ):
    
    if (val >= np.pi and val <= (np.pi + 0.1)) or ( val <= -np.pi and val >= (-np.pi - 0.1)):
        #print "Pi", val
        return True

    if (val <= np.pi and val >= (np.pi - 0.1)) or ( val >= -np.pi and val <= (-np.pi + 0.1)):
        #print "Pi", val
        return True

    if (val >= 2*np.pi and val <= (2*np.pi + 0.1)) or ( val <= -2*np.pi and val >= (-2*np.pi - 0.1)):
        #print "2Pi", val
        return True

    if val >= 2*np.pi:
        return True
    if val <= -2*np.pi:
        return True

    if (val <= 2*np.pi and val >= (2*np.pi - 0.1)) or ( val >= -2*np.pi and val <= (-2*np.pi + 0.1)):
        #print "2Pi", val
        return True

    return False

for line in fLines1:
    l_str = line.strip().split(',')
    epc = str(l_str[0])
    tstamp = l_str[3]
    channel = l_str[8]
    phase = l_str[9]

    if epc == "E28011606000020612B0327D" :
    
        if channel not in ref_data_dict1:
            ref_data_dict1[channel] = [ float(phase) ]
        else:
            ref_data_dict1[channel].append( float(phase) )
    else:
        print "Terrible Error"

#print "Here1"

for line in fLines2:
    l_str = line.strip().split(',')
    epc = str(l_str[0])
    tstamp = l_str[3]
    channel = l_str[8]
    phase = l_str[9]

    if epc == "E28011606000020612B034AC" :
        
        if channel not in data_dict1:
            data_dict1[channel] = [ float(phase) ]
        else:
            data_dict1[channel].append( float(phase) )
    else:
        print "Terrible Error"

#print "Here2"

for line in fLines3:
    l_str = line.strip().split(',')
    epc = str(l_str[0])
    tstamp = l_str[3]
    channel = l_str[8]
    phase = l_str[9]

    if epc == "E28011606000020612B0327D" :
    
        if channel not in ref_data_dict2:
            ref_data_dict2[channel] = [ float(phase) ]
        else:
            ref_data_dict2[channel].append( float(phase) )

    else:
        print "Terrible Error"

#print "Here3"

for line in fLines4:
    l_str = line.strip().split(',')
    epc = str(l_str[0])
    tstatmp = l_str[3]
    channel = l_str[8]
    phase = l_str[9]

    if epc == "E28011606000020612B034AC" :

        if channel not in data_dict2:
            data_dict2[channel] = [ float(phase) ]
        else:
            data_dict2[channel].append( float(phase) )

    else:
        print "Terrible Error"


#print "Here4"
#print data_dict1.keys()
#print data_dict2.keys()
#print ref_data_dict1.keys()
#print ref_data_dict2.keys()

diff_phase_arr1 = []
diff_phase_arr2 = []

window_size = 25

for channel in ref_data_dict1:

    phase_arr1 = ref_data_dict1[channel]
    phase_arr2 = data_dict1[channel]

    phase_arr1_remove_outlier = []
    phase_arr2_remove_outlier = []

    phase_arr1_smoothed = []
    phase_arr2_smoothed = []

    if (isOutlier(phase_arr1[0]) == True):
        phase_arr1_remove_outlier.append(0.0)
        phase_arr1[0] = 0.0 #Update
    else:
        phase_arr1_remove_outlier.append(phase_arr1[0])

    for i in range(1,len(phase_arr1)):

        cur_phase = phase_arr1[i]
        an_phase = phase_arr1[i-1]
        
        diff_phase = float(cur_phase) - float(phase_arr1[i-1])
        print "Diff", i, i-1, cur_phase, phase_arr1[i-1], isOutlier(diff_phase)

        if isOutlier(diff_phase) == True:
            phase_arr1_remove_outlier.append(float(phase_arr1[i-1])) #Copy the previous phase
            phase_arr1[i] = float(phase_arr1[i-1]) #Update

        else:
            phase_arr1_remove_outlier.append(cur_phase)

    
    if ( isOutlier(phase_arr2[0]) == True):
        phase_arr2_remove_outlier.append(0.0)
        phase_arr2[0] = 0.0 #Update
    else:
        phase_arr2_remove_outlier.append(phase_arr2[0])

    for i in range(1,len(phase_arr2)):

        cur_phase = phase_arr2[i]
        
        diff_phase = float(cur_phase) - float(phase_arr2[i-1])
        #print diff_phase
        
        if isOutlier(diff_phase) == True:
            phase_arr2_remove_outlier.append(float(phase_arr2[i-1])) #Copy the previous phase
            phase_arr2[i] = float(phase_arr2[i-1]) #Update
        else:
            phase_arr2_remove_outlier.append(cur_phase)
    
    #Directly applying Median Filter
    phase_arr1_smoothed = scipy.signal.medfilt(phase_arr1_remove_outlier, window_size)
    phase_arr2_smoothed = scipy.signal.medfilt(phase_arr2_remove_outlier, window_size)
    #phase_arr1_smoothed = scipy.signal.wiener(phase_arr1_remove_outlier, window_size)
    #phase_arr2_smoothed = scipy.signal.wiener(phase_arr2_remove_outlier, window_size)
    
    #phase_arr1_smoothed = scipy.signal.hilbert(phase_arr1)
    #phase_arr2_smoothed = scipy.signal.hilbert(phase_arr2)

    for i in range( min(len(phase_arr2_smoothed), len(phase_arr1_smoothed))):
        diff_phase_arr1.append( float(phase_arr2_smoothed[i]) - float(phase_arr1_smoothed[i]))


#print "Here5"

for channel in ref_data_dict2:

    phase_arr1 = ref_data_dict2[channel]
    phase_arr2 = data_dict2[channel]
    
    phase_arr1_remove_outlier = []
    phase_arr2_remove_outlier = []
    phase_arr1_smoothed = []
    phase_arr2_smoothed = []

    if (isOutlier(phase_arr1[0]) == True):
        phase_arr1_remove_outlier.append(0.0)
        phase_arr1[0] = 0.0 #Update
    else:
        phase_arr1_remove_outlier.append(phase_arr1[0])

    for i in range(1,len(phase_arr1)):

        cur_phase = phase_arr1[i]
        
        diff_phase = float(cur_phase) - float(phase_arr1[i-1])
        #print diff_phase
        
        if isOutlier(diff_phase) == True:
            phase_arr1_remove_outlier.append(float(phase_arr1[i-1])) #Copy the previous phase
            phase_arr1[i] = float(phase_arr1[i-1]) #Update

        else:
            phase_arr1_remove_outlier.append(cur_phase)

    
    if ( isOutlier(phase_arr2[0]) == True):
        phase_arr2_remove_outlier.append(0.0)
        phase_arr2[0] = 0.0 #Update
    else:
        phase_arr2_remove_outlier.append(phase_arr2[0])

    for i in range(1,len(phase_arr2)):

        cur_phase = phase_arr2[i]
        
        diff_phase = float(cur_phase) - float(phase_arr2[i-1])
        #print diff_phase
        
        if isOutlier(diff_phase) == True:
            phase_arr2_remove_outlier.append(float(phase_arr2[i-1])) #Copy the previous phase
            phase_arr2[i] = float(phase_arr2[i-1]) #Update
        else:
            phase_arr2_remove_outlier.append(cur_phase)
    
    #Directly applying Median Filter
    phase_arr1_smoothed = scipy.signal.medfilt(phase_arr1_remove_outlier, window_size)
    phase_arr2_smoothed = scipy.signal.medfilt(phase_arr2_remove_outlier, window_size)
    #phase_arr1_smoothed = scipy.signal.wiener(phase_arr1_remove_outlier, window_size)
    #phase_arr2_smoothed = scipy.signal.wiener(phase_arr2_remove_outlier, window_size)
    #phase_arr1_smoothed = scipy.signal.hilbert(phase_arr1)
    #phase_arr2_smoothed = scipy.signal.hilbert(phase_arr2)

    for i in range( min(len(phase_arr2_smoothed), len(phase_arr1_smoothed))):
        diff_phase_arr2.append( float(phase_arr2_smoothed[i]) - float(phase_arr1_smoothed[i]))

    

#For No smoothing
#diff_phase_arr1_smoothed = diff_phase_arr1 
#diff_phase_arr2_smoothed = diff_phase_arr2

fhandle = open( "line-plot-phase-diff-smoothed.dat", "w")
count = 1

for i in range(min(len(diff_phase_arr1), len(diff_phase_arr2))):
    fhandle.write( str(count) + " " + str(diff_phase_arr1[count-1]) + " " + str(diff_phase_arr2[count-1]) )
    fhandle.write("\n")
    count = count + 1

fhandle.close()

#print sorted_rfid_epc_dict
