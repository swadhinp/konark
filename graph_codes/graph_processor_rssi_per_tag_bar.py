#!/usr/bin/python
import sys
import operator
import numpy as np

if len(sys.argv) < 2:
    print "python <alli_data_file_name>"
    sys.exit(-1)

fhandle = open( sys.argv[1], 'r')
fLines = fhandle.readlines()
fhandle.close()

rfid_epc_dict = {}

for line in fLines:
    l_str = line.strip().split(',')
    epc = l_str[0]
    rssi = l_str[7]

    if epc not in rfid_epc_dict:
        rfid_epc_dict[epc] = [float(rssi)]
    else:
        rfid_epc_dict[epc].append(float(rssi))

fhandle = open( "epc-rssi-errorbar.dat", "w")

for epc in rfid_epc_dict:
    a = np.array( rfid_epc_dict[epc] )
    fhandle.write( str(epc).split("12A")[1] + " " + str(np.mean(a)) + " " + str(np.std(a)))
    fhandle.write("\n")

fhandle.close()

#print sorted_rfid_epc_dict
