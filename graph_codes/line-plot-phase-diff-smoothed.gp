set terminal postscript eps color size 6.0in,4.5in font 'Helvetica,28'
set output 'line-plot-phase-diff-smoothed.eps'
set ylabel 'Phase Difference (in Radian)' font 'Helvetica,28' offset 2
set xlabel 'Time' font 'Helvetica,28' offset 2
set title ''

set key top left
#set xrange [0:23]
#set style fill empty
set xtics auto
set ytics auto

#set style line 1 lc rgb '#0060ad' lt 1 lw 4 pt 10 ps 1.5   # --- blue

plot "line-plot-phase-diff-smoothed.dat" using 1:2 with line lt -1 lw 4 title '8 cm', \
 "line-plot-phase-diff-smoothed.dat" using 1:3 with line lc rgb "red" lt -1 lw 4 title '7 cm'

