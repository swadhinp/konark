set terminal postscript eps color size 6.0in,4.5in font 'Helvetica,28'
set output 'epc-rssi-doppler-bar.eps'
set ylabel 'Normalised Variance of Data' font 'Helvetica,28' offset 2
set xlabel 'Different Settingss' font 'Helvetica,28' offset 2
set title ''

set key top right
set style data histogram
set style histogram cluster gap 1

set style fill solid border rgb "black"
set auto x
set xtic rotate by -25
set yrange [0:*]
set xtics ("Slow / Linear" 0, "Fast / Linear" 1, "Slow / Circular" 2, "Fast / Circular" 3 )
set boxwidth 0.5

plot 'epc-rssi-doppler-bar.dat' using 2 title col, \
        '' using 3 title col


