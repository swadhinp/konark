set terminal postscript eps color size 6.0in,4.5in font 'Helvetica,22'
set output 'channel-phase-errorbar.eps'
set ylabel 'Phase in Radian' font 'Helvetica,28' offset 2
set xlabel 'Channel' font 'Helvetica,22' offset 2
set title ''

#set key top left
#set style fill empty
set xrange [-0.75:50.25]
set xtic rotate by -65
set xtics auto
set ytics auto

set boxwidth 0.30
#set style fill solid

plot 'channel-phase-errorbar.dat' using ($0-.05):2:3:xtic(1) linestyle 1 with boxerrorbars notitle
