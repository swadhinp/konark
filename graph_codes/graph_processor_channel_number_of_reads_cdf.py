#!/usr/bin/python
import sys
import operator
import numpy as np

if len(sys.argv) < 2:
    print "python <all_data_file_name>"
    sys.exit(-1)

fhandle = open( sys.argv[1], 'r')
fLines = fhandle.readlines()
fhandle.close()

rfid_channel_dict = {}
last_channel = 0
last_phase = 0
last_channel_time = 0.0
count = 1
number_of_reads = 0

for line in fLines:
    l_str = line.strip().split(',')
    #print l_str
    last_time = l_str[3]
    channel = l_str[8]
    phase = l_str[9]

    if count == 1:
        last_channel = channel
        number_of_reads = 1
        last_channel_time = last_time
        last_phase = phase
        count = count + 1
        continue
    
    if channel == last_channel:
        count = count + 1
        number_of_reads = number_of_reads + 1

        if phase != last_phase:
            print channel, phase, last_phase

        continue
    else:
        if last_channel not in rfid_channel_dict:
            rfid_channel_dict[last_channel] = [number_of_reads]
        else:
            #print last_time, rfid_channel_last_time[channel]
            rfid_channel_dict[last_channel].append(number_of_reads)
        
        #Update
        last_channel = channel
        last_channel_time = last_time
        number_of_reads = 0
        last_phase = phase

channel_dict = {}

for channel in rfid_channel_dict:
    a = np.array( rfid_channel_dict[channel] )
    channel_dict[channel] = np.mean(a)

sorted_channel_dict = sorted(channel_dict.items(), key=operator.itemgetter(1))

#print sorted_channel_dict

fhandle = open( "channel-read-number.dat", "w")

count = 1
for elem in sorted_channel_dict:
    #print elem[1]
    #fhandle.write( str(count)  + " " + str(count) + " " + str(elem[1]) )
    fhandle.write( str(elem[1]) )
    fhandle.write("\n")
    count = count + 1
fhandle.close();

data = np.loadtxt('channel-read-number.dat')
sorted_data = np.sort(data)
yvals = np.arange(len(sorted_data))/float(len(sorted_data))

#print sorted_data
#print yvals

fhandle = open( "line-plot-channel-number-cdf.dat", "w")

count = 1

for i in range( len(sorted_data) ):
    fhandle.write( str(sorted_data[i]) + " " + str(yvals[i]) )
    fhandle.write("\n")
    count = count + 1

fhandle.close()


#print sorted_rfid_epc_dict
