set terminal postscript eps color size 6.0in,4.5in font 'Helvetica,28'
set output 'line-plot-phase-diff.eps'
set ylabel 'Phase Difference (in Radian)' font 'Helvetica,28' offset 2
set xlabel 'Time' font 'Helvetica,28' offset 2
set title ''

set key top left
#set xrange [0:23]
#set style fill empty
set xtics auto
set ytics auto

#set style line 1 lc rgb '#0060ad' lt 1 lw 4 pt 10 ps 1.5   # --- blue

plot "line-plot-phase-diff.dat" using 1:2 w linespoints lt 1 lw 4 pt 2 ps 2 title 'Full Antenna', \
 "line-plot-phase-diff.dat" using 1:3 w linespoints lt 3 lw 4 pt 2 ps 2 title 'Half Antenna'

