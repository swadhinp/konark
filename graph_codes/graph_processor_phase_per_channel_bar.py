#!/usr/bin/python
import sys
import operator
import numpy as np

if len(sys.argv) < 2:
    print "python <all_data_file_name>"
    sys.exit(-1)

fhandle = open( sys.argv[1], 'r')
fLines = fhandle.readlines()
fhandle.close()

rfid_channel_dict = {}

for line in fLines:
    l_str = line.strip().split(',')
    channel = l_str[8]
    phase = l_str[9]

    if channel not in rfid_channel_dict:
        rfid_channel_dict[channel] = [float(phase)]
    else:
        rfid_channel_dict[channel].append(float(phase))

fhandle = open( "channel-phase-errorbar.dat", "w")

for channel in rfid_channel_dict:
    a = np.array( rfid_channel_dict[channel] )
    fhandle.write( str(channel) + " " + str(np.mean(a)) + " " + str(np.std(a)))
    fhandle.write("\n")

fhandle.close()

#print sorted_rfid_epc_dict
