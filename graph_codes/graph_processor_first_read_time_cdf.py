#!/usr/bin/python
import sys
import operator
import numpy as np

if len(sys.argv) < 2:
    print "python <all_data_file_name>"
    sys.exit(-1)

fhandle = open( sys.argv[1], 'r')
fLines = fhandle.readlines()
fhandle.close()

time_list = []

for line in fLines:
    l_str = line.strip().split(',')
    tm = long(l_str[2])
    time_list.append(tm)

a = np.array(time_list)
min_tm = np.min(a)



rfid_epc_dict = {}

fhandle = open( "epc-time-reads.dat", "w")

for line in fLines:

    l_str = line.strip().split(',')
    epc = l_str[0]
    tm = l_str[2]

    if epc not in rfid_epc_dict:
        rfid_epc_dict[epc] = 1
        fhandle.write( str( float(long(tm) - min_tm)/1000000.0) )
        fhandle.write("\n")

fhandle.close()

data = np.loadtxt('epc-time-reads.dat')
sorted_data = np.sort(data)
yvals = np.arange(len(sorted_data))/float(len(sorted_data))

#print sorted_data
#print yvals

fhandle = open( "line-epc-time-reads-cdf.dat", "w")

count = 1

for i in range( len(sorted_data) ):
    fhandle.write( str(sorted_data[i]) + " " + str(yvals[i]) )
    fhandle.write("\n")
    count = count + 1

fhandle.close()


#print sorted_rfid_epc_dict
