#!/usr/bin/python
import sys
import operator
import numpy as np

if len(sys.argv) < 5:
    print "python <all_data_file_name_linear_slow> <all_data_file_name_linear_fast> <all_data_file_name_circular_slow> <all_data_file_name_circular_fast>"
    sys.exit(-1)

fhandle = open( sys.argv[1], 'r')
fLines1 = fhandle.readlines()
fhandle.close()

fhandle = open( sys.argv[2], 'r')
fLines2 = fhandle.readlines()
fhandle.close()

fhandle = open( sys.argv[3], 'r')
fLines3 = fhandle.readlines()
fhandle.close()

fhandle = open( sys.argv[4], 'r')
fLines4 = fhandle.readlines()
fhandle.close()

rfid_rssi_dict = {}
rfid_doppler_dict = {}

fhandle = open( "epc-rssi-doppler-bar.dat", "w")
fhandle.write("Instance Doppler RSSI\n")
epc = 1

for line in fLines1:
    l_str = line.strip().split(',')
    doppler = l_str[5]
    rssi = l_str[7]


    if epc not in rfid_doppler_dict:
        rfid_doppler_dict[epc] = [float(doppler)]
    else:
        rfid_doppler_dict[epc].append(float(doppler))

    if epc not in rfid_rssi_dict:
        rfid_rssi_dict[epc] = [float(rssi)]
    else:
        rfid_rssi_dict[epc].append(float(rssi))

a1 = np.array(rfid_doppler_dict[epc])
a2 = np.array(rfid_rssi_dict[epc])

a1Max = np.max(a1)
a2Max  = np.max(a2)

newa1 = [float(x) / float(a1Max)  for x in a1]
newa2 = [float(x) / float(a2Max)  for x in a2]

fhandle.write( str(epc) + " " + str(np.std(newa1)) + " " + str(np.std(newa2)) )
fhandle.write("\n")

epc = 2

for line in fLines2:
    l_str = line.strip().split(',')
    doppler = l_str[5]
    rssi = l_str[7]


    if epc not in rfid_doppler_dict:
        rfid_doppler_dict[epc] = [float(doppler)]
    else:
        rfid_doppler_dict[epc].append(float(doppler))

    if epc not in rfid_rssi_dict:
        rfid_rssi_dict[epc] = [float(rssi)]
    else:
        rfid_rssi_dict[epc].append(float(rssi))

a1 = np.array(rfid_doppler_dict[epc])
a2 = np.array(rfid_rssi_dict[epc])

a1Max = np.max(a1)
a2Max  = np.max(a2)

newa1 = [float(x) / float(a1Max)  for x in a1]
newa2 = [float(x) / float(a2Max)  for x in a2]

fhandle.write( str(epc) + " " + str(np.std(newa1)) + " " + str(np.std(newa2)) )
fhandle.write("\n")

epc = 3


for line in fLines3:
    l_str = line.strip().split(',')
    doppler = l_str[5]
    rssi = l_str[7]


    if epc not in rfid_doppler_dict:
        rfid_doppler_dict[epc] = [float(doppler)]
    else:
        rfid_doppler_dict[epc].append(float(doppler))

    if epc not in rfid_rssi_dict:
        rfid_rssi_dict[epc] = [float(rssi)]
    else:
        rfid_rssi_dict[epc].append(float(rssi))

a1 = np.array(rfid_doppler_dict[epc])
a2 = np.array(rfid_rssi_dict[epc])

a1Max = np.max(a1)
a2Max  = np.max(a2)

newa1 = [float(x) / float(a1Max)  for x in a1]
newa2 = [float(x) / float(a2Max)  for x in a2]

fhandle.write( str(epc) + " " + str(np.std(newa1)) + " " + str(np.std(newa2)) )
fhandle.write("\n")
epc = 4


for line in fLines4:
    l_str = line.strip().split(',')
    doppler = l_str[5]
    rssi = l_str[7]

    if epc not in rfid_doppler_dict:
        rfid_doppler_dict[epc] = [float(doppler)]
    else:
        rfid_doppler_dict[epc].append(float(doppler))

    if epc not in rfid_rssi_dict:
        rfid_rssi_dict[epc] = [float(rssi)]
    else:
        rfid_rssi_dict[epc].append(float(rssi))


a1 = np.array(rfid_doppler_dict[epc])
a2 = np.array(rfid_rssi_dict[epc])

a1Max = np.max(a1)
a2Max  = np.max(a2)

newa1 = [float(x) / float(a1Max)  for x in a1]
newa2 = [float(x) / float(a2Max)  for x in a2]

fhandle.write( str(epc) + " " + str(np.std(newa1)) + " " + str(np.std(newa2)) )
fhandle.write("\n")

fhandle.close()

#print sorted_rfid_epc_dict
