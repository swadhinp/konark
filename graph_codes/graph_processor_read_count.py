#!/usr/bin/python
import sys
import operator
import numpy as np

if len(sys.argv) < 2:
    print "python <quicklook_data_file_name>"
    sys.exit(-1)

fhandle = open( sys.argv[1], 'r')
fLines = fhandle.readlines()
fhandle.close()

rfid_epc_dict = {}

for line in fLines:
    l_str = line.strip().split(',')
    epc = l_str[0]

    if epc not in rfid_epc_dict:
        rfid_epc_dict[epc] = 1
    else:
        rfid_epc_dict[epc] += 1


sorted_rfid_epc_dict = sorted(rfid_epc_dict.items(), key=operator.itemgetter(1))

fhandle = open( "bar-dist-epc-reads.dat", "w")

count = 1
for elem in sorted_rfid_epc_dict:
    #print elem[1]
    fhandle.write( str(count)  + " " + str(elem[0]).split("12A")[1]  + " " + str(elem[1]) )
    #fhandle.write( str(elem[1]) )
    fhandle.write("\n")
    count = count + 1

fhandle.close();


#print sorted_rfid_epc_dict
