#!/usr/bin/python
import sys
import operator
import numpy as np
import scipy.signal

if len(sys.argv) < 2:
    print "python <ref_data_file_1> <data_file_1> <ref_data_file_2> <data_file_2>"
    sys.exit(-1)

fhandle = open( sys.argv[1], 'r')
fLines1 = fhandle.readlines()
fhandle.close()

fhandle = open( sys.argv[2], 'r')
fLines2 = fhandle.readlines()
fhandle.close()

fhandle = open( sys.argv[3], 'r')
fLines3 = fhandle.readlines()
fhandle.close()

fhandle = open( sys.argv[4], 'r')
fLines4 = fhandle.readlines()
fhandle.close()

data_dict1 = {}
data_dict2 = {}

ref_data_dict1 = {}
ref_data_dict2 = {}

def remove_outlier( val) :

    ret_val = 0.0

    if val >= np.pi and val < 2*np.pi:
        ret_val = val - np.pi
        return ret_val

    #if val < np.pi and val >= np.pi - 0.9:
     #   ret_val = val - np.pi
      #  return ret_val

    if val >= 2*np.pi :
        ret_val = val - 2*np.pi
        return ret_val

    #if val > -np.pi and val <= -np.pi + 0.9:
     #   ret_val = val + np.pi
      #  return ret_val

    if val <= -np.pi and val > -2*np.pi:
        ret_val = val + np.pi
        return ret_val

    if val <= -2*np.pi:
        ret_val = val + 2*np.pi
        return ret_val

    return val

for line in fLines1:
    l_str = line.strip().split(',')
    epc = str(l_str[0])
    tstamp = l_str[3]
    channel = l_str[8]
    phase = l_str[9]

    if epc == "E28011606000020612B0327D" :
    
        if channel not in ref_data_dict1:
            ref_data_dict1[channel] = [ float(phase) ]
        else:
            ref_data_dict1[channel].append( float(phase) )
    else:
        print "Terrible Error"

#print "Here1"

for line in fLines2:
    l_str = line.strip().split(',')
    epc = str(l_str[0])
    tstamp = l_str[3]
    channel = l_str[8]
    phase = l_str[9]

    if epc != "E28011606000020612B0327D" :
    
        if channel not in data_dict1:
            data_dict1[channel] = [ float(phase) ]
        else:
            data_dict1[channel].append( float(phase) )
    else:
        print "Terrible Error"

#print "Here2"

for line in fLines3:
    l_str = line.strip().split(',')
    epc = str(l_str[0])
    tstamp = l_str[3]
    channel = l_str[8]
    phase = l_str[9]

    if epc == "E28011606000020612B0327D" :
    
        if channel not in ref_data_dict2:
            ref_data_dict2[channel] = [ float(phase) ]
        else:
            ref_data_dict2[channel].append( float(phase) )

    else:
        print "Terrible Error"

#print "Here3"

for line in fLines4:
    l_str = line.strip().split(',')
    epc = str(l_str[0])
    tstatmp = l_str[3]
    channel = l_str[8]
    phase = l_str[9]

    if epc != "E28011606000020612B0327D" :

        if channel not in data_dict2:
            data_dict2[channel] = [ float(phase) ]
        else:
            data_dict2[channel].append( float(phase) )

    else:
        print "Terrible Error"


#print "Here4"
#print data_dict1.keys()
#print data_dict2.keys()
#print ref_data_dict1.keys()
#print ref_data_dict2.keys()

diff_phase_arr1 = []
diff_phase_arr2 = []

window_size = 15

for channel in ref_data_dict1:

    phase_arr1 = ref_data_dict1[channel]
    phase_arr2 = data_dict1[channel]

    phase_arr1_smoothed = []
    phase_arr2_smoothed = []

    k = 0

    while k < (len(phase_arr1) - window_size + 1):
    
        a = np.array(phase_arr1[k:(k+window_size)])
        x = remove_outlier(np.median(a))

        #if x == np.pi or x == -np.pi or x == 2*np.pi or x == -2*np.pi:
         #   x = 0

        phase_arr1_smoothed.append(x)
        k = k + 1

    k = 0

    while k < (len(phase_arr2) - window_size + 1):
    
        a = np.array(phase_arr2[k:(k+window_size)])
        x = remove_outlier(np.median(a))

        #if x == np.pi or x == -np.pi or x == 2*np.pi or x == -2*np.pi:
         #   x = 0

        phase_arr2_smoothed.append(x)
        k = k + 1

    phase_arr1_smoothed1 = []
    phase_arr2_smoothed2 = []
    #Directly applying Median Filter
    phase_arr1_smoothed1 = scipy.signal.medfilt(phase_arr1_smoothed, window_size)
    phase_arr2_smoothed2 = scipy.signal.medfilt(phase_arr2_smoothed, window_size)
    #phase_arr1_smoothed = scipy.signal.hilbert(phase_arr1)
    #phase_arr2_smoothed = scipy.signal.hilbert(phase_arr2)

    for i in range( min(len(phase_arr2_smoothed2), len(phase_arr1_smoothed1))):
        diff_phase_arr1.append( float(phase_arr2_smoothed2[i]) - float(phase_arr1_smoothed1[i]))


#print "Here5"

for channel in ref_data_dict2:

    phase_arr1 = ref_data_dict2[channel]
    phase_arr2 = data_dict2[channel]
    
    phase_arr1_smoothed = []
    phase_arr2_smoothed = []

    k = 0

    while k < (len(phase_arr1) - window_size + 1):
    
        a = np.array(phase_arr1[k:(k+window_size)])
        x = remove_outlier(np.median(a))

        #if x == np.pi or x == -np.pi or x == 2*np.pi or x == -2*np.pi:
         #   x = 0

        phase_arr1_smoothed.append(x)
        k = k + 1

    k = 0

    while k < (len(phase_arr2) - window_size + 1):
    
        a = np.array(phase_arr2[k:(k+window_size)])
        x = remove_outlier(np.median(a))

        #if x == np.pi or x == -np.pi or x == 2*np.pi or x == -2*np.pi:
         #   x = 0

        phase_arr2_smoothed.append(x)
        k = k + 1
    
    phase_arr1_smoothed1 = []
    phase_arr2_smoothed2 = []
    #Directly applying Median Filter
    phase_arr1_smoothed1 = scipy.signal.medfilt(phase_arr1_smoothed, window_size)
    phase_arr2_smoothed2 = scipy.signal.medfilt(phase_arr2_smoothed, window_size)
    #phase_arr1_smoothed = scipy.signal.hilbert(phase_arr1)
    #phase_arr2_smoothed = scipy.signal.hilbert(phase_arr2)

    for i in range( min(len(phase_arr1_smoothed1), len(phase_arr2_smoothed2)) ):
        diff_phase_arr2.append( float(phase_arr2_smoothed2[i]) - float(phase_arr1_smoothed1[i]))

#print "Here6"
diff_phase_arr1_smoothed = []
diff_phase_arr2_smoothed = []

window_size = 5
i = 0

while i < (len(diff_phase_arr1) - window_size):

    a = np.array(diff_phase_arr1[i:(i+window_size)])
    x = remove_outlier(np.median(a))

    #if abs(x - np.pi) < 0.5 or abs(x + np.pi) < 0.5 or abs(x - 2*np.pi) < 0.5 or abs(x + 2*np.pi) < 0.5:
     #   x = 0

    #print x
    #diff_phase_arr1_smoothed.append(np.mean(a))
    diff_phase_arr1_smoothed.append(x)
    i = i + 1

a = np.array(diff_phase_arr1[i:(i+window_size)])
diff_phase_arr1_smoothed.append(np.mean(a))


#Directly applying Median Filter
diff_phase_arr1_smoothed = scipy.signal.medfilt(diff_phase_arr1, window_size)
#diff_phase_arr1_smoothed = scipy.signal.hilbert(diff_phase_arr1)

#print "Here7"
i = 0

while i < (len(diff_phase_arr2) - window_size):

    a = np.array(diff_phase_arr2[i:(i+window_size)])
    x = remove_outlier(np.median(a))

    #if x == np.pi or x == -np.pi or x == 2*np.pi or x == -2*np.pi:
    #if abs(x - np.pi) < 0.5 or abs(x + np.pi) < 0.5 or abs(x - 2*np.pi) < 0.5 or abs(x + 2*np.pi) < 0.5:
     #   x = 0

    #print x
    diff_phase_arr2_smoothed.append(x)
    i = i + 1

a = np.array(diff_phase_arr2[i:(i+window_size)])
diff_phase_arr2_smoothed.append(np.mean(a))


#Directly applying Median Filter
diff_phase_arr2_smoothed = scipy.signal.medfilt(diff_phase_arr2, window_size)
#diff_phase_arr2_smoothed = scipy.signal.wiener(diff_phase_arr2)

#print "Here8"
#For No smoothing
#diff_phase_arr1_smoothed = diff_phase_arr1 
#diff_phase_arr2_smoothed = diff_phase_arr2

fhandle = open( "line-plot-phase-diff-smoothed.dat", "w")
count = 1

for i in range(min(len(diff_phase_arr1_smoothed), len(diff_phase_arr2_smoothed))):
    fhandle.write( str(count) + " " + str(diff_phase_arr1_smoothed[count-1]) + " " + str(diff_phase_arr2_smoothed[count-1]) )
    fhandle.write("\n")
    count = count + 1

fhandle.close()

#print sorted_rfid_epc_dict
