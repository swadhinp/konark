#!/usr/bin/python
import sys
import operator
import numpy as np

if len(sys.argv) < 2:
    print "python <alli_data_file_name>"
    sys.exit(-1)

fhandle = open( sys.argv[1], 'r')
fLines = fhandle.readlines()
fhandle.close()

rfid_epc_dict = {}

for line in fLines:
    l_str = line.strip().split(',')
    epc = l_str[0]
    rssi = l_str[7]

    if epc not in rfid_epc_dict:
        rfid_epc_dict[epc] = [float(rssi)]
    else:
        rfid_epc_dict[epc].append(float(rssi))

rssi_epc_dict = {}

for epc in rfid_epc_dict:
    a = np.array( rfid_epc_dict[epc] )
    rssi_epc_dict[epc] = np.mean(a)

sorted_rssi_epc_dict = sorted(rssi_epc_dict.items(), key=operator.itemgetter(1))

fhandle = open( "epc-rssi-reads.dat", "w")

count = 1
for elem in sorted_rssi_epc_dict:
    print elem[1]
    #fhandle.write( str(count)  + " " + str(count) + " " + str(elem[1]) )
    fhandle.write( str(elem[1]) )
    fhandle.write("\n")
    count = count + 1
fhandle.close();

data = np.loadtxt('epc-rssi-reads.dat')
sorted_data = np.sort(data)
yvals = np.arange(len(sorted_data))/float(len(sorted_data))

#print sorted_data
#print yvals

fhandle = open( "line-plot-epc-rssi-cdf.dat", "w")

count = 1

for i in range( len(sorted_data) ):
    fhandle.write( str(sorted_data[i]) + " " + str(yvals[i]) )
    fhandle.write("\n")
    count = count + 1

fhandle.close()


#print sorted_rfid_epc_dict
