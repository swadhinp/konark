#!/usr/bin/python
import sys
import operator
import numpy as np

if len(sys.argv) < 2:
    print "python <all_data_file_name>"
    sys.exit(-1)

fhandle = open( sys.argv[1], 'r')
fLines = fhandle.readlines()
fhandle.close()

last_time = 0

first_flag = 0
time_list = {}
time_single_list = []
index_count = 0

for line in fLines:
    l_str = line.strip().split(',')
    
    epc = l_str[0]
    rssi = float(l_str[7])

    if rssi in time_list:
        time_list[rssi] += 1
    else:
        time_list[rssi] = 1


print time_list

keylist = sorted(time_list.keys())

for elem in keylist:
    time_single_list.append(long(time_list[elem]))

sorted_time_list = time_single_list

fhandle = open( "epc-tag-reads.dat", "w")

count = 1

for elem in sorted_time_list:
    #print elem[1]
    #fhandle.write( str(count)  + " " + str(count) + " " + str(elem[1]) )
    fhandle.write( str(elem) )
    fhandle.write("\n")
    count = count + 1
fhandle.close();

data = np.loadtxt('epc-tag-reads.dat')
#sorted_data = np.sort(data)
sorted_data = data
yvals = np.arange(len(sorted_data))/float(len(sorted_data))

#print sorted_data
#print yvals

fhandle = open( "line-plot-epc-per-tag-read-cdf.dat", "w")

count = 1

for i in range( len(sorted_data) ):
    fhandle.write( str(keylist[i]) + " " + str(yvals[i]) )
    fhandle.write("\n")
    count = count + 1

fhandle.close()


#print sorted_rfid_epc_dict
