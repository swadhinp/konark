set terminal postscript eps color size 6.0in,4.5in font 'Helvetica,28'
set output 'bar-dist-epc-reads.eps'
set ylabel 'Number of Reads' font 'Helvetica,28' offset 2
set xlabel 'EPC IDs' font 'Helvetica,28' offset 2
set title ''

set key top left
#set style fill empty
set xrange [-0.5:33.50]
set xtic rotate by -75 
set xtics auto
set ytics auto

set boxwidth 0.25

#set style fill solid

plot "bar-dist-epc-reads.dat" using 1:3:xtic(2) with boxes notitle

