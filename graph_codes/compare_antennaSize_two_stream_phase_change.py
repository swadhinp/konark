#!/usr/bin/python
import sys
import operator
import numpy as np

if len(sys.argv) < 2:
    print "python <ref_data_file_1> <data_file_1> <ref_data_file_2> <data_file_2>"
    sys.exit(-1)

fhandle = open( sys.argv[1], 'r')
fLines1 = fhandle.readlines()
fhandle.close()

fhandle = open( sys.argv[2], 'r')
fLines2 = fhandle.readlines()
fhandle.close()

fhandle = open( sys.argv[3], 'r')
fLines3 = fhandle.readlines()
fhandle.close()

fhandle = open( sys.argv[4], 'r')
fLines4 = fhandle.readlines()
fhandle.close()

data_dict1 = {}
data_dict2 = {}

ref_data_dict1 = {}
ref_data_dict2 = {}

for line in fLines1:
    l_str = line.strip().split(',')
    epc = str(l_str[0])
    tstamp = l_str[3]
    channel = l_str[8]
    phase = l_str[9]

    if epc == "E28011606000020612B034AC" :
    
        if channel not in ref_data_dict1:
            ref_data_dict1[channel] = [ float(phase) ]
        else:
            ref_data_dict1[channel].append( float(phase) )
    else:
        print "Terrible Error"


for line in fLines2:
    l_str = line.strip().split(',')
    epc = str(l_str[0])
    tstamp = l_str[3]
    channel = l_str[8]
    phase = l_str[9]

    if epc != "E28011606000020612B034AC" :
    
        if channel not in data_dict1:
            data_dict1[channel] = [ float(phase) ]
        else:
            data_dict1[channel].append( float(phase) )
    else:
        print "Terrible Error"

for line in fLines3:
    l_str = line.strip().split(',')
    epc = str(l_str[0])
    tstamp = l_str[3]
    channel = l_str[8]
    phase = l_str[9]

    if epc == "E28011606000020612B034AC" :
    
        if channel not in ref_data_dict2:
            ref_data_dict2[channel] = [ float(phase) ]
        else:
            ref_data_dict2[channel].append( float(phase) )

    else:
        print "Terrible Error"

for line in fLines4:
    l_str = line.strip().split(',')
    epc = str(l_str[0])
    tstatmp = l_str[3]
    channel = l_str[8]
    phase = l_str[9]

    if epc != "E28011606000020612B034AC" :

        if channel not in data_dict2:
            data_dict2[channel] = [ float(phase) ]
        else:
            data_dict2[channel].append( float(phase) )

    else:
        print "Terrible Error"


#print data_dict1.keys()
#print data_dict2.keys()
#print ref_data_dict1.keys()
#print ref_data_dict2.keys()

diff_phase_arr1 = []
diff_phase_arr2 = []

window_size = 50

for channel in ref_data_dict1:

    phase_arr1 = ref_data_dict1[channel]
    phase_arr2 = data_dict1[channel]

    phase_arr1_smoothed = []
    phase_arr2_smoothed = []

    k = 0

    while k < (len(phase_arr1) - window_size + 1):
    
        a = np.array(phase_arr1[k:(k+window_size)])
        phase_arr1_smoothed.append(np.median(a))
        k = k + 1

    k = 0

    while k < (len(phase_arr2) - window_size + 1):
    
        a = np.array(phase_arr2[k:(k+window_size)])
        phase_arr2_smoothed.append(np.median(a))
        k = k + 1

    for i in range( min(len(phase_arr2_smoothed), len(phase_arr1_smoothed))):
        diff_phase_arr1.append( float(phase_arr2_smoothed[i]) - float(phase_arr1_smoothed[i]))

for channel in ref_data_dict2:

    phase_arr1 = ref_data_dict2[channel]
    phase_arr2 = data_dict2[channel]
    
    phase_arr1_smoothed = []
    phase_arr2_smoothed = []

    k = 0

    while k < (len(phase_arr1) - window_size + 1):
    
        a = np.array(phase_arr1[k:(k+window_size)])
        phase_arr1_smoothed.append(np.median(a))
        k = k + 1

    k = 0

    while k < (len(phase_arr2) - window_size + 1):
    
        a = np.array(phase_arr2[k:(k+window_size)])
        phase_arr2_smoothed.append(np.median(a))
        k = k + 1

    for i in range( min(len(phase_arr1_smoothed), len(phase_arr2_smoothed)) ):
        diff_phase_arr2.append( float(phase_arr2_smoothed[i]) - float(phase_arr1_smoothed[i]))

diff_phase_arr1_smoothed = []
diff_phase_arr2_smoothed = []

window_size = 50
i = 0

while i < (len(diff_phase_arr1) - window_size):

    a = np.array(diff_phase_arr1[i:(i+window_size)])
    diff_phase_arr1_smoothed.append(np.median(a))
    i = i + 1

a = np.array(diff_phase_arr1[i:(i+window_size)])
diff_phase_arr1_smoothed.append(np.median(a))


i = 0

while i < (len(diff_phase_arr2) - window_size):

    a = np.array(diff_phase_arr2[i:(i+window_size)])
    diff_phase_arr2_smoothed.append(np.median(a))
    i = i + 1

a = np.array(diff_phase_arr2[i:(i+window_size)])
diff_phase_arr2_smoothed.append(np.median(a))


#For No smoothing
#diff_phase_arr1_smoothed = diff_phase_arr1 
#diff_phase_arr2_smoothed = diff_phase_arr2

fhandle = open( "line-plot-phase-diff-smoothed.dat", "w")
count = 1

for i in range(min(len(diff_phase_arr1_smoothed), len(diff_phase_arr2_smoothed))):
    fhandle.write( str(count) + " " + str(diff_phase_arr1_smoothed[count-1]) + " " + str(diff_phase_arr2_smoothed[count-1]) )
    fhandle.write("\n")
    count = count + 1

fhandle.close()

#print sorted_rfid_epc_dict
