set terminal postscript eps color size 6.0in,4.5in font 'Helvetica,22'
set output 'epc-rssi-errorbar.eps'
set ylabel 'RSSI in dBm' font 'Helvetica,28' offset 2
set xlabel 'EPC' font 'Helvetica,22' offset 2
set title ''

#set key top left
#set style fill empty
set xrange [-0.75:33.25]
set xtic rotate by -75
set xtics auto
set ytics auto

set boxwidth 0.25
#set style fill solid

plot 'epc-rssi-errorbar.dat' using ($0-.05):2:3:xtic(1) linestyle 1 with boxerrorbars notitle
