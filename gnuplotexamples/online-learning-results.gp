set terminal postscript eps color size 6.0in,4.5in font 'Helvetica,28'
set output 'online-learning-results.eps'

set key left top
set style fill solid 1.00 border 0
set style histogram errorbars gap 2 lw 1
set style data histogram
#set xtics rotate by -45
set grid ytics
set xlabel "Online Learning Techniques"
set ylabel "Prediction Accuracy"
set yrange [0:100]
set datafile separator ","
plot 'online-learning-results.dat' using 2:3:xtic(1) ti "L1 Norm" linecolor rgb "#FF0000", \
'' using 4:5 ti "L2 Norm" lt 1 lc rgb "#00FF00"
