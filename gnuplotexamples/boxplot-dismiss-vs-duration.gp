set terminal postscript eps color size 6.0in,4.5in font 'Helvetica,28'
set output 'boxplot-dismiss-vs-duration.eps'
set ylabel 'Duration (in secs)' font 'Helvetica,28' offset 2
set xlabel 'No. of notifications dismissed' font 'Helvetica,28'
set title ''
set logscale y
set xrange [-0.5:7.5]
set autoscale y
set lmargin 10



set pointsize 0.8
set key top right
set style fill empty
plot "boxplot-dismiss-vs-duration.dat" using 1:3:2:6:5:(0.3):xticlabels(8) with candlesticks title 'Quartiles' whiskerbars , \
'' using 1:4:4:4:4:(0.3) with candlesticks lt -1 lw 2 notitle, \
'' using 1:7 with points title 'Mean'

