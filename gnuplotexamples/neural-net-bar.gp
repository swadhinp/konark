set terminal postscript eps color size 6.0in,4.5in font 'Helvetica,28'
set output 'neural-1layer-units.eps'
set ylabel 'Accuracy' font 'Helvetica,28' offset 2
set xlabel 'Number of Hidden Units' font 'Helvetica,28' offset 2
set title ''

set key top left
#set style fill empty
set xtics auto
set ytics auto

set boxwidth 0.5
set style fill solid

plot "nn-1layer.dat" using 1:3:xtic(2) with boxes notitle

