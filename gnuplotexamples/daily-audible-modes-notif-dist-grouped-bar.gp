set title ''
set terminal postscript eps color size 6.0in,4.5in font 'Helvetica,28'
set output 'daily-audible-modes-notif-dist.eps'
set ylabel 'Average Daily Hours spent in a Mode' font 'Helvetica,28'
set xlabel 'User IDs' font 'Helvetica,28' offset 2

set xrange [-6:301.5]
set yrange [0:24]
set pointsize 3.0
set xtics ("1" 0, "5" 40, "10" 90, "15" 140, "20" 190, "25" 240, "30" 290)
set ytics ("0" 0, "4" 4, "8" 8, "12" 12, "16" 16, "20" 20, "24" 24)
set key below
set boxwidth 2.0
plot \
"daily-audible-modes-notif-dist.dat" using ($1-2.0):($2) title 'Sound On'  with boxes fs solid 0.1 fill pattern 1 lt 1 lc 1 lw 1.0 , \
"daily-audible-modes-notif-dist.dat" using ($1-0.0):($3) title 'Vibration On'  with boxes fs solid 0.1 fill pattern 2 lt 2 lc 2 lw 1.0 , \
"daily-audible-modes-notif-dist.dat" using ($1+2.0):($4) title 'Silent Mode'  with boxes fs solid 0.1 fill pattern 3 lt 3 lc 3 lw 1.0 
