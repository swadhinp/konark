set terminal postscript eps color size 6.0in,4.5in font 'Helvetica,28'
#set output 'zipf-plot-app.eps'
set output 'zipf-plot-app-log.eps'
set ylabel 'Notification Count' font 'Helvetica,28' offset 2
set xlabel 'App Rank' font 'Helvetica,28' offset 2
set title ''

set key top right
#set style fill empty
set xtics auto
set ytics auto

set logscale

#set style line 1 lc rgb '#0060ad' lt 1 lw 4 pt 10 ps 1.5   # --- blue
plot 'zipf-plot-app.dat' with linespoints lt 1 lw 4 ps 4 notitle



