#!/usr/bin/python

import matplotlib.pyplot as plt
import matplotlib.animation as animation
import numpy as np
import csv
import sys
import os

#pkg_resources.require("matplotlib>=1.3.1")

fig, axes = plt.subplots(50)

for ax in axes:
    ax.set_ylim(0, 1)
    ax.grid()

channel_index_dict = {}
channel_reverse_index_dict = {}

fhandle = open( "channels.txt" , "r" )
flines = fhandle.readlines()
fhandle.close()

phase_dict = {}

for line in flines:
    l_lst = line.strip().split(':')
    index = int(l_lst[0])
    channel = str(l_lst[1])

    channel_index_dict[channel] = index
    phase_dict[channel] = []
    channel_reverse_index_dict[index] = channel

print (channel_index_dict)

for i in range(50):

    axes[i].set_ylabel('Phase (radian) of Channel' + channel_reverse_index_dict[i] )
    axes[i].set_ylim(-0.5, 2*np.pi)

window_size = 1024
phase_data = [0 for k in range(window_size)]

lines = []

for i in range(50):
    lines.append( axes[i].plot(range(len(phase_data)), phase_data)[0] )

def init_graph():
    #print("Here")
    for i in range(50):
        lines[i].set_data(range(len(phase_data)), phase_data)

    return lines

input_reader = csv.reader(sys.stdin, delimiter=',')

def update_graph(*args):
        #print("There")
        #print(data)
        #while True:
        raw_data = [next(input_reader) for k in range(50)]
            #if len(raw_data) >= 3:
            #    break

            #print(raw_data)
        data = [[d for d in rd] for rd in raw_data]
        #data = np.random.rand(3)
        #print(data)

        
        for d in data:
            phase_dict[d[8]].append(d[9])

        for key in phase_dict:
            phase_data[0:-len(phase_dict[key])] = phase_data[len(phase_dict[key]):]
            phase_data[-len(phase_dict[key])] = phase_dict[key]
            lines[channel_index_dict[key]].set_data(range(len(phase_data)), phase_data)

        #flush dictionary
        for key in phase_dict:
            phase_dict[key] = []

        for ax in axes:
            ax.figure.canvas.draw()

        return lines


if __name__ == '__main__':
    #dashboard('tag.csv')
    ani = animation.FuncAnimation(fig, update_graph, init_func=init_graph, \
            blit=True, interval=1, repeat=False )
    #ani.save("RFID_graph.mp4", fps=30, extra_args=['-vcodec', 'libx264'])

    plt.show()
    #fig.show()
    #dashboard()






