#!/usr/bin/python

#import scipy
#from scipy.signal import savgol_filter
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from math import factorial
import numpy as np
import csv
import sys
import os

def savitzky_golay(y, window_size, order, deriv=0, rate=1):
    try:
        window_size = np.abs(np.int(window_size))
        order = np.abs(np.int(order))
    except msg:
        raise ValueError("window_size and order have to be of type int")
    	
    if window_size % 2 != 1 or window_size < 1:
        raise TypeError("window_size size must be a positive odd number")
    if window_size < order + 2:
        raise TypeError("window_size is too small for the polynomials order")
    
    order_range = range(order+1)
    half_window = (window_size -1) // 2

    # precompute coefficients
    b = np.mat([[k**i for i in order_range] for k in range(-half_window, half_window+1)])
    m = np.linalg.pinv(b).A[deriv] * rate**deriv * factorial(deriv)


    # pad the signal at the extremes with
    # values taken from the signal itself
    firstvals = y[0] - np.abs( y[1:half_window+1][::-1] - y[0] )
    lastvals = y[-1] + np.abs(y[-half_window-1:-1][::-1] - y[-1])
    y = np.concatenate((firstvals, y, lastvals))

    return np.convolve( m[::-1], y, mode='valid')

#pkg_resources.require("matplotlib>=1.3.1")
def dashboard(csv_filename):

    fifo_name = "dashboard.fifo"

    def read_data():
        #os.mkfifo(fifo_name)
        #fp = open(csv_filename, 'r')
        # for line in csv.reader(sys.stdin.readline(), delimiter=','):
        #     yield [float(v) for v in line]
        
        with open(csv_filename, 'r') as fp:
            reader = csv.reader(fp, delimiter=',')
            for line in reader:
                #print(line)
                yield [float(v) for v in line]
                #yield ','.join(line)
                #yield np.random.rand(3)

fig, axes = plt.subplots(3)

for ax in axes:
    ax.set_ylim(0, 1)
    ax.grid()

axes[1].set_ylabel('RSSI (dBm)', fontsize=25)
axes[0].set_ylabel('Phase Change (radian)', fontsize=25)
axes[2].set_ylabel('Doppler (Hz)', fontsize=25)
axes[1].set_ylim(-80, 0)
axes[0].set_ylim(0, 2*np.pi)
axes[2].set_ylim(-25, 25)

zed = [tick.label.set_fontsize(30) for tick in axes[0].yaxis.get_major_ticks()]
zed = [tick.label.set_fontsize(30) for tick in axes[1].yaxis.get_major_ticks()]
zed = [tick.label.set_fontsize(30) for tick in axes[2].yaxis.get_major_ticks()]

window_size = 512
rssi_data = [0 for k in range(window_size)]
phase_data = [0 for k in range(window_size)]
doppler_data = [0 for k in range(window_size)]

lines = [axes[1].plot(range(len(rssi_data)), rssi_data)[0], \
            axes[0].plot(range(len(phase_data)), phase_data)[0], \
            axes[2].plot(range(len(doppler_data)), doppler_data)[0]]

def init_graph():
    #print("Here")
    lines[0].set_data(range(len(rssi_data)), rssi_data)
    lines[1].set_data(range(len(phase_data)), phase_data)
    lines[2].set_data(range(len(doppler_data)), doppler_data)

    return lines


input_reader = csv.reader(sys.stdin, delimiter=',')
firstFlag = 1
buffSize = 40
remaining_data = []

def update_graph(*args):

        #print (args)
        global firstFlag
        global remaining_data
        global buffSize

        raw_data = []
        data = []
        rssi = []
        phase = []
        doppler = []

        #print("FirstFlag :")
        #print(firstFlag)
        #print("Remain : ")
        #print(remaining_data)

        raw_data = [next(input_reader) for k in range(buffSize)]

        '''
        if firstFlag == 1:
            raw_data = [next(input_reader) for k in range(buffSize)]
            remaining_data = raw_data[buffSize:(buffSize)]
        else:
            raw_data = [next(input_reader) for k in range(buffSize)]
            
            raw_data_merged = remaining_data + raw_data
            remaining_data = raw_data
            raw_data = raw_data_merged
            #print("Raw1 : ")
            #print(raw_data)

            #if len(raw_data) >= 3:
            #    break
            #print(raw_data)
        '''

        firstFlag = 0
        #print("Raw2 : ")
        #print(raw_data)
        
        rssi_list = []
        phase_list = []
        doppler_list = []

        for rd in raw_data:
            rssi_list.append(float(rd[0]))
            
            #Outlier Removal
            if ( (float(rd[1]) == np.pi) or \
                    (float(rd[1]) == -np.pi) or \
                    (float(rd[1]) == 2*np.pi) or \
                    (float(rd[1]) == -2*np.pi) ):
                #print( "Here", float(rd[1]) )
                phase_list.append(float(0.0))
            else:
                phase_list.append(float(rd[1]))
            doppler_list.append(float(rd[2]))

        rssi_list_smoothed = []
        phase_list_smoothed = []
        doppler_list_smoothed = []

        #sliding window
        for i in range(buffSize):
            
            a = np.array(rssi_list[i:(i+buffSize)])
            rssi_list_smoothed.append(np.median(a))
            #rssi_list_smoothed.append(np.mean(a))
            
            a = np.array(phase_list[i:(i+buffSize)])
            phase_list_smoothed.append(np.median(a))
            #phase_list_smoothed.append(np.mean(a))
       
            a = np.array(doppler_list[i:(i+buffSize)])
            doppler_list_smoothed.append(np.median(a))
            #doppler_list_smoothed.append(np.mean(a))
        
        rssi_list_smoothed = rssi_list
        doppler_list_smoothed = doppler_list
        phase_list_smoothed = phase_list
        
        '''
        rssi_list_smoothed = savitzky_golay( np.array(rssi_list), 5, 2 )
        phase_list_smoothed = savitzky_golay( np.array(phase_list), 5, 2 )
        doppler_list_smoothed = savitzky_golay( np.array(doppler_list), 5, 2 )
        '''

        #data = [[float(d) for d in rd] for rd in raw_data]
        #data = np.random.rand(3)
        #print(data)

        #rssi = [d[0] for d in data]
        #phase = [d[1] for d in data]
        #doppler = [d[2] for d in data]

        rssi_data[0:-len(rssi_list_smoothed)] = rssi_data[len(rssi_list_smoothed):]
        rssi_data[-len(rssi_list_smoothed):] = rssi_list_smoothed
        phase_data[0:-len(phase_list_smoothed)] = phase_data[len(phase_list_smoothed):]
        phase_data[-len(phase_list_smoothed):] = phase_list_smoothed
        doppler_data[0:-len(doppler_list_smoothed)] = doppler_data[len(doppler_list_smoothed):]
        doppler_data[-len(doppler_list_smoothed):] = doppler_list_smoothed

        # rssi, phase, doppler = data
        # rssi_data.pop(0)
        # rssi_data.append(rssi)
        # phase_data.pop(0)
        # phase_data.append(phase)
        # doppler_data.pop(0)
        # doppler_data.append(doppler)

        lines[0].set_data(range(len(rssi_data)), rssi_data)
        lines[1].set_data(range(len(phase_data)), phase_data)
        lines[2].set_data(range(len(doppler_data)), doppler_data)

        for ax in axes:
            ax.figure.canvas.draw()

        return lines


if __name__ == '__main__':
    #dashboard('tag.csv')
    ani = animation.FuncAnimation(fig, update_graph, init_func=init_graph, \
            blit=False, interval=1, repeat=False )
    
    #ani.save("RFID_graph.mp4", fps=30, extra_args=['-vcodec', 'libx264'])

    plt.show()
    #fig.show()
    #dashboard()






