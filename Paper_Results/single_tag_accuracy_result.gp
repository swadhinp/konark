set terminal postscript eps color size 6.4in,4.8in font 'Helvetica,45'
set output 'single_tag_accuracy_result.eps'
set ylabel 'Localisation Error (in mm)' font 'Helvetica,45' offset 2
set xlabel 'Time window (in second)' font 'Helvetica,45' offset 2
set title ''

set key top left
set xrange [0:1.7]
#set style fill empty
#set xtics auto
set xtics ("0.1" 0.1, "0.3" 0.3, "0.5" 0.5, "0.7" 0.7, "0.9" 0.9, "1.1" 1.1, "1.3" 1.3, "1.5" 1.5, "1.7" 1.7)
set ytics auto

#set style line 1 lc rgb '#0060ad' lt 1 lw 4 pt 10 ps 1.5   # --- blue

plot "single_tag_accuracy_result.txt" using 1:2:3 w yerrorbars ls 1 lw 2 notitle, \
 "single_tag_accuracy_result.txt" using 1:2 w linespoints lt 1 lw 4 pt 2 ps 2 title 'Pmax', \
"single_tag_accuracy_result.txt" using 1:4:5 w yerrorbars ls 3 lw 2 notitle, \
 "single_tag_accuracy_result.txt" using 1:4 w linespoints lt 3 lw 4 pt 2 ps 2 title 'DeltaP'

