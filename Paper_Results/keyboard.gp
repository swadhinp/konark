set terminal postscript eps color size 6.4in,4.8in font 'Helvetica,45'
set output 'keyboard.eps'
set ylabel 'Value' font 'Helvetica,45' offset 2
set xlabel '' font 'Helvetica,45' offset 2
set title ''

set style fill solid 1.00 border 0
set style histogram errorbars gap 4 lw 2
set style data histogram

#set xtics rotate by -45

#set key top left
#set style fill empty
#set xrange [-0.75:33.25]
#set xtic rotate by -75

set xtics ("FP" 0, "FN" 1, "Acc" 2)
set ytics auto

#set boxwidth 0.25
#set style fill solid

#plot 'keyboard.dat' using ($0 -.05):2:3 linestyle 3 with boxerrorbars notitle

plot 'keyboard.dat' using 2:3 linecolor rgb "#0000FF" notitle
