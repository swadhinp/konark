set terminal postscript eps color size 6.4in,4.8in font 'Helvetica,45'
set output 'phase_pattern.eps'
set ylabel 'Phase (in radian)' font 'Helvetica,45' offset 0
set xlabel 'Relative position on the tag' font 'Helvetica,45' offset 0
set title ''

set key top left
set grid
#set yrange [1:8.0]
#set xrange [0:12]
#set style fill empty
#set xtics ("1,7" 0, "2,6" 2, "3,5" 4, "4,4" 6, "5,3" 8, "6,2" 10, "7,1" 12)
#set ytics auto

#set style line 1 lc rgb '#0060ad' lt 1 lw 4 pt 10 ps 1.5   # --- blue

plot "phase_pattern.dat" using 1:2 w lines lt 1 lw 8 title 'CW', \
    "phase_pattern.dat" using 1:3 w lines lt 3 lw 8 title 'CCW'

