import sys
import numpy as np

if len(sys.argv) < 3:
    print("pyhton <python_script> <data_dir> <output_data_file>")
    sys.exit(-1)


dir_path = str(sys.argv[1])
output_file = str(sys.argv[2])

file_arr = [ "Baseline.csv", "Pos1.csv", "Pos2.csv", "Pos3.csv", "Pos4.csv", "Pos5.csv", "Pos6.csv", "Pos7.csv", "Pos8.csv", "Pos9.csv" ]

freq_phase_dict = {}
freq_rs_dict = {}
freq_xs_dict = {}
freq_zmag_dict = {}
freq_dbloss_dict = {}

max_freq = 0.0
max_std = 0.0

for fname in file_arr:

    file_path = dir_path + "/" + fname

    fhandle = open( file_path, "r" )
    flines = fhandle.readlines()
    fhandle.close()

    count = 0
    for line in flines:

        count += 1

        if count < 6:
            continue


        l_lst = line.strip().replace(" ", "").split(",")


        freq = float( l_lst[0] )
        rs = float( l_lst[2] )
        xs = float( l_lst[3] )
        zmag = float( l_lst[4] )
        phase = float( l_lst[5] )
        dbloss = float( l_lst[7] )

        max_freq = freq

        if freq not in freq_phase_dict:
            
            freq_phase_dict[freq] = [ (180 - phase)*0.0174533 ]
            freq_rs_dict[freq] = [ rs ]
            freq_xs_dict[freq] = [ xs ]
            freq_zmag_dict[freq] = [ zmag ]
            freq_dbloss_dict[freq] = [ dbloss ]
        
        else:

            freq_phase_dict[freq].append( (180 - phase)*0.0174533 )
            freq_rs_dict[freq].append( rs )
            freq_xs_dict[freq].append( xs )
            freq_zmag_dict[freq].append( zmag )
            freq_dbloss_dict[freq].append( dbloss )


freq_test_dict = freq_zmag_dict

max_std = np.std( np.array( freq_test_dict[max_freq] ) )

for freq in freq_test_dict:

    val = np.std(np.array(freq_test_dict[freq]))
    
    if val > max_std:
        max_freq = freq

#max_freq = 946.064026
out_handle = open(output_file, 'w')

#max_freq = 907.064026
max_freq = 922.064026
#max_freq = 910.064026 #for without chip s2 = 907.064026, s1 = 907.047974
#max_freq = 701.506775
lcount = 0

for el in freq_test_dict[max_freq]:
    out_handle.write( str(lcount) + " " + str(el) + "\n")
    lcount += 1

out_handle.close()
        


        

