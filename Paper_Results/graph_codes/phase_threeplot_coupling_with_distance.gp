set terminal postscript eps color size 6.4in,4.8in font 'Helvetica,34'
set output 'phase_threeplot_coupling_with_distance.eps'

set multiplot layout 3, 1 title "" font 'Helvetica,34' \
              margins 0.2,0.9,0.9,0.18 \
              spacing 0.0,0.1

#set tmargin 4

#set title "Top tag"
unset key
set grid
#set ylabel 'Phase (radian)' font 'Helvetica,34' offset 0
#set xlabel 'Position on tag' font 'Helvetica,34' offset 0
set xrange [2:14]
set xtics ("1" 2, "2" 3.5, "3" 5, "4" 6.5, "5" 8, "6" 9.5, "7" 11, "8" 12.5, "9" 14)
#set xtics auto
set ytics 0.5
plot "tag3.dat" using 1:2 w lines lt 1 lw 10 notitle

#
#set title "Touch tag"
set key bottom
set grid
set ylabel 'Phase (radian)' font 'Helvetica,45' offset 0
set xrange [2:14]
set yrange [1:11]
set xtics ("1" 2, "2" 3.5, "3" 5, "4" 6.5, "5" 8, "6" 9.5, "7" 11, "8" 12.5, "9" 14)
set ytics ("0" 0, "3.0" 3.0, "6.0" 6.0, "9.0" 9.0)
#set xtics auto
#set ytics 2
plot "tag4.dat" using 1:2 w lines lt 1 lw 10 title 'No nearby tags', \
    "tag4n.dat" using 1:3 w lines lt 3 lw 10 title 'Nearby tags'

#set title "Bottom tag"
unset key
set grid
set ylabel '' font 'Helvetica,34' offset 0
set xlabel 'Relative position on tag' font 'Helvetica,45' offset 0
set yrange [1.5:2.5]
set xrange [2:14]
set xtics ("1" 2, "2" 3.5, "3" 5, "4" 6.5, "5" 8, "6" 9.5, "7" 11, "8" 12.5, "9" 14)
#set xtics auto
set ytics 0.5
plot "tag5.dat" using 1:2 w lines lt 1 lw 10 notitle

unset multiplot

