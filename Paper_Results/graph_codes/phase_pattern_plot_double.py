#This code is for single tag tracking (Online and DeltaP)
from __future__ import division, print_function
import scipy.signal
import sys
import time
import math
import numpy as np
from scipy import stats
import threading
import signal
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d
from scipy import interpolate
import warnings
from detect_peaks import detect_peaks


warnings.filterwarnings(action="ignore", module="scipy", message="^internal gelsd")

if len(sys.argv) < 2:
    print ("python <file.py> <phase_file1> <phase_file2>")
    sys.exit(-1)

#Basic Info and Global Variables

window_size = 91

fhandle = open( sys.argv[1], 'r' )
loglines1 = fhandle.readlines()
fhandle.close()    

fhandle = open( sys.argv[2], 'r' )
loglines2 = fhandle.readlines()
fhandle.close()    

phase_arr1 = []
tstamp_arr1 = []
tstamp_arr_phase_tag1 = []

phase_arr2 = []
tstamp_arr2 = []
tstamp_arr_phase_tag2 = []

for line in loglines1:
    l_lst = line.strip().split(',')
    
    epc = str(l_lst[0])
    tstamp = float(l_lst[3])
    phase = float(l_lst[9])

    phase_arr1.append( phase )
    tstamp_arr1.append( tstamp )


for i in range(len(tstamp_arr1)):
    tstamp_arr_phase_tag1.append( (tstamp_arr1[i] - tstamp_arr1[0])*1.0/1000000.0 )

phase_arr1 = scipy.signal.savgol_filter( np.unwrap( np.array( phase_arr1 ), discont=np.pi ), window_size +100, 2 )
    #phase_arr = scipy.signal.medfilt( np.unwrap( np.array( phase_arr ), discont=np.pi ), window_size)


for line in loglines2:
    l_lst = line.strip().split(',')
    
    epc = str(l_lst[0])
    tstamp = float(l_lst[3])
    phase = float(l_lst[9])

    phase_arr2.append( phase )
    tstamp_arr2.append( tstamp )


for i in range(len(tstamp_arr2)):
    tstamp_arr_phase_tag2.append( (tstamp_arr2[i] - tstamp_arr2[0])*1.0/1000000.0 )

phase_arr2 = scipy.signal.savgol_filter( np.unwrap( np.array( phase_arr2 ), discont=np.pi ), window_size, 2 )

arr = detect_peaks(phase_arr2, mph=8, mpd=100, threshold=0,  show=False)

#for i in range(arr[0]-2):
 #   phase_arr2[i] = phase_arr2[i] + 4.2
#phase_arr2 = scipy.signal.savgol_filter( np.array( phase_arr2 ), window_size, 2 )

fhandle = open('phase_pattern.dat', 'w')

#'''
for x in range( min(len(tstamp_arr_phase_tag1), len(tstamp_arr_phase_tag2))  ):
    fhandle.write( str(tstamp_arr_phase_tag1[x]) + " " + str(phase_arr1[x]) + " " + str(phase_arr2[x]) + "\n" )

'''
#For Stylus
for x in range( max(len(tstamp_arr_phase_tag1), len(tstamp_arr_phase_tag2))  ):
    if x < len(tstamp_arr_phase_tag2):
        fhandle.write( str(tstamp_arr_phase_tag1[x]) + " " + str(phase_arr1[x]) + " " + str(phase_arr2[x]) + "\n" )
    else:
        fhandle.write( str(tstamp_arr_phase_tag1[x]) + " " + str(phase_arr1[x]) + " " + str(phase_arr2[-1]) + "\n" )
'''
fhandle.close()
