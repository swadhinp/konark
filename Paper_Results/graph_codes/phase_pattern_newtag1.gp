set terminal postscript eps color size 6.0in,4.5in font 'Helvetica,34'
set output 'phase_pattern_newtag1.eps'
set ylabel 'Phase (in radian)' font 'Helvetica,34' offset 2
set xlabel 'Time (in s)' font 'Helvetica,34' offset 2
set title ''

set grid ytics lc rgb "#778899" lw 1 lt 1 dt "   .   "
set grid xtics lc rgb "#778899" lw 1 lt 1 dt "   .   "
#set grid back

set key top left
#set yrange [5:7.5]
set xrange [1:9]
#set style fill empty
#set xtics ("0" 1, "1" 2, "2" 3, "3" 4, "4" 5, "5" 6, "6" 7, "7" 8)
#set xtics ("0" 4.5, "1" 5.5, "2" 6.5, "3" 7.5)
set xtics 1
set ytics 0.5

#set style line 1 lc rgb '#0060ad' lt 1 lw 4 pt 10 ps 1.5   # --- blue

plot "phase_pattern_newtag1.dat" using 1:2 w lines lt 1 lw 4 notitle
