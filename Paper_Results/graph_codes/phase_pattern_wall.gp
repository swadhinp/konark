set terminal postscript eps color size 6.4in,4.8in font 'Helvetica,45'
set output 'phase_pattern_wall.eps'
set ylabel 'Phase (in radian)' font 'Helvetica,45' offset 2
set xlabel 'Position on the tag' font 'Helvetica,45' offset 0
set title ''

set key top left
set grid
set yrange [3:7.5]
set xrange [4:8]
#set style fill empty
set xtics ("1" 4, "2" 4.5, "3" 5, "4" 5.5, "5" 6, "6" 6.5, "7" 7, "8" 7.5, "9" 8)
#set xtics ("0" 4.5, "1" 5.5, "2" 6.5, "3" 7.5)
set ytics auto

#set style line 1 lc rgb '#0060ad' lt 1 lw 4 pt 10 ps 1.5   # --- blue

plot "phase_pattern_wall.dat" using 1:2 w linespoints lt 1 lw 4 notitle
#"time-2engage-plot.dat" using 1:3:5 w yerrorbars ls 3 lw 2 notitle, \
 #"time-2engage-plot.dat" using 1:3 w linespoints lt 3 lw 4 pt 2 ps 2 title 'Interacted'

