set terminal postscript eps color size 6.4in,4.8in font 'Helvetica,45'
set output 'phase_pattern.eps'
set ylabel 'Phase (in radian)' font 'Helvetica,45' offset 0
set xlabel 'Position on the tag' font 'Helvetica,45' offset 0
set title ''

set key top left
set grid
set yrange [1:8.0]
set xrange [0.5:4.5]
#set style fill empty
set xtics ("1" 0.5, "2" 1, "3" 1.5, "4" 2, "5" 2.5, "6" 3, "7" 3.5, "8" 4, "9" 4.5)
set ytics auto

#set style line 1 lc rgb '#0060ad' lt 1 lw 4 pt 10 ps 1.5   # --- blue

plot "phase_pattern.dat" using 1:2 w linespoints lt 1 lw 4 pt 4 ps 1 title 'Touching the tag', \
    "phase_pattern.dat" using 1:3 w linespoints lt 3 lw 4 pt 4 ps 1 title 'Close to the tag'

