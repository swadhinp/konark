set terminal postscript eps color size 6.4in,4.8in font 'Helvetica,45'
set output 'vna_impedance_change.eps'
#set ylabel 'Phase (In Radian)' font 'Helvetica,45' offset 2
set ylabel 'Impedance magnitude (|Z|)' font 'Helvetica,45' offset 0
set xlabel 'Position on the tag' font 'Helvetica,45' offset 0
set title ''

set key top left
set grid
#set xrange [1:9]
#set style fill empty
#set xtic rotate by -45
set xtics ("1" 1, "2" 2, "3" 3, "4" 4, "5" 5, "6" 6, "7" 7, "8" 8, "9" 9)

set ytics auto

plot "vna_impedance_change.dat" using 1:2 w linespoints lt 1 lw 6 pt 2 ps 4 notitle

