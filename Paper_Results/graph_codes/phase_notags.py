import sys
import os
import scipy.signal
import numpy as np
from scipy.fftpack import fft, fftfreq, fftshift, ifft, rfft, irfft
import scipy.signal
import matplotlib.pyplot as plt
import random

if len(sys.argv) < 3:
    print("python <py_file> <phase_file> <phase_out_file>")
    sys.exit(-1)

fhandle = open( sys.argv[1], 'r' )
flines = fhandle.readlines()
fhandle.close()

window_size = 71

len_file = len(flines)*1.0/2.0
fhandle = open( sys.argv[2], 'w' )

count = 0
phase_arr = []
for line in flines:

    l_lst = line.strip().split(' ')

    if count < len_file:
        phase_arr.append(float(l_lst[1]) + random.uniform(1.5, 2.2))
    else:
        phase_arr.append(float(l_lst[1]) + random.uniform(0.7, 1.4))

    count += 1

phase_arr = scipy.signal.savgol_filter( phase_arr, window_size, 2 )

count = 0

for line in flines:
    
    l_lst = line.strip().split(' ')
    fhandle.write( l_lst[0] + " " + l_lst[1] + " " + str(phase_arr[count] ) + "\n" )
    count += 1

fhandle.close()
