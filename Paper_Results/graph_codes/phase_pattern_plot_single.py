#This code is for single tag tracking (Online and DeltaP)
from __future__ import division, print_function
import scipy.signal
import sys
import time
import math
import numpy as np
from scipy import stats
import threading
import signal
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d
from scipy import interpolate
import warnings

warnings.filterwarnings(action="ignore", module="scipy", message="^internal gelsd")

if len(sys.argv) < 2:
    print ("python <file.py> <phase_file>")
    sys.exit(-1)

#Basic Info and Global Variables

window_size = 191

fhandle = open( sys.argv[1], 'r' )
loglines = fhandle.readlines()
fhandle.close()    

phase_arr = []
tstamp_arr = []
tstamp_arr_phase_tag = []

for line in loglines:
    l_lst = line.strip().split(',')
    
    epc = str(l_lst[0])
    tstamp = float(l_lst[3])
    phase = float(l_lst[9])

    phase_arr.append( phase )
    tstamp_arr.append( tstamp )


for i in range(len(tstamp_arr)):
    tstamp_arr_phase_tag.append( (tstamp_arr[i] - tstamp_arr[0])*1.0/1000000.0 )

phase_arr = scipy.signal.savgol_filter( np.unwrap( np.array( phase_arr ), discont=np.pi ), window_size, 2 )
    #phase_arr = scipy.signal.medfilt( np.unwrap( np.array( phase_arr ), discont=np.pi ), window_size)


fhandle = open('phase_pattern.dat', 'w')

for x in range( len(tstamp_arr_phase_tag) ):
    fhandle.write( str(tstamp_arr_phase_tag[x]) + " " + str(phase_arr[len(phase_arr)-x-1]) + "\n" )

fhandle.close()
