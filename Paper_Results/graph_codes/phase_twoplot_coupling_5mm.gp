set terminal postscript eps color size 6.4in,4.8in font 'Helvetica,45'
set output 'phase_pattern.eps'
set ylabel 'Phase (in radian)' font 'Helvetica,45' offset 0
set xlabel 'Position on the tag' font 'Helvetica,45' offset 0
set title ''

set key top left
set grid
set yrange [1.0:7.0]
set xrange [1.5:5.5]
#set style fill empty
set xtics ("1" 1.5, "2" 2, "3" 2.5, "4" 3, "5" 3.5, "6" 4.0, "7" 4.5, "8" 5, "9" 5.5)
#set xtics auto
set ytics auto

#set style line 1 lc rgb '#0060ad' lt 1 lw 4 pt 10 ps 1.5   # --- blue

plot "phase_pattern.dat" using 1:2 w lines lt 1 lw 8 title 'No nearby tags', \
    "phase_pattern.dat" using 1:3 w lines lt 3 lw 8 title 'Nearby tags'

