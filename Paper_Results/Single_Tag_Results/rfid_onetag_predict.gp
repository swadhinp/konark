set terminal postscript eps color size 6.4in,4.8in font 'Helvetica,45'
set output 'rfid_onetag_predict.eps'
set ylabel 'Position on Tag (in mm)' font 'Helvetica,45' offset 0
set xlabel 'Steps' font 'Helvetica,45' offset 0
set title ''

set key bottom left
set grid
#set yrange [1:8.0]
#set xrange [0.5:4.5]
#set style fill empty
#set xtics ("1" 0.5, "2" 1, "3" 1.5, "4" 2, "5" 2.5, "6" 3, "7" 3.5, "8" 4, "9" 4.5)
set ytics auto

#set style line 1 lc rgb '#0060ad' lt 1 lw 4 pt 10 ps 1.5   # --- blue

plot "rfid_onetag_predict.dat" using 1:2 w linespoints lt 1 lw 4 pt 4 ps 1 title 'Predicted', \
    "rfid_onetag_predict.dat" using 1:3 w linespoints lt 3 lw 4 pt 4 ps 1 title 'Ground-truth'

