set terminal postscript eps color size 6.4in,4.8in font 'Helvetica,45'
set output 'cdf_rfid_onetag_predict.eps'
set ylabel 'CDF' font 'Helvetica,45' offset 2
set xlabel 'Localization Error (in mm)' font 'Helvetica,45' offset 2
set title ''

set key top left
#set yrange [0:1.0]
set xrange [0:9.5]
#set style fill empty
#set xtics ("0" 1, "1" 2, "2" 3, "3" 4, "4" 5, "5" 6, "6" 7, "7" 8)
#set xtics ("0" 4.5, "1" 5.5, "2" 6.5, "3" 7.5)
set ytics auto

#set style line 1 lc rgb '#0060ad' lt 1 lw 4 pt 10 ps 1.5   # --- blue

plot "cdf_rfid_onetag_predict.dat" using 1:2 w lines lt 1 lw 6 notitle
#"time-2engage-plot.dat" using 1:3:5 w yerrorbars ls 3 lw 2 notitle, \
 #"time-2engage-plot.dat" using 1:3 w linespoints lt 3 lw 4 pt 2 ps 2 title 'Interacted'

