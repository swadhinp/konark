/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
// package com.example.sdksamples;
package com.nec;

/**
 * This contains the list of possible properties that can be set for the
 * examples.
 *
 * @author paulfdietrich
 */
public class Properties {

    public static String hostname = "10.0.0.96";
    public static String targetTag = "targetTag";
    public static String qtMode = "qtMode";
    public static String powerDbm = "powerdBm";
    public static String sensitivityDbm = "sensitivityDbm";
    public static String targetUser = "targetUser";
}
