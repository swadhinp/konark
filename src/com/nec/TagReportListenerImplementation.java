//package com.example.sdksamples;
//This is the log printing utility
//
package com.nec;


import com.impinj.octane.*;

import java.util.List;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Hashtable;
import java.io.BufferedReader;
import java.util.HashSet;
import java.io.File;
import java.io.FileReader;
import java.lang.Math;
import java.util.Date;
import java.util.Random;
import java.util.ArrayList;

public class TagReportListenerImplementation implements TagReportListener {

    private FileWriter csvWriter;
    private FileWriter csvWriter1;
    private FileWriter csvWriter1_phase;
    private FileWriter csvWriter2;
    private FileWriter csvWriter2_phase;
    private FileWriter csvWriter3;
    private FileWriter csvWriter4;
    private FileWriter csvWriter5;
    private FileWriter csvWriter6;
    private FileWriter csvWriter7;
    private FileWriter csvWriter8;
    private FileWriter csvWriter9;
    private FileWriter csvWriter10;
    private FileWriter csvWriter11;
    private FileWriter csvWriter12;
    private FileWriter csvWriter13;
    private FileWriter csvWriter14;

    private double cur_phase = 0;
    private boolean first_flag = true;
    private double last_channel = 0.0;
    private long last_time = 0;
    private double cur_phase1 = 0;
    private boolean first_flag1 = true;
    private double last_channel1 = 0.0;
    private long last_time1 = 0;
    private double cur_phase2 = 0;
    private boolean first_flag2 = true;
    private double last_channel2 = 0.0;
    private long last_time2 = 0;
    private double cur_phase3 = 0;
    private boolean first_flag3 = true;
    private double last_channel3 = 0.0;
    private long last_time3 = 0;
    private double cur_phase4 = 0;
    private boolean first_flag4 = true;
    private double last_channel4 = 0.0;
    private long last_time4 = 0;
  
    static short EPC_OP_ID = 123;
    static short PC_BITS_OP_ID = 321;
    static int opSpecID = 1;
    public static int outstanding = 0;
    static Random r = new Random();

    private HashSet<String> hashSet;
    BufferedReader br;

    //Getting random EPC
    static String getRandomEpc() {
        String epc = "";

        // get the length of the EPC from 1 to 8 words
        //int numwords = r.nextInt((6 - 1) + 1) + 1;
        int numwords = 24;

        for (int i = 0; i < numwords; i++) {
            Short s = (short) r.nextInt(Short.MAX_VALUE + 1);
            epc += String.format("%04X", s);
        }

        epc = "A00000000000000000006600";


        return epc;
    }

    //Writing new EPC string
    void programEpc(String currentEpc, short currentPC, String newEpc)
            throws Exception {

        if ((currentEpc.length() % 4 != 0) || (newEpc.length() % 4 != 0)) {
            throw new Exception("EPCs must be a multiple of 16- bits: "
                    + currentEpc + "  " + newEpc);
        }

        if (outstanding > 0) {
            return;
        }

        System.out.println("Programming Tag ");
        System.out.println("   EPC " + currentEpc + " to " + newEpc);

        TagOpSequence seq = new TagOpSequence();
        seq.setOps(new ArrayList<TagOp>());
        seq.setExecutionCount((short) 1); // delete after one time
        seq.setState(SequenceState.Active);
        seq.setId(opSpecID++);

        seq.setTargetTag(new TargetTag());
        seq.getTargetTag().setBitPointer(BitPointers.Epc);
        seq.getTargetTag().setMemoryBank(MemoryBank.Epc);
        seq.getTargetTag().setData(currentEpc);

        TagWriteOp epcWrite = new TagWriteOp();
        epcWrite.Id = EPC_OP_ID;
        epcWrite.setMemoryBank(MemoryBank.Epc);
        epcWrite.setWordPointer(WordPointers.Epc);
        epcWrite.setData(TagData.fromHexString(newEpc));

        // add to the list
        seq.getOps().add(epcWrite);

        // have to program the PC bits if these are not the same
        if (currentEpc.length() != newEpc.length()) {
            // keep other PC bits the same.
            String currentPCString = PcBits.toHexString(currentPC);

            short newPC = PcBits.AdjustPcBits(currentPC,
                    (short) (newEpc.length() / 4));
            String newPCString = PcBits.toHexString(newPC);
            
            System.out.println("   PC bits to establish new length: "
                    + newPCString + " " + currentPCString);

            TagWriteOp pcWrite = new TagWriteOp();
            pcWrite.Id = PC_BITS_OP_ID;
            pcWrite.setMemoryBank(MemoryBank.Epc);
            pcWrite.setWordPointer(WordPointers.PcBits);

            pcWrite.setData(TagData.fromHexString(newPCString));
            seq.getOps().add(pcWrite);
        }

        outstanding++;
        Reader.reader.addOpSequence(seq);
    }

    public TagReportListenerImplementation(String outputFilename) {
       
        if(!outputFilename.equals("")) {
            try {
                csvWriter = new FileWriter(outputFilename);
            } catch (IOException e) {
                System.out.print(e);
            }
        }


        try {

            csvWriter1 = new FileWriter("all_data.csv");

        } catch (IOException e) {

            System.out.print(e);
        }

        try {

            csvWriter1_phase = new FileWriter("phase_diff_cart.dat");

        } catch (IOException e) {

            System.out.print(e);
        }

        try {

            csvWriter2_phase = new FileWriter("phase_diff_others.dat");

        } catch (IOException e) {

            System.out.print(e);
        }


        try {

            csvWriter2 = new FileWriter("quicklook.csv");

        } catch (IOException e) {

            System.out.print(e);
        }

        try {

            csvWriter3 = new FileWriter("tag2.csv");

        } catch (IOException e) {

            System.out.print(e);
        }

        try {

            csvWriter4 = new FileWriter("all_data_group2.csv");

        } catch (IOException e) {

            System.out.print(e);
        }

        try {

            csvWriter5 = new FileWriter("quicklook_group2.csv");

        } catch (IOException e) {

            System.out.print(e);
        }

        try {

            csvWriter5 = new FileWriter("quicklook_group2.csv");

        } catch (IOException e) {

            System.out.print(e);
        }

        try {

            csvWriter6 = new FileWriter("tagn2.csv");

        } catch (IOException e) {

            System.out.print(e);
        }

        try {

            csvWriter7 = new FileWriter("tagn3.csv");

        } catch (IOException e) {

            System.out.print(e);
        }

        try {

            csvWriter8 = new FileWriter("tagn4.csv");

        } catch (IOException e) {

            System.out.print(e);
        }

        try {

            csvWriter9 = new FileWriter("tagn5.csv");

        } catch (IOException e) {

            System.out.print(e);
        }

        try {

            csvWriter10 = new FileWriter("tagn6.csv");

        } catch (IOException e) {

            System.out.print(e);
        }

        try {

            csvWriter11 = new FileWriter("tagn7.csv");

        } catch (IOException e) {

            System.out.print(e);
        }

        try {

            csvWriter12 = new FileWriter("tagn8.csv");

        } catch (IOException e) {

            System.out.print(e);
        }

        try {

            csvWriter13 = new FileWriter("tagn9.csv");

        } catch (IOException e) {

            System.out.print(e);
        }


        //Reading in a HashTable group
        //

        // Creating a Hashtable
        hashSet = new HashSet<String>();
        File fin = new File("cart_tags_running.txt");
        
        try{
         
            br = new BufferedReader(new FileReader(fin));
        
        } catch(IOException e){
            
            System.out.print(e);
        }
         
        String line = null;
        try{

        while ((line = br.readLine()) != null) {

            String key = line.replace("\n", "").replace("\r", "");

            if( hashSet.contains(key) ){
            
                System.out.println("Error in File : Duplicate Keys.");
            
            } else {

                hashSet.add(key);
            }
        
        }
        }catch(IOException e){
            System.out.print(e);
        }

        try{
            br.close();
        }catch (IOException e){

            System.out.print(e);
        }
    }


    private boolean checkInGroup(String tag){

        if( hashSet.contains(tag) ){
            return true;
        }
    
        return false;
    }

    @Override
    public void onTagReported(ImpinjReader reader, TagReport report) {
        List<Tag> tags = report.getTags();

        for (Tag t : tags) {

                //    t.getEpc().toString().replaceAll("\\s+", "").equals("E28011606000020612AF52DB") ){

                String epcStr = t.getEpc().toString().replaceAll("\\s+", "");

                System.out.print(" EPC: " + t.getEpc().toString().replaceAll("\\s+", ""));

                if (reader.getName() != null) {
                    System.out.print(" Reader_name: " + reader.getName());
                } else {
                    //System.out.print(" Reader_ip: " + reader.getAddress());
                }

                if (t.isAntennaPortNumberPresent()) {
                    System.out.print(" antenna: " + t.getAntennaPortNumber());
                }

                if (t.isFirstSeenTimePresent()) {
                    //System.out.print(" first: " + t.getFirstSeenTime().ToString());
                }

                if (t.isLastSeenTimePresent()) {
                    System.out.print(" last: " + t.getLastSeenTime().ToString());
                    //System.out.print(" last: " + new Long(t.getLastSeenTime().getUtcTimestamp()).toString());

                }

                if (t.isSeenCountPresent()) {
                    System.out.print(" count: " + t.getTagSeenCount());
                }

                if (t.isRfDopplerFrequencyPresent()) {
                    System.out.print(" doppler: " + t.getRfDopplerFrequency());
                }

                if (t.isPeakRssiInDbmPresent()) {
                    System.out.print(" peak_rssi: " + t.getPeakRssiInDbm());
                }

                //System.out.print(" chan_MHz: " + t.getChannelInMhz());
                
                if(t.isRfPhaseAnglePresent()) {

                    System.out.print(" phase: " + t.getPhaseAngleInRadians());
                }

                if (t.isChannelInMhzPresent()) {
                    System.out.print(" chan_MHz: " + t.getChannelInMhz());
                }

                
                if (t.isFastIdPresent()) {
                    
                    System.out.print("\n     fast_id: " + t.getTid().toHexString());

                    System.out.print(" model: " +
                            t.getModelDetails().getModelName());

                    System.out.print(" epcsize: " +
                            t.getModelDetails().getEpcSizeBits());

                    System.out.print(" usermemsize: " +
                            t.getModelDetails().getUserMemorySizeBits());
                    
                }

        //Code snippet to wite EPC codes
        
            
            /*String newEpc = getRandomEpc();

            if (t.isPcBitsPresent()) {
                short pc = t.getPcBits();
                String currentEpc = t.getEpc().toHexString();

                try {

                    programEpc(currentEpc, pc, newEpc);
                } catch (Exception e) {
                    System.out.println("Failed To program EPC: " + e.toString());
                }
            }*/

        
                System.out.println("");

                try {

                    if( checkInGroup( epcStr ) ){

                        //Writing tag.csv
                        StringBuffer csvBuffer = new StringBuffer();
                        csvBuffer.append(new Float(t.getPeakRssiInDbm()).toString());
                        csvBuffer.append(",");
                        double phase_diff = 0.0;

                        //phase difference calculation
                        if ( first_flag == true ) {
                        
                            last_channel = t.getChannelInMhz();
                            //phase_diff = t.getPhaseAngleInRadians();
                            phase_diff = 0.0;
                            cur_phase = t.getPhaseAngleInRadians();
                            first_flag = false;
                            last_time = t.getLastSeenTime().getLocalDateTime().getTime();

                        }else{
                        
                            if( last_channel == t.getChannelInMhz() ) {
                                //System.out.println("HERE");
                                //phase_diff = Math.abs(t.getPhaseAngleInRadians() - cur_phase);
                                phase_diff = t.getPhaseAngleInRadians() - cur_phase;
                                cur_phase = t.getPhaseAngleInRadians();
                            }else{
                                //System.out.println("Channel Switch :" + phase_diff + " : " + (t.getLastSeenTime().getLocalDateTime().getTime() - last_time));
                                //phase_diff = t.getPhaseAngleInRadians();
                                //phase_diff = cur_phase;
                                last_channel = t.getChannelInMhz();
                                cur_phase = t.getPhaseAngleInRadians();
                            }

                            last_time = t.getLastSeenTime().getLocalDateTime().getTime();

                        }

                        csvWriter1_phase.write(phase_diff + "\n");
                        csvWriter1_phase.flush();
                        csvBuffer.append(new Double(phase_diff).toString());
                        csvBuffer.append(",");
                        csvBuffer.append(new Float(t.getRfDopplerFrequency()).toString());

                        csvWriter.append(csvBuffer.toString());
                        csvWriter.append("\n");
                        csvWriter.flush();

                        //Writing for all_data.csv
                        StringBuffer csvBuffer1 = new StringBuffer();
                        
                        //EPC
                        csvBuffer1.append(t.getEpc().toString().replaceAll("\\s+", ""));
                        //csvBuffer1.append(t.getTid().toHexString().replaceAll("\\s+", "")); //TID
                        csvBuffer1.append(",");


                        //Number of Antennas
                        if (t.isAntennaPortNumberPresent()) {
                            csvBuffer1.append(t.getAntennaPortNumber());
                        } else {
                            csvBuffer1.append(-1);
                        }

                        csvBuffer1.append(",");

                        //First Seen Time
                        if (t.isFirstSeenTimePresent()) {
                            csvBuffer1.append(t.getFirstSeenTime().ToString());
                        } else {
                            csvBuffer1.append(-1);
                        }
                        csvBuffer1.append(",");

                        //Last Seen Time
                        if (t.isLastSeenTimePresent()) {
                            csvBuffer1.append(t.getLastSeenTime().ToString());
                        } else {
                            csvBuffer1.append(-1);
                        }
                        csvBuffer1.append(",");
                
                        //Tag Seen Count
                        
                        if (t.isSeenCountPresent()) {
                            csvBuffer1.append(t.getTagSeenCount());
                        } else {
                            csvBuffer1.append(-1);
                        }
                        csvBuffer1.append(",");

                        //Doppler Frequency
                        if (t.isRfDopplerFrequencyPresent()) {
                            csvBuffer1.append(t.getRfDopplerFrequency());
                        } else {
                            csvBuffer1.append("-1.0");
                        }

                        csvBuffer1.append(",");

                        //Protocol Control Bits for BackScattered Network
                        if( t.isPcBitsPresent() ) {
                            csvBuffer1.append(String.valueOf(t.getPcBits()));
                        } else {
                            csvBuffer1.append("-1");
                        }
                        csvBuffer1.append(",");

                        //RSSI
                        if (t.isPeakRssiInDbmPresent()) {
                            csvBuffer1.append(t.getPeakRssiInDbm());
                        } else {
                            csvBuffer1.append("-1.0");
                        }

                        csvBuffer1.append(",");

                        //Channel in MHz
                        if (t.isChannelInMhzPresent()) {
                            csvBuffer1.append(new Float(t.getChannelInMhz()).toString());
                        } else{
                            csvBuffer1.append( "-1.0" );
                        }

                        csvBuffer1.append(",");
                       
                        //Phase Angle
                        if(t.isRfPhaseAnglePresent()) {
                            csvBuffer1.append(new Float(t.getPhaseAngleInRadians()).toString());
                        }else{
                            csvBuffer1.append("-1.0");
                        }

                        csvWriter1.append(csvBuffer1.toString());
                        csvWriter1.append("\n");
                        csvWriter1.flush();

                     //Writing for quicklook.csv
                     //
                         StringBuffer csvBuffer2 = new StringBuffer();
                         csvBuffer2.append(t.getEpc().toString().replaceAll("\\s+", ""));
                         //csvBuffer2.append(t.getTid().toHexString().replaceAll("\\s+", ""));
                         csvBuffer2.append(",");
                         csvBuffer2.append(new Float(t.getChannelInMhz()).toString());
                         csvBuffer2.append(",");
                         csvBuffer2.append(new Float(t.getPhaseAngleInRadians()).toString());
                         csvBuffer2.append(",");
                         csvBuffer2.append(new Float(t.getRfDopplerFrequency()).toString());

                         csvWriter2.append(csvBuffer2.toString());
                         csvWriter2.append("\n");
                         csvWriter2.flush();

                  } else {
                            //Writing tag2.csv
                            StringBuffer csvBuffer3 = new StringBuffer();
                            csvBuffer3.append(new Float(t.getPeakRssiInDbm()).toString());
                            csvBuffer3.append(",");
                            double phase_diff1 = 0.0;

                            //phase difference calculation
                            if ( first_flag1 == true ) {
                            
                                last_channel1 = t.getChannelInMhz();
                                //phase_diff = t.getPhaseAngleInRadians();
                                phase_diff1 = 0.0;
                                cur_phase1 = t.getPhaseAngleInRadians();
                                first_flag1 = false;
                                last_time1 = t.getLastSeenTime().getLocalDateTime().getTime();

                            }else{
                            
                                if( last_channel1 == t.getChannelInMhz() ) {
                                    phase_diff1 = t.getPhaseAngleInRadians() - cur_phase1;
                                    cur_phase1 = t.getPhaseAngleInRadians();
                                }else{
                                    last_channel1 = t.getChannelInMhz();
                                    cur_phase1 = t.getPhaseAngleInRadians();
                                }

                                last_time1 = t.getLastSeenTime().getLocalDateTime().getTime();

                            }

                            csvWriter2_phase.write(phase_diff1 + "\n");
                            csvWriter2_phase.flush();
                            csvBuffer3.append(new Double(phase_diff1).toString());
                            csvBuffer3.append(",");
                            csvBuffer3.append(new Float(t.getRfDopplerFrequency()).toString());

                            csvWriter3.append(csvBuffer3.toString());
                            csvWriter3.append("\n");
                            csvWriter3.flush();


                        //Writing for all_data_group2.csv
                        StringBuffer csvBuffer4 = new StringBuffer();
                        
                        //EPC
                        csvBuffer4.append(t.getEpc().toString().replaceAll("\\s+", ""));
                        //csvBuffer4.append(t.getTid().toHexString().replaceAll("\\s+", ""));
                        csvBuffer4.append(",");

                        //Number of Antennas
                        if (t.isAntennaPortNumberPresent()) {
                            csvBuffer4.append(t.getAntennaPortNumber());
                        } else {
                            csvBuffer4.append(-1);
                        }

                        csvBuffer4.append(",");

                        //First Seen Time
                        if (t.isFirstSeenTimePresent()) {
                            csvBuffer4.append(t.getFirstSeenTime().ToString());
                        } else {
                            csvBuffer4.append(-1);
                        }
                        csvBuffer4.append(",");

                        //Last Seen Time
                        if (t.isLastSeenTimePresent()) {
                            csvBuffer4.append(t.getLastSeenTime().ToString());
                        } else {
                            csvBuffer4.append(-1);
                        }
                        csvBuffer4.append(",");
                
                        //Tag Seen Count
                        
                        if (t.isSeenCountPresent()) {
                            csvBuffer4.append(t.getTagSeenCount());
                        } else {
                            csvBuffer4.append(-1);
                        }
                        csvBuffer4.append(",");

                        //Doppler Frequency
                        if (t.isRfDopplerFrequencyPresent()) {
                            csvBuffer4.append(t.getRfDopplerFrequency());
                        } else {
                            csvBuffer4.append("-1.0");
                        }

                        csvBuffer4.append(",");

                        //Protocol Control Bits for BackScattered Network
                        if( t.isPcBitsPresent() ) {
                            csvBuffer4.append(String.valueOf(t.getPcBits()));
                        } else {
                            csvBuffer4.append("-1");
                        }
                        csvBuffer4.append(",");

                        //RSSI
                        if (t.isPeakRssiInDbmPresent()) {
                            csvBuffer4.append(t.getPeakRssiInDbm());
                        } else {
                            csvBuffer4.append("-1.0");
                        }

                        csvBuffer4.append(",");

                        //Channel in MHz
                        if (t.isChannelInMhzPresent()) {
                            csvBuffer4.append(new Float(t.getChannelInMhz()).toString());
                        } else{
                            csvBuffer4.append( "-1.0" );
                        }

                        csvBuffer4.append(",");
                       
                        //Phase Angle
                        if(t.isRfPhaseAnglePresent()) {
                            csvBuffer4.append(new Float(t.getPhaseAngleInRadians()).toString());
                        }else{
                            csvBuffer4.append("-1.0");
                        }

                        csvWriter4.append(csvBuffer4.toString());
                        csvWriter4.append("\n");
                        csvWriter4.flush();
                     
                         //Writing for quicklook_group2.csv

                         StringBuffer csvBuffer5 = new StringBuffer();
                         csvBuffer5.append(t.getEpc().toString().replaceAll("\\s+", ""));
                         csvBuffer5.append(",");
                         csvBuffer5.append(new Float(t.getChannelInMhz()).toString());
                         csvBuffer5.append(",");
                         csvBuffer5.append(new Float(t.getPhaseAngleInRadians()).toString());
                         csvBuffer5.append(",");
                         csvBuffer5.append(new Float(t.getRfDopplerFrequency()).toString());

                         csvWriter5.append(csvBuffer5.toString());
                         csvWriter5.append("\n");
                         csvWriter5.flush();

                    }
                
                    
                    //Specific Tags

                    if( t.getEpc().toString().replaceAll("\\s+", "").equals("70000000000000000000CC00") ){
                    //if( t.getEpc().toString().replaceAll("\\s+", "").equals("E200303121120054185053B0") ){
                    //if( t.getEpc().toString().replaceAll("\\s+", "").equals("E20030312112005319104B32") ){
                    //if( t.getEpc().toString().replaceAll("\\s+", "").equals("700000000000000000006600") ){
                    //if( t.getEpc().toString().replaceAll("\\s+", "").equals("E200303121120053136087CF") ){
                    //if( t.getEpc().toString().replaceAll("\\s+", "").equals("E20030312112005319204B33") ){
                    //if( t.getTid().toHexString().replaceAll("\\s+", "").equals("E28011002000715D57EA08E5") ){
                        
                        StringBuffer csvBuffer6 = new StringBuffer();
                        csvBuffer6.append(new Float(t.getPeakRssiInDbm()).toString());
                        csvBuffer6.append(",");
                        
                        double phase_diff2 = 0.0;

                        //phase difference calculation
                        if ( first_flag2 == true ) {
                            
                            last_channel2 = t.getChannelInMhz();
                            //phase_diff = t.getPhaseAngleInRadians();
                            phase_diff2 = 0.0;
                            cur_phase2 = t.getPhaseAngleInRadians();
                            first_flag2 = false;
                            last_time2 = t.getLastSeenTime().getLocalDateTime().getTime();

                        }else{
                            
                            if( last_channel2 == t.getChannelInMhz() ) {
                                phase_diff2 = t.getPhaseAngleInRadians() - cur_phase2;
                                cur_phase2 = t.getPhaseAngleInRadians();
                            }else{
                                last_channel2 = t.getChannelInMhz();
                                cur_phase2 = t.getPhaseAngleInRadians();
                            }

                            last_time2 = t.getLastSeenTime().getLocalDateTime().getTime();

                        }

                        //csvBuffer6.append(new Double(phase_diff2).toString());
                        csvBuffer6.append(new Double(t.getPhaseAngleInRadians()).toString());
                        csvBuffer6.append(",");


                        csvBuffer6.append(new Float(t.getRfDopplerFrequency()).toString());

                        csvWriter6.append(csvBuffer6.toString());
                        csvWriter6.append("\n");
                        csvWriter6.flush();
                    }

                    //if( t.getTid().toHexString().replaceAll("\\s+", "").equals("000000000000000000000002") ){
                    //if( t.getEpc().toString().replaceAll("\\s+", "").equals("E200303121120054185053B0") ){
                    //if( t.getEpc().toString().replaceAll("\\s+", "").equals("20000000000000000000BB00") ){
                    //if( t.getEpc().toString().replaceAll("\\s+", "").equals("60000000000000000000CC00") ){
                    if( t.getEpc().toString().replaceAll("\\s+", "").equals("200000000000000000007700") ){

                        StringBuffer csvBuffer7 = new StringBuffer();
                        csvBuffer7.append(new Float(t.getPeakRssiInDbm()).toString());
                        csvBuffer7.append(",");
                        
                        double phase_diff3 = 0.0;

                        //phase difference calculation
                        if ( first_flag3 == true ) {
                            
                            last_channel3 = t.getChannelInMhz();
                            //phase_diff = t.getPhaseAngleInRadians();
                            phase_diff3 = 0.0;
                            cur_phase3 = t.getPhaseAngleInRadians();
                            first_flag3 = false;
                            last_time3 = t.getLastSeenTime().getLocalDateTime().getTime();

                        }else{
                            
                            if( last_channel3 == t.getChannelInMhz() ) {
                                phase_diff3 = t.getPhaseAngleInRadians() - cur_phase3;
                                cur_phase3 = t.getPhaseAngleInRadians();
                            }else{
                                last_channel3 = t.getChannelInMhz();
                                cur_phase3 = t.getPhaseAngleInRadians();
                            }

                            last_time3 = t.getLastSeenTime().getLocalDateTime().getTime();

                        }

                        csvBuffer7.append(new Double(t.getPhaseAngleInRadians()).toString());
                        csvBuffer7.append(",");

                        csvBuffer7.append(new Float(t.getRfDopplerFrequency()).toString());

                        csvWriter7.append(csvBuffer7.toString());
                        csvWriter7.append("\n");
                        csvWriter7.flush();
                    }
                    
                    //if( t.getTid().toHexString().replaceAll("\\s+", "").equals("000000000000000000000004") ){
                    //if( t.getEpc().toString().replaceAll("\\s+", "").equals("70000000000000000000CC00") ){
                    if( t.getEpc().toString().replaceAll("\\s+", "").equals("300000000000000000007700") ){
                    //if( t.getEpc().toString().replaceAll("\\s+", "").equals("50000000000000000000DD00") ){

                        StringBuffer csvBuffer8 = new StringBuffer();
                        csvBuffer8.append(new Float(t.getPeakRssiInDbm()).toString());
                        csvBuffer8.append(",");
                        
                        double phase_diff4 = 0.0;

                        //phase difference calculation
                        if ( first_flag4 == true ) {
                            
                            last_channel4 = t.getChannelInMhz();
                            //phase_diff = t.getPhaseAngleInRadians();
                            phase_diff4 = 0.0;
                            cur_phase4 = t.getPhaseAngleInRadians();
                            first_flag4 = false;
                            last_time4 = t.getLastSeenTime().getLocalDateTime().getTime();

                        }else{
                            
                            if( last_channel4 == t.getChannelInMhz() ) {
                                phase_diff4 = t.getPhaseAngleInRadians() - cur_phase4;
                                cur_phase4 = t.getPhaseAngleInRadians();
                            }else{
                                last_channel4 = t.getChannelInMhz();
                                cur_phase4 = t.getPhaseAngleInRadians();
                            }

                            last_time4 = t.getLastSeenTime().getLocalDateTime().getTime();

                        }

                        csvBuffer8.append(new Double(t.getPhaseAngleInRadians()).toString());
                        csvBuffer8.append(",");


                        csvBuffer8.append(new Float(t.getRfDopplerFrequency()).toString());

                        csvWriter8.append(csvBuffer8.toString());
                        csvWriter8.append("\n");
                        csvWriter8.flush();
                    }
                    
                    //if( t.getTid().toHexString().replaceAll("\\s+", "").equals("000000000000000000000008") ){
                    //if( t.getEpc().toString().replaceAll("\\s+", "").equals("80000000000000000000CC00") ){
                    if( t.getEpc().toString().replaceAll("\\s+", "").equals("400000000000000000007700") ){

                        StringBuffer csvBuffer9 = new StringBuffer();
                        csvBuffer9.append(new Float(t.getPeakRssiInDbm()).toString());
                        csvBuffer9.append(",");
                        csvBuffer9.append(new Double(t.getPhaseAngleInRadians()).toString());
                        csvBuffer9.append(",");
                        csvBuffer9.append(new Float(t.getRfDopplerFrequency()).toString());

                        csvWriter9.append(csvBuffer9.toString());
                        csvWriter9.append("\n");
                        csvWriter9.flush();
                    }
                    
                        
                    if( t.getEpc().toString().replaceAll("\\s+", "").equals("50000000000000000000CC00") ){

                        StringBuffer csvBuffer9 = new StringBuffer();
                        csvBuffer9.append(new Float(t.getPeakRssiInDbm()).toString());
                        csvBuffer9.append(",");
                        csvBuffer9.append(new Double(t.getPhaseAngleInRadians()).toString());
                        csvBuffer9.append(",");
                        csvBuffer9.append(new Float(t.getRfDopplerFrequency()).toString());

                        csvWriter10.append(csvBuffer9.toString());
                        csvWriter10.append("\n");
                        csvWriter10.flush();
                    }
                    
                    if( t.getEpc().toString().replaceAll("\\s+", "").equals("60000000000000000000CC00") ){

                        StringBuffer csvBuffer9 = new StringBuffer();
                        csvBuffer9.append(new Float(t.getPeakRssiInDbm()).toString());
                        csvBuffer9.append(",");
                        csvBuffer9.append(new Double(t.getPhaseAngleInRadians()).toString());
                        csvBuffer9.append(",");
                        csvBuffer9.append(new Float(t.getRfDopplerFrequency()).toString());

                        csvWriter11.append(csvBuffer9.toString());
                        csvWriter11.append("\n");
                        csvWriter11.flush();
                    }
                    
                    if( t.getEpc().toString().replaceAll("\\s+", "").equals("70000000000000000000CC00") ){

                        StringBuffer csvBuffer9 = new StringBuffer();
                        csvBuffer9.append(new Float(t.getPeakRssiInDbm()).toString());
                        csvBuffer9.append(",");
                        csvBuffer9.append(new Double(t.getPhaseAngleInRadians()).toString());
                        csvBuffer9.append(",");
                        csvBuffer9.append(new Float(t.getRfDopplerFrequency()).toString());

                        csvWriter12.append(csvBuffer9.toString());
                        csvWriter12.append("\n");
                        csvWriter12.flush();
                    }
                    
                    if( t.getEpc().toString().replaceAll("\\s+", "").equals("80000000000000000000CC00") ){

                        StringBuffer csvBuffer9 = new StringBuffer();
                        csvBuffer9.append(new Float(t.getPeakRssiInDbm()).toString());
                        csvBuffer9.append(",");
                        csvBuffer9.append(new Double(t.getPhaseAngleInRadians()).toString());
                        csvBuffer9.append(",");
                        csvBuffer9.append(new Float(t.getRfDopplerFrequency()).toString());

                        csvWriter13.append(csvBuffer9.toString());
                        csvWriter13.append("\n");
                        csvWriter13.flush();
                    }
                    

                } catch(IOException e) {
                    System.out.println("Unable to write to CSV: " + e);
                }
        }
    }
}
