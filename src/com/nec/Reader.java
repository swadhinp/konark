//package com.example.sdksamples;

package com.nec;

import com.impinj.octane.*;

import java.util.Scanner;
import java.util.ArrayList;
// import org.apache.log4j.BasicConfigurator;
// import org.apache.log4j.Logger;
// import org.apache.log4j.Level;

public class Reader {

    public static ImpinjReader reader = null;
    public static void main(String[] args) {

        try {
            String hostname = System.getProperty(Properties.hostname, args[0]);

            if (hostname == null) {
                throw new Exception("Must specify the '"
                        + Properties.hostname + "' property");
            }

            
            // BasicConfigurator.configure();
            // // Only show root events from the base logger
            // Logger.getRootLogger().setLevel(Level.ERROR);           

            reader = new ImpinjReader();

            System.out.println("Connecting");
            reader.connect(hostname);
            
            FeatureSet features = reader.queryFeatureSet();
            Settings settings = reader.queryDefaultSettings();
            Runtime r = Runtime.getRuntime();
            
            if (!features.isHoppingRegion()) {
                    // settings fixed frequencies is allowed if its non hopping
                    ArrayList<Double> freqList = new ArrayList<Double>();
                    freqList.add(865.7); //lowest frequency allowable in ETSI
                    //freqList.add(867.5); //highest frequency allowable in ETSI
                    settings.setTxFrequenciesInMhz(freqList);
            }

            ReportConfig report = settings.getReport();
            report.setIncludeAntennaPortNumber(true);
            report.setMode(ReportMode.Individual);

            // The reader can be set into various modes in which reader
            // dynamics are optimized for specific regions and environments.
            // The following mode, AutoSetDenseReader, monitors RF noise and interference and then automatically
            // and continuously optimizes the reader’s configuration
            // settings.setReaderMode(ReaderMode.AutoSetDenseReader);

            // Doppler works best at M=8 mode
            //settings.setReaderMode(ReaderMode.DenseReaderM8);
            //settings.setReaderMode(ReaderMode.DenseReaderM4);
            //settings.setReaderMode(ReaderMode.AutoSetDenseReader);
            
            //The reader monitors RF noise and interference and then automatically and continuously 
            //optimizes the readers configuration, selecting the appropriate configuration 
            //settings for the best, most reliable performance for environments where 
            //tags are relatively static.
            
            //settings.setReaderMode(ReaderMode.AutoSetDenseReaderDeepScan);
            
            //Provides a fast read rate when there are is modest interference from other readers, 
            //but a low likelihood of co-channel, adjacent channel, or alternate-channel interferers.

            //settings.setReaderMode(ReaderMode.Hybrid);

            //Provides an intermediate read rate when the population of readers 
            //is large and the likelihood of interference is high.
            
            //settings.setReaderMode(ReaderMode.DenseReaderM8);
            
            //Maxmiller is the fastest read rate with comparison to interference
            //MaxThroughput > MaxMiller > DenseM8 > DenseM4 > Hybrid
            
            //settings.setReaderMode(ReaderMode.DenseReaderM8);
            //settings.setReaderMode(ReaderMode.MaxMiller);
            settings.setReaderMode(ReaderMode.MaxThroughput);
            //settings.setReaderMode(ReaderMode.AutoSetDenseReader);
            //settings.setReaderMode(ReaderMode.AutoSetDenseReaderDeepScan);
            //settings.setReaderMode(ReaderMode.AutoSetStaticDRM);
            
            //Fastest possible data transfer rates between tag and reader when the population of readers is very low and the likelihood of interference between readers is very low.
            
            //settings.setReaderMode(ReaderMode.AutoSetStaticFast);
            
            //The reader selects the best inventory search mode for the chosen session
            //settings.setSearchMode(SearchMode.ReaderSelected);
            
            settings.getReport().setIncludeDopplerFrequency(true);
            //report.setIncludeDopplerFrequency(true);
            settings.getReport().setIncludePhaseAngle(true);
            //report.setIncludePhaseAngle(true);
            settings.getReport().setIncludePeakRssi(true);
            //report.setIncludePeakRssi(true);
            settings.getReport().setIncludeFastId(true);
            //report.setIncludeFastId(true);
            settings.getReport().setIncludeChannel(true);
            settings.getReport().setIncludeFirstSeenTime(true);
            settings.getReport().setIncludeSeenCount(true);
            settings.getReport().setIncludePcBits(true);
            settings.getReport().setIncludeLastSeenTime(true);
            
            //settings.setSearchMode(SearchMode.SingleTarget);
            //Number of Sessions have an Impact on Read Rate ~ Number of Threads
            settings.setSession(1);

            //Search Mode has a big Impact on enumerating different tags
            // DualTarget > ReaderSelected >> Single Target >>> TagFocus
            // Dualtarget makes only One Antenna On which reduces the reading of tags
            
            //Set this if you need to do only for 1 Tag
            //settings.setSearchMode(SearchMode.SingleTarget);
            //settings.setSearchMode(SearchMode.TagFocus);
            //settings.setSearchMode(SearchMode.ReaderSelected);
            settings.setSearchMode(SearchMode.DualTarget);
            settings.setTagPopulationEstimate(1);
            


            //settings.setSearchMode(SearchMode.ReaderSelected);
            settings.getLowDutyCycle().setIsEnabled(false);
            

            //settings.getReport().setIncludePcBits(true);

            // set some special settings for antenna 1
            AntennaConfigGroup antennas = settings.getAntennas();
            antennas.disableAll();
           // antennas.enableById(new short[]{1});
            
            for(short k = 1; k <= 1; k++) {
                
                antennas.enableById(new short[]{k});
                
                //System.out.println(k);
                //For the Cart Antenna Powers
                //antennas.getAntenna((short) k).setIsMaxRxSensitivity(false);
                //antennas.getAntenna((short) k).setIsMaxTxPower(false);
                //antennas.getAntenna((short) k).setTxPowerinDbm(13.0); //10,30,20
                //antennas.getAntenna((short) k).setRxSensitivityinDbm(-70); //80,90 (30,-90 worked)
               

                //For one antenna gesture experiment
                //antennas.getAntenna((short) k).setIsMaxRxSensitivity(true);
                //antennas.getAntenna((short) k).setIsMaxTxPower(true);

                //Gesture Experiment alternative power settings for stopping far-away Reads
                antennas.getAntenna((short) k).setIsMaxRxSensitivity(true);
                antennas.getAntenna((short) k).setIsMaxTxPower(true);
                //antennas.getAntenna((short) k).setTxPowerinDbm(30.0); //10,30,20, You need 25 for 1 Antenna; 25 works well for 2 antennas two read more; 22 dB Used for Tracking
                //antennas.getAntenna((short) k).setRxSensitivityinDbm(-70); //80,90 (30,-90 worked)

                //-80 is the lowest RX Sensitivity and 
                //10 dB is the Tx Power for high gain (9 dB) circular antenna

                //System.out.println(" Antenna ID : "+ k + " ; RXSensitivity : " + antennas.getAntenna((short) k).getRxSensitivityinDbm() + "TxPower :  " + antennas.getAntenna((short) k).getTxPowerinDbm() );
            }
            

            //String[] tagetEPCArr = {"E28011606000020612AF52BF", "E28011606000020612AF52AF", "E28011606000020612AF52FF", "E28011606000020612AF52EF"};

            //int arrSize = tagetEPCArr.length;
            //for ( int i = 0; i < arrSize; i ++ ) {
            // this will match the first 16 bits of the target EPC.
           
           //First Flag
          
            //String targetEpc =  "E28011002000725E57EA08E5";
           
            //String targetEpc =  "E2003031211200531710638E"; //One Short hand
            //String targetEpc =  "E20030312112005317705C40"; //One Long Hand
            //String targetEpc =  "E20030312112005319104B32"; //Both Long hands
            //String targetEpc =  "AA";
            //Monopole : E2003031211200531720638F
            //Triangle : E200303121120053198042E5
            //Dipole : E20030312112005319204B33
            //Fat Dipole : E20030312112005317805AF1
            //Circular tag : E200303121120054185053B0

            //String targetEpc =  "30000000000000000000CC00"; //Single Tag
            //String targetEpc =  "AA"; //One Column Multi-Tag
            //String targetEpc =  "40000000000000000000AA00"; //One Column Multi-Tag

            //String targetEpc =  "E200303121120054185053B0"; //Hexagonal
            //String targetEpc =  "AA";
            //String targetEpc =  "40000000000000000000AA00"; //One Column Multi-Tag
            
            //String targetEpc =  "E200303121120054205041D0"; //Different pixel tag
            //String targetEpc =  "E20030312112005319104B32"; 
            
            //String targetEpc =  "BB";
            //String targetEpc =  "CC";
            
            
            //String targetEpc =  "50000000000000000000CC00"; //One Column Multi-Tag
            //String targetEpc =  "E200303121120054205041D0"; //One Column Multi-Tag
            //String targetEpc =  "E20030312112005319204B33"; //One Column Multi-Tag

            //String targetEpc =  "66";
            String targetEpc =  "70000000000000000000CC00"; //One Column Multi-Tag
	    //String targetEpc =  "E20030312112005419004B35";
            
            if( null != targetEpc ){

                TagFilter t1 = settings.getFilters().getTagFilter1();
                //t1.setBitCount(8);
                t1.setBitCount(96);
                //t1.setBitPointer(BitPointers.Epc+80);
                t1.setBitPointer(BitPointers.Epc);

                t1.setMemoryBank(MemoryBank.Epc);
                t1.setFilterOp(TagFilterOp.Match);
                t1.setTagMask(targetEpc);
                settings.getFilters().setMode(TagFilterMode.OnlyFilter1);
                //settings.getFilters().setMode(TagFilterMode.Filter1OrFilter2);
            }

            /*
            String targetEpc1 =  "77";
            
            if( null != targetEpc ){

                TagFilter t1 = settings.getFilters().getTagFilter2();
                t1.setBitCount(8);
                //t1.setBitCount(96);
                t1.setBitPointer(BitPointers.Epc+80);
                //t1.setBitPointer(BitPointers.Epc);

                t1.setMemoryBank(MemoryBank.Epc);
                t1.setFilterOp(TagFilterOp.Match);
                t1.setTagMask(targetEpc1);
                //settings.getFilters().setMode(TagFilterMode.OnlyFilter1);
                settings.getFilters().setMode(TagFilterMode.Filter1OrFilter2);
            }*/

            /* 
            String targetEpc =  "300833B2DDD9014000000000";
            //String targetEpc =  "EA08E5";

            if( null != targetEpc ){

                TagFilter t1 = settings.getFilters().getTagFilter1();
                t1.setBitCount(96);
                t1.setBitPointer(BitPointers.Epc);
                t1.setMemoryBank(MemoryBank.Epc);
                t1.setFilterOp(TagFilterOp.Match);
                t1.setTagMask(targetEpc);
                settings.getFilters().setMode(TagFilterMode.OnlyFilter1);
            }*/

           /*
            String targetEpc2 =  "30000000000000000000001A";

            if( null != targetEpc2 ){

                TagFilter t2 = settings.getFilters().getTagFilter2();
                t2.setBitCount(96);
                t2.setBitPointer(BitPointers.Epc);
                t2.setMemoryBank(MemoryBank.Epc);
                t2.setFilterOp(TagFilterOp.Match);
                t2.setTagMask(targetEpc2);
                //settings.getFilters().setMode(TagFilterMode.Filter1OrFilter2);
            }

            if ( (null != targetEpc) && (null != targetEpc2) ){
                settings.getFilters().setMode(TagFilterMode.Filter1OrFilter2);
            }*/

            reader.setTagReportListener(new TagReportListenerImplementation("tag.csv"));
            //reader.setTagOpCompleteListener(new TagOpCompleteListenerImplementation());

            /*EPC writing mode
            // Set periodic mode so we reset the tag and it shows up with its
            // new EPC
            settings.getAutoStart().setMode(AutoStartMode.Periodic);
            settings.getAutoStart().setPeriodInMs(2000);
            settings.getAutoStop().setMode(AutoStopMode.Duration);
            settings.getAutoStop().setDurationInMs(1000);
            */

            System.out.println("Applying Settings");
            reader.applySettings(settings);
            
            //Code for arduino data recordning
            //Process p = r.exec(new String[]{"bash", "-c", "python arduino_monitor.py"});
            //Code for finger tracking
            Process p = r.exec(new String[]{"bash", "-c", "python finger_tracking.py"});

            System.out.println("Starting");
            reader.start();


           /* 
            //Getting Reader Status
            System.out.println("Querying reader status");
            Status status = reader.queryStatus();

            System.out.println("Reader Temperature: "+ status.getTemperatureCelsius());
            System.out.println("Singulating: " + status.getIsSingulating());
            System.out.println("Connected:" + status.getIsConnected());

            System.out.println("Antenna Status");
            for (AntennaStatus as : status.getAntennaStatusGroup().getAntennaList()) {
                                                                                                                            System.out.println("  Antenna " + as.getPortNumber() + " status " + as.isConnected());
                                                                                                                       }

           */

            /*
            System.out.println("Press Enter to start another Filter.");
            Scanner s = new Scanner(System.in);
            s.nextLine();

            Process p1 = r.exec(new String[]{"bash", "-c", "ps -ef | grep arduino_monitor.py | grep -v grep | awk '{print $2}' | xargs kill -9"});
            p1.waitFor();


            reader.stop();
            String targetEpc1 =  "E28011606000020612AF52BF";


            if( null != targetEpc1 ){

                TagFilter t1 = settings.getFilters().getTagFilter1();
                t1.setBitCount(96);
                t1.setBitPointer(BitPointers.Epc);
                t1.setMemoryBank(MemoryBank.Epc);
                t1.setFilterOp(TagFilterOp.Match);
                t1.setTagMask(targetEpc1);
                settings.getFilters().setMode(TagFilterMode.OnlyFilter1);
            }

            System.out.println("Applying Settings again");
            reader.applySettings(settings);

            reader.start();
            */

            System.out.println("Press Enter to exit.");
            Scanner s1 = new Scanner(System.in);
            s1.nextLine();
            
            //Stopping the finger tracker code
            //Process p1 = r.exec(new String[]{"bash", "-c", "ps -ef | grep finger_tracking.py | grep -v grep | awk '{print $2}' | xargs kill -9"});
            //p1.waitFor();

            reader.stop();
            reader.disconnect();

            System.exit(0);
        } catch (OctaneSdkException ex) {
            System.out.println(ex.getMessage());
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace(System.out);
        }
    }
}
